!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************


module mdsubs_mod

    use datatypes, only: real64b
    use PhysConsts, only: u, e, eV_to_kbar
    use border_params_mod, only: BorderParams

    use typeparam
    use casc_common
    use Temp_Time_Prog

    use output_logger, only: log_buf, logger
    use para_common, only: myatoms, iprint, debug
    use my_mpi

    implicit none

    private
    public :: Temp_Control
    public :: Press_Control
    public :: Strain_Control
    public :: Predict
    public :: Correct
    public :: is_sputtered_atom
    public :: ConserveTotalMomentum
    public :: RemoveAngularMomentum
    public :: avg_calc_vir
    public :: calc_kinetic_vir
    public :: update_avg_vir
    public :: zero_avg_vir


    real(real64b), parameter :: vir_unithelp = u * 1d10 / e

  contains

!***********************************************************************
! Support subroutines for all versions of the Classical MD code.
!     Temp_Control(), Predict(), Correct(),
!
! Pair_Table() moved to mdlinkedlist.f
! FCC_Gen(), Read_Atoms moved to mdlattice.f
! Dump_Atoms and Movie() moved to mdoutput.f

!***********************************************************************
! Temperature control
!***********************************************************************


subroutine Temp_Control(heat, mtemp, temp, toll, trans, poten, tote, &
        heatbrdr, transbrdr, temp0, trate, deltat_fs, ntimeini, timeini, &
        btctau, istep, time_fs, time_thisrec, rsnum)

    ! KN Main input is values trans and transbrdr, which are used to calculate
    ! the temperature or border temperature. Note that the routine may be called
    ! with transmoving instead of trans!
    !
    ! KN Main output is parameter heat, which determines how much temperature
    ! should be changed. It scales directly the velocities (hence the sqrt).
    !
    ! Modes of temperature control determined by mtemp
    !
    !     0     No temperature control (output heat=1)
    !     1     Scale linearly towards desired value according to temp and toll *
    !     2     Quench - Set all velocities to zero => motion by a, a'... only
    !     3     Energy control
    !     4     As 1, but only for first 20 steps
    !     5     As 1, but only for border atoms *
    !     6     Cool/heat from temp0 to temp at rate trate (K/fs) * ^
    !     7     As 5, but not if not periodic ! *
    !     8     Border+quench; as 7 initially, then quench for all atoms ^
    !     9     As 6 but cooling done by scaling down velocities of atoms
    !           that have v�F<0 � (A.Kuronen, April 2001)
    !    10     First mtemp 1 until either timeini or ntimeini is reached,
    !           then continue with mtemp 5.
    !    11     Used in some PARCAS versions (e.g. aaleino_ttm)
    !    12     Used in some PARCAS versions (e.g. aaleino_ttm)
    !    13     Scale each atom separately according to the Berendsen model
    !           (see below). Ignores toll. Useful for evening out the
    !           temperature profile, i.e. get rid of hot areas.
    !
    ! * Possible btctau influence, see below:
    !
    ! ^ Quench modes: Initially normal scale for ntimeini steps or the time
    !    timeini, whichever is less. After that slow quench of all atoms at
    !    quench rate trate (K/fs).
    !
    ! If btctau is non-zero, velocity scaling factor is
    ! sqrt(1+deltat_fs/btctau*(T0/T-1)) rather than the ordinary T0/T.
    ! according to the Berendsen model,  J. Chem. Phys. 81 (1984) 3684
    ! This is true for mode 1 and 6 only prior to quench


    integer, intent(in) :: mtemp
    real(real64b), intent(in) :: temp, temp0
    real(real64b), intent(in) :: tote, poten
    real(real64b), intent(in) :: trans, transbrdr
    real(real64b), intent(in) :: btctau
    real(real64b), intent(in) :: toll
    real(real64b), intent(in) :: trate, deltat_fs
    real(real64b), intent(in) :: time_fs, time_thisrec
    real(real64b), intent(in) :: timeini
    integer, intent(in) :: ntimeini
    integer, intent(in) :: istep
    integer, intent(in) :: rsnum

    real(real64b), intent(out) :: heat, heatbrdr



    real(real64b) :: time
    real(real64b) :: tempn
    real(real64b) :: ratio
    real(real64b) :: transn
    integer :: ntime
    real(real64b), save :: tempnew

    real(real64b), parameter :: maxratio = 1.2d0
    real(real64b), parameter :: minratio = 0.9d0


    heat = 1d0
    heatbrdr = 1d0

    ntime = istep

    time = time_fs
    ! If rsnum>0, use time_thisrec instead of time_fs for mtemp=6/8
    if (rsnum > 0) then
        time = time_thisrec
    end if

    select case (mtemp)
    case (0)
        ! No temperature control
        return


    case (1, 4)
        ! Berendsen control for all atoms.
        ! For mtemp=4, only run in the first 20 steps.

        if (mtemp == 4 .and. ntime >= 20) return

        tempn = 2d0 * trans / 3d0
        if (abs(tempn - temp) <= toll) return
        ratio = btc_heat_ratio(tempn, temp, deltat_fs, btctau)
        heat = min(ratio, maxratio)


    case (2)
        ! Quench - Set all velocities to zero => motion by a, a'... only
        ! i.e. "cold" relaxation
        ! The heat value does not actually matter, since all velocities
        ! should be zero when coming here. Predict and Correct zero the
        ! velocities after moving the atoms.
        heat = 0


    case (3)
        ! Energy control.

        if (trans <= 0.0) return

        transn = temp - poten
        if (transn <= 0.0) return

        if (abs(tote - temp) <= toll) return

        heat = sqrt(transn / trans)


    case (5, 7)
        ! Surface/border temperature control.

        tempn = 2d0 * transbrdr / 3d0
        ratio = btc_heat_ratio(tempn, temp, deltat_fs, btctau)
        heatbrdr = max(min(ratio, maxratio), minratio)


    case(6, 9)
        ! Complex control: first set to temp0, then transform
        ! to temp at rate trate. For mtemp==9, quench using v.F<0 method
        ! (A. Kuronen, April 2001).

        tempn = 2d0 * trans / 3d0

        if (ntime == 1 .and. tempn > 0.0) then
            tempnew = tempn
        end if

        if (ntime < ntimeini .and. time < timeini) then
            tempnew = temp0
            ratio = btc_heat_ratio(tempn, temp0, deltat_fs, btctau)
            heat = min(ratio, maxratio)
            return
        end if

        if (mtemp == 9) return

        ! Get the starting temperature for the ramp from the previous
        ! TTprog step, if it is activated.
        ! TODO this logic seems broken. TT_tempnewset is never reset to
        ! false, so what happens if the TTprog contains two slots with
        ! mtemp==6?
        if (TT_activated .and. .not. TT_tempnewset) then
            tempnew = TT_temp(TT_step - 1)
            TT_tempnewset = .true.
        end if
        tempnew = btc_rate_new_temp(tempnew, tempn, temp, deltat_fs, trate)
        heat = min(sqrt(tempnew / tempn), maxratio)


    case (8)
        ! Complex border control: first control borders, then quench all
        ! at rate trate.

        if (ntime < ntimeini .and. time < timeini) then
            tempnew = 2d0 * trans / 3d0
            tempn = 2d0 * transbrdr / 3d0
            ratio = btc_heat_ratio(tempn, temp0, deltat_fs, btctau)
            heatbrdr = max(min(ratio, maxratio), minratio)
            return
        end if

        tempn = 2d0 * trans / 3d0
        tempnew = btc_rate_new_temp(tempnew, tempn, temp, deltat_fs, trate)
        heat = min(sqrt(tempnew / tempn), maxratio)


    case (10)
        ! mtemp 1 and then mtemp 5.

        ! mtemp 1
        if (ntime < ntimeini .and. time < timeini) then
            tempn = 2d0 * trans / 3d0
            if (abs(tempn - temp) <= toll) return
            ratio = btc_heat_ratio(tempn, temp, deltat_fs, btctau)
            heat = min(ratio, maxratio)
            return
        end if

        ! mtemp 5
        tempn = 2d0 * transbrdr / 3d0
        ratio = btc_heat_ratio(tempn, temp, deltat_fs, btctau)
        heatbrdr = max(min(ratio, maxratio), minratio)

    case (11, 12)
        call my_mpi_abort("Unsupported mtemp", mtemp)

    case (13)
        ! Scale separately for each atom. Handled elsewhere.

    case default
        call my_mpi_abort("Unknown mtemp", mtemp)

    end select

end subroutine Temp_Control


pure real(real64b) function btc_rate_new_temp( &
        temp_new_in, temp_now, temp_wanted, deltat_fs, trate) result(temp_new)

    real(real64b), intent(in) :: temp_new_in, temp_now, temp_wanted
    real(real64b), intent(in) :: deltat_fs, trate

    real(real64b) :: dtemp

    dtemp = trate * deltat_fs
    temp_new = temp_new_in

    if (temp_now > temp_wanted + dtemp) then
        temp_new = temp_new - dtemp
    else if (temp_now < temp_wanted - dtemp) then
        temp_new = temp_new + dtemp
    else
        temp_new = temp_wanted
    end if

    temp_new = max(temp_new, 0d0)

end function btc_rate_new_temp

pure real(real64b) function btc_heat_ratio( &
        temp_now, temp_wanted, deltat_fs, btctau) result(ratio)

    real(real64b), intent(in) :: temp_now, temp_wanted
    real(real64b), intent(in) :: deltat_fs, btctau

    if (temp_now == 0.0) then
        ratio = 1d0
        return
    end if

    if (btctau == 0.0) then
        ratio = sqrt(temp_wanted / temp_now)
    else
        ratio = sqrt(1d0 + deltat_fs / btctau * (temp_wanted / temp_now - 1d0))
    end if

end function btc_heat_ratio


! ************************************************************
! Berendsen pressure control
! Moved from mdx.f90 into this subroutine by Paul Ehrhart
!
! ***********************************************************

subroutine Press_Control(bpcbeta, bpctau, bpcmode, tmodtau, temp, &
        bpcP0, bpcP0x, bpcP0y, bpcP0z, wxx, wyy, wzz, Pxx, Pyy, Pzz, &
        istep, deltat_fs, time_fs, transv, box, ncells, unitcell, &
        dh, dh0, dhprev, x1, x2, x3, x4, x5)

    ! Parameters of BPC
    real(real64b), intent(in) :: bpcbeta, bpctau, tmodtau
    integer, intent(in) :: bpcmode
    ! Desired and current pressures, desired temperature
    real(real64b), intent(in) :: bpcP0, bpcP0x, bpcP0y, bpcP0z
    real(real64b), intent(in) :: wxx, wyy, wzz
    real(real64b), intent(out) :: Pxx, Pyy, Pzz
    real(real64b), intent(inout) :: temp
    ! Time step information
    real(real64b), intent(in) :: deltat_fs, time_fs
    integer, intent(in) :: istep
    real(real64b), intent(in) :: transv(:)
    ! System size
    integer, intent(in) :: ncells(3)
    real(real64b), intent(out) :: unitcell(3)
    real(real64b), intent(inout) :: box(3)
    real(real64b), intent(inout) :: dh, dh0, dhprev
    ! Derivatives of the positions
    real(real64b), intent(inout) :: x1(:), x2(:), x3(:), x4(:), x5(:)


    real(real64b) :: scalefactor(3), virarray(3), help
    integer :: i, j, i3


    if (bpcbeta == 0d0) return

    ! Because of the fact that x0 is already scaled by the box size,
    ! no change of the internal coordinates is necessary.

    ! Note that the virial wxx is Sigma fijx*rijx,
    ! with no factor 0.5 in front!


    virarray(1) = wxx
    virarray(2) = wyy
    virarray(3) = wzz
    call my_mpi_sum(virarray, 3)

    Pxx = eV_to_kbar * (virarray(1) + transv(1)) / dh
    Pyy = eV_to_kbar * (virarray(2) + transv(2)) / dh
    Pzz = eV_to_kbar * (virarray(3) + transv(3)) / dh

    ! Don't change size during first 10 steps
    if (bpctau == 0.0 .or. bpcmode == 0 .or. istep <= 10) return

    help = bpcbeta * deltat_fs / 3d0 / bpctau

    select case (bpcmode)
    case (1)  ! independent x, y, z
        scalefactor(1) = 1d0 - help * (bpcP0x - Pxx)
        scalefactor(2) = 1d0 - help * (bpcP0y - Pyy)
        scalefactor(3) = 1d0 - help * (bpcP0z - Pzz)

    case (2)  ! isotropic in x, y, z
        scalefactor(:) = 1d0 - help * (bpcP0 - (Pxx + Pyy + Pzz) / 3d0)

    case (3)  ! x, y controlled; z fixed
        scalefactor(1) = 1d0 - help * (bpcP0x - Pxx)
        scalefactor(2) = 1d0 - help * (bpcP0y - Pyy)
        scalefactor(3) = 1d0

    case (4)  ! z controlled; x, y fixed
        scalefactor(1) = 1d0
        scalefactor(2) = 1d0
        scalefactor(3) = 1d0 - help * (bpcP0z  -Pzz)

    case (5)  ! x controlled; y, z fixed
        scalefactor(1) = 1d0 - help * (bpcP0x - Pxx)
        scalefactor(2) = 1d0
        scalefactor(3) = 1d0

    case default
        call my_mpi_abort('Unknown bpcmode', bpcmode)
    end select

    if (debug) print *, 'pres.', scalefactor(:)

    box(:) = box(:) * scalefactor(:)
    unitcell(:) = box(:) / ncells(:)

    dhprev = dh
    dh = product(box(:))

    ! Since the x1-5 vectors contain the box size,
    ! they must all be corrected!
    do i = 1, myatoms
        i3 = 3*i - 3
        do j = 1, 3
            x1(i3+j) = x1(i3+j) / scalefactor(j)
            x2(i3+j) = x2(i3+j) / scalefactor(j)
            x3(i3+j) = x3(i3+j) / scalefactor(j)
            x4(i3+j) = x4(i3+j) / scalefactor(j)
            x5(i3+j) = x5(i3+j) / scalefactor(j)
        end do
    end do

    if (tmodtau /= 0.0 .and. time_fs > 10d0*abs(tmodtau)) then
        help = (1d0 + (dhprev - dh) / dh0 * deltat_fs * 1000.0 / tmodtau)
        temp = temp * help
    end if

end subroutine Press_Control


subroutine Strain_Control(bpcextz, x1, x2, x3, x4, x5, box, boxorig, &
        ncells, unitcell, time_fs, deltat_fs, tmodtau, temp, dh, dhorig, dhprev)

    ! Strain rate, unit = strain/fs relative to original box size
    real(real64b), intent(in) :: bpcextz
    real(real64b), intent(inout) :: x1(:), x2(:), x3(:), x4(:), x5(:)
    real(real64b), intent(inout) :: box(3)
    real(real64b), intent(in) :: boxorig(3)
    integer, intent(in) :: ncells(3)
    real(real64b), intent(inout) :: unitcell(3)
    real(real64b), intent(in) :: time_fs, deltat_fs
    real(real64b), intent(in) :: tmodtau
    real(real64b), intent(inout) :: temp
    ! TODO: These are product(box(:)), and should probably be updated
    ! here after streching, but the code does not do it??
    real(real64b), intent(in) :: dh, dhorig, dhprev

    real(real64b) :: boxzratio
    real(real64b) :: boxzprev
    real(real64b) :: help
    integer :: i

    boxzprev = box(3)

    box(3) = boxorig(3) * (1d0 + bpcextz * time_fs)
    unitcell(3) = box(3) / ncells(3)

    ! Since the x1-5 vectors contain the box size, they must all
    ! be corrected! But only in z-direction.
    boxzratio = box(3) / boxzprev

    do i = 3, 3*myatoms, 3
        x1(i) = x1(i) / boxzratio
        x2(i) = x2(i) / boxzratio
        x3(i) = x3(i) / boxzratio
        x4(i) = x4(i) / boxzratio
        x5(i) = x5(i) / boxzratio
    end do

    if (tmodtau /= 0.0 .and. time_fs > 10d0 * abs(tmodtau)) then
        help = (1d0 + (dhprev - dh) / dhorig * deltat_fs * 1000.0 / tmodtau)
        temp = temp * help
    end if

end subroutine Strain_Control



!***********************************************************************
! update positions, calculate kinetic energy
!
! Returns among other things:
!     Ekin()       Kinetic energy of each atom i (eV)
!     trans        Kinetic energy of all atoms (eV)
!     transv       Kinetic energy xyz componenets (eV)
!     transbrdr    Kinetic energy of border atoms (eV)
!     nbrdr        Number of border atoms
!     Emax         Maximum kinetic energy of all atoms (eV)
!     vmax         Maximum velocity of all atoms (Å/fs)
!     Emaxnosput   Maximum kinetic energy of atoms within original cell (eV)
!     vmaxnosput   Maximum velocity of atoms within original cell (Å/fs)
!
!***********************************************************************

subroutine Predict(x0,x1,x2,x3,x4,x5,atype, &
        trans,transin,natomsin,transv,heat,box,pbc,delta, &
        Emax,Emaxnosput,vmax,vmaxnosput,heatbrdr,transbrdr, &
        temp, btctau, deltat_fs, &
        x0nei,xneimax,xneimax2, &
        Ekin,nbrdr,mtemp,timesputlim,nisputlim,Elosssum, &
        Emaxbrdr,Fzmaxz,FzmaxYz,dydtmaxz,fdamp,border_params, &
        Ethree,Epair)

    real(real64b), intent(inout), contiguous :: &
        x0(:), x1(:), x2(:), x3(:), x4(:), x5(:), x0nei(:)
    real(real64b), intent(out) :: trans, transin, transbrdr, transv(3)
    real(real64b), intent(out) :: Emax, Emaxnosput, vmax, Vmaxnosput
    real(real64b), intent(out) :: Ekin(:), Elosssum(2)
    real(real64b), intent(out) :: xneimax, xneimax2
    integer, intent(out) :: natomsin, nbrdr

    integer, intent(in) :: atype(:), mtemp
    real(real64b), intent(in) :: box(3), pbc(3), Emaxbrdr
    real(real64b), intent(in) :: heat, heatbrdr, fdamp, delta(itypelow:itypehigh)
    real(real64b), intent(in) :: Epair(:), Ethree(:)
    real(real64b), intent(in) :: timesputlim, nisputlim
    real(real64b), intent(in) :: Fzmaxz, FzmaxYz, dydtmaxz
    type(BorderParams), intent(in) :: border_params
    real(real64b), intent(in) :: btctau, deltat_fs, temp


    real(real64b) :: tlim(3), E, vsq, v, help
    real(real64b) :: Ebefore, Eafter
    real(real64b) :: boxsq_over_deltasq(3, itypelow:itypehigh)
    real(real64b) :: comm_array(6)

    integer :: i, j, j2, i1, i2, i3, typej
    logical :: ifixed, isborderat, Fzmax_fix

    ! Local variables for neighbor list displacement calc.
    real(kind=real64b) :: xneisqmax, xneisqmax2, xneisq

    !------------------------------------------------------------------

    trans = 0.0d0
    transin = 0.0d0
    transv(:) = 0.0d0
    transbrdr = 0.0d0
    natomsin = 0
    nbrdr = 0
    tlim(:) = 0.5d0 - border_params%thickness / box(:)

    Emax = -1.0d0
    Emaxnosput = -1.0d0
    vmax = -1.0d0
    vmaxnosput = -1.0d0
    xneisqmax = 0.0d0
    xneisqmax2 = 0.0d0

    do i = itypelow, itypehigh
        boxsq_over_deltasq(:, i) = (box(:) / delta(i))**2
    end do

    do j = 1, myatoms
        ifixed = (atype(j) < 0)
        typej = abs(atype(j))

        i1 = 3*j - 2
        i2 = 3*j - 1
        i3 = 3*j - 0

        ! Calculate E before T scale.
        Ebefore = 0.5d0 * sum(x1(i1:i3)**2 * boxsq_over_deltasq(:, typej))

        if (mtemp /= 13) then
            ! Perform border atom T scaling if necessary.
            if (heatbrdr /= 1d0) then
                isborderat = is_border_atom(j, typej, x0(i1:i3), box, pbc, &
                    border_params, tlim, mtemp, ifixed, myproc, irecproc, irec)

                if (isborderat) then
                    x1(i1:i3) = heatbrdr * x1(i1:i3)

                    ! Calculate E after border T scale.
                    Eafter = 0.5d0 * sum(x1(i1:i3)**2 * boxsq_over_deltasq(:, typej))

                    ! Get heat loss sum.
                    Elosssum(1) = Elosssum(1) + Ebefore - Eafter

                    ! Now use Eafter as start of E calc for overall E loss.
                    Ebefore = Eafter
                end if
            end if

            ! If no border scaling was done, then Ebefore is still valid,
            ! since x1 was not changed.
            x1(i1:i3) = heat * x1(i1:i3)

        else
            ! Compute the scaling factor separately for each atom.
            help = btc_heat_ratio(2d0 * Ebefore / 3d0, temp, deltat_fs, btctau)
            x1(i1:i3) = min(1.2d0, help) * x1(i1:i3)
        end if

        ! Calculate E after T scale
        Eafter = 0.5d0 * sum(x1(i1:i3)**2 * boxsq_over_deltasq(:, typej))

        ! Get heat loss sum.
        Elosssum(2) = Elosssum(2) + Ebefore - Eafter


        vsq = 0d0
        xneisq = 0d0
        do j2 = 1, 3
            i = (j-1) * 3 + j2  ! one of i1,i2,i3 depending on j2

            Fzmax_fix = ((FzmaxYz /= 0d0 .or. dydtmaxz /= 0d0) .and. &
                j2 == 3 .and. x0(i)*box(3) >= Fzmaxz)

            ! KN This is the Gear algorithm.
            if (.not. ifixed .and. .not. Fzmax_fix) then
                help = x1(i) + x2(i) + x3(i) + x4(i) + x5(i)
                x0(i) = x0(i) + help
                x0nei(i) = x0nei(i) + help
                xneisq = xneisq + (x0nei(i) * box(j2))**2
                x1(i) = x1(i) + 2d0*x2(i) + 3d0*x3(i) + 4d0*x4(i) + 5d0*x5(i)
            else
                x1(i) = 0d0
            endif
            x2(i) = x2(i) + 3d0*x3(i) + 6d0*x4(i) + 10d0*x5(i)
            x3(i) =             x3(i) + 4d0*x4(i) + 10d0*x5(i)
            x4(i) =                         x4(i) +  5d0*x5(i)

            if (mtemp == 2) x1(i) = 0d0

            if (mtemp == 9 .and. j2 == 1) then
                if (sum(x1(i:i+2) * x2(i:i+2)) < 0d0) then
                    x1(i:i+2) = fdamp * x1(i:i+2)
                end if
            end if

            help = x1(i)**2 * boxsq_over_deltasq(j2, typej)
            transv(j2) = transv(j2) + help
            vsq = vsq + help

            if (x0(i) >= 0.5d0) then
                x0(i) = x0(i) - pbc(j2)
            else if (x0(i) < -0.5d0) then
                x0(i) = x0(i) + pbc(j2)
            end if

        end do ! End of loop over dimensions 1 - 3

        ! KN Get max and next to max displacement.
        if (xneisq > xneisqmax) then
            xneisqmax2 = xneisqmax
            xneisqmax  = xneisq
        else if (xneisq > xneisqmax2) then
            xneisqmax2 = xneisq
        end if

        ! KN Get max E and v. (Be careful with the units!)
        E = 0.5d0 * vsq
        v = sqrt(vsq) * vunit(typej)
        Ekin(j) = E
        trans = trans + E
        Emax = max(Emax, E)
        vmax = max(vmax, v)

        ! If atoms have just been shuffled, this may go wrong since Ethree and Epair
        ! are from previous time step. But even at worst this only makes the
        ! time step shorter monentaneously, i.e. this is not a big problem.
        if (.not. is_sputtered_atom(x0(i1:i3), timesputlim, nisputlim, &
                Epair(j), Ethree(j))) then
            transin = transin + E
            natomsin = natomsin + 1
            Emaxnosput = max(Emaxnosput, E)
            vmaxnosput = max(vmaxnosput, v)
        end if

        isborderat = is_border_atom(j, typej, x0(i1:i3), box, pbc, border_params, &
            tlim, mtemp, ifixed, myproc, irecproc, irec)

        ! Get border kinetic energy
        if (isborderat) then
            transbrdr = transbrdr + E
            nbrdr = nbrdr + 1
            if (E > Emaxbrdr) then
                call logger()
                call logger("ERROR: atom ", j, 0)
                call logger("Ekin:    ", E, 4)
                call logger("Emaxbrdr:", Emaxbrdr, 4)
                call logger("position:", x0(i1:i3)*box(:), 4)
                call my_mpi_abort('Emaxbrdr exceeded', myproc)
            end if
        end if

    end do

    xneimax = sqrt(xneisqmax)
    xneimax2 = sqrt(xneisqmax2)

    comm_array(1) = xneimax
    comm_array(2) = xneimax2
    comm_array(3) = vmax
    comm_array(4) = vmaxnosput
    comm_array(5) = Emax
    comm_array(6) = Emaxnosput
    call my_mpi_max(comm_array, 6)
    xneimax    = comm_array(1)
    xneimax2   = comm_array(2)
    vmax       = comm_array(3)
    vmaxnosput = comm_array(4)
    Emax       = comm_array(5)
    Emaxnosput = comm_array(6)

    if (debug) print *,'Emax', Emax, Emaxnosput

end subroutine Predict


!
! Return whether the given atom is counted as a border atom.
! This is terribly complex - but there is a reason for everything.
!
logical pure function is_border_atom(atomj, typej, x, box, pbc, border_params, &
        tlim, mtemp, ifixed, myproc, irecproc, irec) result(isborderat)

    integer, intent(in) :: atomj, typej, mtemp, myproc, irecproc, irec
    real(kind=real64b), intent(in) :: x(3), box(3), pbc(3), tlim(3)
    logical, intent(in) :: ifixed
    type(BorderParams), intent(in) :: border_params

    logical :: xout(3)
    real(real64b) :: absx(3)


    ! If Tcontroltype is valid, only it matters.
    ! Count all atoms of the given type and ignore others.
    if (border_params%Tcontroltype >= 0) then
        isborderat = (typej == border_params%Tcontroltype)
        return
    end if

    ! Ignore all fixed atoms.
    if (ifixed) then
        isborderat = .false.
        return
    end if

    ! Ignore the recoil atom.
    if (myproc == irecproc .and. atomj == irec) then
        isborderat = .false.
        return
    end if

    absx(:) = abs(x(:))

    ! Ignore sputtered atoms.
    if (any(absx > border_params%sputlim)) then
        isborderat = .false.
        return
    end if

    ! Count atoms within the defined limits for each direction.
    if (x(3)*box(3) > border_params%zmin .and. &
        x(3)*box(3) < border_params%zmax) then

        isborderat = .true.
        return
    end if
    if (absx(1)*box(1) > border_params%xout) then
        isborderat = .true.
        return
    end if
    if (absx(2)*box(2) > border_params%yout) then
        isborderat = .true.
        return
    end if

    xout = (absx > tlim)

    ! Don't scale at open borders with mtemp == 7 or 8.
    if ((mtemp == 7 .or. mtemp == 8) .and. any(pbc /= 1.0d0 .and. xout)) then
        isborderat = .false.
        return
    end if

    ! Finally, count atoms that are at the borders.
    isborderat = any(xout)
end function is_border_atom


!
! Return whether the given atom is considered sputtered or not.
!
! NOTE: This only takes into account the first EAM band when
! checking for non-interacting atoms. This should not be a problem
! since the result is only compared to zero.
!
logical pure function is_sputtered_atom(x, timesputlim, nisputlim, &
        Epair, Ethree) result(sputtered)

    real(kind=real64b), intent(in) :: x(3), timesputlim, nisputlim
    real(kind=real64b), intent(in) :: Epair, Ethree

    real(kind=real64b) :: Epot

    ! If any atom is outside the sputtering limit "timesputlim",
    ! count it as sputtered.
    if (any(abs(x) > timesputlim)) then
        sputtered = .true.
        return
    end if

    ! If any atom with zero interactions is outside the non-interacting
    ! atom sputtering limit "nisputlim", count it as sputtered.
    if (nisputlim < 1.0d30 .and. any(abs(x) > nisputlim)) then
        Epot = Epair + Ethree

        if (Epot == 0.0) then
            sputtered = .true.
            return
        end if
    end if

    ! If none of the above applies, the atom is inside all limits
    ! and is not sputtered.
    sputtered = .false.
end function is_sputtered_atom


!***********************************************************************
! Correct particle positions (periodic boundary conditions)
!***********************************************************************
subroutine Correct(x0, x1, x2, x3, x4, x5, atype, xnp, pbc, deltas, &
        x0nei, box, mtemp, Fzmaxz, FzmaxYz, dydtmaxz)

    real(kind=real64b), intent(inout) :: &
        x0(:), x1(:), x2(:), x3(:), x4(:), x5(:), x0nei(:)

    integer, intent(in) :: mtemp, atype(:)
    real(kind=real64b), intent(in) :: xnp(:)
    real(kind=real64b), intent(in) :: pbc(3), box(3)
    real(kind=real64b), intent(in) :: deltas(itypelow:itypehigh)
    real(kind=real64b), intent(in) :: Fzmaxz, FzmaxYz, dydtmaxz

    real(kind=real64b), parameter :: &
        f02 =   3.0d0 /  16.0d0, &
        f12 = 251.0d0 / 360.0d0, &
        f32 =  11.0d0 /  18.0d0, &
        f42 =   1.0d0 /   6.0d0, &
        f52 =   1.0d0 /  60.0d0

    integer i, i1, iat
    real(kind=real64b) :: daccel


    do i = 1, 3 * myatoms
        iat = int((i + 2) / 3)
        i1 = mod(i - 1, 3) + 1

        ! Only place where time step enters solution of equations of motion!
        daccel = x2(i) - deltas(abs(atype(iat))) * xnp(i)


        ! Gear algorithm corrector step
        if (atype(iat) >= 0 .and..not. ( &
                (FzmaxYz /= 0 .or. dydtmaxz /= 0) .and. &
                i1 == 3 .and. x0(i) * box(3) >= Fzmaxz)) then
            x0(i) = x0(i) - daccel * f02
            x1(i) = x1(i) - daccel * f12
        end if

        x2(i) = x2(i) - daccel
        x3(i) = x3(i) - daccel * f32
        x4(i) = x4(i) - daccel * f42
        x5(i) = x5(i) - daccel * f52

        x0nei(i) = x0nei(i) - daccel * f02


        ! Enforce periodic boundaries
        if (pbc(i1) == 1.0d0 .and. abs(x0(i)) >= 1.5d0) then
            call logger("PROBLEM IN CORRECT:")
            call logger("myproc:", myproc, 4)
            call logger("i:     ", i, 4)
            call logger("x0(i): ", x0(i), 4)
            call logger("xnp(i):", xnp(i), 4)
            call logger("daccel:", daccel, 4)
        endif

        if (x0(i) >= 0.5d0) then
            x0(i) = x0(i) - pbc(i1)
        else if (x0(i) < -0.5d0) then
            x0(i) = x0(i) + pbc(i1)
        end if
    end do

    if (mtemp == 2) x1(1 : 3*myatoms) = 0.0

end subroutine Correct



!***********************************************************************
!
! Scale total momentum/velocity of cell to 0
!
!  Note: be careful with this, it may cause trouble with open
!  boundaries or fixed atoms...
!
!  Be careful with units: should do conversion in real v units,
!  not the internal ones (might cause trouble for different masses !)
!
!***********************************************************************

subroutine ConserveTotalMomentum(x1, natoms, pscale, delta, atype, box)

    real(real64b), intent(inout) :: x1(:)
    real(real64b), intent(in) :: delta(itypelow:itypehigh)
    real(real64b), intent(in) :: box(3)
    integer, intent(in) :: natoms, pscale
    integer, intent(in), contiguous :: atype(:)

    integer :: i, i3
    real(real64b) :: vsum(3)
    real(real64b) :: unitfactor(itypelow:itypehigh)

    select case (pscale)
    case (1)
        ! Scale center of mass momentum to zero.
        unitfactor(:) = mass(:) / delta(:) * vunit(:)
    case (2)
        ! Scale center of mass velocity to zero.
        unitfactor(:) = 1.0d0 / delta(:) * vunit(:)
    case default
        call my_mpi_abort("Unknown pscale", pscale)
    end select


    vsum = 0.0

    do i = 1, myatoms
        i3 = 3 * i - 3
        vsum(:) = vsum(:) + x1(i3+1:i3+3) * unitfactor(abs(atype(i))) * box(:)
    end do

    call my_mpi_sum(vsum, 3)
    vsum(:) = vsum(:) / natoms

    do i = 1, myatoms
        if (myproc /= irecproc .or. i /= irec) then
            i3 = 3 * i - 3
            x1(i3+1:i3+3) = x1(i3+1:i3+3) - vsum(:) / unitfactor(abs(atype(i))) / box(:)
        end if
    end do

    if (debug) print *, 'Vel. sums', x1(1), vsum(:)

end subroutine ConserveTotalMomentum


!***********************************************************************

subroutine RemoveAngularMomentum(x0, x1, delta, natoms, atype, box)

    !     Variables passed in and out
    real(real64b), intent(inout) :: x1(:)

    real(real64b), intent(in) :: x0(:)
    real(real64b), intent(in) :: box(3)
    real(real64b), intent(in) :: delta(itypelow:itypehigh)
    integer, intent(in) :: natoms
    integer, intent(in) :: atype(:)

    !     Local variables
    integer :: i, i3
    real(real64b) :: unithelp, factor, rescale
    real(real64b) :: vsum(3), v(3)
    real(real64b) :: cm(3), x_t(3), dx(3), distance
    real(real64b) :: angmom(3), Itot, msum
    real(real64b) :: normal(3)


    ! Find the center of mass.
    vsum(:) = 0.0
    msum = 0.0
    do i = 1, myatoms
        i3 = 3 * i - 3
        vsum(:) = vsum(:) + x0(i3+1:i3+3) * box(:) * mass(atype(i))
        msum = msum + mass(atype(i))
    end do
    call my_mpi_sum(msum, 1)
    call my_mpi_sum(vsum, 3)
    cm(:) = vsum(:) / (msum * box(:))  ! center of mass

    ! Now find the total angular momentum.
    angmom(:) = 0.0
    do i = 1, myatoms
        i3 = 3 * i - 3
        unithelp = mass(atype(i)) / delta(atype(i)) * vunit(atype(i))
        dx(:) = x0(i3+1:i3+3) - cm(:)
        angmom(1) = angmom(1) + (dx(2)*x1(i3+3) - dx(3)*x1(i3+2)) * box(2)*box(3) * unithelp
        angmom(2) = angmom(2) + (dx(3)*x1(i3+1) - dx(1)*x1(i3+3)) * box(1)*box(3) * unithelp
        angmom(3) = angmom(3) + (dx(1)*x1(i3+2) - dx(2)*x1(i3+1)) * box(2)*box(1) * unithelp
    end do
    call my_mpi_sum(angmom, 3)
    Itot = sqrt(sum(angmom**2))
    ! Now the total momentum, and the axis of rotation
    ! (parallel to the angular momentum vector) are known.

    x_t(:) = (cm(:) * box(:)  + angmom(:)) / box(:)
    ! x_t and cm are two points on the axis of rotation.

    do i = 1, myatoms
        i3 = 3 * i - 3

        ! If the atom happens to have exactly zero velocity, the equations below would
        ! divide by zero, producing NaNs. Avoid this by explicitly skipping these atoms.
        ! They do not contribute to the angular momentum anyway.
        if (all(x1(i3+1:i3+3) == 0.0d0)) cycle

        ! Find the normal to the plane common to the axis of rotation and the point x0(i).
        normal(1) = (x0(i3+2)*(x_t(3)-cm(3)) + x_t(2)*(cm(3)-x0(i3+3)) + cm(2)*(x0(i3+3)-x_t(3))) * box(2)*box(3)
        normal(2) = (x0(i3+3)*(x_t(1)-cm(1)) + x_t(3)*(cm(1)-x0(i3+1)) + cm(3)*(x0(i3+1)-x_t(1))) * box(1)*box(3)
        normal(3) = (x0(i3+1)*(x_t(2)-cm(2)) + x_t(1)*(cm(2)-x0(i3+2)) + cm(1)*(x0(i3+2)-x_t(2))) * box(1)*box(2)
        ! normal(1:3) are the components of the vector perpendicular to the plane.

        ! v(1:3) are the component of the velocity vector parallel to Nx,Ny,Nz.
        factor = sum(x1(i3+1:i3+3) * normal(:) * box(:)) / sum(normal**2) &
                / delta(atype(i)) * vunit(atype(i))
        v(:) = factor * normal(:)

        ! Find the distance between the point x0(i) and the axis of rotation.
        distance = sqrt(sum(((x_t - cm) * (cm - x0(i3+1:i3+3)) * box**2)**2)) &
                / sqrt(sum(((x_t - cm) * box)**2))

        rescale = (Itot / (natoms * mass(atype(i)) * distance)) / sqrt(sum(v**2))
        x1(i3+1:i3+3) = x1(i3+1:i3+3) - v(:) * rescale / box(:) * delta(atype(i)) / vunit(atype(i))
    end do
end subroutine RemoveAngularMomentum



!******************************************************************
subroutine calc_tot_vel(box, atype, delta, natoms, x1, totvel)

    real(real64b), contiguous, intent(in) :: x1(:)
    real(real64b), intent(in) :: delta(itypelow:itypehigh)
    integer, contiguous, intent(in) :: atype(:)
    real(real64b), intent(in) :: box(3)
    integer, intent(in) :: natoms
    real(real64b), intent(out) :: totvel(3)

    integer :: i, i3, itype

    totvel(:) = 0
    do i = 1, myatoms
        i3 = 3*i - 3
        itype = abs(atype(i))
        totvel(:) = totvel(:) + x1(i3+1:i3+3)*box(:)/delta(itype)*vunit(itype)
    end do
    call my_mpi_sum(totvel, 3)
    totvel = totvel / natoms

end subroutine calc_tot_vel

!******************************************************************

subroutine update_avg_vir( &
        wxxiavg, wxxi, wyyiavg, wyyi, wzziavg, wzzi, &
        wxyiavg, wxyi, wxziavg, wxzi, wyziavg, wyzi)

    real(real64b), contiguous, intent(in) :: wxxi(:), wyyi(:), wzzi(:)
    real(real64b), contiguous, intent(inout) :: wxxiavg(:), wyyiavg(:), wzziavg(:)
    real(real64b), contiguous, intent(in), optional :: wxyi(:), wxzi(:), wyzi(:)
    real(real64b), contiguous, intent(inout), optional :: wxyiavg(:), wxziavg(:), wyziavg(:)

    wxxiavg(:myatoms) = wxxiavg(:myatoms) + wxxi(:myatoms)
    wyyiavg(:myatoms) = wyyiavg(:myatoms) + wyyi(:myatoms)
    wzziavg(:myatoms) = wzziavg(:myatoms) + wzzi(:myatoms)
    if (present(wxyiavg)) then
        wxyiavg(:myatoms) = wxyiavg(:myatoms) + wxyi(:myatoms)
        wxziavg(:myatoms) = wxziavg(:myatoms) + wxzi(:myatoms)
        wyziavg(:myatoms) = wyziavg(:myatoms) + wyzi(:myatoms)
    end if

end subroutine update_avg_vir

!******************************************************************

subroutine calc_kinetic_vir(natoms, box, atype, delta, x1, &
        wxxi, wyyi, wzzi, wxyi, wxzi, wyzi)

    integer, intent(in) :: natoms
    real(real64b), intent(in) :: box(3)
    real(real64b), contiguous, intent(in) :: x1(:)
    real(real64b), intent(in) :: delta(itypelow:itypehigh)
    integer, contiguous, intent(in) :: atype(:)

    real(real64b), contiguous, intent(inout) :: &
        wxxi(:), wyyi(:), wzzi(:)
    real(real64b), contiguous, intent(inout), optional :: &
        wxyi(:), wxzi(:), wyzi(:)

    real(real64b) :: dvel(3), totvel(3)
    integer :: i, i3, itype

    call calc_tot_vel(box, atype, delta, natoms, x1, totvel)

    do i = 1, myatoms
        itype = abs(atype(i))
        i3 = 3*i - 3
        dvel(:) = x1(i3+1:i3+3) * box(:) / delta(itype)*vunit(itype) - totvel(:)

        wxxi(i) = wxxi(i) + mass(itype)*vir_unithelp * dvel(1)*dvel(1)
        wyyi(i) = wyyi(i) + mass(itype)*vir_unithelp * dvel(2)*dvel(2)
        wzzi(i) = wzzi(i) + mass(itype)*vir_unithelp * dvel(3)*dvel(3)

        if (present(wxyi)) then
            wxyi(i) = wxyi(i) + mass(itype)*vir_unithelp * dvel(1)*dvel(2)
            wxzi(i) = wxzi(i) + mass(itype)*vir_unithelp * dvel(1)*dvel(3)
            wyzi(i) = wyzi(i) + mass(itype)*vir_unithelp * dvel(2)*dvel(3)
        end if
    end do

end subroutine calc_kinetic_vir

!******************************************************************

subroutine avg_calc_vir(vir_istep_new, vir_istep_old, &
        wxxiavg, wxxi, wyyiavg, wyyi, wzziavg, wzzi, &
        wxyiavg, wxyi, wxziavg, wxzi, wyziavg, wyzi)

    integer, intent(in) :: vir_istep_new, vir_istep_old
    real(real64b), contiguous, intent(out) :: wxxi(:), wyyi(:), wzzi(:)
    real(real64b), contiguous, intent(in) :: wxxiavg(:), wyyiavg(:), wzziavg(:)
    real(real64b), contiguous, intent(out), optional :: wxyi(:), wxzi(:), wyzi(:)
    real(real64b), contiguous, intent(in), optional :: wxyiavg(:), wxziavg(:), wyziavg(:)

    if (vir_istep_new == vir_istep_old) return

    wxxi(:myatoms) = wxxiavg(:myatoms) / (vir_istep_new - vir_istep_old)
    wyyi(:myatoms) = wyyiavg(:myatoms) / (vir_istep_new - vir_istep_old)
    wzzi(:myatoms) = wzziavg(:myatoms) / (vir_istep_new - vir_istep_old)
    if (present(wxyiavg)) then
        wxyi(:myatoms) = wxyiavg(:myatoms) / (vir_istep_new - vir_istep_old)
        wxzi(:myatoms) = wxziavg(:myatoms) / (vir_istep_new - vir_istep_old)
        wyzi(:myatoms) = wyziavg(:myatoms) / (vir_istep_new - vir_istep_old)
    end if

end subroutine avg_calc_vir

!******************************************************************

subroutine zero_avg_vir( &
        wxxiavg, wyyiavg, wzziavg, &
        wxyiavg, wxziavg, wyziavg)

    real(real64b), contiguous, intent(out) :: &
        wxxiavg(:), wyyiavg(:), wzziavg(:)
    real(real64b), contiguous, intent(out), optional :: &
        wxyiavg(:), wxziavg(:), wyziavg(:)

    wxxiavg(:myatoms) = 0
    wyyiavg(:myatoms) = 0
    wzziavg(:myatoms) = 0

    if (present(wxyiavg)) then
        wxyiavg(:myatoms) = 0
        wxziavg(:myatoms) = 0
        wyziavg(:myatoms) = 0
    end if

end subroutine zero_avg_vir


end module mdsubs_mod
