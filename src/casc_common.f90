!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************


! Parameters and state for recoil atoms.

module casc_common
    use datatypes, only: real64b

    implicit none

    public
    private :: real64b


    ! These are read in from md.in or recoildata.in.
    integer            :: irec, recatype, recatypem
    real(kind=real64b) :: recen, rectheta, recphi
    real(kind=real64b) :: xrec, yrec, zrec

    ! These are simulation state.
    integer :: &
        irecproc, & ! Rank of the processor containing the recoil.
        iatrec      ! The atomindex of the recoil.

end module casc_common
