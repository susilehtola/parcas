!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module para_common

    use datatypes, only: real64b, int64b

    use defs, only: NPMAX, NPPASSMAX


    implicit none

    public
    private :: real64b, int64b

    ! All sorts of temporary buffers, used mainly for communication.
    real(real64b), allocatable :: buf(:), buf2(:)
    integer, allocatable:: ibuf(:), ibuf2(:)
    real(real64b), allocatable :: pbuf(:)
    real(real64b), allocatable :: dbuf(:)

    ! Number of atoms in the neighbor list, including atoms in the border
    ! region of neighboring processes. Limited by NPMAX.
    integer :: np0pairtable

    !     ------------------------------------------------------------------

    integer :: myproc
    integer :: nprocs

    integer :: myatoms

    integer :: idebug

    ! iprint is .true. for the root processor and it's used to control
    ! printing to stdout.
    logical :: iprint

    logical :: debug


    !
    ! The variables below have to do with the communication patterns
    ! implemented in mdparsubs. See there for documentation.
    !

    ! The number of nodes for each dimension of the simulation box.
    integer :: nnodes(3)

    ! Dimrank of this process, i.e. position in x, y, z in the node grid.
    integer :: dimrank(3)

    ! The locations of the edges of all nodes, by dimrank and dimension.
    ! rmn(i) and rmx(i) for a node with dimrank(i) == n are located in
    ! node_edges(n, i) and node_edges(n+1, i), respectively.
    real(real64b), allocatable :: node_edges(:,:)

    ! The node edges for this node in each dimension.
    real(real64b) :: rmn(3)  ! Minimum
    real(real64b) :: rmx(3)  ! Maximum

    ! The rmn and rmx values for the nearest neighbors in each dimension,
    ! taking into account periodic borders.
    real(real64b) :: neighbor_rmn(3, 2)
    real(real64b) :: neighbor_rmx(3, 2)

    ! MPI ranks of processes to send to and receive from when passing atoms
    ! between neighboring processes. Indexes: dimension (x,y,z = 1,2,3) and
    ! direction (1,2 = left,right).
    ! Note that these are reversed when "passing back" atoms to processes
    ! where they originally came from.
    integer :: sendproc(3, 2)
    integer :: recvproc(3, 2)

    ! Whether this process needs to send/receive atoms during the swap with the
    ! neighbors given by sendproc/recvproc at the same indexes.
    logical :: needsend(3, 2)
    logical :: needrecv(3, 2)

    ! How many atoms must be sent / received during the swap with the neighbors.
    ! Set before updating the neighbor list.
    integer :: numsendatoms(3, 2)
    integer :: numrecvatoms(3, 2)

    ! The index of the first atom received from the neighbor. Combine with
    ! numrecvatoms to know which atoms to send back to that neighbor.
    ! Set before updating the neighbor list.
    integer :: firstrecvatom(3, 2)

    ! This process' local indexes of the atoms that were sent to the neighbor
    ! in the direction indicated by the indexes (i, dim, dir).
    ! Valid from 1 to numsendatoms(dim, dir) in the first index.
    integer, allocatable :: sendatomslist(:, :, :)

contains

    subroutine para_common_allocate(nprocs)
        integer, intent(in) :: nprocs

        integer :: bufsize, ibufsize

        bufsize = 3 * NPMAX ! neighbor list
        bufsize = max(bufsize, 4 * NPMAX)  ! liquid analysis
        bufsize = max(bufsize, 10 * NPMAX)  ! potential comms
        bufsize = max(bufsize, 27 * NPMAX)  ! shuffling (max)

        ibufsize = NPMAX  ! liquid analysis, movie slice output
        ibufsize = max(ibufsize, 2 * NPMAX + 1)  ! shuffling (max)

        allocate(buf(bufsize))
        allocate(buf2(bufsize))
        allocate(ibuf(ibufsize))
        allocate(ibuf2(ibufsize))

        allocate(pbuf(NPMAX))
        allocate(dbuf(NPMAX))

        allocate(sendatomslist(NPPASSMAX, 3, 2))
        allocate(node_edges(0 : nprocs, 3))

    end subroutine para_common_allocate

end module para_common
