!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module mdlinkedlist_mod

    use datatypes, only: real64b
    use defs, only: NPMAX, NNMAX, NPPASSMAX
    use my_mpi

    use timers, only: &
        tmr, &
        TMR_NBOR_LINK_CELL, &
        TMR_NBOR_GET_PAIRS, &
        TMR_NBOR_COMMS

    use para_common, only: &
        myatoms, np0pairtable, &
        rmn, rmx, &
        nnodes, &
        sendproc, recvproc, &
        needsend, needrecv, &
        numsendatoms, numrecvatoms, &
        firstrecvatom, &
        sendatomslist, &
        buf, &
        debug, iprint

    use output_logger, only: &
        logger, log_buf, logger_write, &
        logger_clear_buffer, logger_append_buffer


    implicit none

    private
    public :: Pair_Table


contains


    subroutine linked_list(x0, rcut, comm_rcut, pbc, head, list, &
            numcells, cellsize, nodemin, nodemax)
        !
        ! All dimensions are handled in internal units.
        !
        ! Size of one cell is set to exactly rcut.
        ! Cells are distributed so that cell 0 is between 0 and rcut.
        ! However, cell size may vary between calls of the subroutine.
        !
        !   head(:)     Indexes to the first atoms of each cell
        !
        !   list(:)     Linked List of atoms for each cell. Zero means end of list.
        !               list(i) contains the index of the next atom in the cell.
        !               The indexes are in [1, np0pairtable].
        !
        real(real64b), contiguous, intent(in) :: x0(:)
        real(real64b), intent(in) :: rcut(3), comm_rcut(3)
        real(real64b), intent(in) :: pbc(3)

        integer, contiguous, intent(out) :: head(0:)
        integer, contiguous, intent(out) :: list(0:)  ! index 0 used as temporary below
        integer, intent(out):: numcells(3)
        real(real64b), intent(out) :: cellsize(3)
        real(real64b), intent(out) :: nodemin(3), nodemax(3)

        real(real64b) :: nodesize(3)

        integer :: icell
        integer :: i, i3

        integer :: ix,iy,iz

        real(real64b) :: minhelp, maxhelp
        real(real64b) :: xhelp, yhelp, zhelp
        real(real64b) :: dx, dy, dz

        real(real64b) :: t1


        ! Calculate the size of this node plus its neighboring 'skin'.
        do i = 1, 3
            if (nnodes(i) == 1) then
                nodesize(i) = 1d0
            else
                maxhelp = min(rmx(i), +0.5d0)
                minhelp = max(rmn(i), -0.5d0)
                nodesize(i) = (maxhelp - minhelp + 2*comm_rcut(i))
            end if
        end do

        if (any(pbc(:) == 1.0d0 .and. 2*comm_rcut(:) >= nodesize(:))) then
            call logger("ERROR: cell size less than twice the cutoff")
            call my_mpi_abort('Cell size and cutoff', myproc)
        endif

        !
        ! TODO: Lower limit is usually inclusive in other parts of PARCAS
        ! Calculate the minimum coordinate of the node. Remember periodicity!
        ! The lower node limit is exclusive, upper inclusive!
        !
        do i = 1, 3
            if (nnodes(i) == 1) then
                nodemin(i) = -0.5d0
                nodemax(i) = +0.5d0
            else
                nodemin(i) = rmn(i) - comm_rcut(i)
                nodemax(i) = rmx(i) + comm_rcut(i)

                if (nodemin(i) <= -0.5d0) then
                    if (pbc(i) == 1.0d0) then
                        nodemin(i) = nodemin(i) + 1.0d0
                    else
                        nodemin(i) = -0.5d0 - comm_rcut(i)
                    end if
                end if

                if (nodemax(i) > 0.5d0) then
                    if (pbc(i) == 1.0d0) then
                        nodemax(i) = nodemax(i) - 1.0d0
                    else
                        nodemax(i) = +0.5d0 + comm_rcut(i)
                    end if
                end if
            end if
        end do


        t1 = mpi_wtime()


        if (debug) print *,'linklist start', myproc, myatoms, np0pairtable

        !
        ! Split the node into cells ranging from 0 to numcells(:)-1
        !
        numcells(:) = max(1, int(nodesize(:) / rcut(:)))


        !
        ! Cell division makes no sense with less than three cells, since all
        ! cells will be looked at anyway.
        !
        where (numcells(:) < 3) numcells(:) = 1


        if (iprint .and. debug) then
            call logger("Linkedlist nx ny nz: ", numcells(:), 0)
        endif

        i = product(numcells(:)) + 1

        if (i > size(head)) then
            call logger("Linkedlist ERROR: head size too small", i, size(head), 0)
            call my_mpi_abort('head size', i)
        endif

        head(0:i-1) = 0


        !
        ! Calculate the actual cell size in all dimensions
        !
        cellsize(:) = nodesize(:) / numcells(:)

        if (debug) then
            write(log_buf, '(A,i9,A,3(A,2i3,3f9.4,A))') &
                'Link cell calculation in proc,', myproc, new_line(''), &
                'proc X cell n, size and range', &
                myproc, numcells(1), cellsize(1), nodemin(1), nodemax(1), new_line(''), &
                'proc Y cell n, size and range', &
                myproc, numcells(2), cellsize(2), nodemin(2), nodemax(2), new_line(''), &
                'proc Z cell n, size and range', &
                myproc, numcells(3), cellsize(3), nodemin(3), nodemax(3), new_line('')
            call logger_write(trim(log_buf), ordered=.true.)
        end if
        if (debug) print *, 'linklist2'

        !
        ! Because the Allen-Tildesley linked list puts atoms in reverse order,
        ! go through atoms from top to bottom to get them in logical order
        !
        xhelp = 1.0 / cellsize(1)
        yhelp = 1.0 / cellsize(2)
        zhelp = 1.0 / cellsize(3)

        do i = np0pairtable, 1, -1
            i3 = 3 * i - 3

            dx = x0(i3+1) - nodemin(1)
            if (pbc(1) == 1.0d0) then
                if (dx < 0) dx = dx + 1
                if (dx > 1) dx = dx - 1
            end if
            ix = int(dx * xhelp)
            if (ix < 0 .or. ix >= numcells(1)) then
                if (pbc(1) == 1.0d0) then
                    write(log_buf, *) 'This is impossible, atom x outside cell', &
                        myproc, i, ix, dx, nodemin(1), nodemax(1), x0(i3+1)
                    call logger(log_buf)
                    call my_mpi_abort('x limit', myproc)
                else
                    ix = max(ix, 0)
                    ix = min(ix, numcells(1) - 1)
                end if
            end if

            dy = x0(i3+2) - nodemin(2)
            if (pbc(2) == 1.0d0) then
                if (dy < 0) dy = dy + 1
                if (dy > 1) dy = dy - 1
            end if
            iy = int(dy * yhelp)
            if (iy < 0 .or. iy >= numcells(2)) then
                if (pbc(2) == 1.0d0) then
                    write(log_buf, *) 'This is impossible, atom y outside cell', &
                        myproc, i, iy, dy, nodemin(2), nodemax(2), x0(i3+2)
                    call logger(log_buf)
                    call my_mpi_abort('y limit', myproc)
                else
                    iy = max(iy, 0)
                    iy = min(iy, numcells(2) - 1)
                end if
            end if

            dz = x0(i3+3) - nodemin(3)
            if (pbc(3) == 1.0d0) then
                if (dz < 0) dz = dz + 1
                if (dz > 1) dz = dz - 1
            end if
            iz = int(dz * zhelp)
            if (iz < 0 .or. iz >= numcells(3)) then
                if (pbc(3) == 1.0d0) then
                    write(log_buf, *) 'This is impossible, atom z outside cell', &
                        myproc, i, iz, dz, nodemin(3), nodemax(3), x0(i3+3)
                    call logger(log_buf)
                    call my_mpi_abort('z limit', myproc)
                else
                    iz = max(iz, 0)
                    iz = min(iz, numcells(3) - 1)
                end if
            end if

            ! Allen-Tildesley link cell creation

            icell = (iz * numcells(2) + iy) * numcells(1) + ix
            list(i) = head(icell)
            head(icell) = i

        end do

        tmr(TMR_NBOR_LINK_CELL) = tmr(TMR_NBOR_LINK_CELL) + (mpi_wtime() - t1)

    end subroutine


    !
    ! Rebuild the Verlet neighbor list
    !
    !
    ! nabors(i)     Index to the first neighbor of atom i in nborlist
    !
    ! nborlist(:)   A standard Verlet neighbor list:
    !
    !   nborlist(1)     Number of neighbors for atom 1 (e.g. N1)
    !                   Accessed with nborlist(nabors(1)-1)
    !   nborlist(2)     Index of first neighbor of atom 1.
    !                   Accessed with nborlist(nabors(1))
    !   nborlist(3)     Index of second neighbor of atom 1
    !   ...
    !   nborlist(N1+1)  Index of last neighbor of atom 1
    !   nborlist(N1+2)  Number of neighbors for atom 2
    !   nborlist(N1+3)  Index of first neighbor of atom 2
    !   ...
    !   ...           And so on for all N atoms
    !
    ! TODO: Update the comment with all parameters
    !
    subroutine Pair_Table(nabors, nborlist, x0, Rneicut, box, pbc, x0nei, &
            atomindex, half_neighbor_list, double_borders, SortNeiList, printnow)

        integer, contiguous, intent(out) :: nabors(:)
        integer, contiguous, intent(out) :: nborlist(:)
        real(real64b), contiguous, intent(out) :: x0nei(:)
        real(real64b), contiguous, intent(inout) :: x0(:)

        real(real64b), intent(in) :: box(3), pbc(3)
        real(real64b), intent(in) :: Rneicut
        integer, contiguous, intent(in) :: atomindex(:)
        logical, intent(in) :: half_neighbor_list
        logical, intent(in) :: double_borders
        logical, intent(in) :: SortNeiList
        logical, intent(in) :: printnow

        !
        ! Allen-Tildesley linked list parameters
        !
        ! Arrays size in list() is well defined, but that in head() not.
        ! If you have trouble with it, just increase the head size to whatever
        ! constant you need, and modify the test statement below as well.
        !
        integer, save :: head(0:NPMAX/2)
        integer, save :: list(0:NPMAX)  ! index 0 used as temporary below

        integer :: numcells(3)
        real(real64b) :: cellsize(3)
        real(real64b) :: nodemin(3), nodemax(3)


        real(real64b) :: rcut(3), comm_rcut(3)
        real(real64b) :: rcutsq

        real(real64b) :: xp(3), xs, xsmin
        logical :: llc_pbc(3)  ! Whether the linked list cells are periodic


        integer :: npairs, np0, myatoms_max

        integer :: i, i3
        integer :: j, j3

        integer :: icell
        integer :: mij, mijptr


        integer :: ix, iy, iz
        integer :: dix, diy, diz
        integer :: dixmin, diymin, dizmin
        integer :: dixmax, diymax, dizmax
        integer :: inx, iny, inz

        real(real64b) :: xhelp, yhelp, zhelp
        real(real64b) :: dx, dy, dz

        integer, parameter :: nborsize = NNMAX * NPMAX

        real(real64b) :: t1


        logical, save :: firsttime = .true.


        x0nei(1 : 3*myatoms) = 0.0


        rcutsq = Rneicut**2
        rcut(:) = Rneicut / box(:)

        comm_rcut(:) = rcut(:)
        if (double_borders) comm_rcut(:) = 2 * rcut(:)


        !
        ! Collect all border atoms from neighboring processes into x0,
        ! into the positions after my own atoms. Sets np0 to the total
        ! number of atoms = myatoms + border atoms from neighbors.
        !
        if (nprocs > 1) then
            t1 = mpi_wtime()
            call pass_border_atoms(myatoms, np0, x0, comm_rcut)
            tmr(TMR_NBOR_COMMS) = tmr(TMR_NBOR_COMMS) + (mpi_wtime() - t1)
        else
            np0 = myatoms
        end if

        np0pairtable = np0


        !
        ! Allan-Tildesley linked list. All positional parameters are in PARCAS
        ! internal units.
        !
        call linked_list(x0, rcut, comm_rcut, pbc, head, list, &
            numcells, cellsize, nodemin, nodemax)


        t1 = mpi_wtime()

        !
        ! Find neighbors using cell list constructed above
        !
        do i = 1, 3
            ! When there is only one node, the linked list cells
            ! go through the periodic boundary. With more nodes,
            ! this is not needed, since the border atoms are in the
            ! last cells.
            if (nnodes(i) > 1) then
                llc_pbc(i) = .false.
            else
                llc_pbc(i) = (pbc(i) == 1)
            end if
        end do

        npairs = 0
        mij = 0
        xsmin = huge(0d0)

        dixmax = ceiling(rcut(1)/cellsize(1))
        dixmin = -dixmax
        diymax = ceiling(rcut(2)/cellsize(2))
        diymin = -diymax
        dizmax = ceiling(rcut(3)/cellsize(3))
        dizmin = -dizmax

        if (numcells(1) == 1) then
            dixmin = 0
            dixmax = 0
        end if
        if (numcells(2) == 1) then
            diymin = 0
            diymax = 0
        end if
        if (numcells(3) == 1) then
            dizmin = 0
            dizmax = 0
        end if

        ! For half_neighbor_list, discard atom pairs xj < xi, hence dixmin = 0.
        if (half_neighbor_list) dixmin = 0

        if (double_borders) then
            ! Requires all atoms (cell and borders) to be in the same list
            myatoms_max = np0
        else
            myatoms_max = myatoms ! Normal neighbor list
        end if

        if (debug) print *, 'linklist3', myatoms, myatoms_max, double_borders

        xhelp = 1.0 / cellsize(1)
        yhelp = 1.0 / cellsize(2)
        zhelp = 1.0 / cellsize(3)

        do i = 1, myatoms_max
            i3 = 3*i - 3
            dx = x0(i3+1) - nodemin(1)
            if (pbc(1) == 1 .and. dx < 0) dx = dx + 1.0d0
            if (pbc(1) == 1 .and. dx > 1) dx = dx - 1.0d0
            ix = int(dx * xhelp)
            if (pbc(1) /= 1 .and. ix < 0) ix = 0
            if (pbc(1) /= 1 .and. ix >= numcells(1)) ix = numcells(1) - 1

            dy = x0(i3+2) - nodemin(2)
            if (pbc(2) == 1 .and. dy < 0) dy = dy + 1.0d0
            if (pbc(2) == 1 .and. dy > 1) dy = dy - 1.0d0
            iy = int(dy * yhelp)
            if (pbc(2) /= 1 .and. iy < 0) iy = 0
            if (pbc(2) /= 1 .and. iy >= numcells(2)) iy = numcells(2) - 1

            dz = x0(i3+3) - nodemin(3)
            if (pbc(3) == 1 .and. dz < 0) dz = dz + 1.0d0
            if (pbc(3) == 1 .and. dz > 1) dz = dz - 1.0d0
            iz = int(dz * zhelp)
            if (pbc(3) /= 1 .and. iz < 0) iz = 0
            if (pbc(3) /= 1 .and. iz >= numcells(3)) iz = numcells(3) - 1

            if ((ix < 0 .or. ix > numcells(1)) .or. &
                (iy < 0 .or. iy > numcells(2)) .or. &
                (iz < 0 .or. iz > numcells(3))) then
                print *, 'i x y z index', i, x0(i3+1:i3+3), atomindex(i)
                print *, 'ix iy iz', ix, iy, iz
                print '(3(A,2i3,3f9.4/))', &
                    'proc X cell n, size and range', &
                    myproc, numcells(1), cellsize(1), nodemin(1), nodemax(1), &
                    'proc Y cell n, size and range', &
                    myproc, numcells(2), cellsize(2), nodemin(2), nodemax(2), &
                    'proc Z cell n, size and range', &
                    myproc, numcells(3), cellsize(3), nodemin(3), nodemax(3)

                call my_mpi_abort('XXX HORROR', myproc)
            endif

            mij = mij + 1
            mijptr = mij

            ! Currently zero neighbors for atom at index mijptr
            nborlist(mijptr) = 0

            ! Store first index of this atom
            nabors(i) = mijptr + 1

            xcell_loop: do dix = dixmin, dixmax
                inx = ix + dix
                if (llc_pbc(1) .and. inx >= numcells(1)) inx = 0
                if (llc_pbc(1) .and. inx < 0) inx = numcells(1) - 1
                if (.not. llc_pbc(1) .and. inx >= numcells(1)) cycle
                if (.not. llc_pbc(1) .and. inx < 0) cycle

                ycell_loop: do diy = diymin, diymax
                    iny = iy + diy
                    if (llc_pbc(2) .and. iny >= numcells(2)) iny = 0
                    if (llc_pbc(2) .and. iny < 0) iny = numcells(2) - 1
                    if (.not. llc_pbc(2) .and. iny >= numcells(2)) cycle
                    if (.not. llc_pbc(2) .and. iny < 0) cycle

                    zcell_loop: do diz = dizmin, dizmax
                        inz = iz + diz
                        if (llc_pbc(3) .and. inz >= numcells(3)) inz = 0
                        if (llc_pbc(3) .and. inz < 0) inz = numcells(3) - 1
                        if (.not. llc_pbc(3) .and. inz >= numcells(3)) cycle
                        if (.not. llc_pbc(3) .and. inz < 0) cycle

                        ! Get first atom in cell
                        icell = (inz * numcells(2) + iny) * numcells(1) + inx
                        ! Set list(0) so that no special case is needed for the
                        ! first iteration.
                        list(0) = head(icell)
                        j = 0

                        list_loop: do
                            j = list(j)
                            if (j == 0) exit list_loop

                            !
                            ! Calculate distance between atoms i and j
                            !
                            if (.not. half_neighbor_list) then
                                ! Accept all neighbors
                                if (i == j) cycle list_loop
                                j3 = 3*j - 3

                                xp(1) = x0(j3+1) - x0(i3+1)
                                if (pbc(1) == 1.0d0) xp(1) = xp(1) - int(2 * xp(1))
                                xs = (xp(1) * box(1))**2
                                if (xs >= rcutsq) cycle list_loop

                                xp(2) = x0(j3+2) - x0(i3+2)
                                if (pbc(2) == 1.0d0) xp(2) = xp(2) - int(2 * xp(2))
                                xs = xs + (xp(2) * box(2))**2
                                if (xs >= rcutsq) cycle list_loop

                                xp(3) = x0(j3+3) - x0(i3+3)
                                if (pbc(3) == 1.0d0) xp(3) = xp(3) - int(2 * xp(3))
                                xs = xs + (xp(3) * box(3))**2
                                if (xs >= rcutsq) cycle list_loop

                            else ! Discard half the atom pairs, xj < xi!
                                j3 = 3*j - 3

                                xp(1) = x0(j3+1) - x0(i3+1)
                                if (xp(1) < 0d0) xp(1) = xp(1) + pbc(1)
                                xs = (xp(1) * box(1))**2
                                if (xs >= rcutsq .or. xp(1) < 0d0) cycle list_loop

                                xp(2) = x0(j3+2) - x0(i3+2)
                                if (xp(1) == 0d0 .and. xp(2) < 0d0) cycle list_loop
                                if (pbc(2) == 1.0d0) xp(2) = xp(2) - int(2 * xp(2))
                                xs = xs + (xp(2) * box(2))**2
                                if (xs >= rcutsq) cycle list_loop

                                xp(3) = x0(j3+3) - x0(i3+3)
                                if (xp(1) == 0d0 .and. xp(2) == 0d0 .and. xp(3) <= 0d0) cycle list_loop
                                if (pbc(3) == 1.0d0) xp(3) = xp(3)- int(2 * xp(3))
                                xs = xs + (xp(3) * box(3))**2
                                if (xs >= rcutsq) cycle list_loop
                            end if

                            ! Accepted neighbor, add to list
                            xsmin = min(xsmin, xs)
                            if (i <= myatoms) npairs = npairs + 1
                            mij = mij + 1
                            if (mij > nborsize) then
                                call my_mpi_abort('nborsize', myproc)
                            end if
                            nborlist(mijptr) = nborlist(mijptr) + 1
                            nborlist(mij) = j

                        end do list_loop ! cell atoms loop
                    end do  zcell_loop
                end do  ycell_loop
            end do xcell_loop
        end do

        nabors(myatoms_max+1) = mij + 2


        if (SortNeiList) then
            ! Because of Stilweb mixed routines
            ! sort neighbors of all atoms in ascending order.

            do i=1, myatoms
                call isort(nabors(i), nabors(i+1) - 2, nborlist)
            end do
        end if

        if (debug) print *, 'nborlist construction done'

        firsttime = .false.
        !
        !     End of link cell calculation
        !***********************************************************************


        ! Including the neighbor counts of atoms > myatoms(with double_borders)
        ! would deceptively lower the average due to including neighbors of
        ! neighbors, some of which are missing (not passed along).
        xp(1) = npairs
        xp(2) = myatoms
        call my_mpi_sum(xp, 2)
        xs = xp(1) / xp(2)
        xsmin = sqrt(xsmin)
        call my_mpi_min(xsmin, 1)
        if (printnow .and. iprint) then
            call logger("Neighbors per atom:", xs, 0)
            call logger("drmin:", xsmin, 4)
        end if

        tmr(TMR_NBOR_GET_PAIRS) = tmr(TMR_NBOR_GET_PAIRS) + (mpi_wtime() - t1)

    end subroutine Pair_Table


    !
    ! Serves two purposes.
    !
    ! 1. Collects position data on atoms in the border region in
    !    neighboring processes into x0. x0 then contains first the
    !    own atoms, then neighbors' border atoms.
    !    Sets np to the total number of atoms = myatoms + neighbor
    !    atoms collected.
    !
    ! 2. Initializes the meta-data for passing of atom data between
    !    neighboring processes. Fills in firstrecvatoms, numrecvatoms,
    !    an numsendatoms, since these change over time.
    !    Things like needsend and sendproc are not set here, since
    !    they never change.
    !
    subroutine pass_border_atoms(myatoms, np, x0, comm_rcut)

        integer, intent(in) :: myatoms
        integer, intent(out) :: np
        real(real64b), contiguous, intent(inout) :: x0(:)
        real(real64b), intent(in) :: comm_rcut(3)

        character(1), parameter :: dimnames(3) = ["x", "y", "z"]

        integer :: dimen, dir
        integer :: i, i3
        integer :: nlast
        logical :: send
        integer :: numsend, numrecv
        integer :: tag
        integer :: request
        integer :: stat(MPI_STATUS_SIZE)
        integer :: ierror

        np = myatoms

        firstrecvatom(:,:) = 0
        numrecvatoms(:,:) = 0
        numsendatoms(:,:) = 0

        dimen_loop: do dimen = 1, 3
            ! The last index to consider for sending atoms. This means that
            ! atoms received from dir=1 are not sent to dir=2.
            nlast = np

            dir_loop: do dir = 1, 2
                tag = 2 * dimen + dir
                firstrecvatom(dimen, dir) = np + 1

                ! Start receiving atoms from the other neighbor into the position array.
                if (needrecv(dimen, dir)) then
                    call MPI_Irecv(x0(3*np+1), 3*NPPASSMAX, mpi_double_precision, &
                        recvproc(dimen, dir), tag, mpi_comm_world, request, ierror)
                end if

                ! Find atoms near the borders in this dimension and
                ! pack them into the temporary buffer.
                if (needsend(dimen, dir)) then
                    numsend = 0

                    do i = 1, nlast
                        i3 = 3*i - 3
                        ! Make sure atoms on the edges are handled correctly!
                        ! The upper bound for each node (i.e. the lower bound for
                        ! sending to the left) is exclusive. The lower bound for
                        ! each node is inclusive. If this is wrong, the cell
                        ! generation above can abort.
                        if (dir == 1) then
                            send = (x0(i3+dimen) < rmn(dimen) + comm_rcut(dimen))
                        else
                            send = (x0(i3+dimen) >= rmx(dimen) - comm_rcut(dimen))
                        end if
                        if (send) then
                            numsend = numsend + 1
                            if (numsend >= NPPASSMAX) call my_mpi_abort("NPPASSMAX overflow", myproc)

                            sendatomslist(numsend, dimen, dir) = i

                            buf(3*numsend-2) = x0(i3+1)
                            buf(3*numsend-1) = x0(i3+2)
                            buf(3*numsend-0) = x0(i3+3)
                        end if
                    end do

                    numsendatoms(dimen, dir) = numsend

                    call MPI_Send(buf, 3*numsend, mpi_double_precision, &
                        sendproc(dimen, dir), tag, mpi_comm_world, ierror)
                end if

                ! Wait for the previously started receive operation to finish.
                if (needrecv(dimen, dir)) then
                    call MPI_Wait(request, stat, ierror)
                    call MPI_Get_count(stat, mpi_double_precision, numrecv, ierror)
                    numrecv = numrecv / 3  ! atoms, not reals

                    numrecvatoms(dimen, dir) = numrecv

                    np = np + numrecv
                    if (np > NPMAX) call my_mpi_abort("NPMAX overflow", myproc)
                end if
            end do dir_loop
        end do dimen_loop

        if (debug) then
            if (iprint) then
                call logger("After neighborlist comms:")
                call logger("-------------------------")
            end if
            call logger_clear_buffer()
            call logger_append_buffer("Node rank:", myproc, 0)
            call logger_append_buffer("numsendatoms:", 4)
            do dimen = 1, 3
                call logger_append_buffer(dimnames(dimen), numsendatoms(dimen, 1), numsendatoms(dimen, 2), 8)
            end do
            call logger_append_buffer("numrecvatoms:", 4)
            do dimen = 1, 3
                call logger_append_buffer(dimnames(dimen), numrecvatoms(dimen, 1), numrecvatoms(dimen, 2), 8)
            end do
            call logger_append_buffer("firstrecvatom:", 4)
            do dimen = 1, 3
                call logger_append_buffer(dimnames(dimen), firstrecvatom(dimen, 1), firstrecvatom(dimen, 2), 8)
            end do
            call logger_write(trim(log_buf), ordered=.true.)
        end if

    end subroutine pass_border_atoms


    !***********************************************************************
    !***********************************************************************
    !***********************************************************************


    !
    ! Quicksort from Numerical Recipes in Fortran77 p. 324-325.
    ! copied from PDF document by Kai Nordlund Jan 21, 2003
    ! modified for integers and in arbitrary range in arr
    !

    subroutine isort(n1, n2, iarr)

        integer, intent(in) :: n1, n2
        integer, intent(inout) :: iarr(:)

        integer, parameter :: M = 7
        integer, parameter :: NSTACK = 2*NNMAX

        !
        !     Sorts an array iarr(1:n) into ascending numerical order using the
        !     Quicksort algorithm. n is input; iarr is replaced on output by its
        !     sorted rearrangement.
        !     Parameters: M is the size of subarrays sorted by straight
        !     insertion and NSTACK is the require auxiliary storage.

        integer :: i,ir,j,jstack,k,l,istack(NSTACK)
        integer :: a,temp

        jstack=0
        l=n1
        ir=n2

1       if (ir-l.lt.M) then

            do j=l+1,ir
                a=iarr(j)

                do i=j-1,l,-1
                    if(iarr(i).le.a)goto 2
                    iarr(i+1)=iarr(i)
                enddo

                i=l-1
2               iarr(i+1)=a
            enddo

            if(jstack.eq.0)return
            ir=istack(jstack)

            l=istack(jstack-1)
            jstack=jstack-2
        else
            k=(l+ir)/2

            temp=iarr(k)
            iarr(k)=iarr(l+1)
            iarr(l+1)=temp
            if(iarr(l).gt.iarr(ir))then
                temp=iarr(l)
                iarr(l)=iarr(ir)
                iarr(ir)=temp
            endif
            if(iarr(l+1).gt.iarr(ir))then
                temp=iarr(l+1)
                iarr(l+1)=iarr(ir)
                iarr(ir)=temp
            endif
            if(iarr(l).gt.iarr(l+1))then
                temp=iarr(l)
                iarr(l)=iarr(l+1)
                iarr(l+1)=temp
            endif
            i=l+1
            j=ir
            a=iarr(l+1)
3           continue
            i=i+1
            if(iarr(i).lt.a)goto 3
4           continue
            j=j-1
            if(iarr(j).gt.a)goto 4
            if(j.lt.i)goto 5
            temp=iarr(i)
            iarr(i)=iarr(j)
            iarr(j)=temp
            goto 3
5           iarr(l+1)=iarr(j)
            iarr(j)=a
            jstack=jstack+2
            if(jstack .gt. NSTACK) call my_mpi_abort('NSTACK too small in sort', int(jstack))
            if(ir-i+1.ge.j-l)then
                istack(jstack)=ir
                istack(jstack-1)=i
                ir=j-1
            else
                istack(jstack)=j-1
                istack(jstack-1)=l
                l=i
            endif
        endif
        goto 1

    end subroutine isort

end module mdlinkedlist_mod
