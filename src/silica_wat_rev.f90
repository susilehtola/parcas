!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

!-------------------------------------------------------------------------------
! MODIFIED VERSION TO INCLUDE Ge-O Ge-Si by Konstantin Avchachov 2015
! New parameter for Ge/GeO2 system was publish but not used here
! This implementation uses old Watanabe functions from 1999
! Uses original Posselt parametrization for Ge
! wata - Watanabe potential for silica
!
! Usage
!   Use delta=0.01 or smaller value.
!
! Description
!   Implements the silica potential proposed by Watanabe et al. in
!   Appl. Surf. Sci. 234 (2004) 207-213. Note that there is a wrong
!   bond softening function in the paper and some wrong parameters, too.
!   The correct function and parameters received by e-mail
!   from Dr. Watanabe.
!
!   Fortan90 mmodule structure used instead of plain old subroutines, because it
!   ensures correct delivery of array data from mdx.f90 to this module.
!
!   The code is optimized for speed, not for readability.
!
! History
!   prior 1997     Code originates from ancient HARWELL code
!   Jan 1997       Modified for PARCAS SW routine by Kai Nordlund
!   Aug 2006       Rewritten for silica by Juha Samela
!   Feb 2008>       Parameters for Ge atoms added for GeO2 and Ge-Si-O etc. by Olli Pakarinen
!
! Code description
!   Language       Fortran 90
!
!-------------------------------------------------.-----------------------------

module silica_wat_mod_rev

    use output_logger, only: logger

    use datatypes, only: real64b
    use defs
    use PhysConsts, only: pi
    use typeparam
    use my_mpi
    use splinereppot_mod, only: reppot_only, reppot_fermi
    use timers, only: tmr, TMR_SEMICON_COMMS
    use para_common, only: &
        myatoms, np0pairtable, &
        buf, debug

    use mdparsubs_mod, only: potential_pass_back_border_atoms

    use silica_wat_old
    use silica_wat_locals


    implicit none

    private
    public :: silica_wat_force_rev
    public :: map_silica_arrays


    integer, parameter :: DP = kind(1.0d1)

    real (kind=DP), parameter :: TWOPI = 2.0_DP * pi


contains

    !-----------------------------------------------------------------------------
    !
    ! silica_wat_force - Calculates interatomic forces using the Watanabe potential
    !
    ! Method
    !   Calculates acceleration (xnp) of particles. The particle position are
    !   given in x0, and their interactions through a central force are
    !   calculated according to the Watanabe potential for silica.
    !   The skeleton for this subroutine is taken from stilweb.f90.
    !
    !   If you modify the parameters, remember (Watanabe et al.):
    !   Although the three-body term was originally introduced
    !   just to describe the bond bending forces, it has an additional
    !   role to cancel out long-range two-body interactions
    !   at the second nearest-neighbor distances. Therefore,
    !   the cutoff distance of a three-body term should be set
    !   at a longer value than that of two-body terms.
    !   In a three-body term for a tight bond angle,
    !   the long-range component is strong and inevitably causes
    !   an unnatural steric hindrance. In order to solve
    !   this problem, we split the three-body term into two components;
    !   a shortrange term to describe correctly the bond bending
    !   force, and a long-range term to cancel out moderately
    !   the extra two-body interaction at the second nearestneighbor
    !   distances.
    !
    !   There is only the repulsive part of the two-body potential between
    !   two oxygen atoms. Therefore, O-O-O and Si-O-O interaction
    !   are omitted.
    !
    ! Input values
    !   x0           Contains positions in angtroms scaled by 1/box
    !   atype        Parcas atom types
    !   nborlist     Neighborlist
    !   myatoms      Number of atoms in my node, in para_common.f90
    !   np0pairtable Number of atoms in my node + received neighbor atoms
    !   box(3)       Box size (box centered on 0)
    !   pbc(3)       Periodics: if = 1.0d0 periodic
    !   reppotcutin  Cut-off distance for rep. pot.
    !
    ! Output values
    !   xnp          Contains forces in eV/A scaled by 1/box
    !   Epair        V_2 per atom
    !   Ethree       V_3 per atom
    !   wxxi         Virials
    !   wyyi
    !   wzzi
    !-----------------------------------------------.-----------------------------

    subroutine silica_wat_force_rev(x0,atype,xnp,box,pbc,                 &
            nborlist,Epair,Ethree, &
            wxxi,wyyi,wzzi,wxyi,wxzi,wyzi,reppotcutin,calc_vir)

        ! Arguments
        real(kind=DP), contiguous, intent(in) :: x0(:)
        real(kind=DP), contiguous, intent(out) :: xnp(:)
        real(kind=DP), intent(in) :: box(3), pbc(3)
        integer, contiguous, intent(in) ::  nborlist(:)
        integer, contiguous, intent(in) :: atype(:)

        real(kind=DP), contiguous, intent(out) :: Epair(:), Ethree(:)
        real(kind=DP), contiguous, intent(out) :: &
            wxxi(:), wyyi(:), wzzi(:), wxyi(:), wxzi(:), wyzi(:)

        real(kind=DP), intent(in) :: reppotcutin
        logical, intent(in) :: calc_vir

        ! Local variables
        real(kind=DP) :: dx, dy, dz
        real(kind=DP) :: dxij, dyij, dzij
        real(kind=DP) :: dxik, dyik, dzik
        real(kind=DP) :: rijsw, riksw ! atom separationsin ANG

        real(kind=DP) :: box2x, box2y, box2z
        real(kind=DP) :: ph                ! potential
        real(kind=DP) :: dph               ! force
        real(kind=DP) :: gij               ! bond softening value
        real(kind=DP) :: help1, help2

        ! EDIP
        integer       :: nj, nk
        integer       :: neip,indi,indj,indk,typei,nneip,latn,typej, typek, jstart2, jend2, jstart3, jend3
        real(kind=DP) :: pair_cut, rsqr, r, rinv, par_a, par_b, Z, fermi, dfermi, lcos
        real(kind=DP) :: dV2j, dV2ijx, dV2ijy, dV2ijz, fix, fiy, fiz
        real(kind=DP) :: fjx, fjy, fjz
        real(kind=DP) :: fkx, fky, fkz
        integer, save :: NP_MAX = -1  ! currently allocated size of arrays

        integer :: i, j, k, n, n2, n3
        real(real64b) :: t1


        ! Convert from internal units to Angstrom.
        ! Use buf(i) instead of x0(i) in the computations.
        do i = 1, 3*np0pairtable, 3
            buf(i+0) = x0(i+0) * box(1)
            buf(i+1) = x0(i+1) * box(2)
            buf(i+2) = x0(i+2) * box(3)
        end do


        !-----------------------------------------------------------------------------
        ! Compute the accelerations into xnp in actual units, then convert
        ! to internal units at the end. Compute energies into Epair and Ethree.
        !
        ! Take care to calculate pressure, total pot. only for atoms in my node!
        !
        ! Boundary conditions:
        ! If pcb == 1.0d0, periodic
        ! Note that different conditions may be specified in x, y and z direction.
        !
        ! Loop over third atoms goes from the next atom to the last one, because
        ! every angle between atoms should be calculated only once. Instead,
        ! the coordination number loop goes over the all neighbours of an atom.
        !
        ! Configurations of dimers and trimers are calculated in separate code
        ! blocks to optimize them individually.
        !
        ! In the potential and force calculation Stillinger-Weber length units are
        ! used instead of A, which otherwise is the length unit in this subroutine.
        ! Note that the reppot subroutine does not use SW units.
        !
        ! Some parameters are hard-coded to optimize performance.
        !-----------------------------------------------------------------------------

        ! Initialize force calculation stuff

        box2x = box(1) / 2.0_DP
        box2y = box(2) / 2.0_DP
        box2z = box(3) / 2.0_DP


        xnp(:3*np0pairtable) = 0.0

        Epair(1 : myatoms) = 0.0
        Ethree(1 : myatoms) = 0.0

        wxxi(1 : myatoms) = 0.0
        wyyi(1 : myatoms) = 0.0
        wzzi(1 : myatoms) = 0.0
        if (calc_vir) then
            wxyi(1 : myatoms) = 0.0
            wxzi(1 : myatoms) = 0.0
            wyzi(1 : myatoms) = 0.0
        end if


        ! Allocate or reallocate arrays as necessary when more atoms show up.
        if (NP_MAX < np0pairtable) then
            if (allocated(num2_index)) then
                deallocate(num2_index, s2_dx, s2_dy, s2_dz, s2_ph, s2_dph, s2_r)
                deallocate(num2_natoms, sz_Z)
                deallocate(num3_natoms, num3_index, s3_dx, s3_dy, s3_dz, s3_r)
            end if

            NP_MAX = int(np0pairtable*1.05)
            n = max_neib * NP_MAX
            allocate(num2_natoms(NP_MAX), num3_natoms(NP_MAX))
            allocate(num2_index(n), s2_dx(n), s2_dy(n), s2_dz(n), s2_ph(n), s2_dph(n), s2_r(n) )
            allocate(num3_index(n), s3_dx(n), s3_dy(n), s3_dz(n), s3_r(n) )
            allocate(sz_Z(n))
        end if

        ph = 0.0
        dph = 0.0

        sz_Z = 0.0 ! Reset coordination
        n2 = 0
        n3 = 0
        neip=0

        do i = 1, np0pairtable !PRE LOOP OVER local atoms + boarder atoms
            !  --- LEVEL 2:
            ! LOOP PREPASS OVER PAIRS ---
            neip  = neip+1
            indi  = i*3-2
            typei = abs(atype(i))

            nneip = nborlist(neip)
            if (typei /= Otype .and. i > myatoms) then ! We want to calculate coordination only for Oxygen
                neip=neip+nneip
                cycle
            end if

            do latn=1, nneip
                neip=neip+1
                j=nborlist(neip)
                typej = abs(atype(j))
                pair_cut=rcut(typei, typej)            ! maximal cutoff
                par_a   =r02_arr(typei, typej)         ! 2 body  cutoff
                par_b   =maxval(r03_arr(typej,typei,:))
                if(iac(typei,typej)==0) cycle
                if(i == j) cycle

                if(iac(typei,typej)==-1) then
                    call logger("ERROR: IMPOSSIBLE INTERACTION")
                    call my_mpi_abort('INTERACTION -1', int(myproc))
                endif

                indj=j*3-2
                !if (indi==indj) cycle
                dx=buf(indi)-buf(indj)
                if(pbc(1)==1.0d0) then
                    dx = dx - (sign(box2x, dx+box2x) + sign(box2x, dx-box2x))
                end if
                if(abs(dx) >= pair_cut) cycle

                dy = buf(indi+1)-buf(indj+1)
                if(pbc(2)==1.0d0) then
                    dy = dy - (sign(box2y, dy+box2y) + sign(box2y, dy-box2y))
                end if
                if(abs(dy) >= pair_cut) cycle

                dz = buf(indi+2)-buf(indj+2)
                if(pbc(3)==1.0d0) then
                    dz = dz - (sign(box2z, dz+box2z) + sign(box2z, dz-box2z))
                end if
                if(abs(dz) >= pair_cut) cycle

                rsqr = dx*dx + dy*dy + dz*dz
                if(rsqr >= pair_cut**2) cycle

                r = sqrt(rsqr)
                rinv = 1.0_DP/r
                dx = dx * rinv
                dy = dy * rinv
                dz = dz * rinv

                if(iac(typei,typej)==2 .and. i <= myatoms) then
                    call reppot_only(r,ph,dph,typei,typej)
                    ph=ph*0.5_DP
                    dph=dph*0.5_DP
                    Epair(i)=Epair(i)+ph

                    ! dx/dy/dz contain 1/r
                    help1=dx*dph
                    xnp(indi)=xnp(indi)+help1
                    xnp(indj)=xnp(indj)-help1
                    wxxi(i)=wxxi(i)+help1*dx

                    if (calc_vir) then
                        wxyi(i)=wxyi(i)+help1*dy
                        wxzi(i)=wxzi(i)+help1*dz
                    endif

                    help1=dy*dph
                    xnp(indi+1)=xnp(indi+1)+help1
                    xnp(indj+1)=xnp(indj+1)-help1
                    wyyi(i)=wyyi(i)+help1*dy

                    if (calc_vir) then
                        wyzi(i)=wyzi(i)+help1*dz
                    endif

                    help1=dz*dph
                    xnp(indi+2)=xnp(indi+2)+help1
                    xnp(indj+2)=xnp(indj+2)-help1
                    wzzi(i)=wzzi(i)+help1*dz

                    cycle
                endif

                !   PARTS OF THREE-BODY INTERACTION r < rcut;  rcut is always not smaller than par_a
                if (i <= myatoms .and. r < par_b) then
                    !print *, "DEB", n3, j
                    n3 = n3 + 1
                    num3_index(n3) = j
                    s3_dx(n3)  = dx
                    s3_dy(n3)  = dy
                    s3_dz(n3)  = dz
                    s3_r(n3)   = r
                endif

                !   PARTS OF TWO-BODY INTERACTION r<par_a
                if (r < par_a .and. i <= myatoms) then
                    n2 = n2 + 1
                    num2_index(n2) = j
                    if (p_arr(typei,typej) == 4.0_DP .and. q_arr(typei,typej) == 0.0_DP) then
                        call sw_pair_p4q0(r, typei, typej, ph, dph)
                    elseif (p_arr(typei,typej) == 0.0_DP) then
                        call sw_pair_p0q(r, typei, typej, ph, dph)
                    else
                        call sw_pair_pq(r, typei, typej, ph, dph)
                    endif
                    s2_dx(n2)  = dx
                    s2_dy(n2)  = dy
                    s2_dz(n2)  = dz
                    s2_ph(n2)  = ph
                    s2_dph(n2) = dph
                    s2_r(n2)   = r
                endif

                !   COORDINATION AND NEIGHBOR FUNCTION FOR OXIGEN
                if(typei == Otype .and. typej /= Otype ) then
                    if(r .lt. coordmax_arr(typei,typej)) then
                        if(r .lt. coordmin_arr(typei,typej)) then
                            sz_Z(i) = sz_Z(i) + 1.0_DP
                        else
                            sz_Z(i) = sz_Z(i) + f_cut(r,typei,typej)
                        end if
                        !  r < par_C
                    end if
                    !  r < coordmax
                end if
            end do ! end of pre evaluation neighbor loop

            num2_natoms(i)=n2
            num3_natoms(i)=n3
        enddo ! end of pre evaluation loop

        jstart2 = 0
        jstart3 = 0
        do i=1, myatoms
            !  --- LEVEL 2:
            ! LOOP FOR PAIR INTERACTIONS ---
            jend2 = num2_natoms(i)

            if ( jend2 < jstart2 ) cycle

            indi  = i*3-2
            typei = abs(atype(i))

            do nj=jstart2+1, jend2

                !if(iac(typei,typej)==2) cycle
                j = num2_index(nj)
                indj = j*3-2
                typej = abs(atype(j))
                r=s2_r(nj)
                dx=s2_dx(nj); dy=s2_dy(nj); dz=s2_dz(nj)

                !   two-body energy V2(rij,Z)
                if ( typei == typej ) then ! Do not apply soften function if atom types are same
                    ph    = s2_ph(nj)
                    dV2j  = s2_dph(nj)
                elseif (typei == Otype) then ! Apply only if atom is connected with oxygen
                    Z     = sz_Z(i)
                    gij   = soften_oxide(Z, typei, typej)
                    ph    = s2_ph(nj)*gij
                    dV2j  = s2_dph(nj)*gij ! f2'*g + g'*f2
                    if (r >= coordmin_arr(typei,typej) .and. r < coordmax_arr(typei,typej)) then
                        dV2j  = dV2j - s2_ph(nj)*soften_oxide_der(Z, gij, typei, typej)*f_cut_der(r,typei,typej)
                    end if
                elseif ( typej == Otype ) then
                    Z     = sz_Z(j)
                    gij   = soften_oxide(Z, typei, typej)
                    ph    = s2_ph(nj)*gij
                    dV2j  = s2_dph(nj)*gij
                    if (r >= coordmin_arr(typei,typej) .and. r < coordmax_arr(typei,typej)) then
                        dV2j  = dV2j - s2_ph(nj)*soften_oxide_der(Z, gij, typei, typej)*f_cut_der(r,typei,typej)
                    end if
                else
                    ph    =  s2_ph(nj)
                    dV2j  =  s2_dph(nj)
                endif

                if (r < reppotcutin) then
                    fermi=1d0
                    dfermi=1d0
                    help1=0
                    help2=0
                    !WARNING: Parameters for reppot_fermi should be revised!
                    call reppot_fermi(r,help1,help2,14.0d0,1.50d0,typei,typej,fermi,dfermi)
                    dV2j =dV2j *fermi+0.5*help2+dfermi*ph
                    ph=ph*fermi+0.5*help1
                endif

                Epair(i) = Epair(i) + ph

                !   two-body forces
                !   dx/dy/dz contain 1/r
                dV2ijx = dV2j * dx
                dV2ijy = dV2j * dy
                dV2ijz = dV2j * dz
                xnp(indi)   = xnp(indi)   + dV2ijx
                xnp(indi+1) = xnp(indi+1) + dV2ijy
                xnp(indi+2) = xnp(indi+2) + dV2ijz
                xnp(indj)   = xnp(indj)   - dV2ijx
                xnp(indj+1) = xnp(indj+1) - dV2ijy
                xnp(indj+2) = xnp(indj+2) - dV2ijz

                !   dV2/dr contribution to virial
                wxxi(i) = wxxi(i) + r*dV2ijx*dx
                wyyi(i) = wyyi(i) + r*dV2ijy*dy
                wzzi(i) = wzzi(i) + r*dV2ijz*dz
                !              virial = virial - s2_r(nj) * (dV2ijx*s2_dx(nj)+ dV2ijy*s2_dy(nj) + dV2ijz*s2_dz(nj))

                if (calc_vir) then
                    wxyi(i)=wxyi(i) - r*(dV2ijx*dy)
                    wxzi(i)=wxzi(i) - r*(dV2ijx*dz)
                    wyzi(i)=wyzi(i) - r*(dV2ijy*dz)
                endif
            end do

            jstart2 = jend2
            jend3 = num3_natoms(i)
            do nj=jstart3+1, jend3-1!   LEVEL 2: ! FIRST LOOP FOR THREE-BODY INTERACTIONS

                j = num3_index(nj)
                indj=j*3-2
                typej = abs(atype(j))
                dxij=s3_dx(nj)
                dyij=s3_dy(nj)
                dzij=s3_dz(nj)
                rijsw = s3_r(nj)

                do nk=nj+1, jend3 !   LEVEL 3:
                    k = num3_index(nk) !   SECOND LOOP FOR THREE-BODY INTERACTIONS
                    indk=k*3-2
                    typek = abs(atype(k))
                    dxik=s3_dx(nk)
                    dyik=s3_dy(nk)
                    dzik=s3_dz(nk)
                    riksw = s3_r(nk)
                    lcos = (dxij * dxik + dyij* dyik+ dzij* dzik)

                    call sw_three(rijsw, riksw, lcos,&
                        dxij, dyij, dzij, &
                        dxik, dyik, dzik, &
                        typei, typej, typek, &
                        ph, fjx, fkx, fix, &
                        fjy, fky, fiy, &
                        fjz, fkz, fiz)

                    !   three-body energy
                    Ethree(i)=Ethree(i)+ph

                    !   apply radial + angular forces to i, j, k
                    xnp(indj)   = xnp(indj)   + fjx
                    xnp(indj+1) = xnp(indj+1) + fjy
                    xnp(indj+2) = xnp(indj+2) + fjz
                    xnp(indk)   = xnp(indk)   + fkx
                    xnp(indk+1) = xnp(indk+1) + fky
                    xnp(indk+2) = xnp(indk+2) + fkz
                    xnp(indi)   = xnp(indi)   + fix
                    xnp(indi+1) = xnp(indi+1) + fiy
                    xnp(indi+2) = xnp(indi+2) + fiz

                    !   dV3/dR contributions to virial
                    wxxi(i) = wxxi(i) - rijsw*fjx*dxij - riksw*fkx*dxik
                    wyyi(i) = wyyi(i) - rijsw*fjy*dyij - riksw*fky*dyik
                    wzzi(i) = wzzi(i) - rijsw*fjz*dzij - riksw*fkz*dzik

                    if (calc_vir) then
                        wxyi(i)=wxyi(i) - rijsw*(fjx*dyij)
                        wxzi(i)=wxzi(i) - rijsw*(fjx*dzij)
                        wyzi(i)=wyzi(i) - rijsw*(fjy*dzij)

                        wxyi(i)=wxyi(i) - riksw*(fkx*dyik)
                        wxzi(i)=wxzi(i) - riksw*(fkx*dzik)
                        wyzi(i)=wyzi(i) - riksw*(fky*dzik)
                    endif
                end do
            end do
            jstart3 = jend3

        end do  ! End of loop over atoms i


        ! Send back forces to the neighboring nodes.
        if (nprocs > 1) then
            t1 = mpi_wtime()
            ! Since Newton's III law is not used, and all energies are
            ! assigned to atoms i<=myatoms, there is no need to send back
            ! Epair or Ethree values.
            call potential_pass_back_border_atoms(xnp)
            tmr(TMR_SEMICON_COMMS) = tmr(TMR_SEMICON_COMMS) + (mpi_wtime() - t1)
        end if


        ! Convert from eV/A to internal units.
        do i = 1, 3*myatoms, 3
            xnp(i+0) = xnp(i+0) / box(1)
            xnp(i+1) = xnp(i+1) / box(2)
            xnp(i+2) = xnp(i+2) / box(3)
        end do

    end subroutine silica_wat_force_rev


    subroutine sw_pair_p4q0(rijsw, atypi, atypj, ph, dph) ! optimized for p = 4 and q = 0

        real(kind=DP), intent(in) :: rijsw
        real(kind=DP), intent(inout) :: ph, dph
        integer, intent(in) :: atypi, atypj
        real(kind=DP) :: help1, help2, help3, help5, rsigma, sigma_pair

        sigma_pair=sigma_arr(atypi, atypj)
        rsigma=rijsw/sigma_pair
        help1 = rsigma-r02_arr(atypi, atypj)/sigma_pair
        help1 = 1.0_DP/help1
        help2 = A_arr(atypi, atypj)*exp(help1)
        help3 = rsigma*rsigma               ! hard-coded parameter
        help3 = B_arr(atypi, atypj)/(help3*help3)
        help5 = help3-1.0_DP              ! hard-coded parameter
        ph    = help5*help2
        dph   = help2*( help3*4.0_DP/rsigma + help5*help1*help1 )/sigma_pair
        return
    end subroutine

    subroutine sw_pair_p0q(rijsw, atypi, atypj, ph, dph) ! optimized for p = 0 and q /= 0

        real(kind=DP), intent(in) :: rijsw
        real(kind=DP), intent(inout) :: ph, dph
        integer, intent(in) :: atypi, atypj
        real(kind=DP) :: help1, help2, help4, rsigma, rsigma_inv,sigma_pair

        sigma_pair=sigma_arr(atypi, atypj)
        rsigma=rijsw/sigma_pair
        rsigma_inv = 1.0_DP/rsigma
        help1 = rsigma-r02_arr(atypi, atypj)/sigma_pair
        help2 = A_arr(atypi, atypj)*exp(1.0_DP/help1)
        help4 = rsigma_inv**q_arr(atypi, atypj)
        ph = -help4*help2                 ! hard-coded parameter
        help1 = help1*help1
        dph = -help2*help4*( q_arr(atypi, atypj)*rsigma_inv + 1.0_DP/help1 )/sigma_pair ! hard-coded parameter
        return
    end subroutine

    subroutine sw_pair_pq(rijsw, atypi, atypj, ph, dph) ! arbitarry p and q

        real(kind=DP), intent(in) :: rijsw
        real(kind=DP), intent(inout) :: ph, dph
        integer, intent(in) :: atypi, atypj
        real(kind=DP) :: help1, help2, help3, help4, help5, rsigma, rsigma_inv, sigma_pair

        sigma_pair=sigma_arr(atypi, atypj)
        rsigma=rijsw/sigma_pair
        rsigma_inv = 1.0_DP/rsigma
        help1 = rsigma-r02_arr(atypi, atypj)/sigma_pair
        help1 = 1.0_DP/help1
        help2 = A_arr(atypi, atypj)*exp(help1)   ! softening applied
        help3 = B_arr(atypi, atypj)*(rsigma_inv**p_arr(atypi, atypj))
        help4 = rsigma_inv**q_arr(atypi, atypj)
        help5 = help3-help4
        ph = help5*help2
        help1 = help1*help1
        dph = -help2*( help4*q_arr(atypi, atypj)*rsigma_inv - help3*p_arr(atypi, atypj)*rsigma_inv - help5*help1 )/sigma_pair
        return
    end subroutine

    function soften_oxide(zetaj, atypi, atypj)

        real(kind=DP), intent(in) :: zetaj
        integer, intent(in) :: atypi, atypj
        real(kind=DP) :: soften_oxide
        soften_oxide = p5_arr(atypi, atypj)*exp(p8_arr(atypi, atypj)*(zetaj-p9_arr(atypi, atypj))*(zetaj-p9_arr(atypi, atypj)))&
            /(exp((p6_arr(atypi, atypj)-zetaj)*op7_arr(atypi, atypj)) + 1.0_DP)
        return
    end function

    function soften_oxide_der(zetaj, g, atypi, atypj)

        real(kind=DP), intent(in) :: zetaj, g
        integer, intent(in) :: atypi, atypj
        real(kind=DP) :: soften_oxide_der
        soften_oxide_der = op7_arr(atypi, atypj)*g*exp((p6_arr(atypi, atypj)-zetaj)*op7_arr(atypi, atypj))&
            /(exp((p6_arr(atypi, atypj)-zetaj)*op7_arr(atypi, atypj))+1.0_DP) + &
            g*2.0_DP*p8_arr(atypi, atypj)*(zetaj-p9_arr(atypi, atypj))
        return
    end function

    function f_cut(r,atomi,atomj)

        real(kind=DP), intent(in) :: r
        integer, intent(in) :: atomi,atomj
        real(kind=DP) :: f_cut
        real(kind=DP) :: help1
        help1 = (r - coordmin_arr(atomi,atomj))/Dco_arr(atomi,atomj) ! Warning: Oxygen should be atomj
        f_cut = 0.5_DP*( 2.0_DP - help1 + sin(pi*help1)/pi)
        return
    end function f_cut

    function f_cut_der(r,atomi,atomj)

        real(kind=DP), intent(in) :: r
        integer, intent(in) :: atomi,atomj
        real(kind=DP) :: f_cut_der
        real(kind=DP) :: help1
        help1 = (r - coordmin_arr(atomi,atomj))/Dco_arr(atomi,atomj)
        f_cut_der = 0.5_DP/Dco_arr(atomi,atomj)  * ( -1.0_DP + cos(pi*help1) )
        return
    end function f_cut_der

    subroutine sw_three(rijsw, riksw, cosjik,&
            dxij, dyij, dzij, &
            dxik, dyik, dzik, &
            atypi, atypj, atypk, &
            ph, dphjx, dphkx, dphix, &
            dphjy, dphky, dphiy, &
            dphjz, dphkz, dphiz)

        real(kind=DP), intent(in) :: rijsw,riksw,cosjik, dxij, dyij, dzij, dxik, dyik, dzik
        real(kind=DP), intent(out) :: ph, dphjx, dphkx, dphix, dphjy, dphky, dphiy, dphjz, dphkz, dphiz
        real (kind=DP), parameter :: cutofflimit = 0.0_DP
        integer, intent(in) :: atypi, atypj, atypk
        real(kind=DP) :: diff1ij, diff1ik, dcos1, dcos1sq, gamij1, gamik1
        real(kind=DP) :: HCOS
        real(kind=DP) :: dexp_drij, dexp_drik, dcos_rij1, dcos_rik1, dcos_rik2, dcos_rij2

        ph=0.0_DP
        dphjx=0.0_DP; dphkx=0.0_DP; dphix=0.0_DP;
        dphjy=0.0_DP; dphky=0.0_DP; dphiy=0.0_DP;
        dphjz=0.0_DP; dphkz=0.0_DP; dphiz=0.0_DP;

        if (lambda_arr(atypj,atypi,atypk) == 0.0_DP) return

        dcos1 = cosjik - costheta0_arr(atypj,atypi,atypk)
        if ( abs(dcos1) < cutofflimit ) return

        diff1ij = rijsw - r03_arr(atypj,atypi,atypk)
        if ( diff1ij >= cutofflimit ) return

        diff1ik = riksw - r03_arr(atypk,atypi,atypj)
        if ( diff1ik >= cutofflimit ) return

        dcos1sq = dcos1*dcos1

        ! Only the short range 3-body interaction
        diff1ij = 1.0_DP/diff1ij
        diff1ik = 1.0_DP/diff1ik

        gamij1  = gamma_arr(atypj,atypi,atypk)*diff1ij
        gamik1  = gamma_arr(atypk,atypi,atypj)*diff1ik

        ph = lambda_arr(atypj,atypi,atypk)*exp(gamij1 + gamik1)*dcos1sq

        dexp_drij = (gamij1*diff1ij)*ph     !!! cos^2*exp'
        dexp_drik = (gamik1*diff1ik)*ph     !!! cos^2*exp'
        HCOS  = 2.0_DP*ph/dcos1
        dcos_rij1 = HCOS/rijsw              !!! cos^2'*exp
        dcos_rik1 = HCOS/riksw              !!! cos^2'*exp
        dcos_rij2 = dcos_rij1*cosjik
        dcos_rik2 = dcos_rik1*cosjik

        dphjx=-dexp_drij*dxij+dcos_rij1*dxik-dcos_rij2*dxij
        dphkx=-dexp_drik*dxik+dcos_rik1*dxij-dcos_rik2*dxik
        dphix=-dphjx-dphkx

        dphjy=-dexp_drij*dyij+dcos_rij1*dyik-dcos_rij2*dyij
        dphky=-dexp_drik*dyik+dcos_rik1*dyij-dcos_rik2*dyik
        dphiy=-dphjy-dphky

        dphjz=-dexp_drij*dzij+dcos_rij1*dzik-dcos_rij2*dzij
        dphkz=-dexp_drik*dzik+dcos_rik1*dzij-dcos_rik2*dzik
        dphiz=-dphjz-dphkz
        return

    end subroutine  sw_three

    !-----------------------------------------------------------------------------
    ! map_silica_arrays - Initialization routine for the module
    !
    ! Output values
    ! Assigns values to the following tables:
    !
    ! Array silica_wat_types maps the Parcas atom types given in md.in file
    ! to the local type indeces hard-coded in this module.
    !
    ! Array rcut contains the cut-off distances for each pair of atom types.
    !
    !-----------------------------------------------.-----------------------------

    subroutine map_silica_arrays()

        integer :: i, j, k

        !TWo-body arrays
        allocate(A_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(B_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(p_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(q_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(r02_arr(itypelow:itypehigh,itypelow:itypehigh))

        !Coordination
        allocate(coordmin_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(coordmax_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(Dco_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(p5_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(p6_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(op7_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(p8_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(p9_arr(itypelow:itypehigh,itypelow:itypehigh))

        !Three-body arrays
        allocate(sigma_arr(itypelow:itypehigh,itypelow:itypehigh))
        allocate(lambda_arr(itypelow:itypehigh,itypelow:itypehigh,itypelow:itypehigh))
        allocate(gamma_arr(itypelow:itypehigh,itypelow:itypehigh,itypelow:itypehigh))
        allocate(r03_arr(itypelow:itypehigh,itypelow:itypehigh,itypelow:itypehigh))
        allocate(costheta0_arr(itypelow:itypehigh,itypelow:itypehigh,itypelow:itypehigh))

        A_arr=0.0_DP; B_arr=0.0_DP; p_arr=0.0_DP; q_arr=0.0_DP; r02_arr=0.0_DP; sigma_arr=0.0_DP
        lambda_arr=0.0_DP; gamma_arr=0.0_DP; r03_arr=0.0_DP; costheta0_arr=0.0_DP
        coordmin_arr =0.0_DP; coordmax_arr =0.0_DP; Dco_arr=0.0_DP;
        p5_arr =0.0_DP; p6_arr =0.0_DP; op7_arr =0.0_DP; p8_arr =0.0_DP; p9_arr =0.0_DP;

        do i = itypelow,itypehigh
            do j = itypelow,itypehigh !Pair interactions are symmetric!
                if ( element(i) == 'Si' .and. element(j) == 'Si') then      !Si-Si
                    A_arr(i,j)   = A_SiSi;   A_arr(j,i)=A_arr(i,j)
                    B_arr(i,j)   = B_SiSi;   B_arr(j,i)=B_arr(i,j)
                    p_arr(i,j)   = p_SiSi;   p_arr(j,i)=p_arr(i,j)
                    q_arr(i,j)   = q_SiSi;   q_arr(j,i)=q_arr(i,j)
                    r02_arr(i,j) = r0_SiSi; r02_arr(j,i)=r02_arr(i,j)
                    sigma_arr(i,j) = swsigma_Si; sigma_arr(j,i)=sigma_arr(i,j)
                    do k = itypelow,itypehigh
                        if (element(k) == 'Si') then
                            lambda_arr(j,i,k)= lambda_SiSiSi; lambda_arr(k,i,j)=lambda_arr(j,i,k)
                            gamma_arr(j,i,k) = gamma_SiSiSi;   gamma_arr(k,i,j)= gamma_arr(j,i,k)
                            r03_arr(j,i,k)   = r0_SiSiSi;        r03_arr(k,i,j)=   r03_arr(j,i,k)
                            costheta0_arr(j,i,k)   = costheta0_SiSiSi; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        elseif (element(k) == 'O') then
                            lambda_arr(j,i,k) = lambda_SiSiO; lambda_arr(k,i,j)=lambda_arr(j,i,k)
                            gamma_arr(j,i,k) = gamma_SiSi_SiSiO
                            gamma_arr(k,i,j) = gamma_SiO_SiSiO
                            r03_arr(j,i,k)   = r0_SiSi_SiSiO;
                            r03_arr(k,i,j)   = r0_SiO_SiSiO;
                            costheta0_arr(j,i,k)   = costheta0_SiSiO; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        elseif (element(k) == 'Ge') then ! Si-Si-Ge
                            lambda_arr(j,i,k) = lambda_SiSiGe; lambda_arr(k,i,j) = lambda_arr(j,i,k)
                            gamma_arr(j,i,k)  = gamma_SiSi_SiSiGe;
                            gamma_arr(k,i,j)  = gamma_SiGe_SiSiGe;
                            r03_arr(j,i,k)   = r0_SiSi_SiSiGe;
                            r03_arr(k,i,j)   = r0_SiGe_SiSiGe;
                            costheta0_arr(j,i,k)   = costheta0_SiSiGe; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        endif
                    end do
                else if ( element(i) == 'Si' .and. element(j) == 'O') then  !Si-O
                    A_arr(i,j)   = A_SiO;    A_arr(j,i)=A_arr(i,j)
                    B_arr(i,j)   = B_SiO;    B_arr(j,i)=B_arr(i,j)
                    p_arr(i,j)   = p_SiO;    p_arr(j,i)=p_arr(i,j)
                    q_arr(i,j)   = q_SiO;    q_arr(j,i)=q_arr(i,j)
                    r02_arr(i,j) = r0_SiO;  r02_arr(j,i)=r02_arr(i,j)
                    sigma_arr(i,j) = swsigma_Si; sigma_arr(j,i)=sigma_arr(i,j)
                    coordmin_arr(i,j) = (Rco-Dco)*sigma_arr(i,i); coordmin_arr(j,i)=coordmin_arr(i,j)
                    coordmax_arr(i,j) = (Rco+Dco)*sigma_arr(i,i); coordmax_arr(j,i)=coordmax_arr(i,j)
                    Dco_arr(i,j) = (Rco+Dco)*sigma_arr(i,i); Dco_arr(j,i)=Dco_arr(i,j)
                    p5_arr(i,j) =  p5_Si; p5_arr(j,i) =  p5_arr(i,j)
                    p6_arr(i,j) =  p6_Si; p6_arr(j,i) =  p6_arr(i,j)
                    op7_arr(i,j)= op7_Si; op7_arr(j,i)=  op7_arr(i,j)
                    p8_arr(i,j) =  p8_Si; p8_arr(j,i) =  p8_arr(i,j)
                    p9_arr(i,j) =  p9_Si; p9_arr(j,i) =  p9_arr(i,j)
                    do k = itypelow,itypehigh
                        if (element(k) == 'Si') then ! O-Si-Si
                            ! nothing to do here; same as Si-Si-O
                        elseif (element(k) == 'O') then ! O-Si-O
                            lambda_arr(j,i,k) = lambda_OSiO; lambda_arr(k,i,j)=lambda_arr(j,i,k)
                            gamma_arr(j,i,k)  = gamma_OSiO;   gamma_arr(k,i,j)= gamma_arr(j,i,k)
                            r03_arr(j,i,k)   = r0_OSiO; r03_arr(k,i,j) = r03_arr(j,i,k)
                            costheta0_arr(j,i,k)   = costheta0_OSiO; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        elseif (element(k) == 'Ge') then ! O-Si-Ge
                            lambda_arr(j,i,k) =  lambda_OSiGe; lambda_arr(k,i,j) = lambda_arr(j,i,k)
                            gamma_arr(j,i,k)  = gamma_SiO_OSiGe;
                            gamma_arr(k,i,j)  = gamma_SiGe_OSiGe;
                            r03_arr(j,i,k)    = r0_SiO_OSiGe;
                            r03_arr(k,i,j)    = r0_SiGe_OSiGe;
                            costheta0_arr(j,i,k)   = costheta0_OSiGe; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        endif
                    end do
                else if ( element(i) == 'O' .and. element(j) == 'Si') then  !O-Si
                    do k = itypelow,itypehigh
                        if (element(k) == 'Si') then ! Si-O-Si
                            lambda_arr(j,i,k) = lambda_SiOSi; lambda_arr(k,i,j)=lambda_arr(j,i,k)
                            gamma_arr(j,i,k)  = gamma_SiOSi;   gamma_arr(k,i,j)= gamma_arr(j,i,k)
                            r03_arr(j,i,k)   = r0_SiOSi;        r03_arr(k,i,j) = r03_arr(j,i,k)
                            costheta0_arr(j,i,k)   = costheta0_SiOSi; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        elseif (element(k) == 'O') then ! Si-O-O
                            ! Si-O-O does not exist
                        elseif (element(k) == 'Ge') then ! Si-O-Ge
                            lambda_arr(j,i,k) = lambda_SiOGe; lambda_arr(k,i,j) = lambda_arr(j,i,k)
                            gamma_arr(j,i,k)  = gamma_OSi_SiOGe;
                            gamma_arr(k,i,j)  = gamma_OGe_SiOGe;
                            r03_arr(j,i,k)    = r0_OSi_SiOGe;
                            r03_arr(k,i,j)    = r0_OGe_SiOGe;
                            costheta0_arr(j,i,k)   = costheta0_SiOGe; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        endif
                    end do
                else if ( element(i) == 'O' .and. element(j) == 'O') then   !O-O
                    A_arr(i,j)   = A_OO;    A_arr(j,i)=A_arr(i,j)
                    B_arr(i,j)   = B_OO;    B_arr(j,i)=B_arr(i,j)
                    p_arr(i,j)   = p_OO;    p_arr(j,i)=p_arr(i,j)
                    q_arr(i,j)   = q_OO;    q_arr(j,i)=q_arr(i,j)
                    r02_arr(i,j) = r0_OO;  r02_arr(j,i)=r02_arr(i,j)
                    sigma_arr(i,j) = swsigma_Si; sigma_arr(j,i)=sigma_arr(i,j)
                    ! O-O-Si O-O-O O-O-Ge do not exist
                else if ( element(i) == 'Ge' .and. element(j) == 'Ge') then !Ge-Ge
                    A_arr(i,j)   = A_GeGe;    A_arr(j,i)=A_arr(i,j)
                    B_arr(i,j)   = B_GeGe;    B_arr(j,i)=B_arr(i,j)
                    p_arr(i,j)   = p_GeGe;    p_arr(j,i)=p_arr(i,j)
                    q_arr(i,j)   = q_GeGe;    q_arr(j,i)=q_arr(i,j)
                    r02_arr(i,j) = r0_GeGe;  r02_arr(j,i)=r02_arr(i,j)
                    sigma_arr(i,j) = swsigma_Ge; sigma_arr(j,i)=sigma_arr(i,j)
                    do k = itypelow,itypehigh
                        if (element(k) == 'Si') then ! Ge-Ge-Si
                            lambda_arr(j,i,k)= lambda_GeGeSi; lambda_arr(k,i,j)=lambda_arr(j,i,k)
                            gamma_arr(j,i,k) = gamma_GeGe_GeGeSi  ! Ge-Ge-Si
                            gamma_arr(k,i,j) = gamma_GeSi_GeGeSi  ! Si-Ge-Ge
                            r03_arr(j,i,k)   = r0_GeGe_GeGeSi
                            r03_arr(k,i,j)   = r0_GeSi_GeGeSi
                            costheta0_arr(j,i,k)   = costheta0_GeGeSi; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        elseif (element(k) == 'O') then ! Ge-Ge-O
                            lambda_arr(j,i,k) = lambda_GeGeO; lambda_arr(k,i,j)=lambda_arr(j,i,k)
                            gamma_arr(j,i,k) = gamma_GeGe_GeGeO
                            gamma_arr(k,i,j) = gamma_GeO_GeGeO
                            r03_arr(j,i,k)   = r0_GeGe_GeGeO;
                            r03_arr(k,i,j)   = r0_GeO_GeGeO;
                            costheta0_arr(j,i,k)   = costheta0_GeGeO; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        elseif (element(k) == 'Ge') then ! Ge-Ge-Ge
                            lambda_arr(j,i,k) = lambda_GeGeGe; lambda_arr(k,i,j) = lambda_arr(j,i,k)
                            gamma_arr(j,i,k)  = gamma_GeGeGe;   gamma_arr(k,i,j) = gamma_arr(j,i,k)
                            r03_arr(j,i,k)   = r0_GeGeGe; r03_arr(k,i,j) = r03_arr(j,i,k)
                            costheta0_arr(j,i,k)   = costheta0_GeGeGe; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        endif
                    end do

                else if ( element(i) == 'Ge' .and. element(j) == 'Si') then !Ge-Si
                    A_arr(i,j)   = A_GeSi;    A_arr(j,i)=A_arr(i,j)
                    B_arr(i,j)   = B_GeSi;    B_arr(j,i)=B_arr(i,j)
                    p_arr(i,j)   = p_GeSi;    p_arr(j,i)=p_arr(i,j)
                    q_arr(i,j)   = q_GeSi;    q_arr(j,i)=q_arr(i,j)
                    r02_arr(i,j) = r0_GeSi;  r02_arr(j,i)=r02_arr(i,j)
                    sigma_arr(i,j) = swsigma_SiGe; sigma_arr(j,i)=sigma_arr(i,j)
                    do k = itypelow,itypehigh
                        if (element(k) == 'Si') then ! Si-Ge-Si
                            lambda_arr(j,i,k)= lambda_SiGeSi; lambda_arr(k,i,j)=lambda_arr(j,i,k)
                            gamma_arr(j,i,k) = gamma_SiGeSi ;  gamma_arr(k,i,j)= gamma_arr(j,i,k)
                            r03_arr(j,i,k)   = r0_SiGeSi    ;    r03_arr(k,i,j)=   r03_arr(j,i,k)
                            costheta0_arr(j,i,k)   = costheta0_SiGeSi; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        elseif (element(k) == 'O') then ! Si-Ge-O
                            lambda_arr(j,i,k) = lambda_SiGeO; lambda_arr(k,i,j)=lambda_arr(j,i,k)
                            gamma_arr(j,i,k)  = gamma_GeSi_SiGeO
                            gamma_arr(k,i,j)  = gamma_GeO_SiGeO
                            r03_arr(j,i,k)    = r0_GeSi_SiGeO
                            r03_arr(k,i,j)    = r0_GeO_SiGeO
                            costheta0_arr(j,i,k)   = costheta0_SiSiO; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        elseif (element(k) == 'Ge') then ! Si-Ge-Ge
                            ! nothing to do here; same as Ge-Ge-Si
                        endif
                    end do
                else if ( element(i) == 'Si' .and. element(j) == 'Ge') then !Si-Ge
                    do k = itypelow,itypehigh
                        if (element(k) == 'Si') then ! Ge-Si-Si
                            !Same as Si-Si-Ge
                        elseif (element(k) == 'O') then ! Ge-Si-O
                            !Same as O-Si-Ge
                        elseif (element(k) == 'Ge') then ! Ge-Si-Ge
                            lambda_arr(j,i,k) = lambda_GeSiGe; lambda_arr(k,i,j)=lambda_arr(j,i,k)
                            gamma_arr(j,i,k) = gamma_GeSiGe;  gamma_arr(k,i,j) =gamma_arr(j,i,k)
                            r03_arr(j,i,k)   = r0_GeSiGe;      r03_arr(k,i,j)   =r03_arr(j,i,k)
                            costheta0_arr(j,i,k)   = costheta0_GeSiGe; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        endif
                    end do
                else if ( element(i) == 'Ge' .and. element(j) == 'O') then  !Ge-O
                    A_arr(i,j)   = A_GeO;    A_arr(j,i)=A_arr(i,j)
                    B_arr(i,j)   = B_GeO;    B_arr(j,i)=B_arr(i,j)
                    p_arr(i,j)   = p_GeO;    p_arr(j,i)=p_arr(i,j)
                    q_arr(i,j)   = q_GeO;    q_arr(j,i)=q_arr(i,j)
                    r02_arr(i,j) = r0_GeO;  r02_arr(j,i)=r02_arr(i,j)
                    sigma_arr(i,j) = swsigma_Ge; sigma_arr(j,i)=sigma_arr(i,j)
                    coordmin_arr(i,j) = (Rco-Dco)*sigma_arr(i,i); coordmin_arr(j,i)=coordmin_arr(i,j)
                    coordmax_arr(i,j) = (Rco+Dco)*sigma_arr(i,i); coordmax_arr(j,i)=coordmax_arr(i,j)
                    Dco_arr(i,j) = (Rco+Dco)*sigma_arr(i,i); Dco_arr(j,i)=Dco_arr(i,j)
                    p5_arr(i,j) =  p5_Ge; p5_arr(j,i) =  p5_arr(i,j)
                    p6_arr(i,j) =  p6_Ge; p6_arr(j,i) =  p6_arr(i,j)
                    op7_arr(i,j)= op7_Ge; op7_arr(j,i)=  op7_arr(i,j)
                    p8_arr(i,j) =  p8_Ge; p8_arr(j,i) =  p8_arr(i,j)
                    p9_arr(i,j) =  p9_Ge; p9_arr(j,i) =  p9_arr(i,j)
                    do k = itypelow,itypehigh
                        if (element(k) == 'Si') then ! O-Ge-Si
                            !  same as Si-Ge-O
                        elseif (element(k) == 'O') then ! O-Ge-O
                            ! TBD
                            lambda_arr(j,i,k) = lambda_OGeO; lambda_arr(k,i,j)=lambda_arr(j,i,k)
                            gamma_arr(j,i,k) = gamma_OGeO
                            gamma_arr(k,i,j) = gamma_OGeO
                            r03_arr(j,i,k)   = r0_OGeO
                            r03_arr(k,i,j)   = r0_OGeO
                            costheta0_arr(j,i,k)   = costheta0_OGeO; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        elseif (element(k) == 'Ge') then ! O-Ge-Ge
                            ! nothing to do here; same as Ge-Ge-O
                        endif
                    end do
                else if ( element(i) == 'O' .and. element(j) == 'Ge') then  !O-Ge
                    do k = itypelow,itypehigh
                        if (element(k) == 'Si') then ! Ge-O-Si
                            ! same as Si-O-Ge
                        elseif (element(k) == 'O') then ! Ge-O-O
                            ! does not exist
                        elseif (element(k) == 'Ge') then ! Ge-O-Ge
                            !TBD
                            lambda_arr(j,i,k) = lambda_GeOGe; lambda_arr(k,i,j) = lambda_arr(j,i,k)
                            gamma_arr(j,i,k)  = gamma_GeOGe
                            gamma_arr(k,i,j)  = gamma_GeOGe
                            r03_arr(j,i,k)    = r0_GeOGe
                            r03_arr(k,i,j)    = r0_GeOGe
                            costheta0_arr(j,i,k)   = costheta0_GeOGe; costheta0_arr(k,i,j)=costheta0_arr(j,i,k)
                        endif
                    end do
                end if
            enddo
        enddo

        do i = itypelow,itypehigh
            do j = itypelow,itypehigh
                rcut(i,j) = MAX(r02_arr(i,j), maxval(r03_arr(j,i,:)), coordmax_arr(i,j)) !=3.77118_DP

                ! Should not be used for Si/Ge/O, but necessary for external pair potentials.
                ! Maybe should have some sort of default.
                if (rcutin(i,j) >= 0.0_DP) then
                    rcut(i,j) = rcutin(i,j)
                end if
            enddo
        enddo

        do i = itypelow,itypehigh  ! FIND WHO IS OXYGEN
            if ( element(i) == 'O' ) Otype=i
        end do

        IF(debug .and. iprint) then
            PRINT *,'Printed Watanabe two-body parameters for LATEX'
            call print_pair_table()
            PRINT *,'Printed Watanabe three-body parameters for LATEX'
            call print_three_table()
        END IF

    end subroutine map_silica_arrays

    subroutine print_pair_table() ! prints pair parameters in LATEX table format, ready for a publication

        integer :: j, k
        !CHARACTER(LEN=*), PARAMETER  :: Part1 = "(8X,"
        !CHARACTER(LEN=*), PARAMETER  :: Part2 = "('&',1X,A2,'--',A2,5X),'\\')"
        CHARACTER(LEN=*), PARAMETER  :: Part3 = "(1X,'&',F12.4),'\\')"
        CHARACTER(LEN=1) :: Repetition

        WRITE(Repetition,"(I1)")  SIZE(element)*2 ! number of pairs
        print "(A6)", "\hline"
        ! The following line works only with intel compiler
        !print Part1 // Repetition // Part2, (((element(j), element(k)), k=j, itypehigh), j=itypelow, itypehigh)
        print "(A)", "\hline"
        print "(A," // Repetition // Part3, "A, eV", (((2.0*A_arr(j,k)), k=j, itypehigh), j=itypelow, itypehigh)
        print "(A," // Repetition // Part3, "B", (((B_arr(j,k)), k=j, itypehigh), j=itypelow, itypehigh)
        print "(A," // Repetition // Part3, "p", (((p_arr(j,k)), k=j, itypehigh), j=itypelow, itypehigh)
        print "(A," // Repetition // Part3, "q", (((q_arr(j,k)), k=j, itypehigh), j=itypelow, itypehigh)
        print "(A," // Repetition // Part3, "a$_2$, \AA", (((r02_arr(j,k)), k=j, itypehigh), j=itypelow, itypehigh)
        print "(A," // Repetition // Part3, "$\sigma$, \AA", (((sigma_arr(j,k)), k=j, itypehigh), j=itypelow, itypehigh)
        print "(A6)", "\hline"
    end subroutine print_pair_table

    subroutine print_three_table() ! prints three body parameters in LATEX table format, ready for a publication

        integer :: i, j, k
        !CHARACTER(LEN=*), PARAMETER  :: Part2 = "('&',1X,A2,'/ \textbackslash ',A2,5X),'\\')"
        CHARACTER(LEN=*), PARAMETER  :: Part3 = "(1X,'&',F12.4),'\\')"
        CHARACTER(LEN=*), PARAMETER  :: Part4 = "('&',1X,A2,8X),'\\')"
        CHARACTER(LEN=1) :: Repetition

        WRITE(Repetition,"(I1)")  SIZE(element)*2 ! number of pairs
        do i=itypelow, itypehigh
            print "(A,A2,A)", "%TABLE FOR --", element(i), "-- triplets"
            print "(A)", "\hline"
            print "(A," // Repetition // Part4, "I", (element(i), j=1,SIZE(element)*2)
            ! This line works only with intel compiler
            !print "(A," // Repetition // Part2, 'J/ \textbackslash K', &
            !    (((element(j), element(k)), k=j, itypehigh), j=itypelow, itypehigh)
            print "(A)", "\hline"
            print "(A," // Repetition // Part3, "$\lambda$, eV", (((lambda_arr(j,i,k)),k=j, itypehigh), j=itypelow, itypehigh)
            print "(A," // Repetition // Part3, "$\gamma^{IJ}$, \AA", (((gamma_arr(j,i,k)), k=j, itypehigh), j=itypelow, itypehigh)
            print "(A," // Repetition // Part3, "$\gamma^{IK}$, \AA", (((gamma_arr(k,i,j)), k=j, itypehigh), j=itypelow, itypehigh)
            print "(A," // Repetition // Part3, "a$_3^{IJ}$, \AA", (((r03_arr(j,i,k)),   k=j, itypehigh), j=itypelow, itypehigh)
            print "(A," // Repetition // Part3, "a$_3^{IK}$, \AA", (((r03_arr(k,i,j)),   k=j, itypehigh), j=itypelow, itypehigh)
            print "(A," // Repetition // Part3, "cos($\theta_0$)",(((costheta0_arr(j,i,k)), k=j, itypehigh), j=itypelow, itypehigh)
            print "(A)", "\hline"
        end do
    end subroutine print_three_table
end module silica_wat_mod_rev
