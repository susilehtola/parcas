!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

!
! This file contains subroutines and types for communication with other
! nodes/processes. The three large areas are:
!  1. Initializing the node grid and partitioning the simulation box.
!  2. Shuffle: Moving atoms from one node to another when they cross
!     the node's boundaries.
!  3. Atom data passing: Utilities for passing atom data, e.g.
!     positions, energies, forces, to neighboring processes.
!

!
! Node division
! =============
!
! The terms "process" and "node" are used interchangeably.
!
! Processes are divided in all three dimensions x, y and z into a
! Cartesian grid. The number of processes per dimension is given by
! the array nnodes(3). E.g. there are nnodes(1) nodes in the x-direction.
!
! Each node has an MPI rank from 0 to nprocs-1. Each node also has a
! position in x, y, and z direction, called dimrank. This ranges from
! 0 to nnodes(i)-1 for each dimension i=1,2,3.
!
! The dimrank of a node determines which physical x, y, and z range of
! the simulation box it deals with, as well as which processes are its
! neighbors. The higher a node's dimrank in a direction, the higher the
! atom coordinates in its portion of the simulation box are for that
! dimension.
!
! The edges of the nodes' regions are saved in node_edges(j,i).
! i ranges from 1 to 3, i.e. it is the Cartesian dimension x, y or z.
! j ranges from 0 (lower edge) to nnodes(i) (upper edge). For a node
! with dimrank(i) == n, the lower edge is at node_edges(n,i) and the
! upper edge at node_edges(n+1,i). The lower and upperedges of this node
! are saved in rmn(1:3) and rmx(1:3), respectively.
!
! For convenience, the rmn and rmx values of the nearest neighbors in
! each dimension (taking into account periodic boundary conditions)
! are saved in neighbor_rmn(dimen,dir) and neighbor_rmx(dimen,dir).
!

!
! Communicating with neighboring nodes: for users
! ===============================================
!
! First, from the point of view of someone wanting to use the provided
! subroutines and utilities: There are two common cases for communicating
! with the neighboring nodes.
!
! 1. I need data on the atoms that are near the node borders, e.g. their
!    positions and types. The data should be appended onto the local data
!    arrays (e.g. x0, atypes), such that the arrays contain
!        |my atoms  |atoms from x-|atoms from x+|atoms from y-|...     |
!         ^        ^ ^             ^             ^                    ^
!        |1  myatoms|f(1,1)       |f(1,2)       |f(2,1)       |...   np|
!    where important indexes are marked, f = firstrecvatoms and
!    np = np0pairtable. Then I can treat the border atoms from the other
!    nodes as my own, since they are also included in the neighbor list.
!
! 2. I have previously acquired data on the border atoms from neighboring
!    nodes, and now I have something to send back to the atoms' actual
!    owning nodes, e.g. force and energy contributions.
!
! These two communication patterns can be implemented using the two
! subroutines (1) pass_border_atoms and (2) pass_back_border_atoms.
! The subroutines take care of the sending/receiving of data, and of
! the communication pattern required.
!
! What the user must provide for these routines is a way to pack the
! necessary data into a single buffer for sending, and unpack it into
! the local arrays after receiving. This is done by providing an object
! that (1) has pointers to the relevant arrays [and optionally includes
! other data], and (2) has two procedures do_pack and do_unpack that do
! exactly what their names say.
!
! More technically, provide an object of a type like
!    type, extends(PassPacker) :: MyPassPacker
!        ! Pointers here, e.g.
!        real(real64b), pointer, contiguous :: x0(:)
!    contains
!        procedure :: do_pack => my_pack_subroutine
!        procedure :: do_unpack => my_unpack_subroutine
!    end type
! and of course also provide the subroutines.
! See the documentation for the types and interfaces below.
!
! The implementation for normal potentials is given below in the form of
! the two subroutines
!     potential_pass_border_atoms(x0, atype)
!     potential_pass_back_border_atoms(xnp, [Epair], &
!                   [wxxi, wyyi, wzzi], [wxyi, wxzi, wyzi])
! where [] indicate optional parameters. All arrays in a single [] must
! be given together, i.e. either all diagonal virials or none of them.
!

!
! Communicating with neighboring nodes: the implementation
! ========================================================
!
! (The Shuffle and neighbor list communication is slighly different.)
!
! Second, from the point of view of someone trying to understand the
! implementation of the provided communication.
!
! In a 3D division of nodes, each node has 26 neighbors, including the
! diagonal ones. This is too much for communication to be efficient.
! This is why the communication is done using an algorithm that only
! requires communicating with the six direct neighbors.
!
! The algorithm is explained below using a 2D grid, but it generalizes
! to N dimensions in a simple manner. The neighbors are named A to H.
! See the diagram below.
!
!     |                |
!  A  |        D       |  F
! --------------------------
!     |                |
!     |                |
!  B  |    my node     |  G
!     |                |
!     |                |
! --------------------------
!  C  |       E        |  H
!     |                |
!
! Communication is done one dimension at a time, first x, then y, then z.
! For each dimension, communication is first done with the neighbor in
! negative direction, then with the neighbor in the positive dimension.
! For each neighbor, all border atoms that are in the local atom arrays
! are checked whether they in the border region for that direction.
! This includes atoms previously received from neighbors.
!
! Border regions start inside my node, at a distance determined by the
! maximum force cutoff from the edge of my node. The border region goes
! on into infinity in the direction of the neighbor that the atoms will
! be sent into. Border regions are defined separately for each dimension,
! so e.g. the z border region can contain atoms that are outside my node
! in x or y direction.
!
! So in the example above, my node sends and receives in the following
! order:
! 1. x- Send to B, receive from G.
!    My atom arrays: | my atoms | G |
! 2. x+ Send to G, receive from B.
!    My atom arrays: | my atoms | G | B |
! 3. y- Send to E, receive from D.
!    My atom arrays: | my atoms | G | B | D, F, A |
! 4. y+ Send to D, receive from E.
!    My atom arrays: | my atoms | G | B | D, F, A | E, H, C |
! 5., 6. similar for z dimension.

! Since D and E have already communicated with A and F or C and H,
! respectively, we automatically get those atoms without having to
! communicate with the corner nodes ourself.
!
! When passing back data on the atoms that were received from the
! neighbor nodes, the order of sending/receiving is reversed, so
! for the 2D example above:
! 1., 2. similar for z dimension.
! 3. y+ Send back to E, receive back from D.
! 4. y- Send back to D, receive back from E.
! 5. x+ Send back to B, receive back from G.
! 6. x+ Send back to G, receive back from B.
!

module mdparsubs_mod

    use output_logger, only: &
        log_buf, logger, logger_write, &
        logger_clear_buffer, logger_append_buffer

    use datatypes, only: real64b
    use defs, only: NPMAX
    use my_mpi
    use casc_common

    use timers, only: &
        tmr, TMR_SHUFFLE_PRE, &
        TMR_SHUFFLE_MAIN, TMR_SHUFFLE_POST, &
        TMR_SHUFFLE_ALL_TO_ALL

    use para_common, only: &
        iprint, debug, &
        myatoms, &
        nnodes, myproc, dimrank, &
        node_edges, rmn, rmx, &
        neighbor_rmn, neighbor_rmx, &
        sendproc, recvproc, &
        needsend, needrecv, &
        numrecvatoms, numsendatoms, &
        firstrecvatom, &
        buf, buf2, &
        ibuf, ibuf2, &
        sendatomslist


    implicit none

    private
    public :: init_node_grid
    public :: print_node_configuration
    public :: divide_simulation_box
    public :: rank_from_dimrank
    public :: Rectangle
    public :: ShuffleData
    public :: Shuffle
    public :: Shuffle_all_to_all
    public :: pass_border_atoms
    public :: pass_back_border_atoms
    public :: PassPacker
    public :: PassBackPacker
    public :: potential_pass_border_atoms
    public :: potential_pass_back_border_atoms


    !
    ! A type that knows how to pack atom data into a single buffer
    ! for passing to neighbor processes. Used in pass_border_atoms.
    ! See the interfaces below for the procedures.
    ! Extend this type for each place that needs to pass different
    ! atom data to neighboring nodes.
    !
    type, abstract :: PassPacker
    contains
        procedure(pass_pack_sub), deferred :: do_pack
        procedure(pass_unpack_sub), deferred :: do_unpack
    end type

    !
    ! A type that knows how to pack atom data into a single buffer
    ! for passing back to the neighbor processes they came from.
    ! Used in pass_back_border_atoms.
    ! See the interfaces below for the procedures.
    ! Extend this type for each place that needs to pass back
    ! different atom data to the neighboring nodes it came from.
    !
    type, abstract :: PassBackPacker
    contains
        procedure(pass_back_pack_sub), deferred :: do_pack
        procedure(pass_bacK_unpack_sub), deferred :: do_unpack
    end type

    !
    ! Subroutine interface for PassPacker.
    ! Go through the atoms indexes (i.e. indexes into the local arrays)
    ! in sendlist, and for each one, write the relevant data into the
    ! buffer. The data can be included as pointers in the Packer type.
    ! Return the number of reals written in filled_size.
    !
    interface
        subroutine pass_pack_sub(this, sendlist, buffer, filled_size)
            import :: real64b, PassPacker
            class(PassPacker), intent(inout) :: this
            integer, intent(in) :: sendlist(:)
            real(real64b), contiguous, intent(out) :: buffer(:)
            integer, intent(out) :: filled_size
        end subroutine pass_pack_sub
    end interface

    !
    ! Subroutine for PassPacker.
    ! For each of the num_atoms atoms starting at the index first_atom,
    ! read the data from the given buffer and write it into the local
    ! atom arrays, which can be included as pointers in the Packer type.
    !
    interface
        subroutine pass_unpack_sub(this, first_atom, num_atoms, buffer)
            import :: real64b, PassPacker
            class(PassPacker), intent(inout) :: this
            integer, intent(in) :: first_atom
            integer, intent(in) :: num_atoms
            real(real64b), contiguous, intent(in) :: buffer(:)
        end subroutine pass_unpack_sub
    end interface

    !
    ! Subroutine for PassBackPacker.
    ! For each of the num_atoms atoms starting at the index first_atom
    ! (each originally received from a neighbor process), copy the
    ! relevant data from the local atom arrays into the given buffer.
    ! The atom arrays can be included as pointers in the Packer type.
    ! Return the number of reals written in filled_size.
    !
    interface
        subroutine pass_back_pack_sub(this, first_atom, num_atoms, buffer, filled_size)
            import :: real64b, PassBackPacker
            class(PassBackPacker), intent(inout) :: this
            integer, intent(in) :: first_atom
            integer, intent(in) :: num_atoms
            real(real64b), contiguous, intent(out) :: buffer(:)
            integer, intent(out) :: filled_size
        end subroutine pass_back_pack_sub
    end interface

    !
    ! Subroutine for PassBackPacker.
    ! For each of the atom indexes to local arrays in recvlist
    ! (each originally received from a neighbor process), copy the
    ! relevant data from the buffer into the local atom arrays.
    ! The atom arrays can be included as pointers in the Packer type.
    !
    interface
        subroutine pass_back_unpack_sub(this, recvlist, buffer)
            import :: real64b, PassBackPacker
            class(PassBackPacker), intent(inout) :: this
            integer, intent(in) :: recvlist(:)
            real(real64b), contiguous, intent(in) :: buffer(:)
        end subroutine pass_back_unpack_sub
    end interface


    ! Implementations of PassPacker and PassBackPacker for the normal
    ! communication for potentials. When not needed, Epair and the
    ! virials can be not associated (nullified).
    type, extends(PassPacker) :: PotentialPassPacker
        real(real64b), pointer, contiguous :: x0(:)
        integer, pointer, contiguous :: atype(:)
    contains
        procedure :: do_pack => potential_pass_pack
        procedure :: do_unpack => potential_pass_unpack
    end type

    type, extends(PassBackPacker) :: PotentialPassBackPacker
        real(real64b), pointer, contiguous :: xnp(:)
        real(real64b), pointer, contiguous :: Epair(:)
        real(real64b), pointer, contiguous :: wxxi(:), wyyi(:), wzzi(:)
        real(real64b), pointer, contiguous :: wxyi(:), wxzi(:), wyzi(:)
    contains
        procedure :: do_pack => potential_pass_back_pack
        procedure :: do_unpack => potential_pass_back_unpack
    end type


    ! Type containing pointers to all data that is Shuffled.
    ! This is completely separate from the Pass/PassBack stuff above.
    ! All arrays included here must be defined with the "target"
    ! attribute in mdx.f90.
    type :: ShuffleData
        real(real64b), pointer, contiguous :: x0(:), x1(:), x2(:), x3(:), x4(:), x5(:)
        real(real64b), pointer, contiguous :: x0nei(:), xnp(:)
        integer, pointer, contiguous :: atype(:), atomindex(:)

        real(real64b), pointer, contiguous :: &
            wxxiavg(:) => null(), wyyiavg(:) => null(), wzziavg(:) => null(), &
            wxyiavg(:) => null(), wxziavg(:) => null(), wyziavg(:) => null()

        ! Number of integers and reals actually present, which
        ! excludes missing optional arrays.
        integer :: numint, numreal
    end type


contains

    !
    ! Initialize the node grid, and everything that is required for
    ! sending data to neighboring nodes.
    !
    subroutine init_node_grid(nnodes, pbc)

        integer, intent(in) :: nnodes(3)
        real(real64b), intent(in) :: pbc(3)

        integer :: i
        integer :: dimen, dir
        integer :: tmpdimrank(3)

        ! Compute this node's own dimrank.
        dimrank(:) = dimrank_from_rank(myproc, nnodes)

        ! Compute ranks of the neighboring nodes.
        do dimen = 1, 3
            tmpdimrank(:) = dimrank(:)

            do dir = 1, 2
                tmpdimrank(dimen) = dimrank(dimen) + merge(-1, 1, dir == 1)
                call wrap_periodic_dimrank(tmpdimrank, nnodes)
                sendproc(dimen,dir) = rank_from_dimrank(tmpdimrank, nnodes)
            end do
        end do
        recvproc(:, 1) = sendproc(:, 2)
        recvproc(:, 2) = sendproc(:, 1)

        ! Check whether there is reason to send and receive in the given
        ! direction based on dimrank and periodicity.
        do dimen = 1, 3
            if (nnodes(dimen) == 1) then
                needsend(dimen, :) = .false.
                needrecv(dimen, :) = .false.
            else if (pbc(dimen) == 1d0) then
                needsend(dimen, :) = .true.
                needrecv(dimen, :) = .true.
            else
                if (dimrank(dimen) == 0) then
                    needsend(dimen, 1) = .false.
                    needrecv(dimen, 1) = .true.
                    needsend(dimen, 2) = .true.
                    needrecv(dimen, 2) = .false.
                else if (dimrank(dimen) == nnodes(dimen) - 1) then
                    needsend(dimen, 1) = .true.
                    needrecv(dimen, 1) = .false.
                    needsend(dimen, 2) = .false.
                    needrecv(dimen, 2) = .true.
                else
                    needsend(dimen, :) = .true.
                    needrecv(dimen, :) = .true.
                end if
            end if
        end do

        ! Compute the locations of all the nodes' edges in all dimensions.
        do dimen = 1, 3
            do i = 0, nnodes(dimen)
                node_edges(i, dimen) = real(i, real64b) / nnodes(dimen) - 0.5d0
            end do
        end do

        call update_edges(pbc)

    end subroutine init_node_grid


    !
    ! After changing node_edges, this needs to be called.
    ! It updates convenience variables such as rmn, rmx, ...,
    ! and takes care of periodic boundaries in node_edges.
    !
    subroutine update_edges(pbc)

        real(real64b), intent(in) :: pbc(3)

        character(1), parameter :: dimnames(3) = ["x", "y", "z"]

        integer :: dimen
        integer :: left_neigh, right_neigh

        do dimen = 1, 3
            ! For non-periodic boundaries, stretch the first and last nodes
            ! to basically infinity, so that atoms do not get lost.
            if (pbc(dimen) /= 1d0) then
                node_edges(0, dimen) = -9.999d32
                node_edges(nnodes(dimen), dimen) = 9.999d32
            end if

            ! Copy this node's edges into rmn and rmx for easier use.
            rmn(dimen) = node_edges(dimrank(dimen), dimen)
            rmx(dimen) = node_edges(dimrank(dimen) + 1, dimen)

            ! Copy edges of neighbor nodes to avoid having to check for
            ! edge cases and periodic boundaries later on.
            left_neigh = dimrank(dimen) - 1
            right_neigh = dimrank(dimen) + 1
            if (dimrank(dimen) == 0) then
                if (pbc(dimen) == 1d0) then
                    left_neigh = nnodes(dimen) - 1
                else
                    left_neigh = 0
                end if
            end if
            if (dimrank(dimen) == nnodes(dimen) - 1) then
                if (pbc(dimen) == 1d0) then
                    right_neigh = 0
                else
                    right_neigh = nnodes(dimen) - 1
                end if
            end if
            neighbor_rmn(dimen, 1) = node_edges(left_neigh, dimen)
            neighbor_rmx(dimen, 1) = node_edges(left_neigh + 1, dimen)
            neighbor_rmn(dimen, 2) = node_edges(right_neigh, dimen)
            neighbor_rmx(dimen, 2) = node_edges(right_neigh + 1, dimen)
        end do

        if (debug) then
            if (iprint) then
                call logger("Node configuration:")
                call logger("-------------------")
            end if
            call logger_clear_buffer()
            call logger_append_buffer("Node rank: ", myproc, 0)
            call logger_append_buffer("dimrank:", dimrank, 4)
            call logger_append_buffer("rmn, rmx:", 4)
            do dimen = 1, 3
                call logger_append_buffer(dimnames(dimen), rmn(dimen), rmx(dimen), 8)
            end do
            call logger_append_buffer("sendproc:", 4)
            do dimen = 1, 3
                call logger_append_buffer(dimnames(dimen), sendproc(dimen, 1), sendproc(dimen, 2), 8)
            end do
            call logger_append_buffer("recvproc:", 4)
            do dimen = 1, 3
                call logger_append_buffer(dimnames(dimen), recvproc(dimen, 1), recvproc(dimen, 2), 8)
            end do
            call logger_append_buffer("needsend:", 4)
            do dimen = 1, 3
                call logger_append_buffer(dimnames(dimen), needsend(dimen, 1), needsend(dimen, 2), 8)
            end do
            call logger_append_buffer("needrecv:", 4)
            do dimen = 1, 3
                call logger_append_buffer(dimnames(dimen), needrecv(dimen, 1), needrecv(dimen, 2), 8)
            end do
            call logger_append_buffer("neighbor_rmn:", 4)
            do dimen = 1, 3
                call logger_append_buffer(dimnames(dimen), neighbor_rmn(dimen, 1), neighbor_rmn(dimen, 2), 8)
            end do
            call logger_append_buffer("neighbor_rmx:", 4)
            do dimen = 1, 3
                call logger_append_buffer(dimnames(dimen), neighbor_rmx(dimen, 1), neighbor_rmx(dimen, 2), 8)
            end do
            call logger_write(trim(log_buf), ordered=.true.)
        end if

    end subroutine update_edges


    subroutine print_node_configuration()

        call my_mpi_barrier()

        if (iprint) then
            write(log_buf, '(I0,A,I0,A,I0)') nnodes(1),' x ',nnodes(2),' x ',nnodes(3)
            call logger("Processor layout:", trim(log_buf), 2)
            call logger("-----------------", 2)
        end if

        write (log_buf, "(2X,I4,6(1X,A,1X,G12.4),1X,I6,A)") &
            myproc, &
            "x =", rmn(1), "to", rmx(1), &
            "y =", rmn(2), "to", rmx(2), &
            "z =", rmn(3), "to", rmx(3), &
            myatoms, " atoms" //  new_line('x')

        call logger_write(trim(log_buf), ordered=.true.)

        if (iprint) call logger()

    end subroutine print_node_configuration


    !
    ! Redivide the simulation box into node cells based on the weight
    ! of the nodes. Currently the weight of a node is simply the number
    ! of atoms in that node. This recomputes node_edges, and calls
    ! update_edges.
    !
    ! Redivision is only done if the weight distribution is imbalanced
    ! enough. Whether or not it was done is returned in "done".
    !
    ! The redivision starts from the current division and improves it
    ! iteratively, so a grid division must have been done by the time
    ! this is called.
    !
    subroutine divide_simulation_box(x0, box, pbc, rcut, done)

        real(real64b), intent(in), contiguous :: x0(:)
        real(real64b), intent(in) :: box(3), pbc(3)
        real(real64b), intent(in) :: rcut
        logical, intent(out) :: done

        real(real64b), parameter :: MAX_IMBALANCE = 1.05d0
        real(real64b), parameter :: TARGET_IMBALANCE = 1.005d0
        integer, parameter :: MAX_ITERATIONS = 100

        integer :: i, i3, node
        integer :: dimen, iter
        integer :: atom_dimrank(3)
        real(real64b) :: start_imbalance
        real(real64b) :: imbalance
        real(real64b) :: min_size(3)
        real(real64b) :: max_change
        real(real64b) :: width
        logical :: is_changed

        real(real64b), allocatable, save :: weight_sums(:,:)
        real(real64b), allocatable, save :: temp_node_edges(:,:)

        ! Check whether rebalancing is necessary.
        start_imbalance = compute_imbalance(node_edges, x0)

        if (start_imbalance <= MAX_IMBALANCE) then
            if (iprint) call logger("Not redividing node grid. Imbalance factor", start_imbalance, 0)
            done = .false.
            return
        end if

        if (iprint) call logger("Redividing node grid. Imbalance factor", start_imbalance, 0)


        ! The minimum size of the node's box is such that there is no
        ! way for an atom to have contact with atoms more than one node
        ! away, i.e. using the neighbor cutoff.
        min_size(:) = rcut / box(:)
        if (iprint) call logger("Min. node size:", min_size, 0)


        ! Use a temporary copy of node_edges, in case we want to use the
        ! old edges. That can happen if the redivision algorithm fails for
        ! some reason.
        if (.not. allocated(weight_sums)) then
            allocate(weight_sums(0:maxval(nnodes), 3))
            allocate(temp_node_edges(0:ubound(node_edges, dim=1), 3))
        end if
        temp_node_edges = node_edges

        ! Erase non-periodicity for now. It will be brought back later, but
        ! it interferes with the computations below.
        do dimen = 1, 3
            temp_node_edges(0, dimen) = -0.5d0
            temp_node_edges(nnodes(dimen), dimen) = 0.5d0
        end do


        ! Initialize an array with the summed weights for each row, column
        ! and stack, i.e. nnodes(i)+1 values for each dimension i, including
        ! a zero at index 0. This is used to optimize the node edges below.
        weight_sums(:,:) = 0d0
        do i = 1, myatoms
            i3 = 3 * i - 3
            atom_dimrank = dimrank_from_x0(x0(i3+1:i3+3), node_edges)
            do dimen = 1, 3
                node = atom_dimrank(dimen) + 1
                weight_sums(node, dimen) = weight_sums(node, dimen) + 1d0
            end do
        end do
        do dimen = 1, 3
            do i = 1, nnodes(dimen)
                weight_sums(i, dimen) = weight_sums(i, dimen) + weight_sums(i-1, dimen)
            end do
        end do
        call MPI_Allreduce(MPI_IN_PLACE, weight_sums, size(weight_sums), &
            MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, i)


        ! Iteratively improve the node division.
        ! In each iteration, move each node edge once, then
        ! check whether the imbalance is sufficiently small.
        imbalance = start_imbalance
        do iter = 1, MAX_ITERATIONS
            is_changed = .false.

            do dimen = 1, 3
                call divide_1d(dimen, temp_node_edges(:, dimen), &
                    weight_sums(:, dimen), x0, max_change)

                is_changed = is_changed .or. (max_change > 1d-4)
            end do
            if (.not. is_changed) exit

            imbalance = compute_imbalance(temp_node_edges, x0)
            if (debug .and. iprint) then
                write(log_buf, *) "Imbalance: ", iter, imbalance
                call logger(log_buf)
            end if

            if (imbalance <= TARGET_IMBALANCE) exit
        end do

        done = .true.


        ! Check the whether the produced node grid is sane.
        do dimen = 1, 3
            do i = 1, nnodes(dimen)
                width = temp_node_edges(i, dimen) - temp_node_edges(i-1, dimen)
                if (width <= min_size(dimen)) then
                    if (iprint) call logger("Node redivision failed with overlapping nodes.")
                    done = .false.
                    exit
                end if
            end do
            if (.not. done) exit
        end do

        ! Check whether the produced node grid is better than the uniform one.
        if (imbalance >= start_imbalance) then
            if (iprint .and. imbalance > start_imbalance) then
                call logger("Node redivision produced worse imbalance", imbalance, 0)
            end if
            done = .false.
        end if


        if (done) then
            node_edges = temp_node_edges

            ! Finish up by updating rmn/rmx and related values.
            call update_edges(pbc)
        else
            if (iprint) call logger("Using original node grid.")
            imbalance = start_imbalance
        end if

        if (iprint) call logger("Done rebalancing node grid. Imbalance factor:", imbalance, 0)

    end subroutine divide_simulation_box


    !
    ! Helper for divide_simulation_box.
    ! Compute the imbalance factor using the atom positions.
    ! imbalance = (max node weight) / (average node weight)
    !
    function compute_imbalance(node_edges, x0) result(imbalance)

        real(real64b), intent(in) :: node_edges(0:, :)
        real(real64b), intent(in), contiguous :: x0(:)
        real(real64b) :: imbalance

        real(real64b) :: node_weights(0 : nprocs-1)
        integer :: i, i3, node

        node_weights(:) = 0d0

        do i = 1, myatoms
            i3 = 3 * i - 3
            node = rank_from_x0(x0(i3+1:i3+3), node_edges)
            node_weights(node) = node_weights(node) + 1d0
        end do

        call my_mpi_sum(node_weights, nprocs)

        if (iprint .and. debug) then
            call logger("min/max/avg weight", &
                minval(node_weights), &
                maxval(node_weights), &
                sum(node_weights) / nprocs, 0)
        end if

        imbalance = maxval(node_weights) / (sum(node_weights) / nprocs)

    end function compute_imbalance


    !
    ! Helper for divide_simulation_box.
    ! Compute new node edges in the given dimension as perturbations
    ! from the current edges, based on the weights of the nodes.
    ! Makes sure that weight_sums stays up to date.
    ! Returns the maximum change of an edge in max_change.
    !
    subroutine divide_1d(dimen, node_edges, weight_sums, x0, max_change)

        integer, intent(in) :: dimen
        real(real64b), intent(inout) :: node_edges(0:)
        real(real64b), intent(inout) :: weight_sums(0:)
        real(real64b), intent(in) :: x0(:)
        real(real64b), intent(out) :: max_change

        integer :: node, i
        real(real64b) :: weight_left
        real(real64b) :: weight_right
        real(real64b) :: width_left
        real(real64b) :: width_right
        real(real64b) :: delta
        real(real64b) :: new_edge


        max_change = 0d0

        ! Shift each of the interior edges. The first and last edge are
        ! constant at +/- 0.5 (internal units), i.e. the edges of the box.
        do node = 1, nnodes(dimen) - 1
            weight_left  = weight_sums(node) - weight_sums(node - 1)
            weight_right = weight_sums(node + 1) - weight_sums(node)

            width_left  = node_edges(node) - node_edges(node - 1)
            width_right = node_edges(node + 1) - node_edges(node)

            ! Assuming that the weight is distributed uniformly inside each
            ! node, compute the change in the edge's position that is required
            ! to even out the difference. This should reach the desired value
            ! in a few iteration except for pathological distributions.
            if (weight_left == weight_right) then
                delta = 0d0
            else if (weight_left < weight_right) then
                delta = 0.5d0 * width_right * (1d0 - weight_left / weight_right)
            else
                delta = -0.5d0 * width_left * (1d0 - weight_right / weight_left)
            end if

            max_change = max(max_change, abs(delta))

            new_edge = node_edges(node) + delta
            node_edges(node) = new_edge

            ! Update the weight sum array to take into account the new
            ! edge location.
            weight_sums(node) = 0d0
            do i = dimen, 3*myatoms, 3
                if (x0(i) <= new_edge) then
                    weight_sums(node) = weight_sums(node) + 1d0
                end if
            end do
            call my_mpi_sum(weight_sums(node), 1)
        end do

    end subroutine divide_1d


    !
    ! Convert from dimrank to MPI rank.
    !
    pure integer function rank_from_dimrank(dimrank, nnodes) result(rank)
        integer, intent(in) :: dimrank(3), nnodes(3)

        rank = 0
        rank = rank + dimrank(1)
        rank = rank + dimrank(2) * nnodes(1)
        rank = rank + dimrank(3) * nnodes(1) * nnodes(2)
    end function rank_from_dimrank


    !
    ! Convert from MPI rank to dimrank.
    !
    pure function dimrank_from_rank(rank, nnodes) result(dimrank)
        integer, intent(in) :: rank, nnodes(3)
        integer :: dimrank(3)

        dimrank(1) = mod(rank, nnodes(1))
        dimrank(2) = mod(rank / nnodes(1), nnodes(2))
        dimrank(3) = mod(rank / (nnodes(1) * nnodes(2)), nnodes(3))
    end function dimrank_from_rank


    !
    ! Wraps the given dimrank around periodically, such that
    ! -1 becomes nnodes(i)-1 and nnodes(i) becomes 0.
    !
    pure subroutine wrap_periodic_dimrank(dimrank, nnodes)
        integer, intent(inout) :: dimrank(3)
        integer, intent(in) :: nnodes(3)
        integer :: i

        do i = 1, 3
            if (dimrank(i) == -1) then
                dimrank(i) = nnodes(i) - 1
            else if (dimrank(i) == nnodes(i)) then
                dimrank(i) = 0
            end if
        end do
    end subroutine wrap_periodic_dimrank


    !
    ! Return the dimrank of f the node in which the position belongs.
    !
    function dimrank_from_x0(x0, node_edges) result(dimrank)
        real(real64b), intent(in) :: x0(3)
        real(real64b), intent(in) :: node_edges(0:, :)
        integer :: dimrank(3)

        integer :: lo, hi, mid
        integer :: dimen

        ! Do a binary search in all three dimensions.
        ! This works even if the size of each node is not uniform.

        do dimen = 1, 3
            lo = 0
            hi = nnodes(dimen)
            do while(hi - lo > 1)
                mid = (hi + lo) / 2
                if (node_edges(mid, dimen) > x0(dimen)) then
                    hi = mid
                else
                    lo = mid
                end if
            end do
            dimrank(dimen) = lo
        end do
    end function dimrank_from_x0


    !
    ! Return the MPI rank of the node in which the position belongs.
    !
    function rank_from_x0(x0, node_edges) result(rank)
        real(real64b), intent(in) :: x0(3)
        real(real64b), intent(in) :: node_edges(0:, :)
        integer :: rank

        integer :: dimrank(3)

        dimrank = dimrank_from_x0(x0, node_edges)
        rank = rank_from_dimrank(dimrank, nnodes)
    end function rank_from_x0


    !
    ! Find a Cartesian arrangement of nodes that best fits
    ! the shape of the simulation box in all three dimensions.
    ! Fills in nnodes(:).
    !
    subroutine Rectangle(box)
        real(real64b), intent(in) :: box(3)

        real(real64b) :: score, best_score
        integer :: xnodes, ynodes, znodes

        if (nprocs == 1) then
            nnodes(:) = 1
            return
        end if

        best_score = -1d0

        do znodes = 1, nprocs
            ! If not evenly divisible by znodes, this cannot work.
            if (mod(nprocs, znodes) /= 0) cycle

            call Rectangle_xy(box, znodes, xnodes, ynodes, score)

            ! If no combination was found, try next zprocs.
            if (xnodes < 0) cycle

            if (score > best_score) then
                best_score = score
                nnodes(1) = xnodes
                nnodes(2) = ynodes
                nnodes(3) = znodes
            end if
        end do

        if (best_score < 0d0) then
            call logger("No suitable procs division found, stopping!")
            call my_mpi_abort("Proc division error", nprocs)
        end if

    end subroutine Rectangle


    subroutine Rectangle_xy(box, znodes, best_xnodes, best_ynodes, best_score)

        real(real64b), intent(in) :: box(3)
        integer, intent(in) :: znodes
        integer, intent(out) :: best_xnodes, best_ynodes
        real(real64b), intent(out) :: best_score

        real(real64b) :: xsize, ysize, zsize
        real(real64b) :: area, score
        integer :: xnodes, ynodes, totnodes

        best_xnodes = -1
        best_ynodes = -1
        best_score = -1d0

        ! Go through combinations of xnodes and ynodes, with each between
        ! one and available procs. Start ynodes high and xnodes low. When
        ! one has gone through all of its possible values, we can stop.
        ynodes = nprocs / znodes
        xnodes = 1

        do while (xnodes <= nprocs / znodes .and. ynodes > 0)
            ! The total number of nodes, from the current attempt.
            ! This must be equal to nprocs for ths combination to be good.
            totnodes = xnodes * ynodes * znodes

            if (totnodes > nprocs) then
                ynodes = ynodes - 1
                cycle
            end if

            if (totnodes < nprocs) then
                xnodes = xnodes + 1
                cycle
            end if

            ! Here totnodes == nprocs, so we can check whether the
            ! ratio is better than the previously found ones.

            ! TODO check whether this is the best criterion.
            ! Try to minimize the "surface" area of the process' cell cuboid,
            ! since that determines the amount of communicated atoms.
            xsize = box(1) / xnodes
            ysize = box(2) / ynodes
            zsize = box(3) / znodes

            area = 2d0 * (ysize * zsize + xsize * zsize + xsize * ysize)
            score = 1d0 / area

            if (debug .and. iprint) then
                write(*, '(A,3(I5,1X),G8.3)') &
                    "Rectangle x y z score ", xnodes, ynodes, znodes, score
            end if

            if (score > best_score) then
                best_xnodes = xnodes
                best_ynodes = ynodes
                best_score = score
            end if

            xnodes = xnodes + 1
        end do

    end subroutine Rectangle_xy


    !
    ! Move atoms to correct nodes based on their positions.
    ! This only communicates with neighbor nodes, unless
    ! communication with other nodes is required. In that case
    ! it calls Shuffle_all_to_all.
    !
    ! For all three dimensions, goes through all atoms. Packs
    ! atoms that are not inside this node into buffers and removes
    ! them from the atom arrays by moving the last atom on top
    ! of the removed atom.
    !
    ! Doing this one dimension at a time means that communication
    ! is only required with the direct neighbors, not the corner
    ! neighbors. Atoms that are in a corner neighbor's cell are
    ! first send in one direction, then in the other, and end up in
    ! the correct location.
    !
    ! In other words, after checking the x direction, all atoms are
    ! in the correct nodes based on x, but may be wrong based on y
    ! or z. After checking the y direction, only z may be wrong.
    ! Finally after the third dimension is gone through, all atoms
    ! must be correct.
    !
    ! While doing all this, keep track of where the recoil atom is.
    !
    ! Note that firstrecvatoms, numrecvatoms, and so on cannot be
    ! used here, since this is generating the prerequisites for
    ! computing those in the first place.
    !
    subroutine Shuffle(sdata, natoms, pbc, printnow)

        type(ShuffleData), intent(inout) :: sdata

        integer, intent(in) :: natoms
        real(real64b), intent(in) :: pbc(3)
        logical, intent(in) :: printnow

        integer :: i, i3
        integer :: old_irecproc
        integer :: dimen, dir
        integer :: npacked, nrecv
        integer :: ntotpacked, ntotrecv
        integer :: num_outside_neighbors
        integer :: irec_pack_index
        integer :: tag
        integer :: requests(2)
        integer :: stat(MPI_STATUS_SIZE)
        integer :: ierror
        real(real64b) :: x(3)
        real(real64b) :: time1


        time1 = mpi_wtime()
        call pre_shuffle_checks(sdata%x0, pbc, natoms)
        tmr(TMR_SHUFFLE_PRE) = tmr(TMR_SHUFFLE_PRE) + (mpi_wtime() - time1)


        time1 = mpi_wtime()

        ntotpacked = 0
        ntotrecv = 0
        num_outside_neighbors = 0

        ! Make sure only one process thinks it has the recoil.
        ! Then the process with irecproc >= 0 has it.
        old_irecproc = irecproc
        if (myproc /= irecproc) irecproc = -1

        dimen_loop: do dimen = 1, 3
            ! If there is only one process in this dimension, there is
            ! no-one to send atoms to. If they are out of bounds, they
            ! will be caught at the sanity check later on.
            if (nnodes(dimen) == 1) cycle

            ! Find all atoms that outside this node's cell.
            ! Check only this dimension!
            npacked = 0
            irec_pack_index = -1

            ! Handle loop variable manually since it goes back one
            ! when removing an atom.
            i = 1
            pack_loop: do while (i <= myatoms)
                i3 = 3*i - 3

                ! Non-periodic edges are taken into account in rmn/rmx.
                if (sdata%x0(i3+dimen) >= rmn(dimen) .and. &
                    sdata%x0(i3+dimen) <  rmx(dimen)) then
                    i = i + 1
                    cycle pack_loop
                end if

                ! The atom is outside this node. If it is not in a neighbor
                ! node, leave it for now. This can happen e.g. if the atom
                ! is outside timesputlim and is fast enough to go through one
                ! whole node's area in one timestep, or if the adaptive time
                ! step is disabled.
                if (.not. any(sdata%x0(i3+dimen) >= neighbor_rmn(dimen, :) .and. &
                              sdata%x0(i3+dimen) <  neighbor_rmx(dimen, :))) then
                    i = i + 1
                    num_outside_neighbors = num_outside_neighbors + 1

                    cycle pack_loop
                end if

                ! The atom is in the target node. Pack it into buf/ibuf, then
                ! copy the last atom on top of it to remove it.

                if (debug) print *, "Packing atom", myproc, dimen, &
                    myatoms, i, sdata%atomindex(i), sdata%x0(i3+1:i3+3)

                npacked = npacked + 1

                ! If packing the recoil, mark it and set irecproc so that we
                ! know we don't have the recoil anymore.
                if (myproc == irecproc .and. irec == i) then
                    if (debug) print *, "Packed the recoil", myproc, dimen, irec, npacked
                    irec_pack_index = npacked
                    irecproc = -1
                end if

                call shuffle_packatom(i, npacked, sdata, buf, ibuf)

                call shuffle_copyatom(myatoms, i, sdata)

                if (myproc == irecproc .and. irec == myatoms) then
                    if (debug) print *, "Moved recoil from ", irec, " to ", i, " on node", myproc
                    irec = i
                end if

                myatoms = myatoms - 1

                ! If reaching here, run again with the same i.
            end do pack_loop

            ntotpacked = ntotpacked + npacked

            ! Pack the index at which the recoil is located, if it is
            ! in this batch, or else -1.
            ibuf(npacked * sdata%numint + 1) = irec_pack_index

            ! Now the bad atoms are removed from the atom arrays, and are
            ! packed into buf and ibuf. Send them in both directions,
            ! but only if there are enough nodes in this dimension.
            ! Accept atoms that are inside this node's boundaries, but
            ! only check this dimension. Atoms that are wrong in other
            ! dimensions are sent away later.

            dir_loop: do dir = 1, 2
                tag = 2 * dimen + dir

                ! If there are only two processes in this dimension, only one
                ! communication round is needed, since the buffers contain atoms
                ! going in both directions (left and right).
                if (nnodes(dimen) == 2 .and. dir == 2) exit dir_loop

                call MPI_Irecv(buf2, size(buf2), mpi_double_precision, &
                    recvproc(dimen, dir), tag, MPI_COMM_WORLD, requests(1), ierror)

                call MPI_Irecv(ibuf2, size(ibuf2), my_mpi_integer, &
                    recvproc(dimen, dir), tag + 1024, MPI_COMM_WORLD, requests(2), ierror)

                call MPI_Send(buf, sdata%numreal*npacked, mpi_double_precision, &
                    sendproc(dimen, dir), tag, MPI_COMM_WORLD, ierror)

                call MPI_Send(ibuf, sdata%numint*npacked+1, my_mpi_integer, &
                    sendproc(dimen, dir), tag + 1024, MPI_COMM_WORLD, ierror)

                call MPI_Wait(requests(1), stat, ierror)
                call MPI_Get_count(stat, mpi_double_precision, nrecv, ierror)
                nrecv = nrecv / sdata%numreal ! atoms, not reals
                call MPI_Wait(requests(2), MPI_STATUS_IGNORE, ierror)

                ! If the recoil is in this batch, this is its index, else -1.
                irec_pack_index = ibuf2(nrecv * sdata%numint + 1)

                ntotrecv = ntotrecv + nrecv

                do i = 1, nrecv
                    i3 = (i - 1) * sdata%numreal
                    x(:) = buf2(i3+1 : i3+3)

                    if (x(dimen) >= rmn(dimen) .and. &
                        x(dimen) <  rmx(dimen)) then
                        ! Accept the atom. If other dimensions are outside this box,
                        ! it will be sent away later.
                        myatoms = myatoms + 1

                        call shuffle_unpackatom(myatoms, i, sdata, buf2, ibuf2)

                        if (debug) print *, "Received atom", myproc, dimen, dir, &
                            i, myatoms, sdata%atomindex(myatoms), x(:)

                        ! Check whether this is the recoil atom.
                        if (i == irec_pack_index) then
                            if (debug) print *, "Unpacked the recoil", myproc, dimen, dir, i, myatoms
                            irec = myatoms
                            irecproc = myproc
                        end if
                    end if
                end do
            end do dir_loop
        end do dimen_loop


        ! Tell everyone where the recoil is.
        ! Only the node with the recoil has irecproc >= 0.
        if (old_irecproc >= 0) then
            i = irecproc
            call my_mpi_max(irecproc, 1)
            if (i >= 0 .and. i /= irecproc) then
                call my_mpi_abort("BUG: More than one irecproc after Shuffle", i)
            end if
            if (irecproc < 0) call my_mpi_abort("BUG: Lost the recoil in Shuffle", 0)

            call my_mpi_bcast(irec, 1, irecproc)

            if (myproc == irecproc .and. sdata%atomindex(irec) /= iatrec) then
                call my_mpi_abort("BUG: Got wrong iatrec after Shuffle", sdata%atomindex(irec))
            end if

            if (iprint .and. irecproc /= old_irecproc) then
                write(log_buf,"(2(A,I5),I9)") "Shuffled recoil from", &
                    old_irecproc, ' to ', irecproc, irec
                call logger(log_buf)
            end if
        end if


        call my_mpi_sum(num_outside_neighbors, 1)
        tmr(TMR_SHUFFLE_MAIN) = tmr(TMR_SHUFFLE_MAIN) + (mpi_wtime() - time1)

        if (num_outside_neighbors > 0) then
            if (iprint) then
                call logger_clear_buffer()
                call logger_append_buffer("WARNING: An atom moved through at least one whole")
                call logger_append_buffer("processor's area between neighbor list rebuilds!!!!")
                call logger_append_buffer("Calling Shuffle_all_to_all from Shuffle!")
                call logger(log_buf)
            end if

            call Shuffle_all_to_all(sdata, natoms, pbc)
        end if


        time1 = mpi_wtime()
        call post_shuffle_checks(sdata%x0, sdata%atomindex, pbc, natoms, &
            printnow, ntotpacked, ntotrecv)
        tmr(TMR_SHUFFLE_POST) = tmr(TMR_SHUFFLE_POST) + (mpi_wtime() - time1)

    end subroutine Shuffle


    !
    ! Move atoms to correct nodes based on their positions.
    ! This communicates with all nodes, so only use it where
    ! necessary.
    !
    ! First, go through all atoms and mark save which node they
    ! belong in, and the number of atoms to send to each node.
    !
    ! Second, pack all atoms that do not belong in this node into
    ! the buffers buf and ibuf. Compute the locations in the send
    ! and receive (buf2, ibuf2) buffers that belong to each node.
    !
    ! Third, trade buffers with all nodes using MPI_Alltoallv,
    ! which does the hard part of figuring out a good communication
    ! strategy.
    !
    ! Fourth, unpack the received atoms onto the end of the local
    ! atom arrays (x0 and so on).
    !
    ! While doing all this, keep track of where the recoil atom is.
    !
    ! Note that firstrecvatoms, numrecvatoms, and so on cannot be
    ! used here, since this is generating the prerequisites for
    ! computing those in the first place.
    !
    subroutine Shuffle_all_to_all(sdata, natoms, pbc)

        type(ShuffleData), intent(inout) :: sdata

        integer, intent(in) :: natoms
        real(real64b), intent(in) :: pbc(3)

        integer :: i
        integer :: node
        integer :: npacked, nreceived
        integer :: npacked_this_target
        integer :: ierror
        integer :: old_irecproc, irec_pack_index
        real(real64b) :: time1

        integer :: num_send(0 : nprocs-1)
        integer :: num_recv(0 : nprocs-1)
        integer :: num_send_vals(0 : nprocs-1)
        integer :: num_recv_vals(0 : nprocs-1)
        integer :: pack_offset(0 : nprocs-1)
        integer :: pack_offset_vals(0 : nprocs-1)
        integer :: unpack_offset(0 : nprocs-1)
        integer :: unpack_offset_vals(0 : nprocs-1)
        integer :: target_nodes(NPMAX)


        if (iprint) call logger("Shuffling atoms between all nodes")

        time1 = mpi_wtime()
        call pre_shuffle_checks(sdata%x0, pbc, natoms)
        tmr(TMR_SHUFFLE_PRE) = tmr(TMR_SHUFFLE_PRE) + (mpi_wtime() - time1)


        time1 = mpi_wtime()

        ! Count the number of atoms to send to each node.
        ! Also save the target node for each atom i in target_nodes(i).
        num_send(:) = 0
        do i = 1, myatoms
            node = rank_from_x0(sdata%x0(3*i-2:3*i), node_edges)
            num_send(node) = num_send(node) + 1
            target_nodes(i) = node
        end do
        num_send(myproc) = 0


        ! Distribute the number of atoms to send to each node.
        call MPI_Alltoall( &
            num_send, 1, my_mpi_integer, &
            num_recv, 1, my_mpi_integer, &
            MPI_COMM_WORLD, ierror)


        ! Find the node which will get the recoil.
        old_irecproc = irecproc
        irec_pack_index = -1
        if (irec /= 0) then
            if (myproc == old_irecproc) irecproc = target_nodes(irec)
            call my_mpi_bcast(irecproc, 1, old_irecproc)

            if (debug .and. iprint) then
                write(log_buf,*) "Shuffling recoil ", irec, iatrec, &
                    " from node ", old_irecproc, " to ", irecproc
                call logger(log_buf)
            end if
        end if


        ! Pack and send the data to each node. While doing so, remove the
        ! atoms that have been packed.
        npacked = 0
        do node = 0, nprocs - 1
            if (num_send(node) == 0) cycle

            npacked_this_target = 0
            i = 1
            do while (i <= myatoms)
                if (target_nodes(i) /= node) then
                    i = i + 1
                    cycle
                end if

                ! The atom is in the target node. Pack it into buf/ibuf, then
                ! copy the last atom on top of it to remove it.

                npacked = npacked + 1
                npacked_this_target = npacked_this_target + 1

                call shuffle_packatom(i, npacked, sdata, buf, ibuf)

                call shuffle_copyatom(myatoms, i, sdata)

                target_nodes(i) = target_nodes(myatoms)
                myatoms = myatoms - 1

                if (myproc == old_irecproc) then
                    ! Mark which index the receiving node must look in (in its
                    ! own receive buffer, from this node) to find the recoil atom.
                    if (i == irec) then
                        irec = -1  ! Once packed, the recoil is no longer here
                        irec_pack_index = npacked_this_target
                    end if

                    ! In case the recoil moved, update irec.
                    if (irec == myatoms + 1) irec = i
                end if
            end do
        end do


        ! Check that there is enough space for all the atoms.
        nreceived = sum(num_recv)
        if (myatoms + nreceived > NPMAX) then
            call logger_clear_buffer()
            call logger_append_buffer("NPMAX exceeded in Shuffle_all_to_all!")
            call logger_append_buffer("myproc:        ", myproc, 0)
            call logger_append_buffer("myatoms before:", myatoms + npacked, 0)
            call logger_append_buffer("num sent:      ", npacked, 0)
            call logger_append_buffer("num received:  ", nreceived, 0)
            call logger_append_buffer("myatoms after: ", myatoms + nreceived, 0)
            call logger_append_buffer("NPMAX:         ", NPMAX, 0)
            call logger(log_buf)

            call my_mpi_abort("NPMAX exceeded in Shuffle_all_to_all", myproc)
        end if


        ! Find the displacements from the start of the buffers to the
        ! first atom in each node, in units of atoms.
        pack_offset(0) = 0
        unpack_offset(0) = 0
        do node = 1, nprocs - 1
            pack_offset(node)   = pack_offset(node - 1)   + num_send(node - 1)
            unpack_offset(node) = unpack_offset(node - 1) + num_recv(node - 1)
        end do


        ! Send and receive data.
        num_send_vals(:) = num_send(:) * sdata%numreal
        num_recv_vals(:) = num_recv(:) * sdata%numreal
        pack_offset_vals(:) = pack_offset(:) * sdata%numreal
        unpack_offset_vals(:) = unpack_offset(:) * sdata%numreal

        call MPI_Alltoallv( &
            buf,  num_send_vals, pack_offset_vals,   mpi_double_precision, &
            buf2, num_recv_vals, unpack_offset_vals, mpi_double_precision, &
            MPI_COMM_WORLD, ierror)

        num_send_vals(:) = num_send(:) * sdata%numint
        num_recv_vals(:) = num_recv(:) * sdata%numint
        pack_offset_vals(:) = pack_offset(:) * sdata%numint
        unpack_offset_vals(:) = unpack_offset(:) * sdata%numint

        call MPI_Alltoallv( &
            ibuf,  num_send_vals, pack_offset_vals,   my_mpi_integer, &
            ibuf2, num_recv_vals, unpack_offset_vals, my_mpi_integer, &
            MPI_COMM_WORLD, ierror)


        ! Unpack the data into this node's atom arrays.
        do i = 1, nreceived
            myatoms = myatoms + 1

            call shuffle_unpackatom(myatoms, i, sdata, buf2, ibuf2)
        end do


        ! Find the new location of the recoil atom.
        if (irec /= 0 .and. old_irecproc /= irecproc) then
            call my_mpi_bcast(irec_pack_index, 1, old_irecproc)

            if (myproc == irecproc) then
                irec = myatoms - nreceived + unpack_offset(old_irecproc) + irec_pack_index

                if (irec <= 0 .or. irec > myatoms) then
                    call my_mpi_abort("BUG: Bad irec after Shuffle_all_to_all", irec)
                end if
                if (sdata%atomindex(irec) /= iatrec) then
                    call my_mpi_abort("BUG: Got wrong iatrec after Shuffle_all_to_all", &
                        sdata%atomindex(irec))
                end if
            end if
            call my_mpi_bcast(irec, 1, irecproc)

            if (iprint) then
                write(log_buf,"(2(A,I5),2I9)") "Shuffled recoil from", &
                    old_irecproc, ' to ', irecproc, irec, iatrec
                call logger(log_buf)
            end if
        end if

        tmr(TMR_SHUFFLE_ALL_TO_ALL) = tmr(TMR_SHUFFLE_ALL_TO_ALL) + (mpi_wtime() - time1)


        time1 = mpi_wtime()
        call post_shuffle_checks(sdata%x0, sdata%atomindex, pbc, natoms, &
            .true., npacked, nreceived)
        tmr(TMR_SHUFFLE_POST) = tmr(TMR_SHUFFLE_POST) + (mpi_wtime() - time1)

    end subroutine Shuffle_all_to_all


    !
    ! Perform checks on sanity that must be done before Shuffling.
    !
    subroutine pre_shuffle_checks(x0, pbc, natoms)

        real(real64b), intent(inout) :: x0(:)
        real(real64b), intent(in) :: pbc(3)
        integer, intent(in) :: natoms

        integer :: i, i1
        integer :: myatomsum
        integer :: ierror

        ! Check that the number of atoms is still consistent in all nodes.
        myatomsum = myatoms
        call my_mpi_sum(myatomsum, 1)
        if (myatomsum /= natoms) then
            call mpi_barrier(mpi_comm_world, ierror)
            print *,'Shuffle: myproc myatoms', myproc, myatomsum
            print *,'Shuffle: rmin rmx',rmn(1), rmn(2), rmx(1), rmx(2)
            call logger("Wrong number of atoms BEFORE Shuffle:")
            call logger("sum of atoms:", myatomsum, 4)
            call logger("natoms:      ", natoms, 4)
            call my_mpi_abort('Wrong number of atoms BEFORE Shuffle', myproc)
        end if

        ! Enforce periodic boundaries in periodic dimensions,
        ! as shuffle will fail if they are not strictly enforced.
        ! Might be possible due to e.g. dydtzmax.
        do i = 1, 3*myatoms
            i1 = mod(i - 1, 3) + 1

            ! TODO Maybe check for abs(x0(i)) >= 1.5 (where even applying
            ! PBC does not correct the positions), and abort?

            if (x0(i) >= 0.5d0) then
                x0(i) = x0(i) - pbc(i1)
            else if (x0(i) < -0.5d0) then
                x0(i) = x0(i) + pbc(i1)
            end if
        end do

    end subroutine pre_shuffle_checks


    !
    ! Perform checks on sanity that must be done after Shuffling.
    !
    subroutine post_shuffle_checks(x0, atomindex, pbc, natoms, printnow, nsent, nreceived)

        real(real64b), intent(in) :: x0(:)
        integer, intent(in) :: atomindex(:)
        real(real64b), intent(in) :: pbc(3)
        integer, intent(in) :: natoms
        logical, intent(in) :: printnow
        integer, intent(in) :: nsent, nreceived

        real(real64b) :: x(3)
        integer :: i, i3
        integer :: myatomsum
        integer :: minatoms, maxatoms
        integer :: ntotsent, ntotreceived

        if (myatoms == 0) then
            call logger("Shuffle notice: zero atoms at processor ", myproc, 0)
        end if

        ! Check that the number of atoms is still consistent in all nodes.
        myatomsum = myatoms
        call my_mpi_sum(myatomsum, 1)
        if (myatomsum /= natoms) then
            call my_mpi_barrier()
            if (iprint) then
                call logger("Wrong number of atoms AFTER Shuffle")
                call logger("sum of atoms:", myatomsum, 4)
                call logger("natoms:      ", natoms, 4)
            end if
            call my_mpi_barrier()
            call logger("myproc myatoms", myproc, myatoms, 0)
            call my_mpi_barrier()
            call my_mpi_abort('Wrong number of atoms AFTER Shuffle', myproc)
        end if

        ! Check that all atoms are within bounds, just to be safe about it.
        do i = 1, myatoms
            i3 = 3*i - 3
            x = x0(i3+1:i3+3)
            if (any(x(:) >= rmx(:) .or. x(:) < rmn(:)) .or. &
                any(pbc(:) == 1d0 .and. (x(:) >= 0.5d0 .or. x(:) < -0.5d0))) then

                call logger_clear_buffer()
                call logger_append_buffer("Severe shuffle error, atom outside bounds")
                call logger_append_buffer("Position:", x, 4)
                call logger_append_buffer("Core extents:", 4)
                call logger_append_buffer("min max, x:", rmn(1), rmx(1), 8)
                call logger_append_buffer("min max, y:", rmn(2), rmx(2), 8)
                call logger_append_buffer("min max, z:", rmn(2), rmx(2), 8)
                call logger_append_buffer("index:    ", i, 4)
                call logger_append_buffer("atomindex:", atomindex(i), 4)
                call logger_append_buffer("myproc:   ", myproc, 4)
                call logger_append_buffer("myatoms:  ", myatoms, 4)
                call logger(log_buf)
                call my_mpi_abort("Severe shuffle error: atom outside bounds", myproc)
            end if
        end do

        maxatoms = myatoms
        minatoms = myatoms
        ntotsent = nsent
        ntotreceived = nreceived
        call my_mpi_max(maxatoms, 1)
        call my_mpi_min(minatoms, 1)
        call my_mpi_sum(ntotsent, 1)
        call my_mpi_sum(ntotreceived, 1)

        if (printnow .and. iprint) then
            write(log_buf,'(A,2I8,A,2I8)') &
                'Shuffled', ntotsent, &
                ntotreceived, ' atoms between nodes, min/max atoms:', &
                minatoms, maxatoms
            call logger(log_buf)
        end if

        if (debug) print *,'Shuffle: myproc myatoms maxatoms', myproc, myatoms, maxatoms

    end subroutine post_shuffle_checks


    !
    !  Copy atom i stuff to atom j in arrays in shuffle data.
    !
    subroutine shuffle_copyatom(i, j, sdata)

        integer, intent(in) :: i, j
        type(ShuffleData), intent(inout) :: sdata

        integer :: i3, j3

        i3 = 3 * i - 2
        j3 = 3 * j - 2

        sdata%x0(j3:j3+2) = sdata%x0(i3:i3+2)
        sdata%x1(j3:j3+2) = sdata%x1(i3:i3+2)
        sdata%x2(j3:j3+2) = sdata%x2(i3:i3+2)
        sdata%x3(j3:j3+2) = sdata%x3(i3:i3+2)
        sdata%x4(j3:j3+2) = sdata%x4(i3:i3+2)
        sdata%x5(j3:j3+2) = sdata%x5(i3:i3+2)
        sdata%x0nei(j3:j3+2) = sdata%x0nei(i3:i3+2)

        if (associated(sdata%wxxiavg)) then
            sdata%wxxiavg(j) = sdata%wxxiavg(i)
            sdata%wyyiavg(j) = sdata%wyyiavg(i)
            sdata%wzziavg(j) = sdata%wzziavg(i)
            if (associated(sdata%wxyiavg)) then
                sdata%wxyiavg(j) = sdata%wxyiavg(i)
                sdata%wxziavg(j) = sdata%wxziavg(i)
                sdata%wyziavg(j) = sdata%wyziavg(i)
            end if
        end if

        sdata%atype(j) = sdata%atype(i)
        sdata%atomindex(j) = sdata%atomindex(i)

    end subroutine shuffle_copyatom


    !
    !  Pack atom i from shuffle data into position j in atbuf and iatbuf.
    !
    subroutine shuffle_packatom(i, j, sdata, atbuf, iatbuf)

        integer, intent(in) :: i, j
        type(ShuffleData), intent(in) :: sdata

        integer, intent(inout) :: iatbuf(:)
        real(kind=real64b), intent(inout) :: atbuf(:)

        integer :: i3, jint, jreal

        i3 = 3 * i - 2
        jint = (j - 1) * sdata%numint + 1
        jreal = (j - 1) * sdata%numreal + 1

        atbuf(jreal:jreal+2) = sdata%x0(i3:i3+2)   ; jreal=jreal+3
        atbuf(jreal:jreal+2) = sdata%x1(i3:i3+2)   ; jreal=jreal+3
        atbuf(jreal:jreal+2) = sdata%x2(i3:i3+2)   ; jreal=jreal+3
        atbuf(jreal:jreal+2) = sdata%x3(i3:i3+2)   ; jreal=jreal+3
        atbuf(jreal:jreal+2) = sdata%x4(i3:i3+2)   ; jreal=jreal+3
        atbuf(jreal:jreal+2) = sdata%x5(i3:i3+2)   ; jreal=jreal+3
        atbuf(jreal:jreal+2) = sdata%x0nei(i3:i3+2)   ; jreal=jreal+3

        if (associated(sdata%wxxiavg)) then
            atbuf(jreal) = sdata%wxxiavg(i)   ; jreal=jreal+1
            atbuf(jreal) = sdata%wyyiavg(i)   ; jreal=jreal+1
            atbuf(jreal) = sdata%wzziavg(i)   ; jreal=jreal+1
            if (associated(sdata%wxyiavg)) then
                atbuf(jreal) = sdata%wxyiavg(i)   ; jreal=jreal+1
                atbuf(jreal) = sdata%wxziavg(i)   ; jreal=jreal+1
                atbuf(jreal) = sdata%wyziavg(i)   ; jreal=jreal+1
            end if
        end if

        iatbuf(jint) = sdata%atype(i)   ; jint=jint+1
        iatbuf(jint) = sdata%atomindex(i)   ; jint=jint+1

    end subroutine shuffle_packatom


    !
    !  Unpack atom i into shuffle data from atbuf and iatbuf at position j.
    !
    subroutine shuffle_unpackatom(i, j, sdata, atbuf, iatbuf)

        integer, intent(in) :: i, j
        type(ShuffleData), intent(inout) :: sdata
        real(kind=real64b), intent(in) :: atbuf(:)
        integer, intent(in) :: iatbuf(:)

        integer :: i3, jint, jreal

        i3 = 3 * i - 2
        jint = (j - 1) * sdata%numint + 1
        jreal = (j - 1) * sdata%numreal + 1

        sdata%x0(i3:i3+2) = atbuf(jreal:jreal+2)   ; jreal=jreal+3
        sdata%x1(i3:i3+2) = atbuf(jreal:jreal+2)   ; jreal=jreal+3
        sdata%x2(i3:i3+2) = atbuf(jreal:jreal+2)   ; jreal=jreal+3
        sdata%x3(i3:i3+2) = atbuf(jreal:jreal+2)   ; jreal=jreal+3
        sdata%x4(i3:i3+2) = atbuf(jreal:jreal+2)   ; jreal=jreal+3
        sdata%x5(i3:i3+2) = atbuf(jreal:jreal+2)   ; jreal=jreal+3
        sdata%x0nei(i3:i3+2) = atbuf(jreal:jreal+2)   ; jreal=jreal+3

        if (associated(sdata%wxxiavg)) then
            sdata%wxxiavg(i) = atbuf(jreal)   ; jreal=jreal+1
            sdata%wyyiavg(i) = atbuf(jreal)   ; jreal=jreal+1
            sdata%wzziavg(i) = atbuf(jreal)   ; jreal=jreal+1
            if (associated(sdata%wxyiavg)) then
                sdata%wxyiavg(i) = atbuf(jreal)   ; jreal=jreal+1
                sdata%wxziavg(i) = atbuf(jreal)   ; jreal=jreal+1
                sdata%wyziavg(i) = atbuf(jreal)   ; jreal=jreal+1
            end if
        end if

        sdata%atype(i) = iatbuf(jint)   ; jint=jint+1
        sdata%atomindex(i) = iatbuf(jint)   ; jint=jint+1

    end subroutine shuffle_unpackatom


    !
    ! Pass "forward" border atom data using the given Packer.
    ! See the extensive documentation at the start of the file.
    !
    subroutine pass_border_atoms(packer)

        class(PassPacker), intent(inout) :: packer

        integer :: np
        integer :: dimen, dir
        integer :: numsend, numrecv
        integer :: sendsize, recvsize
        integer :: tag
        integer :: request
        integer :: stat(MPI_STATUS_SIZE)
        integer :: ierror

        if (debug) print *, "pass_border_atoms start", myproc

        ! Current number of atoms in memory (own + received).
        np = myatoms

        dimen_loop: do dimen = 1, 3
            dir_loop: do dir = 1, 2

                numrecv = numrecvatoms(dimen, dir)
                numsend = numsendatoms(dimen, dir)

                tag = 2 * dimen + dir

                if (numrecv > 0) then
                    ! Start receiving data into a temporary buffer.
                    call MPI_Irecv(buf2, size(buf2), mpi_double_precision, &
                        recvproc(dimen, dir), tag, mpi_comm_world, &
                        request, ierror)
                end if

                if (numsend > 0) then
                    ! Pack atoms indicated by sendatomslist into a temporary buffer.
                    call packer%do_pack(sendatomslist(:numsend, dimen, dir), buf, sendsize)

                    ! Send the atoms to the neighbor process.
                    call MPI_Send(buf, sendsize, mpi_double_precision, &
                        sendproc(dimen, dir), tag, mpi_comm_world, ierror)
                end if

                if (numrecv > 0) then
                    ! Wait for the receive operation to finish.
                    call MPI_Wait(request, stat, ierror)

                    ! Get the number of reals received.
                    call MPI_Get_count(stat, mpi_double_precision, recvsize, ierror)

                    ! Unpack the received atoms from the temporary buffer.
                    call packer%do_unpack(np + 1, numrecv, buf2(:recvsize))

                    np = np + numrecv
                end if

            end do dir_loop
        end do dimen_loop

        if (debug) print *, "pass_border_atoms done", myproc

    end subroutine pass_border_atoms


    !
    ! Pass "back" border atom data using the given Packer.
    ! See the extensive documentation at the start of the file.
    !
    subroutine pass_back_border_atoms(packer)

        class(PassBackPacker), intent(inout) :: packer

        integer :: dimen, dir
        integer :: numsend, numrecv
        integer :: sendsize, recvsize
        integer :: tag
        integer :: request
        integer :: stat(MPI_STATUS_SIZE)
        integer :: ierror

        if (debug) print *, "pass_back_border_atoms start", myproc

        ! Go through neighbors in the reverse order, to make sure the
        ! atoms that were received from one neighbor and passed on to
        ! another go back where they belong.
        dimen_loop: do dimen = 3, 1, -1
            dir_loop: do dir = 2, 1, -1


                ! Note that these are reversed, since we are "passing back" atoms.
                ! Also recvproc and sendproc are reversed.
                numsend = numrecvatoms(dimen, dir)
                numrecv = numsendatoms(dimen, dir)

                tag = 2 * dimen + dir

                if (numrecv > 0) then
                    ! Start receiving atoms from the neighbor into the receive buffer.
                    call MPI_Irecv(buf2, size(buf2), mpi_double_precision, &
                        sendproc(dimen, dir), tag, mpi_comm_world, &
                        request, ierror)
                end if

                if (numsend > 0) then
                    ! Pack atoms that were received from the neighbor process into the send buffer.
                    call packer%do_pack(firstrecvatom(dimen, dir), numsend, buf, sendsize)

                    ! Send the send buffer over to the other process.
                    call MPI_Send(buf, sendsize, mpi_double_precision, &
                        recvproc(dimen, dir), tag, mpi_comm_world, ierror)
                end if

                if (numrecv > 0) then
                    ! Wait for the receive operation to finish.
                    call MPI_Wait(request, stat, ierror)

                    ! Get the number of reals received.
                    call MPI_Get_count(stat, mpi_double_precision, recvsize, ierror)

                    ! Unpack the received atom data from the receive buffer.
                    call packer%do_unpack(sendatomslist(:numrecv, dimen, dir), buf2(:recvsize))
                end if

            end do dir_loop
        end do dimen_loop

        if (debug) print *, "pass_back_border_atoms done", myproc

    end subroutine pass_back_border_atoms


    !
    ! Helper subroutine for potential implementations.
    ! Passes x0 and atype. See PotentialPassPacker.
    !
    subroutine potential_pass_border_atoms(x0, atype)
        real(real64b), contiguous, target, intent(inout) :: x0(:)
        integer, contiguous, target, intent(inout) :: atype(:)

        type(PotentialPassPacker) :: packer

        packer%x0 => x0
        packer%atype => atype

        call pass_border_atoms(packer)

    end subroutine potential_pass_border_atoms


    !
    ! Helper subroutine for potential implementations.
    ! Passes back forces, optionally Epair, optionally virials.
    ! See PotentialPassBackPacker.
    !
    subroutine potential_pass_back_border_atoms(xnp, Epair, wxxi, wyyi, wzzi, wxyi, wxzi, wyzi)
        real(real64b), contiguous, target, intent(inout) :: xnp(:)
        real(real64b), contiguous, target, optional, intent(inout) :: Epair(:)
        real(real64b), contiguous, target, optional, intent(inout) :: wxxi(:), wyyi(:), wzzi(:)
        real(real64b), contiguous, target, optional, intent(inout) :: wxyi(:), wxzi(:), wyzi(:)

        type(PotentialPassBackPacker) :: packer

        packer%xnp => xnp
        if (present(Epair)) then
            packer%Epair => Epair
        else
            nullify(packer%Epair)
        end if
        if (present(wxxi)) then
            packer%wxxi => wxxi
            packer%wyyi => wyyi
            packer%wzzi => wzzi
        else
            nullify(packer%wxxi)
            nullify(packer%wyyi)
            nullify(packer%wzzi)
        end if
        if (present(wxyi)) then
            packer%wxyi => wxyi
            packer%wxzi => wxzi
            packer%wyzi => wyzi
        else
            nullify(packer%wxyi)
            nullify(packer%wxzi)
            nullify(packer%wyzi)
        end if

        call pass_back_border_atoms(packer)

    end subroutine potential_pass_back_border_atoms


    ! Packing subroutine for PotentialPassPacker.
    subroutine potential_pass_pack(this, sendlist, buffer, filled_size)
        class(PotentialPassPacker), intent(inout) :: this
        integer, intent(in) :: sendlist(:)
        real(real64b), contiguous, intent(out) :: buffer(:)
        integer, intent(out) :: filled_size

        integer :: i, i3, isend, iarr(2)
        real(real64b) :: r64

        iarr(2) = 0
        filled_size = 0
        do isend = 1, size(sendlist)
            i = sendlist(isend)
            i3 = 3*i - 3

            filled_size = filled_size + 4
            buffer(filled_size - 3) = this%x0(i3+1)
            buffer(filled_size - 2) = this%x0(i3+2)
            buffer(filled_size - 1) = this%x0(i3+3)
            iarr(1) = this%atype(i)
            buffer(filled_size - 0) = transfer(iarr, r64)
        end do
    end subroutine potential_pass_pack


    ! Unpacking subroutine for PotentialPassPacker.
    subroutine potential_pass_unpack(this, first_atom, num_atoms, buffer)
        class(PotentialPassPacker), intent(inout) :: this
        integer, intent(in) :: first_atom
        integer, intent(in) :: num_atoms
        real(real64b), contiguous, intent(in) :: buffer(:)

        integer :: i, i3, bufind, iarr(2)

        bufind = 0
        do i = first_atom, first_atom + num_atoms - 1
            i3 = 3*i - 3
            bufind = bufind + 4
            this%x0(i3+1) = buffer(bufind - 3)
            this%x0(i3+2) = buffer(bufind - 2)
            this%x0(i3+3) = buffer(bufind - 1)
            iarr(:) = transfer(buffer(bufind - 0), iarr)
            this%atype(i) = iarr(1)
        end do
    end subroutine potential_pass_unpack


    ! Packing subroutine for PotentialPassBackPacker.
    subroutine potential_pass_back_pack(this, first_atom, num_atoms, buffer, filled_size)
        class(PotentialPassBackPacker), intent(inout) :: this
        integer, intent(in) :: first_atom
        integer, intent(in) :: num_atoms
        real(real64b), contiguous, intent(out) :: buffer(:)
        integer, intent(out) :: filled_size

        integer :: i, i3

        filled_size = 0
        do i = first_atom, first_atom + num_atoms - 1
            i3 = 3*i - 3
            filled_size = filled_size + 3
            buffer(filled_size - 2) = this%xnp(i3+1)
            buffer(filled_size - 1) = this%xnp(i3+2)
            buffer(filled_size - 0) = this%xnp(i3+3)
            if (associated(this%Epair)) then
                filled_size = filled_size + 1
                buffer(filled_size) = this%Epair(i)
            end if
            if (associated(this%wxxi)) then
                filled_size = filled_size + 3
                buffer(filled_size - 2) = this%wxxi(i)
                buffer(filled_size - 1) = this%wyyi(i)
                buffer(filled_size - 0) = this%wzzi(i)
            end if
            if (associated(this%wxyi)) then
                filled_size = filled_size + 3
                buffer(filled_size - 2) = this%wxyi(i)
                buffer(filled_size - 1) = this%wxzi(i)
                buffer(filled_size - 0) = this%wyzi(i)
            end if
        end do
    end subroutine potential_pass_back_pack


    ! Unpacking subroutine for PotentialPassBackPacker.
    subroutine potential_pass_back_unpack(this, recvlist, buffer)
        class(PotentialPassBackPacker), intent(inout) :: this
        integer, intent(in) :: recvlist(:)
        real(real64b), contiguous, intent(in) :: buffer(:)

        integer :: irecv, i, i3, bufind

        bufind = 0
        do irecv = 1, size(recvlist)
            i = recvlist(irecv)
            i3 = 3*i - 3
            bufind = bufind + 3
            this%xnp(i3+1) = this%xnp(i3+1) + buffer(bufind - 2)
            this%xnp(i3+2) = this%xnp(i3+2) + buffer(bufind - 1)
            this%xnp(i3+3) = this%xnp(i3+3) + buffer(bufind - 0)
            if (associated(this%Epair)) then
                bufind = bufind + 1
                this%Epair(i) = this%Epair(i) + buffer(bufind)
            end if
            if (associated(this%wxxi)) then
                bufind = bufind + 3
                this%wxxi(i) = this%wxxi(i) + buffer(bufind - 2)
                this%wyyi(i) = this%wyyi(i) + buffer(bufind - 1)
                this%wzzi(i) = this%wzzi(i) + buffer(bufind - 0)
            end if
            if (associated(this%wxyi)) then
                bufind = bufind + 3
                this%wxyi(i) = this%wxyi(i) + buffer(bufind - 2)
                this%wxzi(i) = this%wxzi(i) + buffer(bufind - 1)
                this%wyzi(i) = this%wyzi(i) + buffer(bufind - 0)
            end if
        end do
    end subroutine potential_pass_back_unpack


end module mdparsubs_mod
