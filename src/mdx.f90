!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.


!*******************************************************************************
! On units: using typical MD units where all internal masses are 1:
!
! The length units here are scaled by the box sides so that within the
! program, x always ranges between -0.5 and 0.5. This means that
! distances (x0), velocities (x1), accelerations (x2) etc. must
! be scaled by box(i) to get real units, where i is 1, 2 or 3.
!
! Note that in these MD INTERNAL UNITS, the v, a, t units still are
! functions of the velocity. See Kai Nordlund Master's Thesis for an
! explanation on this (in Swedish). The parameters timeunit, vunit,
! aunit etc. (see Init_Units) can be used for unit transformations
!
! The time step is also part of the length vectors, according to the
! Gear algorithm unit system: x0 = x0, x1=x1*deltat, x2=x2*deltat^2/2!,
! x3=x3*deltat^3/3! etc. Thus, if x is the real length, x' the "real"
! velocity (in internal MD units !), etc., we have
!
!    x0(i) = x  /box(i)*delta^0/0! = x /box(i)
!    x1(i) = x' /box(i)*delta^1/1! = x'/box(i)*delta
!    x2(i) = x''/box(i)*delta^2/2!
!
!    ... etc. until x5(i)
!
! To get real eV-�-fs units, multiply e.g. x' by vunit(atype(i))
!
! The force xnp(i) obtained from the force calculation is scaled in
! there, so if F is the real force in units of eV/A,
!
!    xnp(i) = F/box(idim)
!
! The virials are in units of eV, without box scaling.
!
!*******************************************************************************


program PARCAS

    use defs

    use TYPEPARAM
    use symbolic_constants

    use PhysConsts, only: eV_to_kbar, invkBeV

    use Temp_Time_Prog, only: TT_activated
    use ttprog_mod, only: Init_Temp_Time_Prog, Set_Temp_Control
    use silica_wat_mod
    use silica_wat_mod_rev

    use datatypes, only: int32b, real64b
    use my_mpi
    use binout
    use timers

    use para_common, only: &
        para_common_allocate, &
        debug, iprint, &
        rmn, rmx, &
        myatoms, nnodes
    use casc_common

    use mdinit_mod
    use mdoutput_mod

    use mdparsubs_mod, only: &
        divide_simulation_box, &
        ShuffleData, &
        Shuffle, &
        Shuffle_all_to_all, &
        print_node_configuration, &
        potential_pass_border_atoms

    use mdlinkedlist_mod
    use mdlattice_mod
    use tersoff_compound_mod, only: &
        Init_Tersoff_Compound, Tersoff_Compound_Force
    use mdsubs_mod
    use elstop_mod, only: elstop_init, elstop_loop
    use mdcasc_mod
    use stillinger_weber_mod
    use edip_mod
    use tersoff_mod
    use brenner_beardmore_mod
    use silica_mod

    use SANDIAeam_mod
    use eamforces_eamal_mod
    use eamforces_mod

    use splinereppot_mod, only: reppot_init

    use random


    use output_logger, only: &
        logger_setup, &
        logger_finalize, &
        logger_write, &
        log_buf, &
        logger, &
        logger_append_buffer, &
!        logger_write_buffer, &
        logger_clear_buffer, &
        debugger, &
        time_logger_init, &
        time_logger, &
        time_logger_finalize

    use file_units, only: &
        OUT_RECOIL_FILE, &
        OUT_PRESSURES_FILE, &
        OUT_LIQUIDAT_FILE

    use lat_flags

    use border_params_mod, only: BorderParams


    implicit none

    character(len=*), parameter :: vstring = 'PARCAS V5.22'



    !***************************************************************************
    ! Declarations
    !***************************************************************************

    !     Help variables and their ilk

    real(real64b) :: help(3)
    real(real64b) :: vsq
    integer :: i, j, i3, m
    real(real64b) :: factor
    integer :: isum_array(100)
    real(real64b) :: dsum_array(100)

    type(ShuffleData) :: shuffle_data

    ! Seed for the random number generator. Combined with MPI
    ! rank to produce different numbers in each process.
    integer :: seed

    integer :: natoms, natomsin, nfixed ! Atom counts

    ! The number of atoms each node can handle is dependent on the amount
    ! of memory available on the chosen machine. NPMAX is set in defs.f90
    ! Use enough nodes so that NPMAX > natoms/nprocs, with enough spare
    ! space for extra atoms from neighbor nodes.

    integer(int32b), allocatable, target :: atomindex(:) ! Atom IDs
    integer(int32b), allocatable, target :: atype(:) ! Atom type indices

    ! Force, Charge density, Pair and Embedding energy for each atom
    real(real64b), allocatable :: dfpdp(:,:), P(:,:)
    real(real64b), allocatable :: Ethree(:,:), Epair(:), Ekin(:)
    real(real64b) :: VPair, Vmany

    ! Forces in eV/A/box
    real(real64b), allocatable :: xnp(:)

    ! X0-X5 = positions and their first thru fifth time derivatives
    real(real64b), allocatable, target :: x0(:), x1(:), x2(:), x3(:), x4(:), x5(:)

    ! Distances travelled since last neighbor list build
    real(real64b), allocatable, target :: x0nei(:)
    real(real64b) :: xneimax = 0d0, xneimax2 = 0d0

    ! Scale factor for size of neighbor list skin region
    real(real64b) :: neiskinr
    ! Neighbor list cutoff
    real(real64b) :: cut_nei
    ! Maximum force cutoff
    real(real64b) :: rcutmax
    ! Maximum distance any atom may move before neighbor list update
    real(real64b) :: maxdist
    ! Maximum time steps between neighbor list updates
    integer :: nprtbl

    ! Virials and related parameters.
    ! Use a symmetric stress tensor, i.e. wxy=wyx, wxz=wzx, wyz=wzy.
    ! This saves memory and time, and should apply for all systems
    ! where atoms have no internal degrees of freedom.
    real(real64b) :: wxx, wyy, wzz
    real(real64b), target :: wxxi(NPMAX), wyyi(NPMAX), wzzi(NPMAX)
    real(real64b), target :: wxyi(NPMAX), wxzi(NPMAX), wyzi(NPMAX)

    ! Some virial moviemodes modify the virials by averaging or adding
    ! kinetic terms. Since the originals might be required later, do
    ! the modifying in these copies.
    real(real64b), pointer, contiguous :: &
        wxxi2(:) => null(), wyyi2(:) => null(), wzzi2(:) => null(), &
        wxyi2(:) => null(), wxzi2(:) => null(), wyzi2(:) => null()

    real(real64b), allocatable, target :: wxxiavg(:), wyyiavg(:), wzziavg(:)
    real(real64b), allocatable, target :: wxyiavg(:), wxziavg(:), wyziavg(:)

    logical :: need_nondiag_vir
    logical :: need_kinetic_vir
    logical :: need_avg_vir
    integer :: avgvir
    logical :: calc_vir_now

    integer :: vir_istep_old = 0
    integer :: outtype
    real(real64b) :: outzmin,outzmax
    real(real64b) :: outzmin2,outzmax2
    integer :: virsym
    integer :: virkbar
    real(real64b) :: virboxsiz

    ! Variables for summing the forces in each direction
    real(real64b) :: sumf(3)
    integer :: nsummed
    integer :: forcesum, sumatype

    ! Parameters and variables for Berendsen temperature control (btc).
    integer :: mtemp
    real(real64b) :: btctau
    real(real64b) :: temp, temp0
    real(real64b) :: toll, trate
    real(real64b) :: heat, heatbrdr ! Scaling factor for Berendsen
    type(BorderParams) :: border_params ! Border atom parameters
    real(real64b) :: timeini
    integer :: ntimeini
    real(real64b) :: fdamp ! for mtemp 9
    ! Total energy losses by E scaling (for each processor separately)
    ! index 1 is borders, index 2 is total
    real(real64b) :: Elosssum(2) = 0d0

    ! Berendsen pressure control (bpc) variables
    integer :: bpcmode
    real(real64b) :: bpcbeta, bpctau
    real(real64b) :: bpcP0, bpcP0x, bpcP0y, bpcP0z
    real(real64b) :: Pxx,Pyy,Pzz
    real(real64b) :: dh, dhorig, dhprev
    real(real64b) :: pt, Pave, pressure ! helpers

    real(real64b) :: bpcextz ! Strain rate, not really bpc
    real(real64b) :: box0(3)

    ! Various averages, sums, ...
    real(real64b) :: trans, transin, transmoving, transbrdr ! Kinetic energy sums
    real(real64b) :: transv(3) ! Velocity sum
    real(real64b) :: avgkin = 0d0, avgkinmoving = 0d0, avgenergy = 0d0
    real(real64b) :: avgpot = 0d0, avgtemp, avgtempmoving, avgp(3) = 0d0
    real(real64b) :: poten ! Potential energy sum (later average)
    real(real64b) :: tote, tote0 = 0 ! Total energy per atom (current, initial)
    real(real64b) :: tempp

    real(real64b) :: t1, tmain1 ! Timers

    ! Time steps, including for different atom types
    real(real64b), allocatable :: delta(:), deltas(:)
    real(real64b) :: deltaratio, deltaini, deltamax_fs

    ! Parameters for atom initialization
    integer :: latflag, mdlatxyz
    real(real64b) :: initemp
    real(real64b) :: amp, tdebye
    real(real64b) :: fixzmin, fixzmax, fixperbrdr, fixxybrdr

    ! End conditions
    real(real64b) :: tmax, rstmax, restartt
    real(real64b) :: endtemp
    real(real64b) :: z_height, zmin
    integer :: nsteps, istep = 0

    ! Potential mode selection and parameters
    integer :: potmode
    logical :: EAM, EAMAL, Stilweb, Tersoff, Tersoff_Compound, EDIP, &
               BrennerBeardmore, Silica, Silica_Wat, Silica_Wat_rev

    logical :: SortNeiList ! require sorted neighbors for each atom?
    logical :: double_borders ! pass atoms from CPUs borders with distance of 2*cutoff?
    logical :: half_neighbor_list ! neighbor list contains only atom pairs with xi <= xj?

    integer :: spline, Fpextrap, eamnbands ! EAM params
    real(real64b) :: damp ! Friction force
    real(real64b) :: reppotcutin ! Reppot cutoff from user

    ! Brenner Beardmore parameters
    real(kind=real64b)   :: R1CC, R2CC ! cutoff modification

    ! Stillinger-Weber strength modifications
    real(real64b) :: sw2mod(3) = [ 1d0, 1d0, 1d0 ]
    real(real64b) :: sw3mod(3) = [ 1d0, 1d0, 1d0 ]

    ! Random force parameters: if prandom==0.0 dont use
    real(real64b)   :: prandom, mrandom, timerandom
    integer :: nrandom

    ! Parameters for top level force for C44 calculation
    real(real64b) :: Fzmaxz, FzmaxYz, FzmaxZz, Fzmaxzr
    integer :: nFzmax, Fzmaxtyp
    real(real64b) :: dydtmaxz
    integer :: dydttime

    ! Velocity adding (addvel) variables
    real(real64b) :: taddvel, zaddvel, eaddvel, vaddvel, addvelt, addvelp
    logical :: doneaddvel = .false.

    ! Swift heavy ion track parameters
    integer :: trackmode
    real(real64b) :: trackx, tracky, trackt
    logical :: trackstarted = .false.
    logical :: trackstopped = .false.

    ! Total linear/angular momentum removal
    integer :: pscale, remrot

    ! Stop the recoil atom if it goes below the z-value recstopz.
    ! This is done by changing it to the type recstoptype, which must
    ! have no interactions with other types, and by removing its
    ! velocity.
    real(real64b) :: recstopz
    integer :: recstoptype

    ! The next atomindex to give to a newly created recoil atom.
    integer :: recindex

    ! Time constant of T modification for determining melting point of
    ! a combined liquid-crystalline cell. If liquid density is less
    ! than solid, use positive, otherwise negative.
    ! Typical value ~ 30 fs for FCC, ~ -300 fs for Si.
    real(real64b) :: tmodtau

    ! Elstop and sputtering variables
    ! FDe(1) is for non-sputtered atoms
    ! FDe(2) is for sputtered atoms
    integer :: melstop, melstopst, natelstop, natelstopfar
    real(real64b) :: elstopmin, FDe(2), FDesum(2) = 0d0, elstopsputlim

    ! Nborlist() holds the neighbor lists.
    integer, allocatable :: nabors(:)
    integer, allocatable :: nborlist(:)

    ! Filled by Brenner-Beardmore
    integer :: bondstat(4,0:NNMAX)
    integer :: bondstat2(4,0:NNMAX)
    integer :: bondstat3(4,0:NNMAX)

    ! Parameters for recoil sequences (=rs), i.e. multiple recoils in
    ! one run. If rsnum>0, rs data is read in from file recoildata.in.
    integer :: rsnum, rsmode

    ! Other recoil variables
    integer :: rs_index,recatypemod
    real(kind=real64b) :: recenmax

    ! Maximum kinetic energy, velocity and Force for all particles in node
    real(real64b) :: Emax, vmax = 0d0, Fmax
    ! Maximum Ekin, v, F disregarding sputtered atoms
    real(real64b) :: Emaxnosput, vmaxnosput = 0d0, Fmaxnosput

    ! Number of atoms in border region
    integer :: nbrdr
    ! Maximum allowed Ekin at cell border region
    real(real64b) :: Emaxbrdr

    ! Variables for defect recognition and liquid analysis.
    real(real64b)   :: Ekdef, Ekrec
    integer :: ndefmovie
    integer :: nliqanal

    ! Time parameters
    !
    ! The time step is selected using the Et and kt criteria in Nordlund,
    ! Comp. Mat. Sci 3 (1995) 448-  . timekt and timeEt are read in.
    ! timeunit(ntype) is the conversion factor of internal units to fs.
    !
    integer :: adaptivet ! 1 = use adaptive dt, 0 = don't, -1 = auto (for recoils)
    logical :: adaptive_dt  ! is the adaptive time step enabled for this recoil?
    real(real64b) :: timekt, timeEt, timeCh
    real(real64b) :: time_fs, time_thisrec, deltat_fs
    real(real64b) :: timesputlim, nisputlim

    ! Simulation cell
    real(real64b) :: box(3)  ! size of supercell in Angstroms
    real(real64b) :: pbc(3)  ! periodicity: 0 = non-periodic, 1 = periodic
    real(real64b) :: unitcell(3) ! size of unitcell in Angstroms
    integer :: ncells(3) ! number of unitcells in each direction

    ! Input and output parameters
    integer :: ndump

    integer :: nmovie, nmovie0
    integer :: moviemode
    real(real64b) :: dtmovie(9), tmovie(9)

    integer :: slicemode
    real(real64b) :: dtslice(9),tslice(9)
    real(real64b) :: ECM(3), dslice(3) ! slicing on output
    integer :: ECMfix

    character(256) :: binmode
    integer :: restartmode
    integer :: nrestart
    real(real64b) :: ECMIn(3), dsliceIn(3) ! slicing on restart reading

    integer :: zipout

    ! If final_xyz is true, write the final simulation frame to final_xyz_path
    ! using the same moviemode as md.movie. final_xyz_path can be either
    ! absolute or relative to the PARCAS run directory, e.g. "out/end.xyz".
    logical :: final_xyz
    character(len=120) :: final_xyz_path

    ! How often to rebalance the atoms between nodes.
    ! >0 = every so many steps, =0 = at the start, <0 = never
    integer :: nbalance
    logical :: rebalance_done
    logical :: try_rebalance


    integer(kind=mpi_parameters) :: ierror  ! MPI error code

    integer :: times_shuffled = 0

    ! Whether output files are open on this processor.
    logical :: recoilat_out_open = .false.
    logical :: pressures_out_open = .false.
    logical :: liquidat_out_open = .false.

    logical :: printnow = .true.
    logical :: firsttime = .true.
    logical :: firstprint = .true.

    logical :: last_step_predict = .false.
    logical :: force_update_pairtable = .false.
    logical :: indir_exists

    ! Print movie/slice this step?
    ! The functions save their state, so we need to call them
    ! once per iteration.
    logical :: movietime_val, slicetime_val


    !***************************************************************************


    !***************************************************************************
    !---------------------------------------------------------------------------
    ! Begin of program
    !---------------------------------------------------------------------------
    !***************************************************************************

    ! Initialize timers
    tmr(:) = 0.0


    ! Setup open MPI
    call my_mpi_init()

    ! Allocate the biggest arrays here. Some tools, e.g. Valgrind,
    ! cannot deal with too large static arrays (or more exactly, a
    ! too large BSS section in the ELF binary).
    ! Arrays depending on eamnbands are allocated later.
    allocate(nborlist(NNMAX * NPMAX))
    allocate(nabors(NPMAX + 1))
    allocate(atomindex(NPMAX))
    allocate(atype(NPMAX))
    allocate(x0(3*NPMAX))
    allocate(x1(3*NPMAX))
    allocate(x2(3*NPMAX))
    allocate(x3(3*NPMAX))
    allocate(x4(3*NPMAX))
    allocate(x5(3*NPMAX))
    allocate(x0nei(3*NPMAX))
    allocate(xnp(3*NPMAX))
    allocate(Epair(NPMAX))
    allocate(Ekin(NPMAX))
    call para_common_allocate(nprocs)
    call mdoutput_allocate()


    if (iprint) then
        ! Create the output directory
        call system("mkdir -p out")
    end if

    ! Initializing the output logger
    call logger_setup("out/md.out")


    ! Print software and initialization information at the beginning of the
    ! output log.
    if (iprint) then
        call print_license_header()

        call logger()
        if (nprocs > 1) then
            ! TODO: Change 'multi-core' to 'parallel'?
            call logger(vstring, "started in multi-core mode", 2)
            call logger("Processors:", nprocs, 6)
        else
            call logger(trim(vstring), "started in scalar mode", 2)
        endif
        call logger()
        call logger()

        call logger("Defined parameters, used during compilation:", 2)
        call logger("NPmax =", NPMAX, 6)
        call logger("NNmax =", NNMAX, 6)
        call logger()

        ! Extract in/-directory from tgz-file if it does not exist already.
        ! Since checking for the directory directly is non-standard and does
        ! not work with Intel fortran, check for md.in instead.
        inquire(file="in/md.in", exist=indir_exists)
        if(.not. indir_exists) then
            inquire(file="in.tgz", exist=indir_exists)
            if (indir_exists) then
                call logger("Extracting input directory from in.tgz:", 2)
                call system("tar xzvf in.tgz")
                call logger()
            end if
        end if
        call logger()
    end if


    ! Read the md.in file for simulation specific parameters. This function
    ! is called by all processes (all cores are using the exact same
    ! parameters)
    call read_all_parameters(nsteps,tmax,deltaini,natoms,box,ncells, &
        pbc,mtemp,temp,toll,damp,pscale,remrot,amp,tdebye,latflag,mdlatxyz, &
        nprtbl, &
        ndump,nmovie,timekt,timeEt,timeCh,adaptivet, &
        neiskinr,Ekdef,Ekrec,ndefmovie,nliqanal, &
        temp0,trate,ntimeini,timeini,nrestart,bpcbeta, &
        bpctau,bpcP0,bpcP0x,bpcP0y,bpcP0z,bpcextz,bpcmode, &
        btctau,restartmode,restartt,potmode,spline,eamnbands,Fpextrap,initemp, &
        melstop,melstopst,elstopmin,tmodtau,rsnum,rsmode,endtemp,dtmovie, &
        tmovie,dtslice, &
        tslice,dslice,ECM,ECMfix,dsliceIn,ECMIn,sw2mod,sw3mod,prandom,mrandom, &
        timerandom,Fzmaxz,FzmaxYz,FzmaxZz,Fzmaxzr,Fzmaxtyp, dydtmaxz, &
        fixzmin,fixzmax,fixperbrdr,fixxybrdr, &
        reppotcutin,elstopsputlim,timesputlim,nisputlim, &
        taddvel,zaddvel,eaddvel,vaddvel,Emaxbrdr,moviemode,binmode,slicemode, &
        fdamp, border_params, &
        addvelt,addvelp,trackx,tracky,trackt,trackmode,forcesum, &
        sumatype,zipout,avgvir,dydttime, &
        outtype,outzmin,outzmax,outzmin2,outzmax2,virsym,virkbar,virboxsiz, &
        R1CC,R2CC,zmin,recstopz,recstoptype,recindex, &
        final_xyz, final_xyz_path, nnodes, seed, nbalance)



    if (iprint) then
        call logger()
        call logger("=====================================")
        call logger("Parameter Comments:", 2)
        call logger()

        if (.not. is_valid_moviemode(moviemode)) then
            call logger("Invalid moviemode", moviemode, 6)
            call my_mpi_abort("invalid moviemode", moviemode)
        end if

        if (nprocs > 1 .and. any(nnodes > 0)) then
            if (any(nnodes <= 0)) then
                call logger("Must give either all or none of nnodes(1:3)!")
                call my_mpi_abort("nnodes parameter all or none", 0)
            end if
            if (product(nnodes) /= nprocs) then
                call logger("nnodes parameter is wrong! nodes wanted, have=", product(nnodes), nprocs, 0)
                call my_mpi_abort("nnodes parameter total number wrong", nprocs)
            end if
        end if

        if (any(ncells <= 0)) then
            if (irec /= 0 .and. nliqanal > 0) then
                call logger("ncell>0 required for liquid analysis!", 6)
                ! Cannot abort here since inputs where this happens are far too
                ! common. The users just ignore the liquid analysis, since the
                ! nliqanal>0 just happened to be in an input file they copied
                ! theirs from.
                !call my_mpi_abort("ncells required", minval(ncells))
            end if

            select case (latflag)
            case (LATFLAG_CREATE_FCC, LATFLAG_CREATE_DIA, LATFLAG_CREATE_LATTICE)
                call logger("ncell>0 required for lattice creation!", 6)
                call my_mpi_abort("ncells required", minval(ncells))
            end select
        end if

        if (tmodtau /= 0.0 .and. bpctau == 0.0) then
            call logger("WARNING: tmodtau not meaningful when bpctau = 0")
        end if

        if (irec /= 0) then
            call logger("Recoil atom mode", irec, 6)
            call logger("recatype ", recatype, 10)
            call logger("recatypem", recatypem, 10)
        end if

        if (trackmode == 1) then
            call logger("*** track mode chosen ***")
            call logger("track will be at:", 6)
            call logger("track x =", trackx, 10)
            call logger("track y =", tracky, 10)
            call logger("track t =", trackt, 10)
        end if

        if (recstoptype >= 0) then
            if (recstoptype < itypelow .or. recstoptype > itypehigh) then
                call logger("ERROR: bad recstopty:", recstoptype, 6)
                call my_mpi_abort("bad recstopty", recstoptype)
            end if

            do i = itypelow, itypehigh
                if (iac(recstoptype, i) /= 0 .or. &
                    iac(i, recstoptype) /= 0) then

                    call logger("Recstopty has interactions with type ", i, 6)
                    call my_mpi_abort("recstopty has interactions", i)
                end if
            end do
        else if (recstopz > -huge(1.0d0)) then
            call logger("ERROR: recstopz given but not recstopty!", 6)
            call my_mpi_abort("recstopty not given", 0)
        end if

        call logger("=====================================")
        call logger()
    end if

    if (TT_activated) then
        call Init_Temp_Time_Prog(border_params%thickness)
    end if


    ! Open output files
    if (nmovie > 0) then
        call movie_init(moviemode)
    end if
    call slice_init(dslice, slicemode)

    if (iprint) then
        if (nliqanal > 0) then
            liquidat_out_open = .true.
            open(OUT_LIQUIDAT_FILE, file='out/liquidat.out', status='replace')
        end if

        if (ndefmovie > 0) then
            pressures_out_open = .true.
            open(OUT_PRESSURES_FILE, file='out/pressures.out', status='replace')
        end if

        if (irec /= IREC_NO_RECOIL .or. rsnum > 0) then
            recoilat_out_open = .true.
            open(OUT_RECOIL_FILE, file='out/recoilat.out', status='replace')
        end if
    end if

    ! binary output initialization
    if (moviemode == 9 .or. slicemode == 9 .or. restartmode == 9 ) then
        call initBinaryMode(pbc)
        !init binary writing once we know periodicity
        !initialize filesequence data for each type of output
        if (moviemode == 9) then
            !full system snapshots, write out in real(kind=real32b)
            call initFilesequence(1,binmode,"out/movie.",0,4)
        endif
        if (slicemode == 9) then
            !slice snapshots, write out in real(kind=real32b)
            if ((dslice(1)>=0.0.or.dslice(2)>=0.0.or.dslice(3)>=0.0)) then
                call initFilesequence(2,binmode,"out/slice.",0,4)
            endif
        endif
        if (restartmode == 9) then
            !instead of mdlat stuff
            !mode 28, write out velocities (and of course index and atype)
            !write out as real(kind=real64b)
            call initFilesequence(3, "V.x;V.y;V.z", "out/restart",1 , 8)
        end if
    end if


    !     Handle type-dependent parameters

    ! Allocate some type-dependent arrays
    allocate(delta(itypelow:itypehigh))
    allocate(deltas(itypelow:itypehigh))

    IF(debug)PRINT *,'Init_Units',itypelow,itypehigh
    call Init_Units()


    ! Initialize the simulation, including reading in/constructing of atoms

    IF(debug)PRINT *,'Md_init',myproc

    call Md_Init(x0,x1,atomindex, &
        atype,natoms,nfixed,amp,initemp,tdebye,box,pbc,ncells, &
        latflag,restartmode,mdlatxyz,fixzmin,fixzmax,fixperbrdr, &
        fixxybrdr,dsliceIn,ECMIn, Fzmaxz,dydtmaxz, seed)

    !     Initialize time variables

    if (iprint) call logger("Atom types used in time step calculation")
    deltat_fs = huge(0d0)
    do i = itypelow, itypehigh
        ! Only account for atoms really present in system.
        if (noftype(i) > 0 .or. (irec /= 0 .and. recatype == i)) then
            if (iprint) call logger("type:", element(i), i, 4)
            deltat_fs = min(deltat_fs, deltaini * timeunit(i))
        end if
    end do
    deltamax_fs = deltat_fs

    if (iprint) then
        call logger("Max. time step (fs):", deltamax_fs, 0)
    end if

    delta(:) = 1d0  ! Used in recoil creation.

    ! Give initial velocities to atoms.
    ! initemp > 0 for Gaussian, < 0 for same vel for all atoms.
    !
    ! At this stage, x1 does not have deltat units yet (see below)

    select case (latflag)
    case (LATFLAG_FILE_RESTART_GUESS_ION, LATFLAG_FILE_RESTART_NEW_ION)
        continue
    case default
        if (initemp /= 0.0) then
            call set_initial_velocities(initemp, myatoms, natoms, x1, box)
            if (iprint) then
                if (initemp > 0.0) then
                    call logger("Gaussian initial temperature (K):", initemp*invkBeV, 0)
                else
                    call logger("Equal initial temperature (K):", -initemp*invkBeV, 0)
                endif
            end if
        else
            x1(:3*myatoms) = 0d0
        end if
    end select

    ! Initialize arrays for all atoms.
    x0nei(:3*myatoms) = 0d0
    x2(:3*myatoms) = 0d0
    x3(:3*myatoms) = 0d0
    x4(:3*myatoms) = 0d0
    x5(:3*myatoms) = 0d0


    ! If no recindex is given, pick an index that is sure to be free.
    if (recindex < 0 .and. (irec == IREC_ADD_ATOM .or. rsnum > 0)) then
        recindex = maxval(atomindex(:myatoms)) + 1
        call my_mpi_max(recindex, 1)
    end if


    ! TODO: Change this from boolean flags to enums

    !     Potential initialization
    EAM = .false.
    EAMAL = .false.
    Stilweb = .false.
    Tersoff = .false.
    Tersoff_Compound = .false.
    EDIP = .false.
    Silica = .false.
    Silica_Wat = .false.
    Silica_Wat_rev = .false.
    BrennerBeardmore = .false.

    half_neighbor_list = .false.
    double_borders = .false.
    SortNeiList = .false.

    select case (potmode)
    case (0)
        call logger("Potmode 0 is not implemented.")
        call logger("For Lennard-Jones, create a reppot.*.*.in file with it included.")
        call my_mpi_abort("Lennard-Jones not implemented", 0)

    case (1, 2)
        if (iprint) call logger("EAM potential")
        EAM = .true.
        half_neighbor_list = .true.

        EAMAL = should_use_eamal(eamnbands, iac)
        if (EAMAL .and. iprint) call logger("EAM alloy variant")

    case (3, 4, 300:399)
        if (iprint) call logger("Stillinger-Weber potential")
        Stilweb = .true.

    case (5:8)
        if (iprint) call logger("Tersoff potential")
        Tersoff = .true.

    case (10:29, 40:99)
        if (iprint) call logger("Tersoff Compound potential")
        Tersoff_Compound = .true.

    case (30:39)
        if (iprint) call logger("EDIP Si potential")
        EDIP = .true.

    case (101)
        if (iprint) call logger("BrennerBeardmore potential")
        BrennerBeardmore = .true.
        double_borders = .true.

    case (200:209)
        if (iprint) call logger("Silica Ohta potential")
        Silica = .true.
        SortNeiList = .true.

    case (210, 211)
        if (iprint) call logger("Silica Watanabe potential")
        Silica_Wat = .true.

    case (212, 213)
        if (iprint) call logger("Silica Watanabe potential rev.")
        Silica_Wat_rev = .true.

        ! Do not sort neighbor list for Silica_Wat_rev!

        ! Now Watanabe uses BrennerBeardmore neighbor list style
        ! to treat Z correctly.
        ! TODO make sure the nprocs check is really correct.
        if (nprocs > 1) double_borders = .true.

    case default
        if (iprint) call logger("Unknown potmode:", potmode, 0)
        call my_mpi_abort('Unknown potmode', INT(potmode))
    end select


    ! Allocate arrays depending on eamnbands.
    if (EAMAL) then
        if (eamnbands <= 0) call my_mpi_abort("eamnbands <= 0", eamnbands)
    else
        eamnbands = 1
    end if
    allocate(dFpdp(NPMAX, eamnbands))
    allocate(P(NPMAX, eamnbands))
    allocate(Ethree(NPMAX, eamnbands))

    call my_mpi_barrier() ! Prevent interleaved prints.

    if (melstop > 0) then
        call elstop_init(melstopst, natoms)
    end if
    rcut = 0.0
    call reppot_init()

    ! Initialize potentials, including filling in rcut.
    if (debug) print *, 'Init potential', myproc, potmode
    if (EAM) then
        call Init_EAM_Pot(potmode, spline, eamnbands, EAMAL)
    endif
    if (Stilweb) then
        call Init_SW_Pot(potmode, reppotcutin, sw2mod, sw3mod)
    endif
    if (Tersoff) then
        call Init_Ter_Pot(rcut(1,1),potmode,reppotcutin)
    endif
    if (BrennerBeardmore) then
        call Beardmore_init(R1CC, R2CC, reppotcutin)
    endif
    if (EDIP) then
        call Init_Edip(reppotcutin)
    endif
    if (Silica) then
        ! rcut is set inside this
        call Init_Silica_Pot(reppotcutin)
    endif
    if (Silica_Wat) then
        ! rcut is set inside this
        call init_silica_wat()
    endif
    if (Silica_Wat_rev) then
        ! rcut is set inside this
        call map_silica_arrays()
    endif
    if (Tersoff_Compound) then
        call Init_Tersoff_Compound(potmode, reppotcutin)
    endif

    rcutmax = max(maxval(rcut), maxval(rcutin))

    call my_mpi_barrier() ! Prevent interleaved prints.

    if (iprint) then
        call logger()
        call logger("Atom type cut-offs:")
        do i=itypelow,itypehigh
            do j=itypelow,itypehigh
                call logger("Cutoff(i, j) =", rcut(i, j), i, j, 4)
            end do
        end do
        call logger()
    end if

    cut_nei = neiskinr * rcutmax
    maxdist  = cut_nei - rcutmax - 2d0 * timekt


    ! rmn(1:3) = min borders of this node, in scaled units
    ! rmx(1:3) = max borders of this node, in scaled units
    if (any(rmx(:) - rmn(:) < cut_nei / box(:))) then
        call logger_clear_buffer()
        call logger_append_buffer("Cell or node size too small compared to cut_nei in node", myproc, 0)
        call logger_append_buffer("min max, x:", rmn(1), rmx(1), 4)
        call logger_append_buffer("min max, y:", rmn(2), rmx(2), 4)
        call logger_append_buffer("min max, z:", rmn(3), rmx(3), 4)
        call logger_append_buffer("cut_nei:   ", cut_nei, 4)
        call logger_append_buffer("Reduce the number of nodes, or the cut_nei")
        call logger(log_buf)
        call my_mpi_abort('Cell too small', myproc)
    end if


    dh = product(box(:))
    dhprev = dh
    dhorig = dh
    box0(:) = box(:)

    unitcell(:) = box(:) / ncells(:)

    ! Initialize and allocate virial stuff.

    need_nondiag_vir = .false.
    need_avg_vir = .false.
    need_kinetic_vir = .false.
    select case (moviemode)
    case (5, 6)
        need_avg_vir = (avgvir /= 0)
    case (15, 16)
        need_avg_vir = (avgvir /= 0)
        need_nondiag_vir = .true.
    case (17, 18)
        need_avg_vir = (avgvir /= 0)
        need_nondiag_vir = .true.
        need_kinetic_vir = .true.
    end select

    if (need_avg_vir .or. need_kinetic_vir) then
        ! These modes modify the virials. Allocate second buffers for them.
        allocate(wxxi2(NPMAX), wyyi2(NPMAX), wzzi2(NPMAX))
        if (need_nondiag_vir) then
            allocate(wxyi2(NPMAX), wxzi2(NPMAX), wyzi2(NPMAX))
        end if
    else
        ! The virials are not modified. Point the copies to the originals
        ! to make the code simpler where wxxi2, etc. are used.
        wxxi2 => wxxi
        wyyi2 => wyyi
        wzzi2 => wzzi
        if (need_nondiag_vir) then
            wxyi2 => wxyi
            wxzi2 => wxzi
            wyzi2 => wyzi
        end if
    end if
    if (need_avg_vir) then
        allocate(wxxiavg(NPMAX), wyyiavg(NPMAX), wzziavg(NPMAX))
        if (need_nondiag_vir) then
            allocate(wxyiavg(NPMAX), wxziavg(NPMAX), wyziavg(NPMAX))
        end if
        call zero_avg_vir(wxxiavg, wyyiavg, wzziavg, &
            wxyiavg, wxziavg, wyziavg)
    end if

    ! Prepare ShuffleData instance.
    ! Remember to update buf/ibuf size when adding new arrays here.
    shuffle_data%x0 => x0
    shuffle_data%x1 => x1
    shuffle_data%x2 => x2
    shuffle_data%x3 => x3
    shuffle_data%x4 => x4
    shuffle_data%x5 => x5
    shuffle_data%x0nei => x0nei
    shuffle_data%atype => atype
    shuffle_data%atomindex => atomindex
    shuffle_data%numint = 2
    shuffle_data%numreal = 7 * 3
    if (allocated(wxxiavg)) then
        shuffle_data%wxxiavg => wxxiavg
        shuffle_data%wyyiavg => wyyiavg
        shuffle_data%wzziavg => wzziavg
        shuffle_data%numreal = shuffle_data%numreal + 3
        if (allocated(wxyiavg)) then
            shuffle_data%wxyiavg => wxyiavg
            shuffle_data%wxziavg => wxziavg
            shuffle_data%wyziavg => wyziavg
            shuffle_data%numreal = shuffle_data%numreal + 3
        end if
    end if


    Pxx = 0
    Pyy = 0
    Pzz = 0

    select case (latflag)
    case (LATFLAG_FILE_RESTART_GUESS_ION, LATFLAG_FILE_RESTART_NEW_ION)
        time_fs = restartt
    case default
        time_fs = 0d0
    end select
    time_thisrec = 0d0

    nmovie0 = nmovie

    ! Recoil sequence initializations
    rs_index=0
    rstmax=1d30
    recenmax = 1d0
    recatypemod=-1   ! If the variable is lower than zero, it will not be used.
                     ! mdcasc.f90 reads the variable from in/recoildata.in
    if (rsnum > 0 .and. rsmode > 0) then
        call read_recoil_sequence_data(rsnum)
    end if

    !***************************************************************************
    !---------------------------------------------------------------------------
    !               Loop over Recoil Events Begins Here
    !---------------------------------------------------------------------------
    !***************************************************************************

    100 continue ! Recoil loop starts here
    ! do while(.true.)      could possibly be exchanged by this do-loop
        if (rsnum > 0 .and. rsmode > 0) then
            rs_index = rs_index + 1
            if (iprint) then
                write(log_buf, "(A,A,I8,A)") new_line(''), &
                    ' -------------------- recoil ',rs_index,' --------------------- '
                call logger(log_buf)
            end if

            call getnextrec(rs_index,irec,xrec,yrec,zrec, &
                    rectheta,recphi,recen,rstmax,recatypemod)

            if (recatypemod >= 0) then
                ! Checking if the recatypemod was present in the in/recoildata.in
                ! file and using it for the recatype instead.
                recatype = recatypemod
            end if

            if (rstmax <= 0.0d0 .and. rsmode == 1) then
                call logger('ERROR: cannot handle <=0 time difference in &
                    &recoilspec for recoil', rs_index, 0)
                call my_mpi_abort('rstmax error', rs_index)
            end if
            if (rsmode == 2) then
                ! rstmax should be some large value just not to confuse
                ! time step selection
                rstmax = 1d30
            end if

            nmovie = nmovie0
            time_thisrec = 0.0
            force_update_pairtable = .true.
            if (iprint .and. rsmode /= 2) then
                call logger("This recoil tmax (fs): ", rstmax, 0)
            end if
        else if (irec /= IREC_NO_RECOIL) then
            if (iprint) call logger("Initializing recoil...")
        end if

        irecproc = -1
        iatrec = 0

        !
        !     irec modes:
        !     -2     Add an atom to the system at (xrec,yrec,zrec)
        !     -1     Search for nearest atom to given position
        !      0     No recoil event done
        !
        !     If latflag==3 and mode<0 find most energetic not sputtered
        !     atom and make it the recoil
        !
        if (irec /= IREC_NO_RECOIL .and. recen > 0.0) then
            call init_next_recoil()
        end if

        call my_mpi_barrier()

        if (rsnum>0 .and. rsmode==2) then
            if (rs_index < rsnum) goto 100
        endif


        select case (adaptivet)
        case (1)
            adaptive_dt = .true.
        case (0)
            adaptive_dt = .false.
        case (-1)
            adaptive_dt = (irec /= IREC_NO_RECOIL)
        case default
            call my_mpi_abort("Unknown adaptivet value", adaptivet)
        end select

        ! If enabling the adaptive time step without a recoil, we need to
        ! find the largest velocity to get the proper time step.
        if (firsttime .and. (irec == IREC_NO_RECOIL) .and. adaptive_dt) then
            vsq = vmax**2
            do i = 1, myatoms
                vsq = max(vsq, sum((x1(3*i-2 : 3*i) * box)**2))
            end do
            call my_mpi_max(vsq, 1)
            vmax = sqrt(vsq)
            vmaxnosput = vmax
        end if

        ! Get new timestep since adding a recoil certainly changed
        ! the largest velocity vmax.
        call Get_Tstep(adaptive_dt,vmax,0.0d0,timekt,timeEt,timeCh, &
            deltat_fs,deltamax_fs,delta,deltas,deltaratio,rstmax)

        if (adaptive_dt .and. iprint) then
            call logger_clear_buffer()
            call logger_append_buffer("At start of main loop:")
            call logger_append_buffer("tstep (fs):   ", deltat_fs, 4)
            call logger_append_buffer("v_max (A/fs): ", vmax, 4)
            call logger_write(trim(log_buf))
        end if

        if (firsttime) then
            ! Multiply by delta to get the real internal units.
            do i = 1, myatoms
                x1(3*i-2:3*i) = x1(3*i-2:3*i) * delta(abs(atype(i)))
            end do
        else
            ! Not the first recoil, so x1, ..., already contain delta.
            ! Divide by deltaratio to correct for the new timestep.
            x1(:3*myatoms) = x1(:3*myatoms) * deltaratio
            x2(:3*myatoms) = x2(:3*myatoms) * deltaratio**2
            x3(:3*myatoms) = x3(:3*myatoms) * deltaratio**3
            x4(:3*myatoms) = x4(:3*myatoms) * deltaratio**4
            x5(:3*myatoms) = x5(:3*myatoms) * deltaratio**5
        end if


        call my_mpi_barrier()

        if (iprint) then
            call logger()
            call logger("==================================================")
            call logger("Starting the main loop")
            call logger("==================================================")
            call logger()
        end if

        if (firsttime) tmr(TMR_TOTAL) = mpi_wtime()

        call mpi_barrier(mpi_comm_world, ierror)

        !***********************************************************************
        !
        !                  Main Loop Begins Here
        !
        !***********************************************************************
        main_loop: do

            tmain1=mpi_wtime()

            ! Calculate the pair table every nprtbl steps,
            ! or if xneimax+xneimax2 is greater than maxdist

            ! 19.3.2013 changes
            movietime_val=movietime(istep,nmovie,time_fs,tmovie,dtmovie, &
                            tmax,restartt)
            slicetime_val=slicetime(istep,time_fs,tslice,dtslice)

            if (istep >= dydttime) then
                if (dydtmaxz /= 0) then
                    do i=1,3*myatoms
                        ! Apply strain rate dy/dt
                        if (mod(i,3)==2) then
                            if (x0(i+1)*box(3) >= Fzmaxz) then
                                x0(i)=x0(i)+deltat_fs*dydtmaxz
                            endif
                        endif
                    enddo
                    if (movietime_val .or. slicetime_val) then
                        force_update_pairtable=.true.
                    endif
                endif
            endif


            ! Redivide the simulation box in case of large imbalance in
            ! atom distribution.
            if (nprocs > 1 .and. nbalance >= 0) then
                try_rebalance = .false.
                if (nbalance == 0 .and. istep == 0) try_rebalance = .true.
                if (nbalance > 0) then
                    if (mod(istep, nbalance) == 0) try_rebalance = .true.
                end if
                if (try_rebalance) then
                    call debugger("Balance")

                    help(1) = cut_nei
                    if (double_borders) help(1) = help(1) * 2d0

                    t1 = mpi_wtime()
                    call divide_simulation_box(x0, box, pbc, help(1), rebalance_done)
                    tmr(TMR_REBALANCE) = tmr(TMR_REBALANCE) + (mpi_wtime() - t1)

                    if (rebalance_done) then
                        ! Make sure all atoms reach their new homes safely.
                        t1 = mpi_wtime()
                        call Shuffle_all_to_all(shuffle_data, natoms, pbc)
                        tmr(TMR_SHUFFLE) = tmr(TMR_SHUFFLE) + (mpi_wtime() - t1)

                        call print_node_configuration()

                        force_update_pairtable = .true.
                    end if
                end if
            end if


            !
            ! This has to be in the beginning because of possible redistribution
            ! of atoms (otherwise the Ekin etc. calculations would go wrong)
            !

            ! TODO: move all debugs between a define. This way it should be
            ! possible to make the compiled code shorter.
            call debugger("Pair_table")

            if ( ((MOD(istep,nprtbl) .eq. 0) .and. (istep .ne. nsteps)) .or. &
                    (xneimax+xneimax2 .gt. maxdist) .or. &
                    force_update_pairtable) then

                !KN
                ! Shuffle atoms between nodes; this is necessary in cascades
                ! where atoms may move quite a lot
                ! To be really exact, the heat scaling should be redone, since
                ! shuffling the number of atoms changes the new heat values
                !
                force_update_pairtable = .false.
                call my_mpi_barrier()

                t1 = mpi_wtime()
                if (nprocs > 1) then
                    if (debug) print *, 'Shuffle',myproc,myatoms
                    times_shuffled = times_shuffled + 1
                    call Shuffle(shuffle_data, natoms, pbc, printnow)
                endif
                tmr(TMR_SHUFFLE) = tmr(TMR_SHUFFLE) + (mpi_wtime() - t1)

                t1=mpi_wtime()
                if (debug) then
                    if (iprint) then
                        call logger("Updating the pair table:")
                        call logger("Step:  ", istep, 4)
                        call logger("dxmax1:", xneimax, 4)
                        call logger("dxmax2:", xneimax2, 4)
                        call logger("limit: ", maxdist, 4)
                    end if
                endif

                call mpi_barrier(mpi_comm_world, ierror)

                call debugger("pairtable")

                call Pair_Table(nabors,nborlist,x0,cut_nei,box,pbc,x0nei, &
                    atomindex,half_neighbor_list,double_borders,SortNeiList,printnow)

                printnow=.false.
                tmr(TMR_NBOR) = tmr(TMR_NBOR) + (mpi_wtime()-t1)

                call debugger("pairtable done")

            endif


            ! track velocity adding
            ! Note that Get_Tstep is also called in there
            if (trackmode /= 0 .and. (.not. trackstopped) .and. &
                (trackstarted .or. time_fs > trackt)) then
                call addtrack(x0,x1,x2,x3,x4,x5,atype,box, &
                    trackstarted, trackstopped, &
                    trackx,tracky,trackt,trackmode, &
                    adaptive_dt,vmaxnosput,timekt,timeEt,timeCh, &
                    deltat_fs,deltamax_fs,delta,deltas,deltaratio,time_fs)
                trackstarted=.true.
            endif

            ! Adding a velocity/energy to atoms above zaddvel
            ! Note that Get_Tstep is also called in there
            if ((vaddvel > 0d0 .or. eaddvel > 0d0) .and. .not. doneaddvel .and. &
                    time_fs >= taddvel) then
                call addvel(x0, x1, x2, x3, x4, x5, atype, box, &
                    eaddvel, vaddvel, zaddvel, addvelt, addvelp, &
                    adaptive_dt, vmaxnosput, timekt, timeEt, timeCh, &
                    deltat_fs, deltamax_fs, delta, deltas, deltaratio)

                doneaddvel = .true.
            end if

            ! Temperature control

            call debugger("Temperature control")

            heat = 1
            heatbrdr = 1
            if (istep .gt. 0) then
                t1=mpi_wtime()
                if (TT_activated) then
                    ! Set mtemp, temp, trate, btctau, ... values according to
                    ! temperature-time program
                    call Set_Temp_Control(time_fs,mtemp,temp,temp0,trate, &
                        ntimeini,timeini,btctau)
                end if
                call Temp_Control(heat,mtemp,temp,toll,transmoving,poten,tote, &
                    heatbrdr,transbrdr,temp0,trate,deltat_fs,ntimeini, &
                    timeini,btctau,istep,time_fs,time_thisrec,rsnum)
                tmr(TMR_TEMP_CONTROL) = tmr(TMR_TEMP_CONTROL) + (mpi_wtime() - t1)
            endif

            ! Update positions and calculate the kinetic energy
            call debugger("Predict")

            t1=mpi_wtime()
            call Predict(x0,x1,x2,x3,x4,x5,atype, &
                trans,transin,natomsin,transv,heat,box,pbc,delta, &
                Emax,Emaxnosput,vmax,vmaxnosput,heatbrdr,transbrdr, &
                temp, btctau, deltat_fs, x0nei,xneimax,xneimax2, &
                Ekin,nbrdr,mtemp,timesputlim,nisputlim,Elosssum, &
                Emaxbrdr,Fzmaxz,FzmaxYz,dydtmaxz,fdamp,border_params, &
                Ethree(:,1),Epair)

            tmr(TMR_PREDICT) = tmr(TMR_PREDICT) + (mpi_wtime() - t1)

            !
            ! KN  See file forcesubs on some notes of the operation of these
            !

            if ((nsteps > 0 .and. istep + 1 .ge. nsteps) .or. &
                    (time_fs + deltat_fs .ge. tmax) .or. &
                    (rsnum>0 .and. rsmode==1 .and. &
                    time_thisrec+deltat_fs .ge. rstmax)) then
                last_step_predict = .true.
            endif


            ! Whether the force routines should calculate the non-diagonal virials
            ! this step.
            calc_vir_now = need_nondiag_vir .and. &
                (movietime_val .or. need_avg_vir .or. last_step_predict)

            ! All potentials require the communication of positions and atom types.
            ! Do it here in one place. Since atom type do not change (except for
            ! recstopz...) this means that other places do not have to exchange
            ! atom types. After this, x0 and atype are valid up to the index
            ! np0pairtable.
            if (nprocs > 1) then
                t1 = MPI_Wtime()
                call potential_pass_border_atoms(x0, atype)
                tmr(TMR_SEMICON_COMMS) = tmr(TMR_SEMICON_COMMS) + (mpi_wtime() - t1)
            end if


            !
            ! The actual force calculation. See subroutine below.
            !
            call compute_forces(calc_vir_now)


            Vpair = sum(Epair(:myatoms))
            Vmany = sum(Ethree(:myatoms, :eamnbands))

            ! BrennerBeardmore does not support pressures or virials.
            if (.not. BrennerBeardmore) then
                wxx = sum(wxxi(:myatoms))
                wyy = sum(wyyi(:myatoms))
                wzz = sum(wzzi(:myatoms))
            else
                wxx = 0
                wyy = 0
                wzz = 0
            end if

            ! Apply damping on forces
            if (damp /= 0d0) then
                xnp(:3*myatoms) = xnp(:3*myatoms) - damp * x1(:3*myatoms)
            end if

            nFzmax = 0
            if (FzmaxYz /= 0d0) then
                ! Apply zmax Yz force for C44 calc. (cf. Kittel)
                do i = 2, 3*myatoms, 3
                    if (x0(i+1)*box(3) >= Fzmaxz .or. &
                        (Fzmaxtyp >= 0 .and. atype(int((i+2)/3)) == Fzmaxtyp)) then

                        xnp(i) = xnp(i) + FzmaxYz/box(2)
                        nFzmax = nFzmax + 1
                    end if
                end do
            end if
            if (FzmaxZz /= 0d0) then
                ! Apply zmax Zz force for Y110 or Y111 calc. (cf. Hei98)
                do i = 3, 3*myatoms, 3
                    if (x0(i)*box(3) >= Fzmaxz .or. &
                        (Fzmaxtyp >= 0 .and. atype(int(i/3)) == Fzmaxtyp)) then

                        if (Fzmaxzr <= 0d0 .or. &
                            (sqrt(sum((x0(i-2:i-1) * box(1:2))**2)) < Fzmaxzr) .or. &
                            (Fzmaxtyp >= 0 .and. atype(int(i/3)) == Fzmaxtyp)) then

                            xnp(i) = xnp(i) + FzmaxZz/box(3)
                            nFzmax = nFzmax + 1
                        end if
                    end if
                end do
            end if

            if (debug) then
                print *, "Ftot", myproc, sum(xnp(:3*myatoms)**2)
            end if

            ! Apply random forces
            ! TODO is this supposed to be after the Ftot debug print?
            nrandom = 0
            if (prandom /= 0d0 .and. time_fs < timerandom) then
                do i = 1, 3*myatoms
                    if (MyRanf(0) < prandom) then
                        xnp(i) = xnp(i) * mrandom
                        nrandom = nrandom + 1
                    end if
                end do
            end if

            isum_array(1) = nrandom
            isum_array(2) = nFzmax
            call my_mpi_sum(isum_array, 2)
            nrandom = isum_array(1)
            nFzmax = isum_array(2)

            if (iprint) then
                if (FzmaxYz /= 0 .and. mod(istep,ndump*10) == 0) then
                    write(log_buf,*) "FzmaxYz ", FzmaxYz, " applied on ", &
                        nFzmax, " atoms"
                    call logger(log_buf)
                end if
                if (FzmaxZz /= 0 .and. mod(istep,ndump*10) == 0) then
                    write(log_buf, *) "FzmaxZz ", FzmaxZz, " applied on ", &
                        nFzmax, " atoms"
                    call logger(log_buf)
                    if (Fzmaxzr > 0) then
                        call logger("... inside r=", Fzmaxzr, 4)
                    end if
                end if
                if (prandom /= 0.0d0 .and. mod(istep,ndump*10) == 0) then
                    call logger("Random force applied (times): ", nrandom, 0)
                end if
            end if

            if (melstop /= 0) then
                ! Subtract the electronic stopping
                if (debug) print *,'elstop', melstop
                t1 = mpi_wtime()

                call elstop_loop(natoms, x0, x1, Ekin, atype, box, delta, irec, irecproc, &
                    melstop, melstopst, elstopmin, elstopsputlim, FDe, natelstop, natelstopfar)

                FDesum(:) = FDesum(:) + FDe(:)
                if (iprint .and. mod(istep, ndump * 10) == 0) then
                    write(log_buf, "(A,4(1X,F20.3),A,I8,A,I8,A)") &
                        "Total and last FDe = ", &
                        FDesum(1), FDesum(2), FDe(1), FDe(2), &
                        " eV for ", natelstop, " atoms including ", &
                        natelstopfar, " sputtered atoms."
                    call logger(log_buf)
                end if

                tmr(TMR_ELSTOP) = tmr(TMR_ELSTOP) + (mpi_wtime() - t1)
            endif

            !KN
            ! Get miscellaneous properties
            !

            if (debug) print *, 'miscellaneous properties', myproc, nborlist(1)
            t1=mpi_wtime()

            call get_maximum_force(Fmax, Fmaxnosput, myatoms, timesputlim, nisputlim, &
                box, xnp, x0, Epair, Ethree(:,1))

            if (irec /= 0 .and. nliqanal > 0) then
                call print_recoil_position(x0, Ekin, box, time_fs, Ekrec, &
                    istep, nliqanal, irec, irecproc)
            end if

            block
                ! Compute the energy center of mass (ECM) whenever needed.
                ! Never needed when ECMfix == 1, since then the user gives
                ! us the "ECM".
                logical :: ECM_needed
                real(real64b) :: Ekinsum

                if (ECMfix == 0) then
                    ECM_needed = .false.

                    ! Output slices are centered on the ECM.
                    if (slicetime_val .and. any(dslice > 0)) ECM_needed = .true.

                    ! Pressures.out shells are centered on the ECM.
                    if (ndefmovie > 0) then
                        if (mod(istep, ndefmovie) == 0) ECM_needed = .true.
                    end if

                    if (ECM_needed) then
                        call get_ecm(ECM, Ekinsum, x0, box, Ekin)

                        ! Need to print the position so that user knows where the
                        ! pressures.out shells are centered.
                        if (iprint) then
                            write(log_buf, "(A,F10.1,A,3F10.2,A,3F12.2)") &
                                't', time_fs,' ECM pos.', ECM,' Eksum', Ekinsum
                            call logger(log_buf)
                        end if
                    end if
                end if
            end block

            if (ndefmovie > 0) then
                if (mod(istep, ndefmovie) == 0) then
                    call print_pressures(natoms, x0, box, Ekin, &
                        wxxi, wyyi, wzzi, time_fs, ECM)
                end if
            end if

            ! TODO why not get rid of this sync?
            call mpi_barrier(mpi_comm_world, ierror)   ! Don't remove this sync !

            if (irec /= 0 .and. nliqanal > 0) then
                if (mod(istep, nliqanal) == 0) then
                    call liquid_analysis(time_fs,Ekdef,nabors,nborlist,x0,atomindex, &
                        Ekin,box,pbc,unitcell,latflag,half_neighbor_list)
                end if
            end if

            tmr(TMR_MISC_PROPERTIES) = tmr(TMR_MISC_PROPERTIES) + (mpi_wtime() - t1)

            istep=istep+1
            time_fs=time_fs+deltat_fs
            time_thisrec=time_thisrec+deltat_fs

            if (need_avg_vir) then
                call update_avg_vir( &
                    wxxiavg,wxxi,wyyiavg,wyyi,wzziavg,wzzi, &
                    wxyiavg,wxyi,wxziavg,wxzi,wyziavg,wyzi)
                if (need_kinetic_vir) then
                    call calc_kinetic_vir(natoms,box,atype,delta,x1, &
                        wxxiavg,wyyiavg,wzziavg,wxyiavg,wxziavg,wyziavg)
                end if
            end if

            !     OUTPUT OF MOVIE OR MOVIE SLICE
            IF(debug)PRINT *,'Pair_table & shuffle update for write',myproc

            ! 19.3.2013 Changes
            ! we can only call movietime and slicetime once per iteration as
            ! they save their state
            !
            ! movietime_val=movietime(istep,nmovie,time_fs,tmovie,dtmovie,
            !                         tmax,restartt)
            ! slicetime_val=slicetime(istep,time_fs,tslice,dtslice)
            !
            ! if (movietime_val .or. &
            !     (slicetime_val .and. &
            !     (dslice(1)>=0.0.or.dslice(2)>=0.0.or.dslice(3)>=0.0))) then
            !
            ! Shuffle atoms between nodes; this is necessary here to make sure
            ! atoms are in their processor box when writing out system to disk
            ! (in order to enable porper reading)
            !     if (nprocs > 1) then
            !         call Shuffle(shuffle_data, natoms, pbc, printnow)
            !
            !         call Pair_Table(nborlist,x0,cut_nei,box,pbc,x0nei, &
            !             atomindex,half_neighbor_list,SortNeiList,printnow)
            !     end if
            ! end if
            t1=mpi_wtime()
            if (debug) print *,'Movie',myproc

            if (movietime_val) then
                call movie_output()
            end if

            if (debug) print *,'Slice',myproc

            if (any(dslice(:) >= 0.0) .and. slicetime_val) then
                call mpi_barrier(mpi_comm_world, ierror)
                if (slicemode /= 9) then
                    if (iprint) then
                        call logger("Printing slice region at t=", time_fs, 0)
                    end if

                    call Slice_Movie(x0,x1,x2,delta,atype,Ekin,box,atomindex, &
                        dslice,ECM,istep,time_fs,slicemode)
                    if (iprint) call logger("... Done!", 1)
                else
                    if (iprint) then
                        call logger("Saving binary atom data at t=", time_fs, 1)
                    end if
                    call binarysave(2,myatoms,istep,atype,atomindex, &
                        time_fs,deltat_fs,box,x0,x1,wxxi,wyyi,wzzi,wxyi,wxzi,wyzi, &
                        Epair,Ethree(:,1),Ekin,delta,.true.,dslice,ECM)
                end if
            end if
            tmr(TMR_MAIN_OUTPUT) = tmr(TMR_MAIN_OUTPUT) + (mpi_wtime() - t1)

            dsum_array(1)=Vpair
            dsum_array(2)=Vmany
            dsum_array(3)=trans
            dsum_array(4)=transin
            dsum_array(5)=transbrdr
            dsum_array(6:8)=transv(:)

            call my_mpi_sum(dsum_array, 8)
            Vpair=dsum_array(1)
            Vmany=dsum_array(2)
            trans=dsum_array(3)
            transin=dsum_array(4)
            transbrdr=dsum_array(5)
            transv(:)=dsum_array(6:8)


            isum_array(1)=natomsin
            isum_array(2)=nbrdr
            call my_mpi_sum(isum_array, 2)
            natomsin=isum_array(1)
            nbrdr=isum_array(2)


            ! Modifications here by Heikki Ristolainen 20.5.2008
            ! This sums the forces acting on the atoms of atype=sumatype

            if (forcesum == 1 .and. (istep == 1 .or. mod(istep,ndump*10) == 0)) then
                sumf(:) = 0d0
                nsummed = 0

                do i = 1, myatoms
                    if (atype(i) == sumatype) then
                        i3 = 3*i - 3
                        sumf(i3+1:i3+3) = sumf(i3+1:i3+3) + xnp(i3+1:i3+3) * box(:)
                        nsummed = nsummed + 1
                    endif
                enddo

                call my_mpi_sum(sumf, 3)
                call my_mpi_sum(nsummed, 1)

                if (iprint) then
                    ! Print force
                    call logger(istep, time_fs, natoms, &
                        sumf(1), sumf(2), sumf(3), nsummed, 0)
                end if
            endif


            ! Correct particle positions (periodic boundary conditions)

            if (debug) print *,'Correct',myproc

            t1=mpi_wtime()
            call Correct(x0,x1,x2,x3,x4,x5,atype,xnp,pbc,deltas,x0nei, &
                box,mtemp,Fzmaxz,FzmaxYz,dydtmaxz)
            tmr(TMR_CORRECT) = tmr(TMR_CORRECT) + (mpi_wtime() - t1)

            !
            ! KN Calculate new time step
            !

            if (debug) then
                print *,'Get Tstep',myproc,vmax,vmaxnosput
            end if

            call Get_Tstep(adaptive_dt,vmaxnosput,Fmaxnosput,timekt,timeEt,timeCh, &
                deltat_fs,deltamax_fs,delta,deltas,deltaratio,rstmax)

            if (iprint .and. adaptive_dt .and. mod(istep,10*ndump) == 0) then
                call logger("Deltat (fs): ", deltat_fs, 0)
                call logger("Emax:", Emaxnosput, 4)
                call logger("vmax:", vmaxnosput, 4)
                call logger("Fmax:", Fmax, 4)
            end if

            !
            !  Correct x1,...,x5  units for the new time step
            !
            if (deltaratio /= 1) then
                x1(1:3*myatoms) = x1(1:3*myatoms) * deltaratio
                x2(1:3*myatoms) = x2(1:3*myatoms) * deltaratio**2
                x3(1:3*myatoms) = x3(1:3*myatoms) * deltaratio**3
                x4(1:3*myatoms) = x4(1:3*myatoms) * deltaratio**4
                x5(1:3*myatoms) = x5(1:3*myatoms) * deltaratio**5
            end if

            poten=Vmany+Vpair

            if (pscale>0) then
                call ConserveTotalMomentum(x1,natoms,pscale,delta,atype,box)
            endif

            !Remove total angular momentum every 'remrot' time steps
            if (remrot>0) then
                if (MOD(istep,remrot)==0) then
                    call RemoveAngularMomentum(x0,x1,delta,natoms,atype,box)
                endif
            endif

            avgpot = avgpot+poten
            avgkin = avgkin+trans
            if (nfixed /= natoms) then
                avgkinmoving = avgkinmoving+trans/(natoms-nfixed)*natoms
            endif
            avgenergy = avgenergy+poten+trans

            avgp(1)=avgp(1)+wxx/dh
            avgp(2)=avgp(2)+wyy/dh
            avgp(3)=avgp(3)+wzz/dh
            pt = eV_to_kbar * avgkin * 2d0 / 3d0 / istep / dh

            if (istep == 1 .or. MOD(istep,ndump*10) == 0) then
                help(1:2) = Elosssum(1:2)
                call my_mpi_sum(help, 2)

                if (iprint) then
                    write(log_buf, "(A,F12.2,A,F12.3,A,F12.3)") &
                        "btcloss at ", time_fs, " borders: ", &
                        help(1), " overall: ", help(2)
                    call logger(log_buf)

                    write(log_buf, "(A,F12.3,A,F12.3,A,F12.3)") "At t ", &
                        time_fs, " Ekin outside timesputlim ", trans-transin, &
                        " inside ", transin
                    call logger(log_buf)
                endif
            endif

            if (debug) print *,'avep',istep

            if (mod(istep,ndump*100) == 0) then
                help(1:3) = avgp(:)
                call my_mpi_sum(help, 3)
                pressure = sum(help(1:3)) / 3d0 / istep
                Pave = 1602 * pressure + pt

                if (iprint) then
                    write(log_buf, "(A30,F15.6,A,I10,A)") &
                        "Average pressure (kBar): ", Pave, &
                        " over ", istep, " steps"
                    call logger(log_buf, 2)

                    if (tmodtau /= 0.0 .and. time_fs > 10*abs(tmodtau)) then
                        write(log_buf, "(A,f12.2,A15,F12.4)") "time ", &
                            time_fs, " Average Tmoving (K): ", &
                            avgkinmoving/istep/natoms/1.5*invkBeV
                        call logger(log_buf)
                    end if
                end if
            endif

            call Press_Control(bpcbeta,bpctau,bpcmode,tmodtau,temp, &
                bpcP0,bpcP0x,bpcP0y,bpcP0z,wxx,wyy,wzz,Pxx,Pyy,Pzz, &
                istep,deltat_fs,time_fs,transv, &
                box,ncells,unitcell,dh,dhorig,dhprev,x1,x2,x3,x4,x5)

            if (bpcextz /= 0d0) then
                call Strain_Control(bpcextz, x1, x2, x3, x4, x5, box, box0, &
                    ncells, unitcell, time_fs, deltat_fs, tmodtau, temp, &
                    dh, dhorig, dhprev)
            end if
            if (iprint .and. (istep == 1 .or. MOD(istep,10*ndump) == 0)) then
                ! TODO should this print when no pressure control used?
                write(log_buf, "(A,3F10.4,A,3F9.3,F14.2)") "bpc P", &
                    Pxx, Pyy, Pzz, " sz", box(1), box(2), box(3), dh
                call logger(log_buf)

                if (tmodtau /= 0.0) then
                    call logger("tmodtau desired T", temp*invkBeV, 0)
                endif
            endif

            ! Total energy of single particle=tote

            transmoving = 0
            if (nfixed /= natoms) transmoving=trans/(natoms-nfixed)
            trans=trans/natoms
            if (natomsin>0) transin=transin/natomsin
            if (nbrdr>0) transbrdr=transbrdr/nbrdr
            poten=poten/natoms
            tote=trans+poten
            ! tempp = actual particle temperature FOR MOVING ATOMS
            tempp = 2 * transmoving / 3


            ! Stop the recoil atom if it goes below recstopz.
            if (recstoptype >= 0 .and. irecproc == myproc) then
                if (x0(3*irec) * box(3) < recstopz) then

                    if (atype(irec) /= recstoptype) then
                        call logger("Stopping recoil below z=", recstopz, 0)
                        atype(irec) = recstoptype
                    end if

                    x1(3*irec-2 : 3*irec) = 0.0
                    x2(3*irec-2 : 3*irec) = 0.0
                    x3(3*irec-2 : 3*irec) = 0.0
                    x4(3*irec-2 : 3*irec) = 0.0
                    x5(3*irec-2 : 3*irec) = 0.0
                end if
            end if


            if (istep == 1 .or. MOD(istep,ndump) == 0) then
                t1=mpi_wtime()
                if (firstprint .and. iprint) then
                    write(log_buf,"(7A10)") "Step", "Time", "Atoms", &
                        "Temp", "Epot_ave", "Total En", "Change"
                    call logger(log_buf)
                endif

                if (firstprint) tote0 = tote

                if (iprint) then
                    write(log_buf, "(A,1X,I11,1X,F15.2,I10,1X,4F13.4)") &
                        "ec", istep, time_fs, natoms, tempp*invkBeV, &
                        poten, tote, tote-tote0
                    call logger(log_buf)
                endif


                firstprint = .false.
                printnow = .true.
                if (iprint .and. (istep == 1 .or. MOD(istep,10*ndump) == 0)) then
                    ! print results for atoms inside only
                    write(log_buf, "(A,1X,I11,1X,F15.2,I10,1X,4F13.4)") &
                        "ei", istep, time_fs, natomsin, &
                        2 * transin / 3 * invkBeV, &
                        poten, transin+poten, transin+poten-tote0
                    call logger(log_buf)
                endif

                tmr(TMR_MAIN_OUTPUT) = tmr(TMR_MAIN_OUTPUT) + (mpi_wtime() - t1)
            endif

            if (nrestart > 0 .and. istep > 1 .and. &
                    MOD(istep,nrestart) == 0) then
                ! Shuffle atoms between nodes; this is necessary here to make
                ! sure atoms are in their processor box
                call mpi_barrier(mpi_comm_world, ierror)
                if (nprocs > 1) then
                    t1=mpi_wtime()
                    times_shuffled = times_shuffled + 1
                    call Shuffle(shuffle_data, natoms, pbc, printnow)
                    tmr(TMR_SHUFFLE) = tmr(TMR_SHUFFLE) + (mpi_wtime() - t1)

                    t1=mpi_wtime()
                    call Pair_Table(nabors,nborlist,x0,cut_nei,box,pbc,x0nei, &
                        atomindex,half_neighbor_list,double_borders,SortNeiList,printnow)
                    tmr(TMR_NBOR) = tmr(TMR_NBOR) + (mpi_wtime() - t1)
                end if

                t1=mpi_wtime()

                if (iprint) then
                    call logger("Restart:")
                    call logger("time:", time_fs, 4)
                    call logger("size:", box(:), 4)
                end if

                if (restartmode /= 9) then
                    call write_mdlat_out(x0, x1, box, delta, atype, atomindex)
                else
                    call binarysave(3,myatoms,istep,atype,atomindex, &
                        time_fs,deltat_fs,box,x0,x1,wxxi,wyyi,wzzi,wxyi,wxzi,wyzi, &
                        Epair,Ethree(:,1),Ekin,delta,.false.)
                end if
                tmr(TMR_MAIN_OUTPUT) = tmr(TMR_MAIN_OUTPUT) + (mpi_wtime() - t1)
            endif

            if (debug) then
                print *, "----------------------", myproc
                print *, ""
            end if

            firsttime = .false.

            tmr(TMR_MAIN_LOOP) = tmr(TMR_MAIN_LOOP) + (mpi_wtime() - tmain1)

            !     Check various ending criteria
            if (nsteps > 0 .and. istep >= nsteps) goto 998
            if (time_fs >= tmax) exit main_loop
            if (tempp < endtemp) exit main_loop
            if (rsnum>0 .and. rsmode==1 .and. time_thisrec >= rstmax) exit main_loop

            ! Stop if all atoms are below the minimum height zmin (Angstrom)
            if (zmin /= -1d30) then
                z_height = box(3) * maxval(x0(3 : 3*myatoms : 3))
                call my_mpi_max(z_height, 1)
                if (z_height <= zmin) exit main_loop
            end if

        end do main_loop
        !***********************************************************************
        !
        !          Main Loop Ends Here
        !
        !***********************************************************************

        if (rsnum > 0 .and. rsmode==1) then
            if (rs_index < rsnum) goto 100
        endif

    998 continue
    ! enddo ! Recoil adding loop, could possibly be replaced by this do-loop
    !***************************************************************************
    !
    !               Loop over Recoil Events Ends Here
    !
    !***************************************************************************

    ! Finish main timer

    tmr(TMR_TOTAL) = mpi_wtime() - tmr(TMR_TOTAL)
    ! TODO is this "if" even possible??
    if (tmr(TMR_TOTAL) <= 0) then
        tmr(TMR_TOTAL) = tmr(TMR_MAIN_LOOP)
    end if

    ! Print out final averages

    if (iprint) then
        if (bpcbeta /= 0.0) then
            write(log_buf, "(A,3F10.4,A,3F9.3,F14.2)") "bpc P", &
                Pxx, Pyy, Pzz, " sz", box(1), box(2), box(3), dh
            call logger(log_buf)
        endif

        write(log_buf, "(A,1X,I11,1X,F15.2,I10,1X,4F13.4)") &
            "ec", istep, time_fs, natoms, tempp*invkBeV, &
            poten, tote, tote-tote0
        call logger(log_buf)
    endif

    call mpi_barrier(mpi_comm_world, ierror)

    if (iprint) then
        call logger()
        call logger("Done with main loop.", 1)
        call logger()
    end if

    if (nmovie > 0) then
        call movie_output()

        call movie_finalize(moviemode)

        if (iprint) then
            if (moviemode == 9) then
                call logger("Saved all binary atom data", 1)
            else
                call logger("Done saving positions to md.movie", 1)
            end if
        end if
    endif

    call slice_finalize()

    if (final_xyz) then
        call write_xyz_frame(final_xyz_path, &
            x0,x1,xnp,delta,atype,atomindex,natoms,box,pbc,istep, &
            time_fs,Epair,Ethree(:,1),Ekin,wxxi,wyyi,wzzi, &
            wxyi,wxzi,wyzi, &
            moviemode,outtype,outzmin,outzmax, &
            outzmin2,outzmax2,virsym,virkbar,virboxsiz)
    end if

    if (pressures_out_open) close(OUT_PRESSURES_FILE)
    if (liquidat_out_open) close(OUT_LIQUIDAT_FILE)
    if (recoilat_out_open) close(OUT_RECOIL_FILE)

    if (nrestart >= 0) then
        if (iprint) then
            call logger("Restart:")
            call logger("time:", time_fs, 4)
            call logger("size:", box(:), 4)
        end if

        if (restartmode /= 9) then
            call write_mdlat_out(x0, x1, box, delta, atype, atomindex)

            call write_atoms_out(x0, box, atype, atomindex, &
                Ethree(:,1), P, Epair, EAM, wxxi, wyyi, wzzi)
        else
            call binarysave(3,myatoms,istep,atype,atomindex, &
                time_fs,deltat_fs,box,x0,x1,wxxi,wyyi,wzzi,wxyi,wxzi,wyzi, &
                Epair,Ethree(:,1),Ekin,delta,.false.)
        end if

        if (iprint) then
            call logger("Done writing out atoms.out and mdlat.out", 1)
            call logger()
        endif
    endif

    call my_mpi_sum(avgp, 3)
    call my_mpi_sum(Elosssum, 2)

    if (iprint) then
        factor = 1d0 / natoms / istep
        avgtemp=avgkin/1.5*factor
        avgtempmoving=avgkinmoving/1.5*factor
        avgkin=avgkin*factor
        pt=(natoms*1602*avgtemp)/dh

        call logger()
        call logger("Final potential energy:  ", poten*natoms,4)
        call logger("Average total energy:    ", avgenergy*factor, 4)
        call logger("Average kinetic energy:  ", avgkin, 4)
        call logger("Average potential energy:", avgpot*factor, 4)
        call logger("Average temperature (K): ", avgtemp*invkBeV, 4)
        call logger("Average temp_moving (K): ", avgtempmoving*invkBeV, 4)
        call logger("Total electronic stop.:  ", FDesum(1), FDesum(2), 4)
        call logger("Total btc energy losses: ", Elosssum(1), Elosssum(2), 4)
        call logger()

        call logger("Average sigma_xx (kBar): ", 1602*avgp(1)/istep+pt, 4)
        call logger("Average sigma_yy (kBar): ", 1602*avgp(2)/istep+pt, 4)
        call logger("Average sigma_zz (kBar): ", 1602*avgp(3)/istep+pt, 4)

        pressure = sum(avgp(1:3)) / 3d0 / istep

        write(log_buf, "(G17.6,A,I10,A)") 1602*pressure+pt, " over ", &
            istep, " steps"
        call logger("Average pressure (kBar): ", trim(log_buf), 4)
        call logger()
    endif


    ! Output timing information
    if (iprint) call logger("Number of times Shuffle called:", times_shuffled, 0)
    tmr(TMR_MAIN_OTHER) = tmr(TMR_TOTAL) - sum(tmr(TMR_TEMP_CONTROL : TMR_MAIN_OTHER-1))
    tmr(TMR_COMMS_TOTAL) = sum(tmr(TMR_NBOR_COMMS : TMR_COMMS_TOTAL-1)) + tmr(TMR_SHUFFLE)
    if (iprint .and. (tmr(TMR_TOTAL) /= 0.0d0) ) call print_timers("out/time")
    if (nprocs > 1) then
        call my_mpi_sum(tmr, size(tmr))
        tmr(:) = tmr(:) / nprocs
        if (iprint .and. (tmr(TMR_TOTAL) /= 0.0d0) ) call print_timers("out/time-avg")
    end if
    if (iprint) then
        write(log_buf, "(A,1X,I8,1X,A,1X,I9,1X,A,1X,F10.2,1X,A,1X,E9.4,1X,A)") &
            "For", natoms, "atoms and", istep, "steps used", tmr(TMR_TOTAL), "s =>", &
            tmr(TMR_TOTAL)/natoms/istep, "s/step/nat"
        call logger(log_buf)
    end if

    if (debug) print *, "Final cleanup", myproc


    ! Clean up the logger, has to be done before MPI_FINALIZE
    call logger_finalize()


    call my_mpi_barrier()
    call mpi_finalize(ierror)

    if (iprint .and. zipout > 0) then
        call system("tar czf out.tgz out")
    end if


  contains


    !
    ! Given EAM=.true., return whether the alloy variant of the
    ! potential should be used.
    !
    ! The conditions here are gathered from various calls to mpi_abort
    ! in the elemental (non-EAMAL) variant.
    !
    logical function should_use_eamal(eamnbands, iac) result(eamal)
        integer, intent(in) :: eamnbands, iac(:,:)

        integer :: i, j, num

        num = 0
        eamal = .false.

        if (eamnbands > 1) eamal = .true.

        do i = lbound(iac, dim=1), ubound(iac, dim=1)
            do j = lbound(iac, dim=2), ubound(iac, dim=2)
                if (iac(i,j) == 3) eamal = .true.

                if (iac(i,j) == 1 .or. iac(i,j) == 4 .or. iac(i,j) == 6) then
                    if (i /= j) eamal = .true.
                    num = num + 1
                    if (num > 1) eamal = .true.
                end if
            end do
        end do
    end function should_use_eamal


    ! Call the appropriate force computation routine.
    subroutine compute_forces(calc_nondiag_vir_now)
        logical, intent(in) :: calc_nondiag_vir_now

        real(real64b) :: t1

        if (EAM) then
            ! Begin EAM force calculation

            call debugger("Calc_P")

            t1=mpi_wtime()
            if (EAMAL) then
                call Calc_P_eamal(P, x0,atype,box,pbc,nborlist,rcutmax,eamnbands)
            else
                call Calc_P_eam(P, x0,atype,box,pbc,nborlist,spline)
            end if
            tmr(TMR_EAM_CHARGE) = tmr(TMR_EAM_CHARGE) + (mpi_wtime() - t1)

            ! Get the embedding energy F and its derivative for each atom

            call debugger("Calc_Fp")

            t1=mpi_wtime()
            if (EAMAL) then
                call Calc_Fp_eamal(atype,Ethree,dFpdp, P,Fpextrap,eamnbands)
            else
                call Calc_Fp_eam(Ethree,dFpdp, P,spline,Fpextrap)
            end if
            tmr(TMR_EAM_EMBED) = tmr(TMR_EAM_EMBED) + (mpi_wtime() - t1)

            ! Calculate the force on each atom

            call debugger("Calc_Force")

            t1=mpi_wtime()
            if (EAMAL) then
                call Calc_Force_eamal(x0,xnp,Epair,atype, &
                    wxxi,wyyi,wzzi,wxyi,wxzi,wyzi, &
                    dFpdp,box,pbc,nborlist, &
                    rcutmax,eamnbands,calc_nondiag_vir_now)
            else
                call Calc_Force_eam(x0,xnp,Epair,atype, &
                    wxxi,wyyi,wzzi,wxyi,wxzi,wyzi, &
                    dFpdp,box,pbc,nborlist, &
                    rcutmax,spline,calc_nondiag_vir_now)
            end if

            ! Convert virials from eV/box**2 to eV.
            wxxi(:myatoms) = wxxi(:myatoms) * box(1)**2
            wyyi(:myatoms) = wyyi(:myatoms) * box(2)**2
            wzzi(:myatoms) = wzzi(:myatoms) * box(3)**2
            if (calc_nondiag_vir_now) then
                wxyi(:myatoms) = wxyi(:myatoms) * box(1)*box(2)
                wxzi(:myatoms) = wxzi(:myatoms) * box(1)*box(3)
                wyzi(:myatoms) = wyzi(:myatoms) * box(2)*box(3)
            end if
            tmr(TMR_EAM_FORCE) = tmr(TMR_EAM_FORCE) + (mpi_wtime() - t1)

        endif

        if (Stilweb) then

            call debugger("Stilweb")

            t1=mpi_wtime()
            call Stilweb_Force(x0,atype,xnp,box,pbc, &
                nborlist,Epair,Ethree(:,1), &
                wxxi,wyyi,wzzi,wxyi,wxzi,wyzi,calc_nondiag_vir_now)
            tmr(TMR_SEMICON_FORCE)=tmr(TMR_SEMICON_FORCE)+(mpi_wtime()-t1)
        endif

        if (Tersoff) then

            call debugger("Tersoff")

            t1=mpi_wtime()
            call Tersoff_Force(x0,atype,xnp,box,pbc, &
                nborlist,Epair,Ethree(:,1), &
                wxxi,wyyi,wzzi,wxyi,wxzi,wyzi,calc_nondiag_vir_now)
            tmr(TMR_SEMICON_FORCE)=tmr(TMR_SEMICON_FORCE)+(mpi_wtime()-t1)
        endif

        if (BrennerBeardmore) then
            call debugger("BrennerBeardmore")

            t1=mpi_wtime()
            call Beardmore_Force(x0,atype,xnp,box,pbc, &
                nborlist,Epair, &
                bondstat,bondstat2,bondstat3,reppotcutin)
            tmr(TMR_SEMICON_FORCE)=tmr(TMR_SEMICON_FORCE)+(mpi_wtime()-t1)
        endif

        if (Tersoff_Compound) then

            call debugger("Tersoff_Compound")

            t1=mpi_wtime()
            call Tersoff_Compound_Force(x0,atype,xnp,box, &
                pbc,nborlist,Epair,Ethree(:,1), &
                wxxi,wyyi,wzzi,wxyi,wxzi,wyzi,calc_nondiag_vir_now)
            tmr(TMR_SEMICON_FORCE)=tmr(TMR_SEMICON_FORCE)+(mpi_wtime()-t1)
        endif

        if (EDIP) then

            call debugger("Edip")

            t1=mpi_wtime()
            call EDIP_forces(x0,atype,xnp,box,pbc, &
                nborlist,Epair,Ethree(:,1), &
                wxxi,wyyi,wzzi,wxyi,wxzi,wyzi,calc_nondiag_vir_now)
            tmr(TMR_SEMICON_FORCE)=tmr(TMR_SEMICON_FORCE)+(mpi_wtime()-t1)
        endif

        if (Silica) then

            call debugger("Silica")

            t1=mpi_wtime()
            call Silica_Force(x0,atype,xnp,box,pbc, &
                nborlist,Epair,Ethree(:,1), &
                wxxi,wyyi,wzzi,wxyi,wxzi,wyzi,calc_nondiag_vir_now)
            tmr(TMR_SEMICON_FORCE)=tmr(TMR_SEMICON_FORCE)+(mpi_wtime()-t1)
        endif

        if (Silica_Wat) then

            call debugger("Watanabe")

            t1=mpi_wtime()
            call silica_wat_force(x0,atype,xnp,box,pbc, &
                nborlist,nabors,Epair,Ethree(:,1), &
                wxxi,wyyi,wzzi,wxyi,wxzi,wyzi, &
                potmode,reppotcutin,calc_nondiag_vir_now)
            tmr(TMR_SEMICON_FORCE)=tmr(TMR_SEMICON_FORCE)+(mpi_wtime()-t1)
        endif

        if (Silica_Wat_rev) then

            call debugger("Watanabe rev")

            t1=mpi_wtime()
            call silica_wat_force_rev(x0,atype,xnp,box,pbc, &
                nborlist,Epair,Ethree(:,1), &
                wxxi,wyyi,wzzi,wxyi,wxzi,wyzi, &
                reppotcutin,calc_nondiag_vir_now)
            tmr(TMR_SEMICON_FORCE)=tmr(TMR_SEMICON_FORCE)+(mpi_wtime()-t1)
        endif

    end subroutine compute_forces


    subroutine init_next_recoil()

        real(real64b) :: r, rmin
        real(real64b) :: rec_pos(3)
        real(real64b) :: v, vm
        real(real64b) :: help
        integer :: i

        character(1), parameter :: dimnames(3) = ["x", "y", "z"]

        select case (irec)
        case (IREC_NEAREST, IREC_ADD_ATOM)
            continue
        case default
            call my_mpi_abort('only irec modes -2, -1, and 0 are supported!', irec)
        end select


        if (latflag == LATFLAG_FILE_RESTART_GUESS_ION) then
            if (iprint) call logger("RESTART mode, searching for atom with highest energy")

            ! For restart, we must find the maximum energy atom and
            ! pretend it was the recoil atom to get the correct energy.
            ! TODO: why energy and not velocity (different atom masses...)
            vm = -huge(0d0)
            do i = 1, myatoms
                v = sqrt(sum((x1(3*i-2 : 3*i) * box)**2))
                if (v > vm) then
                    irec = i
                    iatrec = atomindex(i)
                    vm = v
                end if
            end do
            call getmaxofallprocs(vm, irecproc)
            recen = 0.5d0 * vm**2

            if (myproc == irecproc) then
                write(log_buf, "(A,G0.5,A,G0.5,A)") &
                    "Highest energy ", recen, " eV, velocity ", &
                    vm / delta(abs(atype(irec))) * vunit(abs(atype(irec))), " A/fs"
                call logger(log_buf)
            end if

        else ! latflag /= LATFLAG_FILE_RESTART_GUESS_ION

            if (recen > 1.0d0 .and. iprint) then
                select case (mtemp)
                case (1, 6, 9)
                    call logger()
                    call logger()
                    call logger("WARNING: TEMPERATURE MODE", mtemp, 0)
                    call logger("CHOSEN WITH CASCADE. IS THIS OK?")
                    call logger()
                end select
            end if

            if (irec == IREC_NEAREST) then
                ! Pick the atom that is nearest to (xrec, yrec, zrec).

                rec_pos(1) = xrec
                rec_pos(2) = yrec
                rec_pos(3) = zrec
                if (iprint) call logger("Searching for atom nearest to", rec_pos, 0)

                rmin = huge(0d0)
                do i = 1, myatoms
                    r = sqrt(sum((x0(3*i-2 : 3*i) * box(:) - rec_pos(:))**2))
                    if (r < rmin) then
                        if (recatype < 0 .and. atype(i) /= abs(recatype)) cycle
                        irec = i
                        iatrec = atomindex(i)
                        rmin = r
                    end if
                end do
                call getminofallprocs(rmin, irecproc)

            else ! IREC_ADD_ATOM
                if (iprint) call logger("Creating new atom as recoil")

                natoms = natoms + 1

                rec_pos(1) = xrec / box(1)
                rec_pos(2) = yrec / box(2)
                rec_pos(3) = zrec / box(3)

                do i = 1, 3
                    if (pbc(i) == 1d0 .and. &
                        (rec_pos(i) < -0.5d0 .or. rec_pos(i) >= 0.5d0)) then

                        call logger("ERROR: Can't place recoil outside cell")
                        call logger("with periodic " // dimnames(i), rec_pos(i), 0)
                        call my_mpi_abort(dimnames(i) // ' rec outside', int(rec_pos(i)))
                    end if
                end do

                ! Figure out to which node the recoil atom should be added.
                ! Note: for non-periodic edges of the simulation cell, rmn/rmx
                ! of the nodes at the edges go to near infinity.
                if (all(rec_pos >= rmn .and. rec_pos < rmx)) then
                    myatoms = myatoms + 1

                    iatrec = recindex
                    irec = myatoms
                    irecproc = myproc

                    x0(3*irec-2 : 3*irec) = rec_pos
                    atype(irec) = recatype
                    atomindex(irec) = iatrec

                    x0nei(3*irec-2 : 3*irec) = 0d0
                    x1(3*irec-2 : 3*irec) = 0d0
                    x2(3*irec-2 : 3*irec) = 0d0
                    x3(3*irec-2 : 3*irec) = 0d0
                    x4(3*irec-2 : 3*irec) = 0d0
                    x5(3*irec-2 : 3*irec) = 0d0
                end if

                recindex = recindex + 1

                ! Since irecproc > 0 only in one processor, use max.
                call my_mpi_max(irecproc, 1)
            end if

        end if ! latflag

        ! Distribute irecproc and irec to every processor.
        call my_mpi_bcast(irec,   1, irecproc)
        call my_mpi_bcast(iatrec, 1, irecproc)


        m = myatoms
        call my_mpi_sum(m, 1)
        if (m /= natoms) then
            if (iprint) then
                call logger("After RECOIL atom selection:")
                call logger("Total number of atoms in nodes")
                call logger("is not what it should be", 4)
            end if
            write(log_buf, *) myproc, natoms, m, irec, irecproc
            call logger(log_buf)
            call my_mpi_abort('natoms wrong in main', myproc)
        end if

        if (myproc == irecproc .and. irec > myatoms) then
            call logger("ERROR: irec > myatoms")
            call logger("irec:   ", irec, 4)
            call logger("myatoms:", myatoms, 4)
            call logger("node:   ", irecproc, 4)
            call my_mpi_abort('irec > myatoms', myproc)
        end if


        if (myproc == irecproc) then
            write(log_buf, '(A,I0,A,I0,A,I0)') &
                "Recoil is atom ", iatrec, " (internal ", irec, ") on node ", irecproc
            call logger(log_buf)
            call logger("Recoil position (A):", x0(irec*3-2:irec*3)*box(:), 0)
        end if

        !
        ! Handle the recoil atom type.
        !
        if (myproc == irecproc) then
            if (recatype >= 0 .and. latflag /= LATFLAG_FILE_RESTART_GUESS_ION) then
                if (recatypem == 0) then
                    call logger_clear_buffer()
                    call logger_append_buffer("Recoil atom type originally was", atype(irec), 0)
                    call logger_append_buffer("Setting recoil atom type to", recatype, 0)
                    call logger_write(trim(log_buf))
                    atype(irec) = recatype
                else
                    recatype = atype(irec)
                    call logger_clear_buffer()
                    call logger_append_buffer("recatypem /= 0, so not changing recatype")
                    call logger_append_buffer("Recoil atom has type", recatype, 0)
                    call logger_write(trim(log_buf))
                end if
            else
                recatype = atype(irec)
                call logger("Recoil atom has type", recatype, 0)
            end if
        end if
        if (nprocs > 1) then
            if (myproc /= irecproc) recatype = -1
            call my_mpi_max(recatype, 1)
        end if
        if (recatype < 0) then
            call logger("Recoil atom fixed, are you crazy?", recatype, 0)
            call my_mpi_abort('recoil atom fixed', recatype)
        end if
        if (recatype < itypelow .or. recatype > itypehigh) then
            call logger("Recoil atom type does not exist, are you mad?", recatype, 0)
            call my_mpi_abort('recoil atom type nonexistent', recatype)
        end if


        ! Update the largest velocity for choosing the time step.
        v = sqrt(2 * recen) * vunit(recatype)
        vmax = max(vmax, v)
        vmaxnosput = vmax


        if (latflag /= LATFLAG_FILE_RESTART_GUESS_ION) then
            ! Give recoil atom located above recoil velocity.

            if (myproc == irecproc) then
                ! In internal units, mass == 1!
                ! If the time step has not yet been initialized, delta == 1
                ! and will be changed later.
                help = delta(recatype) * sqrt(2 * recen)
                x1(irec*3-2) = help * sin(rectheta) * cos(recphi) / box(1)
                x1(irec*3-1) = help * sin(rectheta) * sin(recphi) / box(2)
                x1(irec*3-0) = help * cos(rectheta) / box(3)
                Ekin(irec) = recen
                Emax = recen
                Emaxnosput = recen

                write(log_buf, "(A,G0.5,A,G0.5,A,G0.5)") &
                    "Recoil energy ", recen, " eV, theta ", rectheta, ", phi ", recphi
                call logger(log_buf)
                call logger("Recoil velocity (A/fs):", &
                    x1(irec*3-2:irec*3)*box(:)/delta(recatype)*vunit(recatype), 0)
            end if
        end if

    end subroutine init_next_recoil


    subroutine movie_output()
        integer :: i

        if (moviemode /= 9) then
            if (iprint) then
                call logger("Saving positions to md.movie at t=", time_fs, 1)
            end if

            if (need_avg_vir) then
                ! This overwrites wxxi2, ...
                call avg_calc_vir(istep, vir_istep_old, &
                    wxxiavg, wxxi2, wyyiavg, wyyi2, wzziavg, wzzi2, &
                    wxyiavg, wxyi2, wxziavg, wxzi2, wyziavg, wyzi2)
            else if (need_kinetic_vir) then
                ! Writing these loops instead of array syntax avoids an
                ! unnecessary temporary array on Gfortran.
                do i = 1, myatoms ; wxxi2(i) = wxxi(i) ; end do
                do i = 1, myatoms ; wyyi2(i) = wyyi(i) ; end do
                do i = 1, myatoms ; wzzi2(i) = wzzi(i) ; end do
                if (need_nondiag_vir) then
                    do i = 1, myatoms ; wxyi2(i) = wxyi(i) ; end do
                    do i = 1, myatoms ; wxzi2(i) = wxzi(i) ; end do
                    do i = 1, myatoms ; wyzi2(i) = wyzi(i) ; end do
                end if
                call calc_kinetic_vir(natoms,box,atype,delta,x1, &
                    wxxi2,wyyi2,wzzi2,wxyi2,wxzi2,wyzi2)
            else
                ! wxxi2 points to wxxi, no need to copy.
            end if

            vir_istep_old = istep

            call Movie(x0,x1,xnp,delta,atype,atomindex,natoms,box,pbc, &
                istep,time_fs,Epair,Ethree(:,1),Ekin, &
                wxxi2,wyyi2,wzzi2,wxyi2,wxzi2,wyzi2, &
                moviemode,outtype, &
                outzmin,outzmax,outzmin2,outzmax2, &
                virsym,virkbar,virboxsiz)

            if (need_avg_vir) then
                call zero_avg_vir(wxxiavg,wyyiavg,wzziavg, &
                    wxyiavg,wxziavg,wyziavg)
            end if

        else
            if (iprint) then
                call logger("Saving binary atom data at=", time_fs, 1)
            end if
            call binarysave(1,myatoms,istep,atype,atomindex, &
                time_fs,deltat_fs,box,x0,x1,wxxi,wyyi,wzzi,wxyi,wxzi,wyzi, &
                Epair,Ethree(:,1),Ekin,delta,.false.)
        end if

        if (iprint) call logger("... Done!", 1)

    end subroutine movie_output


    subroutine print_license_header()

        call logger("**************************************************************************")
        call logger("* PARCAS MD  - PARallell CAScade Molecular Dynamics code                 *")
        call logger("*                                                                        *")
        call logger("* Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,      *")
        call logger("* University of Helsinki                                                 *")
        call logger("*                                                                        *")
        call logger("* This program is free software: you can redistribute it and/or modify   *")
        call logger("* it under the terms of the GNU General Public License as published by   *")
        call logger("* the Free Software Foundation, either version 3 of the License, or      *")
        call logger("* (at your option) any later version.                                    *")
        call logger("*                                                                        *")
        call logger("* This program is distributed in the hope that it will be useful,        *")
        call logger("* but WITHOUT ANY WARRANTY; without even the implied warranty of         *")
        call logger("* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *")
        call logger("* GNU General Public License for more details.                           *")
        call logger("*                                                                        *")
        call logger("* You should have received a copy of the GNU General Public License      *")
        call logger("* along with this program.  If not, see <https://www.gnu.org/licenses/>. *")
        call logger("**************************************************************************")

    end subroutine print_license_header


    subroutine print_timers(filename)
        character(*), intent(in) :: filename

        real(real64b) :: tsec
        integer :: ihours, imin

        call time_logger_init(filename, tmr(TMR_TOTAL), istep)

        call time_logger("****************************************************")

        write(log_buf, "(5(I0,A))") &
            natoms, " atoms on ", nprocs, " = ", &
            nnodes(1), " x ", nnodes(2), " x ", nnodes(3), " nodes"
        call time_logger(log_buf)
        call time_logger()

        ihours = int(tmr(TMR_TOTAL) / 3600)
        imin = int((tmr(TMR_TOTAL) - ihours*3600d0) / 60)
        tsec = tmr(TMR_TOTAL) - ihours*3600d0 - imin*60

        write(log_buf, "(I5,1X,A,1X,I2,1X,A,1X,F5.2,1X,A)") &
            ihours, "hours", imin, "mins", tsec, "secs"
        call time_logger(log_buf)
        call time_logger()

        write(log_buf, "(F10.3,1X,A,1X,F8.2,1X,A)") &
            tmr(TMR_TOTAL)*1000d0/istep, "ms per time step", &
            tmr(TMR_TOTAL)*1000d0/(60*istep), "min/1000 steps"
        call time_logger(log_buf)
        call time_logger()

        call time_logger("Time spent on different tasks per time step:")
        call time_logger(tmr(TMR_REBALANCE), "node rebalance")
        call time_logger(tmr(TMR_TEMP_CONTROL), "temperature control")
        call time_logger(tmr(TMR_PREDICT), "coordinate prediction")

        call time_logger(tmr(TMR_NBOR), "pair table evaluation")
        call time_logger(tmr(TMR_NBOR_LINK_CELL), "linkcell", tmr(TMR_NBOR))
        call time_logger(tmr(TMR_NBOR_GET_PAIRS), "getpairs", tmr(TMR_NBOR))
        call time_logger(tmr(TMR_NBOR_COMMS), "communication", tmr(TMR_NBOR))

        call time_logger(tmr(TMR_EAM_CHARGE), "calc charge density")
        call time_logger(tmr(TMR_EAM_CHARGE_COMMS), "communication", tmr(TMR_EAM_CHARGE))
        call time_logger(tmr(TMR_EAM_EMBED), "calc embedding energy")
        call time_logger(tmr(TMR_EAM_FORCE), "calc EAM forces")
        call time_logger(tmr(TMR_EAM_FORCE_COMMS), "communication", tmr(TMR_EAM_FORCE))

        call time_logger(tmr(TMR_SEMICON_FORCE), "calc Semicon force")
        call time_logger(tmr(TMR_SEMICON_REPPOT), "reppot", tmr(TMR_SEMICON_FORCE))
        call time_logger(tmr(TMR_SEMiCON_COMMS), "communication", tmr(TMR_SEMICON_FORCE))

        call time_logger(tmr(TMR_CORRECT), "coordinate correction")

        call time_logger(tmr(TMR_SHUFFLE), "shuffle among nodes")
        call time_logger(tmr(TMR_SHUFFLE_PRE), "pre-checks", tmr(TMR_SHUFFLE))
        call time_logger(tmr(TMR_SHUFFLE_MAIN), "main loop", tmr(TMR_SHUFFLE))
        call time_logger(tmr(TMR_SHUFFLE_POST), "post-process", tmr(TMR_SHUFFLE))
        call time_logger(tmr(TMR_SHUFFLE_ALL_TO_ALL), "all-to-all", tmr(TMR_SHUFFLE))

        call time_logger(tmr(TMR_MISC_PROPERTIES), "miscellaneous properties")
        call time_logger(tmr(TMR_ELSTOP), "elstop subtraction")
        call time_logger(tmr(TMR_MAIN_OUTPUT), "main loop output")
        call time_logger(tmr(TMR_MAIN_OTHER),"other main loop time")
        call time_logger()

        call time_logger(tmr(TMR_COMMS_TOTAL), "overall communication")
        call time_logger(tmr(TMR_MOVIE), "movie output")
        call time_logger()

        write(log_buf, "(A,1X,I8,1X,A,1X,I9,1X,A,1X,F10.2,1X,A,1X,E9.4,1X,A)") &
            "For", natoms, "atoms and", istep, "steps used", tmr(TMR_TOTAL), "s =>", &
            tmr(TMR_TOTAL)/natoms/istep, "s/step/nat"
        call time_logger(log_buf)

    end subroutine print_timers

end program PARCAS

