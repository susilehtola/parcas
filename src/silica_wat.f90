!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

!-------------------------------------------------------------------------------
! Silica_Wat - Watanabe potential for silica
!
! Usage
!   Use delta=0.01 or smaller value.
!
! Description
!   Implements the silica potential proposed by Watanabe et al. in
!   Appl. Surf. Sci. 234 (2004) 207-213. Note that there is a wrong
!   bond softening function in the paper and some wrong parameters, too.
!   The correct function and parameters received by e-mail
!   from Dr. Watanabe.
!
!   The implemented potential differs from the one described in Watanabe's
!   article in the following ways:
!   - The bond softening function for silicon, g_Si, is set to 1.
!   - The bond softening function for oxygen, g_O, is set to 1 when the
!     coordination is less than 2.
!   - The cutoff function f_C for the coordination function has been
!     changed from in order to get the derivatives to zero at R-D and R+D.
!   - The cutoff values R, D, a_OO, a^SiSi_1_SiSiSi, a^SiSi_2_SiSiSi,
!     and a^SiO_1_OSiO have been changed.
!   In addition to this, a Ge parameterization has been added.
!
!   The code is optimized for speed, not for readability.
!
! History
!   prior 1997     Code originates from ancient HARWELL code
!   Jan 1997       Modified for PARCAS SW routine by Kai Nordlund
!   Aug 2006       Rewritten for silica by Juha Samela
!   Feb 2008>      Parameters for Ge atoms added for GeO2 and Ge-Si-O etc. by Olli Pakarinen
!
!-------------------------------------------------------------------------------

module silica_wat_params
    implicit none
    save
    public

    ! TODO:
    ! Unused parameters, need to check if it is because of a typo or
    ! other bug, or whether they are just not needed.
    ! sw3mod2
    ! sw3mod3
    ! B_OO
    ! p_GeO
    ! p_GeGe
    ! p_GeSi
    ! q_GeO
    ! q_SiSi
    ! q_GeGe
    ! q_GeSi
    ! r0_GeO
    ! r0_GeGe
    ! r0_GeSi
    ! p1, p2, p3, p4, p7   ! Used in commented code
    ! costheta0_2_SiSiO
    ! costheta0_2_SiSiSi
    ! cutoff2sq            ! Used in commented code
    ! cutoff3              ! Used only for cutoff3sq
    ! cutoff3sq
    ! coordminGeO
    ! coordmaxGeO

    integer, parameter :: DP = kind(1.0d1)

    real(kind=DP), parameter :: swsigma = 2.0951_DP  ! SW length unit (A)
    !  real(kind=DP), parameter :: gesigma = 2.3163_DP  ! SW length unit (A) for Ge ! NOT NEEDED?
    real(kind=DP), parameter :: ucut = 1.9_DP        ! Si-Si-Si cutoff (swsigma)

    ! Scaling parameters for Ge, compared to Si
    real(kind=DP), parameter :: sw2mod2 = 0.82_DP     ! Modification of Ge-Ge S-W potential strength, x*-3.86
    real(kind=DP), parameter :: sw3mod2 = 0.67742_DP  ! Modification of S-W three-body strength, x*31
    real(kind=DP), parameter :: sw2mod3 = 0.90556_DP  ! Si-Ge
    real(kind=DP), parameter :: sw3mod3 = 0.82320_DP  ! Si-Ge

    real(kind=DP), parameter :: GeO_Lscale = 1.74_DP/1.61_DP ! Ge-O bond compared to Si-O, from S-Weber-pot.
    real(kind=DP), parameter :: GeGe_Lscale = 2.181_DP/2.0951_DP  ! Ge-Ge bond compared to Si-Si
    real(kind=DP), parameter :: GeSi_Lscale = 2.1376_DP/2.0951_DP ! Ge-Si bond compared to Si-Si

    real(kind=DP), parameter :: GeGe_Escale = 1.93_DP/2.16722_DP*sw2mod2  ! Ge-Ge compared to Si-Si
    real(kind=DP), parameter :: GeSi_Escale = 2.0451_DP/2.16722_DP*sw2mod3   !Ge-Si compared to Si-Si
    real(kind=DP), parameter :: GeO_Escale = 5.00_DP/6.42_DP            ! GeO2 Ecoh compared to SiO2
    !    real(kind=DP), parameter :: GeO_Escale = 580.0_DP/910.0_DP         ! GeO2 ? compared to SiO2

    ! Scaling for Ge parameters: start with Ding&Andersen PRB 34 (1986) 6987,
    ! add sw2mod and sw3mod values (see stilweb.f90)
    !    EPS(i)=sw2mod(i)*EPS(i)
    !    EPSA(i)=sw2mod(i)*EPSA(i)
    !    ALAM(i)=sw3mod(i)*ALAM(i)
    !    EPSLAM(i)=EPS(i)*ALAM(i)
    ! swpotmod=0.82, sw3mod=0.67742
    ! Aim for c-Ge:
    !a=5.653621              (Expt. 5.66 (?))
    !Epot=-3.1652
    !B = 660 kbar (Experimental B= 810 kbar).


    ! Pair interation parameters, Ge parameters scaled from Si by Ecoh values
    real(kind=DP), parameter :: epsilon = 2.16722_DP  ! = 50 kcal/mol
    real(kind=DP), parameter :: epsA_OO    = -12.29_DP*epsilon
    real(kind=DP), parameter :: epsA_SiO   = 21.0_DP*epsilon
    real(kind=DP), parameter :: epsA_GeO   = 21.0_DP*epsilon*GeO_Escale !
    real(kind=DP), parameter :: epsA_SiSi  = 7.049556277_DP*epsilon
    real(kind=DP), parameter :: epsA_GeGe  = 7.049556277_DP*epsilon*GeGe_Escale !
    real(kind=DP), parameter :: epsA_GeSi  = 7.049556277_DP*epsilon*GeSi_Escale !
    real(kind=DP), parameter :: B_OO    = 0.0_DP            ! now hard-coded
    real(kind=DP), parameter :: B_SiO   = 0.038_DP
    real(kind=DP), parameter :: B_GeO   = 0.038_DP ! Scaling needed in A only
    real(kind=DP), parameter :: B_SiSi  = 0.60222_DP
    real(kind=DP), parameter :: B_GeGe  = 0.60222_DP ! Scaling needed in A only
    real(kind=DP), parameter :: B_GeSi  = 0.60222_DP ! Scaling needed in A only
    real(kind=DP), parameter :: p_SiO   = 5.3_DP
    real(kind=DP), parameter :: p_GeO   = 5.3_DP !
    real(kind=DP), parameter :: p_SiSi  = 4.0_DP             ! now hard-coded
    real(kind=DP), parameter :: p_GeGe  = 4.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: p_GeSi  = 4.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: q_OO    = 2.24_DP
    real(kind=DP), parameter :: q_SiO   = -1.1_DP
    real(kind=DP), parameter :: q_GeO   = -1.1_DP !
    real(kind=DP), parameter :: q_SiSi  = 0.0_DP             ! now hard-coded
    real(kind=DP), parameter :: q_GeGe  = 0.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: q_GeSi  = 0.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: r0_OO   = 1.35_DP           ! originally 1.25_DP
    real(kind=DP), parameter :: r0_SiO  = 1.3_DP
    real(kind=DP), parameter :: r0_GeO  = 1.3_DP*GeO_Lscale !
    real(kind=DP), parameter :: r0_SiSi = 1.8_DP
    real(kind=DP), parameter :: r0_GeGe = 1.8_DP*GeGe_Lscale !
    real(kind=DP), parameter :: r0_GeSi = 1.8_DP*GeSi_Lscale !

    ! Si-O interaction softening function parameters  ! Used also for Ge-O
    real(kind=DP), parameter :: p1 = 0.30367_DP
    real(kind=DP), parameter :: p2 = 3.93233_DP
    real(kind=DP), parameter :: p3 = 0.25345_DP
    real(kind=DP), parameter :: p4 = 3.93233_DP
    real(kind=DP), parameter :: p5 = 5.274_DP
    real(kind=DP), parameter :: p6 = 0.712_DP
    real(kind=DP), parameter :: p7 = 0.522_DP
    real(kind=DP), parameter :: p8 = -0.0372_DP
    real(kind=DP), parameter :: p9 = -4.52_DP
    real(kind=DP), parameter :: op7 = 1.0_DP/p7

    ! The old values used in the first version of the Watanabe potential
    !!$    real(kind=DP), parameter :: p5 = 0.097_DP
    !!$    real(kind=DP), parameter :: p6 = 1.6_DP
    !!$    real(kind=DP), parameter :: p7 = 0.3654_DP
    !!$    real(kind=DP), parameter :: p8 = 0.1344_DP
    !!$    real(kind=DP), parameter :: p9 = 6.4176_DP

    ! Three-body interaction parameters     ! Only energy via myy (1&2) and r0 scaled for Ge
    real(kind=DP), parameter :: alpha_1_SiOSi       = 1.6_DP
    real(kind=DP), parameter :: alpha_1_SiSiO       = 0.3_DP
    real(kind=DP), parameter :: alpha_1_SiSiSi      = -1.35_DP
    real(kind=DP), parameter :: alpha_2_SiSiO       = 0.3_DP
    real(kind=DP), parameter :: alpha_2_SiSiSi      = 0.3_DP

    real(kind=DP), parameter :: costheta0_1_OSiO    = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: costheta0_1_SiOSi   = -0.812_DP
    real(kind=DP), parameter :: costheta0_1_SiSiO   = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: costheta0_1_SiSiSi  = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: costheta0_2_SiSiO   = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: costheta0_2_SiSiSi  = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: gamma_OSi_1_SiOSi   = 0.15_DP
    real(kind=DP), parameter :: gamma_SiO_1_OSiO    = 0.31_DP
    real(kind=DP), parameter :: gamma_SiO_1_SiSiO   = 0.124_DP
    real(kind=DP), parameter :: gamma_SiO_2_SiSiO   = 1.082_DP
    real(kind=DP), parameter :: gamma_SiSi_1_SiSiO  = 0.032_DP
    real(kind=DP), parameter :: gamma_SiSi_2_SiSiO  = 1.01_DP
    real(kind=DP), parameter :: gamma_SiSi_1_SiSiSi = 0.088_DP
    real(kind=DP), parameter :: gamma_SiSi_2_SiSiSi = 1.01_DP
    real(kind=DP), parameter :: ksi_1_SiSiO         = 2.0_DP
    real(kind=DP), parameter :: ksi_2_SiSiSi        = 6.0_DP
    real(kind=DP), parameter :: myy_1_OSiO          = 10.5_DP
    real(kind=DP), parameter :: myy_1_SiOSi         = 2.5_DP
    real(kind=DP), parameter :: myy_1_SiSiSi        = 4.0_DP
    real(kind=DP), parameter :: myy_1_SiSiO         = 3.0_DP
    real(kind=DP), parameter :: myy_2_SiSiO         = 5.878_DP
    real(kind=DP), parameter :: myy_2_SiSiSi        = 5.878_DP

    real(kind=DP), parameter :: myy_1_OGeO          = 10.5_DP*GeO_Escale ! These scale the 3-body potential energy
    real(kind=DP), parameter :: myy_1_GeOGe         = 2.5_DP*GeO_Escale !
    real(kind=DP), parameter :: myy_1_GeOSi         = 2.5_DP*(1+GeO_Escale)/2.0_DP ! Avg. of Si-O and Ge-O
    real(kind=DP), parameter :: myy_1_GeGeGe        = 4.0_DP*GeGe_Escale !
    real(kind=DP), parameter :: myy_1_SiGeSi        = 4.0_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_1_GeSiGe        = 4.0_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_1_GeSiSi        = 4.0_DP*(1+GeSi_Escale)/2.0_DP ! Avg. of Si-Si and Ge-Si
    real(kind=DP), parameter :: myy_1_GeGeSi        = 4.0_DP*(GeGe_Escale+GeSi_Escale)/2.0_DP ! Avg. of Ge-Ge and Ge-Si
    real(kind=DP), parameter :: myy_1_GeGeO         = 3.0_DP*(GeGe_Escale+GeO_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_1_GeSiO         = 3.0_DP*(1+GeSi_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_1_SiGeO         = 3.0_DP*(GeSi_Escale+GeO_Escale)/2.0_DP !

    real(kind=DP), parameter :: myy_2_GeGeGe        = 5.878_DP*GeGe_Escale !
    real(kind=DP), parameter :: myy_2_SiGeSi        = 5.878_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_2_GeSiGe        = 5.878_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_2_GeSiSi        = 5.878_DP*(1+GeSi_Escale)/2.0_DP ! Avg. of Si-Si and Ge-Si
    real(kind=DP), parameter :: myy_2_GeGeSi        = 5.878_DP*(GeGe_Escale+GeSi_Escale)/2.0_DP ! Avg. of Ge-Ge and Ge-Si
    real(kind=DP), parameter :: myy_2_GeGeO         = 5.878_DP*(GeGe_Escale+GeO_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_2_GeSiO         = 5.878_DP*(1+GeSi_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_2_SiGeO         = 5.878_DP*(GeSi_Escale+GeO_Escale)/2.0_DP !

    real(kind=DP), parameter :: nyy_1_SiSiO         = 3.6_DP
    real(kind=DP), parameter :: nyy_2_SiSiSi        = 1.6_DP
    real(kind=DP), parameter :: r0_OSi_1_SiOSi      = 1.2_DP ! 0.2 in the paper is a mistake
    real(kind=DP), parameter :: r0_SiO_1_OSiO       = 1.2_DP ! originally 1.1
    real(kind=DP), parameter :: r0_SiO_1_SiSiO      = 1.10_DP
    real(kind=DP), parameter :: r0_SiO_2_SiSiO      = 1.5_DP
    real(kind=DP), parameter :: r0_SiSi_1_SiSiO     = 1.15_DP
    real(kind=DP), parameter :: r0_SiSi_2_SiSiO     = 1.8_DP
    real(kind=DP), parameter :: r0_SiSi_1_SiSiSi    = 1.0_DP ! originally 1.2
    real(kind=DP), parameter :: r0_SiSi_2_SiSiSi    = ucut ! originally 1.8
    ! Ge r0 parameters
    real(kind=DP), parameter :: r0_OGe_1_GeOGe      = 1.2_DP*GeO_Lscale ! 0.2 in the paper a mistake
    real(kind=DP), parameter :: r0_GeO_1_OGeO       = 1.2_DP*GeO_Lscale ! originally 1.1
    real(kind=DP), parameter :: r0_GeO_1_GeGeO      = 1.10_DP*GeO_Lscale
    real(kind=DP), parameter :: r0_GeGe_1_GeGeGe    = 1.0_DP*GeGe_Lscale ! originally 1.2
    real(kind=DP), parameter :: r0_GeGe_1_GeGeO     = 1.15_DP*GeGe_Lscale
    real(kind=DP), parameter :: r0_GeO_2_GeGeO      = 1.5_DP*GeO_Lscale
    real(kind=DP), parameter :: r0_GeGe_2_GeGeO     = 1.8_DP*GeGe_Lscale
    real(kind=DP), parameter :: r0_GeGe_2_GeGeGe    = ucut*GeGe_Lscale ! originally 1.8
    ! Ge-Si r0 parameters HERE WE ASSUME THAT AT ANOTHER BOND Si/Ge CHANGE DOES NOT CHANGE THINGS
    ! i.e. for example r0_GeO_1_GeOSi=r0_GeO_1_GeOGe
    real(kind=DP), parameter :: r0_GeSi_1_GeSiGe    = 1.0_DP*GeSi_Lscale ! originally 1.2
    real(kind=DP), parameter :: r0_GeSi_1_SiGeSi    = 1.0_DP*GeSi_Lscale ! originally 1.2
    real(kind=DP), parameter :: r0_GeSi_1_GeSiO     = 1.15_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_2_GeSiO     = 1.8_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_1_SiGeO     = 1.15_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_2_SiGeO     = 1.8_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_2_GeSiGe    = ucut*GeSi_Lscale ! originally 1.8
    real(kind=DP), parameter :: r0_GeSi_2_SiGeSi    = ucut*GeSi_Lscale ! originally 1.8

    real(kind=DP), parameter :: z0_1_SiSiO          = 2.6_DP  ! Same used for Ge ?

    ! Cutoff parameters
    real(kind=DP), parameter :: opswsigma = 1.0_DP/swsigma
    real(kind=DP), parameter :: cutoff2 = 1.8_DP*swsigma    ! pair interaction cutoff (A)
    real(kind=DP), parameter :: cutoff3 = 1.8_DP*swsigma    ! trimer interaction cutoff (A)
    real(kind=DP), parameter :: cutoff2sq = cutoff2*cutoff2
    real(kind=DP), parameter :: cutoff3sq = cutoff2*cutoff3
    real(kind=DP), parameter :: eosigma = epsilon/swsigma

    ! Coordination number parameters
    real(kind=DP), parameter :: Dco = 0.1_DP*swsigma ! original 0.05
    real(kind=DP), parameter :: Rco = 1.15_DP*swsigma ! original 1.2
    real(kind=DP), parameter :: coordmin = Rco-Dco
    real(kind=DP), parameter :: coordmax = Rco+Dco
    real(kind=DP), parameter :: coordminGeO = (Rco-Dco)*GeO_Lscale
    real(kind=DP), parameter :: coordmaxGeO = (Rco+Dco)*GeO_Lscale

    ! 2-body parameter arrays, indexed by Wata type in order (wti, wtj).

    ! Fermi joining parameters
    real(kind=DP), dimension(3,3) :: bfs, rfs
    real(kind=DP), dimension(3,3) :: r0_arr

    ! 3-body parameter arrays, indexed by Wata type in order (wti, wtj, wtk).
    ! arr(i, j, k  ) = [JJ-II-Si, JJ-II-O, JJ-II-Ge]

    ! arr(1, 1, 1:3) = [Si-Si-Si, Si-Si-O, Si-Si-Ge]
    ! arr(1, 2, 1:3) = [O -Si-Si, O -Si-O, O -Si-Ge]
    ! arr(1, 3, 1:3) = [Ge-Si-Si, Ge-Si-O, Ge-Si-Ge]
    ! arr(2, 1, 1:3) = [Si-O -Si, Si-O -O, Si-O -Ge]
    ! arr(2, 2, 1:3) = [O -O -Si, O -O -O, O -O -Ge]
    ! arr(2, 3, 1:3) = [Ge-O -Si, Ge-O -O, Ge-O -Ge]
    ! arr(3, 1, 1:3) = [Si-Ge-Si, Si-Ge-O, Si-Ge-Ge]
    ! arr(3, 2, 1:3) = [O -Ge-Si, O -Ge-O, O -Ge-Ge]
    ! arr(3, 3, 1:3) = [Ge-Ge-Si, Ge-Ge-O, Ge-Ge-Ge]

    integer, parameter :: &
        THREE_NONE = 1, &
        THREE_SHORT = 2, &
        THREE_LONG_OXY = 3, &
        THREE_LONG_NO_OXY = 4

    integer, dimension(3,3,3) :: three_interact_arr

    real(kind=DP), dimension(3,3,3) :: &
        alpha_1_arr, alpha_2_arr, &
        costheta0_1_arr, &
        myy_1_arr, myy_2_arr, &
        gamma_1_arr, gamma_2_arr, &
        r0_1_arr, r0_2_arr

contains

    subroutine fill_param_arrays()
        ! TODO why is costheta0_2 not used?

        real(kind=DP), parameter :: no = 0.0d0

        ! These Ge parameters from stilweb.f90, checked to be OK 9.6.2008.
        ! GeO parameters optimized 9.6.2008 in parcas V3.90e.
        bfs(1:3, 1:3) = 15.0d0
        rfs(1, 1:3) = [1.40d0, 0.90d0, 1.45d0]
        rfs(2, 1:3) = [0.90d0, 0.50d0, 1.00d0]
        rfs(3, 1:3) = [1.45d0, 1.00d0, 1.50d0]

        r0_arr(1, 1:3) = [r0_SiSi, r0_SiO, r0_GeSi]
        r0_arr(2, 1:3) = [r0_SiO , r0_OO , r0_GeO ]
        r0_arr(3, 1:3) = [r0_GeSi, r0_GeO, r0_GeGe]


        three_interact_arr(1, 1, 1:3) = [THREE_LONG_NO_OXY, THREE_LONG_OXY, THREE_LONG_NO_OXY]
        three_interact_arr(1, 2, 1:3) = [THREE_LONG_OXY   , THREE_SHORT   , THREE_LONG_OXY   ]
        three_interact_arr(1, 3, 1:3) = [THREE_LONG_NO_OXY, THREE_LONG_OXY, THREE_LONG_NO_OXY]
        three_interact_arr(2, 1, 1:3) = [THREE_SHORT      , THREE_NONE    , THREE_SHORT      ]
        three_interact_arr(2, 2, 1:3) = [THREE_NONE       , THREE_NONE    , THREE_NONE       ]
        three_interact_arr(2, 3, 1:3) = [THREE_SHORT      , THREE_NONE    , THREE_SHORT      ]
        three_interact_arr(3, 1, 1:3) = [THREE_LONG_NO_OXY, THREE_LONG_OXY, THREE_LONG_NO_OXY]
        three_interact_arr(3, 2, 1:3) = [THREE_LONG_OXY   , THREE_SHORT   , THREE_LONG_OXY   ]
        three_interact_arr(3, 3, 1:3) = [THREE_LONG_NO_OXY, THREE_LONG_OXY, THREE_LONG_NO_OXY]

        alpha_1_arr(1, 1, 1:3) = [alpha_1_SiSiSi, alpha_1_SiSiO, alpha_1_SiSiSi]
        alpha_1_arr(1, 2, 1:3) = [alpha_1_SiSiO ,            no, alpha_1_SiSiO ]
        alpha_1_arr(1, 3, 1:3) = [alpha_1_SiSiSi, alpha_1_SiSiO, alpha_1_SiSiSi]
        alpha_1_arr(2, 1, 1:3) = [alpha_1_SiOSi ,            no, alpha_1_SiOSi ]
        alpha_1_arr(2, 2, 1:3) = [            no,            no,             no]
        alpha_1_arr(2, 3, 1:3) = [alpha_1_SiOSi ,            no, alpha_1_SiOSi ]
        alpha_1_arr(3, 1, 1:3) = [alpha_1_SiSiSi, alpha_1_SiSiO, alpha_1_SiSiSi]
        alpha_1_arr(3, 2, 1:3) = [alpha_1_SiSiO ,            no, alpha_1_SiSiO ]
        alpha_1_arr(3, 3, 1:3) = [alpha_1_SiSiSi, alpha_1_SiSiO, alpha_1_SiSiSi]

        alpha_2_arr(1, 1, 1:3) = [alpha_2_SiSiSi, alpha_2_SiSiO, alpha_2_SiSiSi]
        alpha_2_arr(1, 2, 1:3) = [alpha_2_SiSiO ,            no, alpha_2_SiSiO ]
        alpha_2_arr(1, 3, 1:3) = [alpha_2_SiSiSi, alpha_2_SiSiO, alpha_2_SiSiSi]
        alpha_2_arr(2, 1, 1:3) = [            no,            no,             no]
        alpha_2_arr(2, 2, 1:3) = [            no,            no,             no]
        alpha_2_arr(2, 3, 1:3) = [            no,            no,             no]
        alpha_2_arr(3, 1, 1:3) = [alpha_2_SiSiSi, alpha_2_SiSiO, alpha_2_SiSiSi]
        alpha_2_arr(3, 2, 1:3) = [alpha_2_SiSiO ,            no, alpha_2_SiSiO ]
        alpha_2_arr(3, 3, 1:3) = [alpha_2_SiSiSi, alpha_2_SiSiO, alpha_2_SiSiSi]

        costheta0_1_arr(1, 1, 1:3) = [costheta0_1_SiSiSi, costheta0_1_SiSiO, costheta0_1_SiSiSi]
        costheta0_1_arr(1, 2, 1:3) = [costheta0_1_SiSiO , costheta0_1_OSiO , costheta0_1_SiSiO ]
        costheta0_1_arr(1, 3, 1:3) = [costheta0_1_SiSiSi, costheta0_1_SiSiO, costheta0_1_SiSiSi]
        costheta0_1_arr(2, 1, 1:3) = [costheta0_1_SiOSi ,                no, costheta0_1_SiOSi ]
        costheta0_1_arr(2, 2, 1:3) = [                no,                no,                 no]
        costheta0_1_arr(2, 3, 1:3) = [costheta0_1_SiOSi ,                no, costheta0_1_SiOSi ]
        costheta0_1_arr(3, 1, 1:3) = [costheta0_1_SiSiSi, costheta0_1_SiSiO, costheta0_1_SiSiSi]
        costheta0_1_arr(3, 2, 1:3) = [costheta0_1_SiSiO , costheta0_1_OSiO , costheta0_1_SiSiO ]
        costheta0_1_arr(3, 3, 1:3) = [costheta0_1_SiSiSi, costheta0_1_SiSiO, costheta0_1_SiSiSi]

        gamma_1_arr(1, 1, 1:3) = [gamma_SiSi_1_SiSiSi, gamma_SiSi_1_SiSiO, gamma_SiSi_1_SiSiSi]
        gamma_1_arr(1, 2, 1:3) = [gamma_SiO_1_SiSiO  , gamma_SiO_1_OSiO  , gamma_SiO_1_SiSiO  ]
        gamma_1_arr(1, 3, 1:3) = [gamma_SiSi_1_SiSiSi, gamma_SiSi_1_SiSiO, gamma_SiSi_1_SiSiSi]
        gamma_1_arr(2, 1, 1:3) = [gamma_OSi_1_SiOSi  ,                 no, gamma_OSi_1_SiOSi  ]
        gamma_1_arr(2, 2, 1:3) = [                 no,                 no,                  no]
        gamma_1_arr(2, 3, 1:3) = [gamma_OSi_1_SiOSi  ,                 no, gamma_OSi_1_SiOSi  ]
        gamma_1_arr(3, 1, 1:3) = [gamma_SiSi_1_SiSiSi, gamma_SiSi_1_SiSiO, gamma_SiSi_1_SiSiSi]
        gamma_1_arr(3, 2, 1:3) = [gamma_SiO_1_SiSiO  , gamma_SiO_1_OSiO  , gamma_SiO_1_SiSiO  ]
        gamma_1_arr(3, 3, 1:3) = [gamma_SiSi_1_SiSiSi, gamma_SiSi_1_SiSiO, gamma_SiSi_1_SiSiSi]

        gamma_2_arr(1, 1, 1:3) = [gamma_SiSi_2_SiSiSi, gamma_SiSi_2_SiSiO, gamma_SiSi_2_SiSiSi]
        gamma_2_arr(1, 2, 1:3) = [gamma_SiO_2_SiSiO  ,                 no, gamma_SiO_2_SiSiO  ]
        gamma_2_arr(1, 3, 1:3) = [gamma_SiSi_2_SiSiSi, gamma_SiSi_2_SiSiO, gamma_SiSi_2_SiSiSi]
        gamma_2_arr(2, 1, 1:3) = [                 no,                 no,                  no]
        gamma_2_arr(2, 2, 1:3) = [                 no,                 no,                  no]
        gamma_2_arr(2, 3, 1:3) = [                 no,                 no,                  no]
        gamma_2_arr(3, 1, 1:3) = [gamma_SiSi_2_SiSiSi, gamma_SiSi_2_SiSiO, gamma_SiSi_2_SiSiSi]
        gamma_2_arr(3, 2, 1:3) = [gamma_SiO_2_SiSiO  ,                 no, gamma_SiO_2_SiSiO  ]
        gamma_2_arr(3, 3, 1:3) = [gamma_SiSi_2_SiSiSi, gamma_SiSi_2_SiSiO, gamma_SiSi_2_SiSiSi]

        myy_1_arr(1, 1, 1:3) = [myy_1_SiSiSi, myy_1_SiSiO, myy_1_GeSiSi]
        myy_1_arr(1, 2, 1:3) = [myy_1_SiSiO , myy_1_OSiO , myy_1_GeSiO ]
        myy_1_arr(1, 3, 1:3) = [myy_1_GeSiSi, myy_1_GeSiO, myy_1_GeSiGe]
        myy_1_arr(2, 1, 1:3) = [myy_1_SiOSi ,          no, myy_1_GeOSi ]
        myy_1_arr(2, 2, 1:3) = [          no,          no,           no]
        myy_1_arr(2, 3, 1:3) = [myy_1_GeOSi ,          no, myy_1_GeOGe ]
        myy_1_arr(3, 1, 1:3) = [myy_1_SiGeSi, myy_1_SiGeO, myy_1_GeGeSi]
        myy_1_arr(3, 2, 1:3) = [myy_1_SiGeO , myy_1_OGeO , myy_1_GeGeO ]
        myy_1_arr(3, 3, 1:3) = [myy_1_GeGeSi, myy_1_GeGeO, myy_1_GeGeGe]

        myy_2_arr(1, 1, 1:3) = [myy_2_SiSiSi, myy_2_SiSiO, myy_2_GeSiSi]
        myy_2_arr(1, 2, 1:3) = [myy_2_SiSiO ,          no, myy_2_GeSiO ]
        myy_2_arr(1, 3, 1:3) = [myy_2_GeSiSi, myy_2_GeSiO, myy_2_GeSiGe]
        myy_2_arr(2, 1, 1:3) = [          no,          no,           no]
        myy_2_arr(2, 2, 1:3) = [          no,          no,           no]
        myy_2_arr(2, 3, 1:3) = [          no,          no,           no]
        myy_2_arr(3, 1, 1:3) = [myy_2_SiGeSi, myy_2_SiGeO, myy_2_GeGeSi]
        myy_2_arr(3, 2, 1:3) = [myy_2_SiGeO ,          no, myy_2_GeGeO ]
        myy_2_arr(3, 3, 1:3) = [myy_2_GeGeSi, myy_2_GeGeO, myy_2_GeGeGe]

        r0_1_arr(1, 1, 1:3) = [r0_SiSi_1_SiSiSi, r0_SiSi_1_SiSiO, r0_SiSi_1_SiSiSi]
        r0_1_arr(1, 2, 1:3) = [r0_SiO_1_SiSiO  , r0_SiO_1_OSiO  , r0_SiO_1_SiSiO  ]
        r0_1_arr(1, 3, 1:3) = [r0_GeSi_1_GeSiGe, r0_GeSi_1_GeSiO, r0_GeSi_1_GeSiGe]
        r0_1_arr(2, 1, 1:3) = [r0_OSi_1_SiOSi  ,              no, r0_OSi_1_SiOSi  ]
        r0_1_arr(2, 2, 1:3) = [              no,              no,               no]
        r0_1_arr(2, 3, 1:3) = [r0_OGe_1_GeOGe  ,              no, r0_OGe_1_GeOGe  ]
        r0_1_arr(3, 1, 1:3) = [r0_GeSi_1_SiGeSi, r0_GeSi_1_SiGeO, r0_GeSi_1_SiGeSi]
        r0_1_arr(3, 2, 1:3) = [r0_GeO_1_GeGeO  , r0_GeO_1_OGeO  , r0_GeO_1_GeGeO  ]
        r0_1_arr(3, 3, 1:3) = [r0_GeGe_1_GeGeGe, r0_GeGe_1_GeGeO, r0_GeGe_1_GeGeGe]

        r0_2_arr(1, 1, 1:3) = [r0_SiSi_2_SiSiSi, r0_SiSi_2_SiSiO, r0_SiSi_2_SiSiSi]
        r0_2_arr(1, 2, 1:3) = [r0_SiO_2_SiSiO  ,              no, r0_SiO_2_SiSiO  ]
        r0_2_arr(1, 3, 1:3) = [r0_GeSi_2_GeSiGe, r0_GeSi_2_GeSiO, r0_GeSi_2_GeSiGe]
        r0_2_arr(2, 1, 1:3) = [              no,              no,               no]
        r0_2_arr(2, 2, 1:3) = [              no,              no,               no]
        r0_2_arr(2, 3, 1:3) = [              no,              no,               no]
        r0_2_arr(3, 1, 1:3) = [r0_GeSi_2_SiGeSi, r0_GeSi_2_SiGeO, r0_GeSi_2_SiGeSi]
        r0_2_arr(3, 2, 1:3) = [r0_GeO_2_GeGeO  ,              no, r0_GeO_2_GeGeO  ]
        r0_2_arr(3, 3, 1:3) = [r0_GeGe_2_GeGeGe, r0_GeGe_2_GeGeO, r0_GeGe_2_GeGeGe]

        ! Make sure the largest cutoff for each wti,wtj,wtk is in r0_2_arr.
        where (r0_2_arr == 0)
            r0_2_arr = r0_1_arr
        end where

    end subroutine fill_param_arrays

end module silica_wat_params


module silica_wat_mod

    use output_logger, only: logger

    use datatypes, only: real64b
    use defs, only: NNMAX
    use PhysConsts, only: pi
    use typeparam
    use my_mpi
    use splinereppot_mod, only: reppot_only, reppot_fermi
    use timers, only: tmr, TMR_SEMICON_COMMS, TMR_SEMICON_REPPOT
    use para_common, only: myatoms, np0pairtable, buf

    use mdparsubs_mod, only: potential_pass_back_border_atoms

    use silica_wat_params


    implicit none
    save

    private
    public :: init_silica_wat
    public :: silica_wat_force


    ! This parameter determines how near the cut-off limit the interactions are cut-off.
    ! If the exact cut-off limit is used instead, numerical instabilities may appear.
    real(DP), parameter :: cutofflimit = -1.0d-8

    ! Map from PARCAS types (>=0) to Watanabe types.
    integer, allocatable :: wata_types(:)

    real(DP), parameter :: TWOPI = 2.0_DP * pi


contains

    !-----------------------------------------------------------------------------
    !
    ! silica_wat_force - Calculates interatomic forces using the Watanabe potential
    !
    ! Method
    !   Calculates acceleration (xnp) of particles. The particle position are
    !   given in x0, and their interactions through a central force are
    !   calculated according to the Watanabe potential for silica.
    !
    !   If you modify the parameters, remember (Watanabe et al.):
    !   Although the three-body term was originally introduced just to
    !   describe the bond bending forces, it has an additional role to
    !   cancel out long-range two-body interactions at the second
    !   nearest-neighbor distances. Therefore, the cutoff distance of a
    !   three-body term should be set at a longer value than that of
    !   two-body terms.
    !   In a three-body term for a tight bond angle, the long-range
    !   component is strong and inevitably causes an unnatural steric
    !   hindrance. In order to solve this problem, we split the three-body
    !   term into two components; a shortrange term to describe correctly
    !   the bond bending force, and a long-range term to cancel out
    !   moderately the extra two-body interaction at the second nearest
    !   neighbor distances.
    !
    !   There is only the repulsive part of the two-body potential between
    !   two oxygen atoms. Therefore, O-O-O and Si-O-O interaction
    !   are omitted.
    !
    !-----------------------------------------------------------------------------
    !
    ! Input values
    !   x0           Contains positions in Angtroms scaled by 1/box
    !   atype        Parcas atom types
    !   nborlist     Neighborlist
    !   myatoms      Number of atoms in my node, in para_common.f90
    !   np0pairtable Number of atoms in my node, plus atoms from neighbors
    !   box(3)       Box size (box centered on 0)
    !   pbc(3)       Periodics: if = 1.0d0 periodic, for x/y/z separately
    !   reppotcutin  Cut-off distance for rep. pot.
    !
    ! Output values
    !   xnp          Contains forces in eV/A scaled by 1/box
    !   Epair        V_2 per atom in eV
    !   Ethree       V_3 per atom in eV
    !   wxxi, ...    Virials in eV
    !-----------------------------------------------------------------------------
    !
    ! Compute the accelerations into xnp in actual units, then convert
    ! to internal units at the end. Compute energies into Epair and Ethree.
    !
    ! Take care to calculate pressure, total pot. only for atoms in my node!
    !
    ! Loop over third atoms goes from the next neighbor to the last one, because
    ! every angle between atoms should be calculated only once.
    !
    ! In the potential and force calculation Stillinger-Weber length units are
    ! used instead of A, which otherwise is the length unit in this subroutine.
    ! Note that the reppot subroutine does not use SW units.
    !
    ! Some parameters are hard-coded to optimize performance.
    !
    !-----------------------------------------------------------------------------

    subroutine silica_wat_force(x0, atype, xnp, box, pbc, &
            nborlist, nabors, Epair, Ethree, &
            wxxi, wyyi, wzzi, wxyi, wxzi, wyzi, &
            potmode, reppotcutin, calc_vir)

        ! Arguments
        real(kind=DP), contiguous, intent(in) :: x0(:)
        real(kind=DP), contiguous, intent(out) :: xnp(:)
        real(kind=DP), intent(in) :: box(3), pbc(3)
        integer, contiguous, intent(in) ::  nborlist(:), nabors(:)
        integer, contiguous, intent(in) :: atype(:)

        real(kind=DP), contiguous, intent(out) :: Epair(:), Ethree(:)
        real(kind=DP), contiguous, intent(out) :: &
            wxxi(:), wyyi(:), wzzi(:), wxyi(:), wxzi(:), wyzi(:)

        integer, intent(in) :: potmode
        real(kind=DP), intent(in) :: reppotcutin
        logical, intent(in) :: calc_vir


        ! Local variables
        real(kind=DP) :: dxij, dyij, dzij
        real(kind=DP) :: dxik, dyik, dzik
        real(kind=DP) :: rij, oprij, oprik ! atom separations in Å
        real(kind=DP) :: rijsw, riksw ! atom separations in SW units
        real(kind=DP) :: rijsc ! scaled atom separation for Ge pair potential
        real(kind=DP) :: cosjik, dcos, diff2ij, diff2ik

        real(kind=DP) :: ph, halfph             ! potential
        real(kind=DP) :: dph, dphz, dphz_tot    ! force
        real(kind=DP) :: gij, dgijdz            ! bond softening value
        real(kind=DP) :: ai, daix, daiy, daiz   ! force components
        real(kind=DP) :: help1, help2, help3, help4, help5
        real(kind=DP) :: hjik
        real(kind=DP) :: gamij1, gamij2, gamik1, gamik2
        real(kind=DP) :: biglambda1, biglambda2, bigtheta1, bigtheta2
        real(kind=DP) :: lambdatheta1, lambdatheta2
        real(kind=DP) :: coss1, coss2
        real(kind=DP) :: dphij1, dphij2, dphik1, dphik2
        real(kind=DP) :: coordination ! coordination numbers

        ! Arrays used to save per-neighbor values.
        real(kind=DP), dimension(NNMAX) :: dx_arr, dy_arr, dz_arr
        real(kind=DP), dimension(NNMAX) :: r_arr, rsw_arr
        integer, dimension(NNMAX) :: parcas_types
        real(DP), dimension(NNMAX) :: dzdr_arr ! coordination number derivatives

        integer :: atomi, atomj, atomk ! numbers of atoms
        integer :: nngbr  ! number of neighbours of the current atom
        integer :: ingbr, jngbr
        integer :: i, j, k
        integer :: nlistposj, nlistposk
        integer :: atypi, atypj, atypk ! Parcas atom tyoes
        integer :: wti, wtj, wtk       ! Watanabe atom types

        logical :: do_twobody
        logical :: is_pbc(3)
        real(DP) :: halfbox(3)
        real(DP) :: fermi, dfermi
        real(DP) :: t1


        ! Convert from internal units to Angstrom.
        ! Use buf(i) instead of x0(i) in the computations.
        do i = 1, 3*np0pairtable, 3
            buf(i+0) = x0(i+0) * box(1)
            buf(i+1) = x0(i+1) * box(2)
            buf(i+2) = x0(i+2) * box(3)
        end do

        is_pbc(:) = (pbc(:) == 1d0)
        halfbox(:) = box(:) / 2.0d0

        xnp(:3*np0pairtable) = 0.0
        Epair(:np0pairtable) = 0.0
        Ethree(:myatoms) = 0.0

        wxxi(:np0pairtable) = 0.0
        wyyi(:np0pairtable) = 0.0
        wzzi(:np0pairtable) = 0.0
        if (calc_vir) then
            wxyi(:np0pairtable) = 0.0
            wxzi(:np0pairtable) = 0.0
            wyzi(:np0pairtable) = 0.0
        end if


        ! Main loop over atoms n starts --------------------------------------------
        do atomi = 1, myatoms
            i = 3*atomi - 2  ! pos. of coordinate x in buf

            atypi = abs(atype(atomi))  ! Parcas type of atom i
            wti = wata_types(atypi)    ! Watanabe type

            nlistposj = nabors(atomi) - 1  ! neighborlist index
            nngbr = nborlist(nlistposj)    ! number of neighbours

            if (nngbr == 0) cycle
            if (nngbr < 0) then
                print *,'Neighbourlist HORROR ERROR in wata_forces()!', nngbr
                call my_mpi_abort("BUG: silica_wat nngbr < 0", nngbr)
            end if
            if (nngbr > NNMAX) then
                print *,'Neighbour array size exceeded in silica_wat',i,nngbr,NNMAX
                call my_mpi_abort('silica_wat NNMAX exceeded', nngbr)
            end if


            ! Calculate and save distances to all neighbours of the atom i.
            ! Compute coordination number and its derivatives.
            call init_neighbors(i, wti, nlistposj, nborlist, is_pbc, halfbox, &
                atype, parcas_types, r_arr, dx_arr, dy_arr, dz_arr, &
                coordination, dzdr_arr)

            ! convert to SW length units
            rsw_arr(:nngbr) = r_arr(:nngbr) * opswsigma

            dphz_tot = 0


            ! 2-body interaction loop starts ----------------------------------------
            do ingbr = 1,nngbr
                nlistposj = nlistposj + 1
                atomj = nborlist(nlistposj)  ! number of atom j
                j = 3*atomj - 2              ! pos. of coordinate x in buf

                atypj = parcas_types(ingbr)  ! Parcas type of atom j
                wtj   = wata_types(atypj)    ! Watanabe type

                ! No interaction defined between atoms i and j
                if (iac(atypi,atypj) /=  1) then
                    if (iac(atypi,atypj) == 0) cycle
                    if (iac(atypi,atypj) <= -1) then
                        call logger("ERROR: Impossible interaction")
                        call logger("myproc:", myproc, 4)
                        call logger("i:     ", i, 4)
                        call logger("j:     ", j, 4)
                        call logger("ATYPI: ", ATYPI, 4)
                        call logger("ATYPJ: ", ATYPJ, 4)
                        call my_mpi_abort('Impossible interaction iac = -1', myproc)
                    endif
                endif

                rij = r_arr(ingbr)  ! distance between i and j (Angstrom)

                if (rij > rcut(atypi,atypj)) cycle

                oprij  = 1.0_DP/rij
                dxij   = dx_arr(ingbr)   ! Angstrom
                dyij   = dy_arr(ingbr)   ! Angstrom
                dzij   = dz_arr(ingbr)   ! Angstrom
                rijsw  = rsw_arr(ingbr)  ! SW units


                ! Twobody using Newton's III law.
                ! Make sure that for pairs where gij has to be computed, the
                ! i atom is oxygen, to avoid having to call init_neighbors.
                ! Otherwise we would need the neighbors of neighbors, which
                ! would require double_borders (in mdx).
                if ((wti == 2) .neqv. (wtj == 2)) then
                    do_twobody = (wti == 2)
                else
                    do_twobody = (dxij > 0d0 .or. &
                        (dxij == 0d0 .and. dyij > 0d0) .or. &
                        (dxij == 0d0 .and. dyij == 0d0 .and. dzij > 0d0))
                end if
                if (do_twobody) then

                    ! The values ph and dph computed here are the energy of the
                    ! pair of atoms (i.e. twice the value for one atom) and the
                    ! force on each atom.

                    if (iac(atypi,atypj) == 1) then

                        dphz = 0

                        if ( wti == 1 .and. wtj == 1 ) then
                            ! Si-Si
                            ! hard-coded p = 4, q = 0

                            help1 = rijsw-r0_SiSi
                            if (help1 > cutofflimit) goto 100 ! skip twobody
                            help1 = 1.0_DP/help1
                            help2 = epsA_SiSi*exp(help1)
                            help3 = rijsw*rijsw
                            help3 = B_SiSi/(help3*help3)
                            help5 = help3-1.0_DP
                            ph = help5*help2
                            dph = -help2*( help3*p_SiSi/rijsw + help5*help1*help1 )
                            dph = dph*opswsigma  ! change to eV/A

                        else if ( wti == 2 .and. wtj == 2 ) then
                            ! O-O
                            ! hard-coded p = 0

                            help1 = rijsw-r0_OO
                            if ( help1 > cutofflimit ) goto 100 ! skip twobody
                            help2 = epsA_OO*exp(1.0_DP/help1)
                            help4 = 1.0_DP/rijsw**q_OO
                            ph = -help4*help2
                            help1 = help1*help1
                            dph = help2*help4*( q_OO/rijsw + 1.0_DP/help1 )
                            dph = dph*opswsigma  ! change to eV/A

                        else if ( wti == 3 .and. wtj == 3 ) then
                            ! Ge-Ge
                            ! hard-coded p = 4, q = 0

                            rijsc=rijsw/GeGe_Lscale  ! scale r for Ge
                            help1 = rijsc-r0_SiSi
                            if ( help1 > cutofflimit ) goto 100 ! skip twobody
                            help1 = 1.0_DP/help1
                            help2 = epsA_GeGe*exp(help1)
                            help3 = rijsc*rijsc
                            help3 = B_GeGe/(help3*help3)
                            help5 = help3-1.0_DP
                            ph = help5*help2
                            dph = -help2*( help3*p_SiSi/rijsc + help5*help1*help1 )
                            dph = dph*opswsigma/GeGe_Lscale  ! to eV/A, scale for Ge

                        else if (( wti == 1 .and. wtj == 3 ) .or.( wti == 3 .and. wtj == 1 )) then
                            ! Ge-Ge
                            ! hard-coded p = 4, q = 0

                            rijsc=rijsw/GeSi_Lscale  ! scale r for Ge
                            help1 = rijsc-r0_SiSi
                            if ( help1 > cutofflimit ) goto 100 ! skip twobody
                            help1 = 1.0_DP/help1
                            help2 = epsA_GeSi*exp(help1)
                            help3 = rijsc*rijsc
                            help3 = B_GeSi/(help3*help3)
                            help5 = help3-1.0_DP
                            ph = help5*help2
                            dph = -help2*( help3*p_SiSi/rijsc + help5*help1*help1 )
                            dph = dph*opswsigma/GeSi_Lscale  ! to eV/A, scale for Ge

                        else  ! Si/Ge-O or O-Si/Ge

                            ! Calculate the coordination number of the O atom
                            potmode_sel: select case (potmode)
                            case (210)
                                ! The twobody Newton's III law was handled such that here
                                ! atomi is always the oxygen atom.
                                call bond_softening(coordination, gij, dgijdz)

                            case (211)
                                ! Potmode 211 removes the bond softening function.
                                gij = 1
                                dgijdz = 0

                            case default
                                call my_mpi_abort('Unknown silica potmode', potmode)
                                cycle ! Silence compiler warning
                            end select potmode_sel


                            if (( wti == 2 .and. wtj == 1 ) .or.( wti == 1 .and. wtj == 2 )) then
                                ! Si-O

                                help1 = rijsw-r0_SiO
                                if ( help1 > cutofflimit ) goto 100 ! skip twobody
                                help1 = 1.0_DP/help1
                                help2 = gij*epsA_SiO*exp(help1)   ! softening applied
                                help3 = B_SiO/rijsw**p_SiO
                                help4 = 1.0_DP/rijsw**q_SiO
                                help5 = help3-help4
                                ph = help5*help2

                                help1 = help1*help1
                                dph = help2*( help4*q_SiO/rijsw - help3*p_SiO/rijsw - help5*help1 )
                                dph = dph*opswsigma  ! to eV/A

                            else if (( wti == 2 .and. wtj == 3 ) .or.( wti == 3 .and. wtj == 2 )) then
                                ! Ge-O

                                rijsc=rijsw/GeO_Lscale  ! scale r for Ge
                                help1 = rijsc-r0_SiO
                                if ( help1 > cutofflimit ) goto 100 ! skip twobody
                                help1 = 1.0_DP/help1
                                help2 = gij*epsA_GeO*exp(help1)   ! softening applied
                                help3 = B_GeO/rijsc**p_SiO
                                help4 = 1.0_DP/rijsc**q_SiO
                                help5 = help3-help4
                                ph = help5*help2

                                help1 = help1*help1
                                dph = help2*( help4*q_SiO/rijsc - help3*p_SiO/rijsc - help5*help1 )
                                dph = dph*opswsigma/GeO_Lscale  ! to eV/A, scale for Ge

                            else
                                call logger("BUG: silica_wat unhandled atom types", wti, wtj, 0)
                                call my_mpi_abort("BUG: silica_wat unhandled atom types", wti)
                                cycle ! Silence compiler warning
                            end if

                            ! Force contributions to all neighbors from derivative of
                            ! the coordination number in gij. Still needs factor of
                            ! Fermi function from reppot below.
                            if (dgijdz /= 0) then
                                dphz = ph / gij * dgijdz
                            end if

                        end if ! End of atom pair type selection

                        ! Short range repulsive potential times Fermi function.
                        if (rij < reppotcutin) then
                            t1 = mpi_wtime()

                            ! The reppot expects the energy of the atom pair, which is
                            ! what we have in ph. Outputs the full force on each atom
                            ! in dph.

                            call reppot_fermi(rij, ph, dph, bfs(wti,wtj), rfs(wti,wtj), &
                                atypi, atypj, fermi, dfermi)

                            dphz = dphz * fermi

                            tmr(TMR_SEMICON_REPPOT) = tmr(TMR_SEMICON_REPPOT) + (mpi_wtime() - t1)
                        end if

                        dphz_tot = dphz_tot + dphz

                    else if (iac(atypi,atypj) == 2) then
                        ! Short range repulsion only
                        ! As above, energy for the full atom pair, force on each atom.
                        call reppot_only(rij, ph, dph, atypi, atypj)

                    else
                        ! Wrong interaction type or error in algorithm
                        call logger("ERROR in wat.f90 impossible interaction type")
                        call logger("myproc:", myproc, 4)
                        call logger("i:     ", i, 4)
                        call logger("j:     ", j, 4)
                        call logger("ATYPI: ", ATYPI, 4)
                        call logger("ATYPJ: ", ATYPJ, 4)
                        call my_mpi_abort('ERROR in silica_wat.f90 or impossible interaction type', myproc)
                    endif

                    halfph = 0.5_DP*ph
                    Epair(atomi) = Epair(atomi)+halfph
                    Epair(atomj) = Epair(atomj)+halfph

                    ai = -dph*oprij

                    daix = dxij * ai
                    xnp(i) = xnp(i) + daix
                    xnp(j) = xnp(j) - daix

                    daiy = dyij * ai
                    xnp(i+1) = xnp(i+1) + daiy
                    xnp(j+1) = xnp(j+1) - daiy

                    daiz = dzij * ai
                    xnp(i+2) = xnp(i+2) + daiz
                    xnp(j+2) = xnp(j+2) - daiz

                    ! Halve the force so the virials are correct, i.e. 0.5*f*r
                    ! for each atom to get the correct total virial.
                    daix = 0.5_DP * daix
                    daiy = 0.5_DP * daiy
                    daiz = 0.5_DP * daiz

                    wxxi(atomi) = wxxi(atomi) + daix * dxij
                    wxxi(atomj) = wxxi(atomj) + daix * dxij
                    wyyi(atomi) = wyyi(atomi) + daiy * dyij
                    wyyi(atomj) = wyyi(atomj) + daiy * dyij
                    wzzi(atomi) = wzzi(atomi) + daiz * dzij
                    wzzi(atomj) = wzzi(atomj) + daiz * dzij

                    if (calc_vir) then
                        wxyi(atomi) = wxyi(atomi) + daix * dyij
                        wxyi(atomj) = wxyi(atomj) + daix * dyij
                        wxzi(atomi) = wxzi(atomi) + daix * dzij
                        wxzi(atomj) = wxzi(atomj) + daix * dzij
                        wyzi(atomi) = wyzi(atomi) + daiy * dzij
                        wyzi(atomj) = wyzi(atomj) + daiy * dzij
                    end if

                end if


100             continue  ! Skipping two-body leads here

                if ( wti == 2 .and. wtj == 2 ) cycle   ! no 3-body interaction for O-O

                if (iac(atypi,atypj) /= 1) cycle     ! Handle other interaction types: skip rest

                ! 3-body interaction loop starts -------------------------------------
                nlistposk = nlistposj
                do jngbr=ingbr+1,nngbr
                    nlistposk = nlistposk+1
                    atomk = nborlist(nlistposk)
                    k = atomk*3-2

                    atypk = parcas_types(jngbr)          ! Parcas type

                    if (iac(atypi,atypk) /= 1) cycle     ! Handle other interaction types

                    wtk = wata_types(atypk)              ! Watanabe type

                    if (three_interact_arr(wti, wtj, wtk) == THREE_NONE) cycle

                    ! Check cutoffs. Long-range r0_2_ cutoff is always longer than
                    ! the short-range r0_1_ cutoff, so check it here. For type triplets
                    ! without long-range interaction, r0_1_ is copied into r0_2_, so
                    ! it also skips here.
                    diff2ij = rijsw - r0_2_arr(wti,wtj,wtk)
                    if (diff2ij > cutofflimit) cycle

                    riksw  = rsw_arr(jngbr)  ! SW units

                    diff2ik = riksw - r0_2_arr(wti,wtk,wtj)
                    if (diff2ik > cutofflimit) cycle

                    oprik = 1.0_DP / r_arr(jngbr)  ! 1/Angstrom
                    dxik   = dx_arr(jngbr)  ! Angstrom
                    dyik   = dy_arr(jngbr)  ! Angstrom
                    dzik   = dz_arr(jngbr)  ! Angstrom

                    ! Use costheta0_1 = costheta0_2
                    cosjik = (dxij*dxik + dyij*dyik + dzij*dzik)*oprij*oprik
                    dcos = cosjik - costheta0_1_arr(wti,wtj,wtk)
                    if (abs(dcos) < -cutofflimit) cycle


                    if (three_interact_arr(wti, wtj, wtk) == THREE_SHORT) then

                        ! diff2ij and diff2ik are actually diff1ij and diff1ik
                        call threebody_short_range( &
                            wti, wtj, wtk, dcos, diff2ij, diff2ik, coss1, &
                            gamij1, gamik1, biglambda1, bigtheta1)

                        biglambda2 = 0.0
                        gamij2 = 0.0
                        gamik2 = 0.0
                        coss2 = 0.0
                        lambdatheta2 = 0.0
                    else
                        if (three_interact_arr(wti,wtj, wtk) == THREE_LONG_NO_OXY) then

                            call threebody_with_long_range_no_oxygen( &
                                wti, wtj, wtk, coordination, dcos, &
                                diff2ij, diff2ik, rijsw, riksw, coss1, coss2, &
                                gamij1, gamik1, gamij2, gamik2, &
                                biglambda1, biglambda2, bigtheta1, bigtheta2, dphz)

                        else if (three_interact_arr(wti, wtj, wtk) == THREE_LONG_OXY) then

                            call threebody_with_long_range_oxygen( &
                                wti, wtj, wtk, coordination, dcos, &
                                diff2ij, diff2ik, rijsw, riksw, coss1, coss2, &
                                gamij1, gamik1, gamij2, gamik2, &
                                biglambda1, biglambda2, bigtheta1, bigtheta2, dphz)

                        else
                            call my_mpi_abort("BUG: Silica_wat: Unexpected three_interact", &
                                three_interact_arr(wti, wtj, wtk))
                            cycle  ! Silence compiler warnings
                        end if

                        ! Force contributions to all neighbors from derivative of
                        ! the coordination number in lambda.
                        dphz_tot = dphz_tot + dphz

                        lambdatheta2 = biglambda2 * bigtheta2
                    endif

                    lambdatheta1 = biglambda1*bigtheta1

                    hjik = epsilon * (lambdatheta1 + lambdatheta2)
                    Ethree(atomi) = Ethree(atomi) + hjik

                    help1 = epsilon * (biglambda1*coss1 + biglambda2*coss2)
                    help2 = help1 * cosjik

                    dphij1 = eosigma * (lambdatheta1*gamij1 + lambdatheta2*gamij2) * oprij
                    dphij1 = dphij1 + help2 * oprij**2
                    dphik1 = eosigma * (lambdatheta1*gamik1 + lambdatheta2*gamik2) * oprik
                    dphik1 = dphik1 + help2 * oprik**2
                    dphij2 = help1 * oprij * oprik
                    dphik2 = dphij2

                    help1 = -dphij1*dxij + dphij2*dxik
                    help2 = -dphik1*dxik + dphik2*dxij
                    xnp(i) = xnp(i) - help1 - help2
                    xnp(j) = xnp(j) + help1
                    xnp(k) = xnp(k) + help2
                    wxxi(atomi) = wxxi(atomi) - help1*dxij - help2*dxik

                    if (calc_vir) then
                        wxyi(atomi) = wxyi(atomi) - help1*dyij - help2*dyik
                        wxzi(atomi) = wxzi(atomi) - help1*dzij - help2*dzik
                    endif

                    help1 = - dphij1*dyij + dphij2*dyik
                    help2 = - dphik1*dyik + dphik2*dyij
                    xnp(i+1) = xnp(i+1) - help1 - help2
                    xnp(j+1) = xnp(j+1) + help1
                    xnp(k+1) = xnp(k+1) + help2
                    wyyi(atomi) = wyyi(atomi) - help1*dyij - help2*dyik

                    if (calc_vir) then
                        wyzi(atomi) = wyzi(atomi) - help1*dzij - help2*dzik
                    endif

                    help1 = - dphij1*dzij + dphij2*dzik
                    help2 = - dphik1*dzik + dphik2*dzij
                    xnp(i+2) = xnp(i+2) - help1 - help2
                    xnp(j+2) = xnp(j+2) + help1
                    xnp(k+2) = xnp(k+2) + help2
                    wzzi(atomi) = wzzi(atomi) - help1*dzij - help2*dzik

                end do ! end of the 3-body loop over atoms k -------------------------

            end do ! end of the 2-body loop over atoms j ----------------------------


            ! Apply all the forces terms from the derivative of the coordination
            ! function z(i), used in gij and the threebody lambda.
            if (dphz_tot /= 0) then
                call apply_dz_forces(dphz_tot, atomi, nabors(atomi) - 1, nborlist, &
                    dzdr_arr, dx_arr, dy_arr, dz_arr, r_arr, &
                    xnp, wxxi, wyyi, wzzi, wxyi, wxzi, wyzi, calc_vir)
            end if

        end do ! end of the main loop over atoms i ---------------------------------


        ! Send back forces, energies and virials to the neighboring nodes.
        if (nprocs > 1) then
            t1 = mpi_wtime()
            if (calc_vir) then
                call potential_pass_back_border_atoms( &
                    xnp, Epair, wxxi, wyyi, wzzi, wxyi, wxzi, wyzi)
            else
                call potential_pass_back_border_atoms( &
                    xnp, Epair, wxxi, wyyi, wzzi)
            end if
            tmr(TMR_SEMICON_COMMS) = tmr(TMR_SEMICON_COMMS) + (mpi_wtime() - t1)
        end if


        ! Convert from eV/A to internal units.
        do i = 1, 3*myatoms, 3
            xnp(i+0) = xnp(i+0) / box(1)
            xnp(i+1) = xnp(i+1) / box(2)
            xnp(i+2) = xnp(i+2) / box(3)
        end do

    end subroutine silica_wat_force


    subroutine apply_dz_forces(dphz, atomi, nlistpos, nborlist, &
            dzdr_arr, dx_arr, dy_arr, dz_arr, r_arr, &
            xnp, wxxi, wyyi, wzzi, wxyi, wxzi, wyzi, calc_vir)

        real(DP), intent(in) :: dphz
        integer, intent(in) :: atomi
        integer, intent(in) :: nlistpos, nborlist(:)
        real(DP), intent(in) :: dzdr_arr(NNMAX)
        real(DP), intent(in) :: dx_arr(NNMAX), dy_arr(NNMAX), dz_arr(NNMAX)
        real(DP), intent(in) :: r_arr(NNMAX)
        real(DP), contiguous, intent(inout) :: xnp(:)
        real(DP), contiguous, intent(inout) :: wxxi(:), wyyi(:), wzzi(:)
        real(DP), contiguous, intent(inout) :: wxyi(:), wxzi(:), wyzi(:)
        logical, intent(in) :: calc_vir

        integer :: i, l, atoml, lngbr, nngbr
        real(DP) :: ai, daix, daiy, daiz

        nngbr = nborlist(nlistpos)
        i = 3*atomi - 2

        do lngbr = 1, nngbr
            if (dzdr_arr(lngbr) == 0) cycle

            atoml = nborlist(nlistpos + lngbr)
            l = 3*atoml - 2

            ai = -dphz * dzdr_arr(lngbr) / r_arr(lngbr)

            daix = ai * dx_arr(lngbr)
            xnp(i) = xnp(i) + daix
            xnp(l) = xnp(l) - daix

            daiy = ai * dy_arr(lngbr)
            xnp(i+1) = xnp(i+1) + daiy
            xnp(l+1) = xnp(l+1) - daiy

            daiz = ai * dz_arr(lngbr)
            xnp(i+2) = xnp(i+2) + daiz
            xnp(l+2) = xnp(l+2) - daiz

            daix = 0.5 * daix
            daiy = 0.5 * daiy
            daiz = 0.5 * daiz

            wxxi(atomi) = wxxi(atomi) + daix * dx_arr(lngbr)
            wxxi(atoml) = wxxi(atoml) + daix * dx_arr(lngbr)
            wyyi(atomi) = wyyi(atomi) + daiy * dy_arr(lngbr)
            wyyi(atoml) = wyyi(atoml) + daiy * dy_arr(lngbr)
            wzzi(atomi) = wzzi(atomi) + daiz * dz_arr(lngbr)
            wzzi(atoml) = wzzi(atoml) + daiz * dz_arr(lngbr)

            if (calc_vir) then
                wxyi(atomi) = wxyi(atomi) + daix * dy_arr(lngbr)
                wxyi(atoml) = wxyi(atoml) + daix * dy_arr(lngbr)
                wxzi(atomi) = wxzi(atomi) + daix * dz_arr(lngbr)
                wxzi(atoml) = wxzi(atoml) + daix * dz_arr(lngbr)
                wyzi(atomi) = wyzi(atomi) + daiy * dz_arr(lngbr)
                wyzi(atoml) = wyzi(atoml) + daiy * dz_arr(lngbr)
            end if
        end do

    end subroutine apply_dz_forces


    !
    ! Calculate and save distances to all neighbours of the atom at buf index i.
    ! Compute the coordination number into coord, and its derivatives into dzdr_arr.
    !
    pure subroutine init_neighbors(i, wti, nlistpos, nborlist, is_pbc, halfbox, &
            atype, parcas_types, r_arr, dx_arr, dy_arr, dz_arr, coord, dzdr_arr)

        integer, intent(in) :: i, wti, nlistpos
        integer, intent(in) :: nborlist(:)
        logical, intent(in) :: is_pbc(3)
        real(DP), intent(in) :: halfbox(3)
        integer, contiguous, intent(in) :: atype(:)
        integer, intent(out) :: parcas_types(NNMAX)
        real(DP), intent(out) :: r_arr(NNMAX)
        real(DP), intent(out) :: dx_arr(NNMAX), dy_arr(NNMAX), dz_arr(NNMAX)
        real(DP), intent(out) :: coord
        real(DP), intent(out) :: dzdr_arr(NNMAX)

        integer :: ingbr, atomj, j, jtype
        real(DP) :: dx, dy, dz, r
        real(DP) :: help1, help2

        coord = 0

        do ingbr = 1, nborlist(nlistpos)
            atomj = nborlist(nlistpos + ingbr)
            j = 3*atomj - 2

            jtype = abs(atype(atomj))
            parcas_types(ingbr) = jtype

            dx = buf(i) - buf(j)
            if (is_pbc(1)) then ! periodic x boundary
                if (dx >= halfbox(1)) then
                    dx = dx - 2 * halfbox(1)
                else if (dx < -halfbox(1)) then
                    dx = dx + 2 * halfbox(1)
                end if
            end if
            dx_arr(ingbr) = dx

            dy = buf(i+1) - buf(j+1)
            if (is_pbc(2)) then ! periodic y boundary
                if (dy >= halfbox(2)) then
                    dy = dy - 2 * halfbox(2)
                else if (dy < -halfbox(2)) then
                    dy = dy + 2 * halfbox(2)
                end if
            end if
            dy_arr(ingbr) = dy

            dz = buf(i+2) - buf(j+2)
            if (is_pbc(3)) then ! periodic z boundary
                if (dz >= halfbox(3)) then
                    dz = dz - 2 * halfbox(3)
                else if (dz < -halfbox(3)) then
                    dz = dz + 2 * halfbox(3)
                end if
            end if
            dz_arr(ingbr) = dz

            r = sqrt(dx**2 + dy**2 + dz**2)
            r_arr(ingbr) = r


            ! Compute the coordination number
            dzdr_arr(ingbr) = 0

            ! Only count heterotype atoms, treating Si and Ge as equal.
            if ((wti == 2) .eqv. (wata_types(jtype) == 2)) cycle

            if (r < coordmin) then
                coord = coord + 1
            else if (r < coordmax) then
                help1 = (r - Rco + Dco) / Dco

                ! original
                !coord = coord + 1.0_DP - 0.5_DP*help1 + sin(pi*help1)/TWOPI

                ! by Juha Samela, gives 0 at lower limit
                !coord = coord + 0.5_DP*( 1.0_DP + sin(pi*(help1-0.5_DP)) )

                ! by Kai Nordlund 26.6.2007, see sicasc/sio2/test/README
                help2 = cos(help1 / 2.0_DP * PI)
                coord = coord + 0.5_DP * (1.0_DP + help2)

                dzdr_arr(ingbr) = - PI / (4 * Dco) * sqrt(1 - help2**2)
            end if
        end do

    end subroutine init_neighbors


    !
    ! Compute the bond softening factor for a Si-O or Ge-O pair.
    ! zetaO is the coordination number of the O atom.
    !
    pure subroutine bond_softening(zetaO, gij, dgijdz)

        real(DP), intent(in) :: zetaO
        real(DP), intent(out) :: gij, dgijdz

        real(real64b) :: help1, help2, help3, help4

        ! The original bond softening function WITH suboxide penalty
        !if (zetai > 0.0 .and. zetaj > 0.0) then
        !   if ((wti == 1) .or. (wti == 3)) then
        !      if (zetai < 4.0_DP) then
        !         gSi = (p1*sqrt(zetai+p2) - p4)*exp(p3/(zetai-4.0_DP)) + p4
        !      else
        !         gSi = p4
        !      end if
        !      gO = p5*exp(p8*(zetaj-p9)*(zetaj-p9))/(exp((p6-zetaj)/p7) + 1.0_DP)
        !   else
        !      if (zetaj < 4.0_DP) then
        !         gSi = (p1*sqrt(zetaj+p2) - p4)*exp(p3/(zetaj-4.0_DP)) + p4
        !      else
        !         gSi = p4
        !      end if
        !      gO = p5*exp(p8*(zetai-p9)*(zetai-p9))/(exp((p6-zetai)/p7) + 1.0_DP)
        !   end if
        !   gij = gSi*gO
        !else
        !   gij = 1.0_DP
        !end if

        ! Bond-softening WITHOUT suboxide penalty
        if (zetaO > 2) then
            help1 = zetaO - p9
            help2 = exp(p8 * help1**2)
            help3 = exp((p6 - zetaO) * op7)
            help4 = help3 + 1

            gij = p5 * help2 / help4
            dgijdz = p5 * help2 * (2 * p8 * help1 * help4 + op7 * help3) / help4**2
        else
            gij = 1
            dgijdz = 0
        end if

    end subroutine bond_softening


    subroutine threebody_short_range( &
            wti, wtj, wtk, dcos, diff1ij, diff1ik, coss1, &
            gamij1, gamik1, biglambda1, bigtheta1)

        integer, intent(in) :: wti, wtj, wtk  ! element types
        real(kind=real64b), intent(in) :: dcos
        real(kind=real64b), intent(inout) :: diff1ij, diff1ik
        real(kind=real64b), intent(out) :: coss1
        real(kind=real64b), intent(out) :: gamij1, gamik1
        real(kind=real64b), intent(out) :: biglambda1, bigtheta1

        real(kind=real64b) :: lambda1, dcossq

        dcossq = dcos*dcos
        coss1   = 2.0_DP*dcos + 3.0_DP*alpha_1_arr(wti, wtj, wtk)*dcossq

        ! Only the short range 3-body interaction
        diff1ij = 1.0_DP/diff1ij
        diff1ik = 1.0_DP/diff1ik
        bigtheta1 = dcossq + alpha_1_arr(wti, wtj, wtk)*dcossq*dcos
        ! nyy_1 is zero here, so lambda1 simplifies to myy_1.
        lambda1 = myy_1_arr(wti, wtj, wtk)
        gamij1  = gamma_1_arr(wti, wtj, wtk)*diff1ij
        gamik1  = gamma_1_arr(wti, wtk, wtj)*diff1ik

        biglambda1 = lambda1 * exp(gamij1 + gamik1)
        gamij1 = gamij1*diff1ij
        gamik1 = gamik1*diff1ik
    end subroutine threebody_short_range


    subroutine threebody_with_long_range_no_oxygen( &
            wti, wtj, wtk, coordination, dcos, &
            diff2ij, diff2ik, rijsw, riksw, coss1, coss2, &
            gamij1, gamik1, gamij2, gamik2, &
            biglambda1, biglambda2, bigtheta1, bigtheta2, dphz)

        integer, intent(in) :: wti, wtj, wtk  ! element types
        real(kind=real64b), intent(out) :: coss1, coss2
        real(kind=real64b), intent(out) :: gamij1, gamik1, gamij2, gamik2
        real(kind=real64b), intent(out) :: biglambda1, biglambda2
        real(kind=real64b), intent(out) :: bigtheta1, bigtheta2
        real(kind=real64b), intent(out) :: dphz
        real(kind=real64b), intent(inout) :: diff2ij, diff2ik

        real(kind=real64b), intent(in) :: dcos
        real(kind=real64b), intent(in) :: rijsw, riksw
        real(kind=real64b), intent(in) :: coordination

        real(kind=real64b) :: lambda1, lambda2
        real(kind=real64b) :: dcossq
        real(kind=real64b) :: diff1ij, diff1ik
        real(kind=real64b) :: expgam, expterm

        dcossq = dcos*dcos
        coss1   = 2.0_DP*dcos + 3.0_DP*alpha_1_arr(wti, wtj, wtk)*dcossq
        coss2   = 2.0_DP*dcos + 3.0_DP*alpha_2_arr(wti, wtj, wtk)*dcossq
        diff1ij = rijsw - r0_1_arr(wti, wtj, wtk)
        diff1ik = riksw - r0_1_arr(wti, wtk, wtj)

        ! Short range 3-body interaction
        if (diff1ij < cutofflimit .and. diff1ik < cutofflimit) then
            diff1ij = 1.0_DP/diff1ij
            diff1ik = 1.0_DP/diff1ik
            bigtheta1 = dcossq + alpha_1_arr(wti, wtj, wtk)*dcossq*dcos
            ! nyy_1 is zero here, so lambda1 simplifies to myy_1.
            lambda1 = myy_1_arr(wti, wtj, wtk)
            gamij1  = gamma_1_arr(wti, wtj, wtk)*diff1ij
            gamik1  = gamma_1_arr(wti, wtk, wtj)*diff1ik

            biglambda1 = lambda1 * exp(gamij1 + gamik1)
            gamij1 = gamij1*diff1ij
            gamik1 = gamik1*diff1ik
        else
            bigtheta1 = 0.0
            biglambda1 = 0.0
            gamij1 = 0.0
            gamik1 = 0.0
        endif

        ! Long range 3-body interaction
        bigtheta2 = dcossq + alpha_2_arr(wti, wtj, wtk)*dcossq*dcos

        ! lambda had wrong ")" in the Watanabe paper.
        ! Optimize away the exp-call where possible, since pure Si/Ge hits that case.
        if (coordination == 0d0) then
            expterm = 1.0_DP
        else
            expterm = exp(-ksi_2_SiSiSi * coordination**2)
        end if
        lambda2 = myy_2_arr(wti, wtj, wtk) * (1.0_DP + nyy_2_SiSiSi * expterm)

        diff2ij = 1.0_DP/diff2ij
        diff2ik = 1.0_DP/diff2ik
        gamij2  = gamma_2_arr(wti, wtj, wtk)*diff2ij
        gamik2  = gamma_2_arr(wti, wtk, wtj)*diff2ik
        expgam = exp(gamij2 + gamik2)

        ! Forces on neighbors from z(i) derivative, without actual dzdr.
        if (lambda2 - myy_2_arr(wti,wtj,wtk) == 0) then
            dphz = 0
        else
            dphz = -2 * epsilon * ksi_2_SiSiSi * bigtheta2 * expgam * &
                coordination * (lambda2 - myy_2_arr(wti,wtj,wtk))
        end if

        biglambda2 = lambda2 * expgam
        gamij2 = gamij2 * diff2ij
        gamik2 = gamik2 * diff2ik
    end subroutine threebody_with_long_range_no_oxygen


    subroutine threebody_with_long_range_oxygen( &
            wti, wtj, wtk, coordination, dcos, &
            diff2ij, diff2ik, rijsw, riksw, coss1, coss2, &
            gamij1, gamik1, gamij2, gamik2, &
            biglambda1, biglambda2, bigtheta1, bigtheta2, dphz)

        integer, intent(in) :: wti, wtj, wtk  ! element types
        real(kind=real64b), intent(out) :: coss1, coss2
        real(kind=real64b), intent(out) :: gamij1, gamik1, gamij2, gamik2
        real(kind=real64b), intent(out) :: biglambda1, biglambda2
        real(kind=real64b), intent(out) :: bigtheta1, bigtheta2
        real(kind=real64b), intent(out) :: dphz
        real(kind=real64b), intent(inout) :: diff2ij, diff2ik

        real(kind=real64b), intent(in) :: dcos
        real(kind=real64b), intent(in) :: rijsw, riksw
        real(kind=real64b), intent(in) :: coordination

        real(kind=real64b) :: lambda1, lambda2
        real(kind=real64b) :: dcossq
        real(kind=real64b) :: diff1ij, diff1ik
        real(kind=real64b) :: expgam, expterm


        dcossq = dcos*dcos
        coss1  = 2.0_DP*dcos + 3.0_DP*alpha_1_arr(wti, wtj, wtk)*dcossq
        coss2  = 2.0_DP*dcos + 3.0_DP*alpha_2_arr(wti, wtj, wtk)*dcossq
        diff1ij = rijsw - r0_1_arr(wti, wtj, wtk)
        diff1ik = riksw - r0_1_arr(wti, wtk, wtj)

        ! Short range 3-body interaction
        if (diff1ij < cutofflimit .and. diff1ik < cutofflimit) then
            diff1ij = 1.0_DP/diff1ij
            diff1ik = 1.0_DP/diff1ik
            bigtheta1 = dcossq + alpha_1_arr(wti, wtj, wtk)*dcossq*dcos

            ! lambda had wrong ")" in the Watanabe paper.
            ! Optimize away the exp-call where possible, since pure Si/Ge hits that case.
            if (coordination == 0d0) then
                block
                    real(real64b), parameter :: const = exp(-ksi_1_SiSiO * z0_1_SiSiO**2)
                    expterm = const
                end block
            else
                expterm = exp(-ksi_1_SiSiO * (coordination - z0_1_SiSiO)**2)
            end if
            lambda1 = myy_1_arr(wti, wtj, wtk) * (1.0_DP + nyy_1_SiSiO * expterm)

            gamij1  = gamma_1_arr(wti, wtj, wtk) * diff1ij
            gamik1  = gamma_1_arr(wti, wtk, wtj) * diff1ik
            expgam =  exp(gamij1 + gamik1)

            ! Forces on neighbors from z(i) derivative, without actual dzdr.
            dphz = -2 * epsilon * ksi_1_SiSiO * bigtheta1 * expgam * &
                (coordination - z0_1_SiSiO) * (lambda1 - myy_1_arr(wti,wtj,wtk))

            biglambda1 = lambda1 * expgam
            gamij1 = gamij1*diff1ij
            gamik1 = gamik1*diff1ik
        else
            dphz = 0.0
            bigtheta1 = 0.0
            biglambda1 = 0.0
            gamij1 = 0.0
            gamik1 = 0.0
        endif

        ! Long range 3-body interaction
        bigtheta2 = dcossq + alpha_2_arr(wti, wtj, wtk)*dcossq*dcos
        ! nyy_2 is zero here, so lambda1 simplifies to myy_2.
        lambda2 = myy_2_arr(wti, wtj, wtk)
        diff2ij = 1.0_DP/diff2ij
        diff2ik = 1.0_DP/diff2ik
        gamij2  = gamma_2_arr(wti, wtj, wtk) * diff2ij
        gamik2  = gamma_2_arr(wti, wtk, wtj) * diff2ik

        biglambda2 = lambda2 * exp(gamij2 + gamik2)
        gamij2 = gamij2 * diff2ij
        gamik2 = gamik2 * diff2ik
    end subroutine threebody_with_long_range_oxygen


    !-----------------------------------------------------------------------------
    !
    ! silica_wat_init - Initialization routine for the module
    !
    ! Output values
    !   Assigns values to the following tables:
    !
    !   Array silica_wat_types maps the Parcas atom types given in md.in file
    !   to the local type indices hard-coded in this module.
    !
    !   Array rcut contains the cut-off distances for each pair of atom types.
    !   Neighbourlist radius is skin*rcutmax, where rcutmax is the max value
    !   in table rcut.
    !
    ! Warning: Includes hard-coded parameters
    !
    !-----------------------------------------------.-----------------------------

    subroutine init_silica_wat()

        integer i, j, wti, wtj
        real(real64b) :: cutoffmax

        call fill_param_arrays()

        allocate(wata_types(itypelow:itypehigh))

        do i = itypelow, itypehigh
            if (element(i) == 'Si') then
                wata_types(i) = 1
            else if (element(i) == 'O') then
                wata_types(i) = 2
            else if (element(i) == 'Ge') then
                wata_types(i) = 3
            else
                wata_types(i) = 0

                if (any(iac(i,:) == 1)) then
                    call logger("ERROR in silica_wat.f90: unknown element for iac==1")
                    call logger("myproc: ", myproc, 4)
                    call logger("atype:  ", i, 4)
                    call logger("element:", element(i), 4)
                    call my_mpi_abort('ERROR in silica_wat.f90: unknown element for iac==1', i)
                end if
            end if
        end do

        do i = itypelow, itypehigh
            do j = itypelow, itypehigh
                wti = wata_types(i)
                wtj = wata_types(j)

                if (wti /= 0 .and. wtj /= 0) then
                    rcut(i,j) = max(&
                        swsigma * r0_arr(wti,wtj), &
                        swsigma * maxval(r0_1_arr(wti,wtj,:)), &
                        swsigma * maxval(r0_2_arr(wti,wtj,:)), &
                        coordmax)
                end if

                ! Should not be used for wata potential,
                ! but may be used for external pair pots.
                if (rcutin(i,j) >= 0.0d0) then
                    rcut(i,j) = rcutin(i,j)
                end if

            end do
        end do

        cutoffmax = maxval(rcut)

        ! Unless overridden by rcutin, use the largest cutoff
        ! in the system for pure reppot interactions.
        where (iac == 2 .and. rcutin < 0)
            rcut = cutoffmax
        end where

        if (iprint) then
            call logger('Silica_Wat maximum cutoff', cutoffmax, 0)
        end if

    end subroutine init_silica_wat

end module silica_wat_mod
