module random
    use datatypes, only: real64b

    implicit none

    private
    public :: MyRanf
    public :: gasdev


contains
    !***********************************************************************

    !KN
    ! Replaced the old, horrifying random number generator, with this,
    ! which should be at least fairly decent
    !

    real(kind=real64b) function MyRanf(iseed)
        !       ---------------------------
        !       Random number generator
        !       uniform distribution [0,1[
        !       If seed is -1, previous seed is used
        !       ---------------------------

        integer, intent(in) :: iseed
        integer, save :: ix
        integer, parameter :: ii =127773, jj=2147483647
        integer, parameter :: i1=16807, i2=2836
        real(kind=real64b), parameter :: p = 4.656612875d-10

        integer :: k1

        if (iseed .ne. -1 .and. iseed .ne. 0) ix=iseed

        k1 = ix/ii
        ix = i1*( ix - k1*ii) - k1 * i2
        if ( ix < 0) ix = ix + jj
        MyRanf = ix * p
        return
    end function MyRanf

    !*********************************************************************
    !       ---------------------------------------------------
    !       Random numbers with normal (Gaussian) distribution.
    !       See W.H.Press et al., Numerical Recipies, page 203
    !       ---------------------------------------------------
    real(kind=real64b) function gasdev()
        integer :: iset
        real(kind=real64b) :: gset,fac,v1,v2,r
        data iset /0/
        save iset,gset
        !        real(kind=real64b) MyRanf

        if (iset.eq.0) then
1           v1 = 2.*MyRanf(0)-1.
            v2 = 2.*MyRanf(0)-1.
            r = v1*v1+v2*v2
            if (r.ge.1.) goto 1
            fac = sqrt(-2.*log(r)/r)
            gset = v1*fac
            gasdev = v2*fac
            iset = 1
        else
            gasdev = gset
            iset = 0
        endif
        return
    end function gasdev
end module random
