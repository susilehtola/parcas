/*
 * output.c
 *
 *  Created on: 3.7.2012
 *      Author: dlandau
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "output.h"
#include "types.h"
#include "input.h"

FILE * open_output_file_with_correct_name(const struct configuration * conf) {
      FILE * out_fp;
      char * new_file_name = (char *) malloc(strlen(conf->fname) + 4 + 1);
      strcpy(new_file_name, conf->fname);
      if (conf->format == FORMAT_XYZ)
        out_fp = try_fopen(strcat(new_file_name,".xyz"), "w+");
      else if (conf->format == FORMAT_ASCII)
        out_fp = try_fopen(strcat(new_file_name,".txt"), "w+");
      free(new_file_name);
      return out_fp;
}

void print_atom(const struct configuration* conf,
        const struct variable_header* vhdr, const struct fixed_header* fhdr,
        const struct atom* atom, FILE * out_fp) {
  if (conf->format == FORMAT_ASCII)
    fprintf(out_fp,"%lld %4s %d ", (long long) atom->ind,
            vhdr->types[abs(atom->type) - fhdr->mintype], (int) atom->type);
  else if (conf->format == FORMAT_XYZ)
    fprintf(out_fp,"%4s ", vhdr->types[abs(atom->type) - fhdr->mintype]);

  int f;
  for (f = 0; f < fhdr->n_fields + 3; f++) {
    fprintf(out_fp,"%g ", atom->fields[f]);
    if (conf->format == FORMAT_XYZ && f == 2) {
      if (conf->print_xyz_indices) {
        fprintf(out_fp,"%lld %d ", (long long) atom->ind, (int) atom->type);
      }
      else {
        fprintf(out_fp,"%d ", (int) atom->type);
      }
    }
  }
  putc('\n', out_fp);
}


void print_headers(const struct fixed_header * fhdr,
        const struct variable_header * vhdr,
        const struct configuration * conf,
        FILE * out_fp) {

  int i;
  if (conf->format == FORMAT_ASCII) {
    fprintf(out_fp,"-----header------\n");
    fprintf(out_fp,"file byte order: %s\n",
            (fhdr->file_order == BO_BIG_ENDIAN) ? "big endian" : "little endian");

    fprintf(out_fp,"prototype real: 0x%08x (should be 0x%08x)\n",
            fhdr->prot_real, 0x3f302010);
    fprintf(out_fp,"prototype integer: 0x%08x (should be 0x%08x)\n",
            fhdr->prot_int, 0x11223344);
    fprintf(out_fp,"fileversion: %d\n", (int)fhdr->fileversion);
    fprintf(out_fp,"realsize: %d\n", (int)fhdr->realsize);
    fprintf(out_fp,"description offset: %lld\n", (long long)fhdr->desc_off);
    fprintf(out_fp,"atom data offset: %lld\n", (long long)fhdr->atom_off);
    fprintf(out_fp,"frame num: %d\n", (int)fhdr->frame_num);
    fprintf(out_fp,"part num: %d\n", (int)fhdr->part_num);
    fprintf(out_fp,"total parts: %d\n", (int)fhdr->total_parts);
    fprintf(out_fp,"number of per atom fields %d\n", (int)fhdr->n_fields);
    fprintf(out_fp,"Number of atoms: %lld\n", (long long)fhdr->natoms);
    fprintf(out_fp,"itypelow: %d\n", (int)fhdr->mintype);
    fprintf(out_fp,"itypehigh: %d\n", (int)fhdr->maxtype);
    fprintf(out_fp,"cpus: %d\n", (int)fhdr->cpus);
    fprintf(out_fp,"simu time: %g\n", fhdr->simu_time);
    fprintf(out_fp,"timescale: %g\n", fhdr->timescale);
    fprintf(out_fp,"box-x: %g\n", fhdr->box_x);
    fprintf(out_fp,"box-y: %g\n", fhdr->box_y);
    fprintf(out_fp,"box-z: %g\n", fhdr->box_z);

    for (i = 0; i < fhdr->n_fields; i++) {
      fprintf(out_fp,"Field-%d name: %4s unit: %4s\n", i + 1,
              vhdr->field_names[i], vhdr->field_units[i]);
    }

    for (i = fhdr->mintype; i <= fhdr->maxtype; i++) {
      fprintf(out_fp,"Type-%d %4s\n", i, vhdr->types[i - fhdr->mintype]);
    }

    for (i = 0; i < fhdr->cpus; i++) {
      fprintf(out_fp,"cpu %d: atoms %d xmin %g xmax %g"
              " ymin %g ymax %g zmin %g zmax %g\n",
              i, vhdr->atomsperproc[i],
              vhdr->boxes[i][0], vhdr->boxes[i][1], vhdr->boxes[i][2],
              vhdr->boxes[i][3], vhdr->boxes[i][4], vhdr->boxes[i][5]);
    }
  } else if (conf->format == FORMAT_XYZ){
    fprintf(out_fp, "%lld\n", (long long)(conf->printproc < 0 ? fhdr->natoms : vhdr->atomsperproc[conf->printproc]));
    fprintf(out_fp, "Frame number %d %g fs size %.10g %.10g %.10g"
            " extracted from binary\n", fhdr->frame_num, fhdr->simu_time,
            fhdr->box_x, fhdr->box_y, fhdr->box_z);
  }

}
