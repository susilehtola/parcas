#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>
#include <unistd.h> // for off_t

enum byte_order {
  BO_UNKNOWN,
  BO_BIG_ENDIAN,
  BO_LITTLE_ENDIAN
};

/*
 * Notice that file_order is not in the file header, but calculated from it. */
struct fixed_header {
  int32_t     prot_real;
  int32_t     prot_int;
  int32_t     fileversion;
  int32_t     realsize;
  int64_t     desc_off;
  int64_t     atom_off;
  int32_t     frame_num;
  int32_t     part_num;
  int32_t     total_parts;
  int32_t     n_fields;
  int64_t     natoms;
  int32_t     mintype;
  int32_t     maxtype;
  int32_t     cpus;
  double      simu_time;
  double      timescale;
  double      box_x;
  double      box_y;
  double      box_z;
  enum byte_order file_order;
};

/* Notice that file_offsets is not in the file header, but calculated from it. */
struct variable_header {
  double **boxes;
  int *atomsperproc;
  char **field_names, **field_units, **types;
  off_t * file_offsets;
};

struct atom {
  int64_t ind;
  int32_t type;
  double * fields;
};

enum filters {
  FILTER_TOP,
  FILTER_FIXED,
  FILTER_BY_ID,
  FILTER_RECOIL,
  FILTER_NONE
};

enum formats {
  FORMAT_ASCII,
  FORMAT_XYZ
};

struct configuration {
  char * progname;
  char * fname;
  enum filters filter;
  enum formats format;
  int printproc;
  int write_to_file;
  int top_filter_n_layers;
  int print_xyz_indices;
  int n_recoil;
  /*char filter_by_id_file_name[150];*/

};

#endif
