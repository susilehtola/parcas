Input file for PARCAS MD 

Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = 99999999      Max number of time steps
tmax     = 3000.0        Maximum time in fs 
restartt = 0.0

seed     = 723762        Seed for random number generator

Time step criteria and related parameters. Internal time unit
is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.05          Max time step (in MD units of SQRT(M*L^2/E))
mass     = 195.09        Atom mass in u
potmode  = 2             2 for direct read in of pair potential part.

Simulation cell
---------------

latflag  = 0             Lattice flag (0=gen fcc, 1=read from mdlat.in)
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.20          Neighbour list skin thickness ratio, use ~ 1.15 

natoms   = 6910         Number of atoms in simulation
box(1)   = 47.04         Box size in the X-dir (Angstroms, sigma for LJ)
box(2)   = 47.04         Box size in the Y-dir (Angstroms, sigma for LJ)
box(3)   = 47.04         Box size in the Z-dir (Angstroms, sigma for LJ)
ncell(1) = 12            Number of unit cells along X-direction
ncell(2) = 12            Number of unit cells along Y-direction
ncell(3) = 12            Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------

mtemp    = 1             Temp. control (0 none,1=linear,4=set,5=borders)
temp0    = 3000.0        Initial T for mtemp=6
ntimeini = 100           Time steps at temp0 before quench
initemp  = 600.0         Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 600.0         Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 0.0           Berendsen temperature control tau (fs)
trate    = 5.0           Quench rate for mtemp=6 (K/fs)

1/B should be 5.5d-4 (Cu), 6.2d-4 (Pt), 7.0d-4 (Au), 1.3d-3 (Al)

bpcbeta  = 6.2d-4        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 100.0         Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)
bpcP0z	 = 0.0		 Berendsen pres. control. desired P_z (kbar)

tscaleth = 3.61          Min. thickness of border region at which T is scaled 

damp     = 0.00000       Damping factor
amp      = 0.000000      Amplitude of initial displacement (Angstroms)

ndump    = 2             Print data every ndump steps
nmovie   = 200           Number of steps btwn writing to md.movie (0=no movie)
ndefmovie= 200           Number of steps btwn writing to defects.out 

nintanal = 1000          Number of steps between interstitial analysis
nliqanal = 100   	 Nsteps between liquid analysis; works in parallell !

Ekdef    = 0.26          Ekin threshold for labeling an atom a defect/liquid atom


Recoil calculation definitions
------------------------------

irec     = 0             Index of recoiling atom (-1 closest, -2 create)
xrec     = -5.0          Desired initial position
yrec     = -5.0    
zrec     = -5.0    

recen    = 500.0        Initial recoil energy in eV
rectheta = 47.0          Initial recoil direction in degrees
recphi   = 35.0          Initial recoil direction in degrees
