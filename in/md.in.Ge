Input file for PARCAS MD 

Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = 99999999      Max number of time steps
tmax     = 1000.0        Maximum time in fs 
restartt = 0.0
endtemp  = 0.0           Ending T: end if T < endtemp

seed     = 723762        Seed for random number generator

Time step criteria and related parameters. Internal time unit
is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.05          Max time step (in MD units of SQRT(M*L^2/E))
mass     = 72.610        Atom mass in u
potmode  = 4             0 LJ; 1 EAM; 2 EAM, direct Epair; 3 Si SW; 4 Ge SW

Simulation cell
---------------

latflag  = 2             Lattice: 0 FCC, 1 Readin, 2 DIA, 3-4 Restart
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.15          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2

natoms   = 8000          Number of atoms in simulation
box(1)   = 56.53         Box size in the X-dir (Angstroms, sigma for LJ)
box(2)   = 56.53         Box size in the Y-dir (Angstroms, sigma for LJ)
box(3)   = 56.53         Box size in the Z-dir (Angstroms, sigma for LJ)
ncell(1) = 10            Number of unit cells along X-direction
ncell(2) = 10            Number of unit cells along Y-direction
ncell(3) = 10            Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------

mtemp    = 1             Temp. control (0 none,1=linear,4=set,5=borders)
temp0    = 3000.0        Initial T for mtemp=6
ntimeini = 100           Time steps at temp0 before quench
initemp  = 100.0         Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 100.0         Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 100.0         Berendsen temp. control tau (fs), if 0 not used
trate    = 5.0           Quench rate for mtemp=6 (K/fs)

amp      = 0.000000      Amplitude of initial displacement (Angstroms)

1/B: 7.3d-4 Cu, 5.3d-4 Ni, 3.6d-4 Pt, 5.8d-4 Au, 1.3d-3 Al, 1.0d-3 Si, 
     1.2d-3 Ge

bpcbeta  = 1.0d-3        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 100.0         Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)
bpcP0z   = 0.0           Berendsen pres. control. desired P_z (kbar)

tscaleth = 5.43          Min. thickness of border region at which T is scaled 

damp     = 0.00000       Damping factor

ndump    = 1             Print data every ndump steps
nmovie   = 300           Number of steps btwn writing to md.movie (0=no movie)
ndefmovie= 200           Number of steps btwn writing to defects.out 

nintanal = 1000          Number of steps between interstitial analysis
nliqanal = 100           Nsteps between liquid analysis; works in parallell !

Ekdef    = 0.22          Ekin threshold for labeling an atom defect/liquid 


Recoil calculation definitions
------------------------------

irec     = 0             Index of recoiling atom (-1 closest, -2 create)
xrec     = -3.53         Desired initial position
yrec     = -3.53    
zrec     = -3.53  

recen    = 23.0          Initial recoil energy in eV
rectheta = 54.7356       Initial recoil direction in degrees
recphi   = 45.0          Initial recoil direction in degrees

melstop  = 0             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek>10
