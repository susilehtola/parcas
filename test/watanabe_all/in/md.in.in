Input file for PARCAS MD

Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0

nsteps   = STEPS         Max number of time steps
tmax     = 99999         total duration [fs]

seed     = 528214        Seed for random number generator

nnodes(1)= 2
nnodes(2)= 2
nnodes(3)= 1


Atom type-dependent parameters.
Time step is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.02          Max time step (in MD units of SQRT(M*L^2/E))
ntype    = 4             Number of atom types
mass(0)  = 72.64
name(0)  = Ge
mass(1)  = 28.0855       Atom mass in u
name(1)  = Si            Name of atom type
mass(2)  = 28.0855
name(2)  = O
mass(3)  = 28.0855       This is just a copy of Ge, for testing reppot
name(3)  = Kr
substrate= SiO2          Substrate name, for use in elstop file name

iac(0,0) = 1             Interaction types: -1 kill 0 none 1 potmode 2 pair
iac(0,1) = 1             - automatic symmetry used: iac(1,0) = iac(0,1)
iac(0,2) = 1
iac(0,3) = 2
iac(1,1) = 1
iac(1,2) = 1
iac(1,3) = 2
iac(2,2) = 1
iac(2,3) = 2
iac(3,3) = 2

potmode  = 210           Watanabe potential

reppotcut= 10.0

Simulation cell
---------------

latflag  = 4             Read in coordinates and velocities from file
mdlatxyz = 1             Use a mdlat.in.xyz file as input

natoms   = -1            Total number of atoms minus recoil to be created (-1 = auto)
box(1)   = 43.264728     Box size in the X-dir (Angstroms, sigma for LJ)
box(2)   = 43.070562     Box size in the Y-dir (Angstroms, sigma for LJ)
box(3)   = 43.505896     Box size in the Z-dir (Angstroms, sigma for LJ)
pb(1)    = 1             Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1             Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1             Periodicity in Z-direction (1=per, 0=non)

nprtbl   = 30000         Number of steps between pair table calculations
neiskinr = 1.15          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2


Simulation
----------

mtemp    = 1             Temp. control (0 none,1=linear,4=set,5/7=borders)
initemp  = 300.0         Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 300.0         Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 100.0         Berendsen temp. control tau (fs), if 0 not used

bpcbeta  = 1.0d-3        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 200.0         Berendsen pressure control time const. tau (fs)
bpcP0    = 1.0           Berendsen pressure control desired P (kbar)
bpcmode  = 1             Pressure control mode: 0 none, 1 xyz, 2 isotropic


Debye T is 394 Al, 375 Ni, 315 Cu, 230 Pt, 170 Au, 625 Si, 360 Ge

tdebye   = 625.0         Debye temperature for displacements, overrides amp
amp      = 0.000000      Amplitude of initial displacement (Angstroms)
damp     = 0.00000       Damping factor

ndump    = 1             Print data every ndump steps
moviemode= 8             Positions and forces
nmovie   = 5             Print movie every nmovie steps


Recoil calculation definitions
------------------------------
irec     = -1            Index of recoiling atom (-1 closest, -2 create)
recatype = 3             Recoil atom type

xrec     = 0             Initial recoil position
yrec     = 0
zrec     = 0

recen    = 500           Initial recoil energy in eV
rectheta = -187          Initial recoil direction in degrees
recphi   = 27            Initial recoil direction in degrees

melstop  = 0             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek>elstopmin
elstopmin= 10.0          Minimum energy for electronic stoppin (eV)
