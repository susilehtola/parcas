#!/bin/bash

PATH=$PWD/..:$PATH
start=$PWD

# To add a test, just add it between the parentheses.
# You need to also first produce the "correct" answers.
# The way to produce them is to simply run parcas once on your
# test input (with 4 processes) and remove the file out/time.
# Also remove the final timing line from out/md.out.
tests=(
    tersoff_compound/gaas
    sw
    tersoff
    eam
    EDIP
    FeCr_eamal
    ameinand
    apohjone_eamal
    silica_ohta_quartz
    watanabe_samela
    silica_wat_rev
    watanabe_all
    watanabe_minimal
    brennerbeardmore
    rsmode1
    rsmode2
    rsmode1-latflag3
    rsmode2-latflag3
    trackmode1
    trackmode2
    verysmall-all
    verysmall-two
)

num_limit=1e-3 # numeric tolerance

n_failed=0

for i in $(seq 0 $((${#tests[@]} - 1)) )
do
    dir=${tests[$i]}
    cd "${start}"
    cd "${dir}"
    printf '%-25s: ' $dir
    m4 -DSTEPS=10 in/md.in.in > in/md.in
    mpirun -np 4 parcas >/dev/null
    rm out/time
    sed -i -e '/s\/step\/nat/d' out/md.out

    # use 'numdiff' if avaiable to test differences
    if [[ -z "$(which numdiff 2>/dev/null)" ]]
    then
        echo "WARNING: Please install 'numdiff' (GNUtool)"
        diff_out=$(diff -q out out_correct -x .svn | grep -v "Only in out:")
    else
        for f in $(ls out_correct/)
        do
            diff_out+=$(numdiff out{,_correct}/$f -a ${num_limit} \
                | sed -e '/^$/d' -e '/.*equal$/d')
        done
    fi

    if [[ -z "${diff_out}" ]]
    then
        printf 'Ok\n'
    else
        printf 'FAIL\n'
        n_failed=$((n_failed+1))
        diff -q out out_correct -x .svn | grep -v "Only in out:"
        for f in $(diff -q out out_correct -x .svn | \
            grep -v "Only in out:" | \
            awk '{gsub("out/","",$2); print $2}')
        do
            file -bi "out/${f}"
            file -bi "out_correct/${f}"
            echo -n "Compare '$f' in detail [yn]: "
            read yn
            case $yn in
                y) diff -y out{,_correct}/"${f}"
                ;;
            esac
            echo -n "Move '$f' in to 'out_correct' [yn]: "
            read yn
            case $yn in
                y) cp out{,_correct}"/${f}"
                ;;
            esac
        done
    fi

    rm -rf out
done

if [[ $n_failed -gt 0 ]]
then
    echo """
********************************
* a total of $(printf "%3d" $n_failed) tests failed *
*******************************
"""
fi
