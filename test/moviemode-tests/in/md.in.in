Input file for PARCAS MD 
 
Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = STEPS
tmax     = 5000.0        Maximum time in fs 
restartt = 0.0           Restart time in fs for latflag=3-4
endtemp  = 0.0           Ending T: end if T < endtemp

seed     = 723762        Seed for random number generator

nnodes(1)= 2
nnodes(2)= 2
nnodes(3)= 1

Atom type-dependent parameters. 
Time unit is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.05          Max time step (in MD units of SQRT(M*L^2/E))
ntype    = 2             Number of atom types
mass(0)  = 83.80         Atom mass in u
name(0)  = Kr            Name of atom type
mass(1)  = 28.086        Atom mass in u
name(1)  = Si            Name of atom type
substrate= Si            Substrate name, for use in elstop file name

iac(0,0) = -1            Interaction types: -1 kill 0 none 1 potmode 2 pair
iac(0,1) = 2               3 create EAM cross potential
iac(1,1) = 1             - automatic symmetry used: iac(1,0) = iac(0,1) 
potmode  = 3             0 LJ; 1 EAM; 2 EAM, direct Epair; 3-4 SW

reppotcut= 10.0          Reppotcut for SW and Tersoff: use 0 for no reppot

Simulation cell
---------------

latflag  = 2             Lattice: 0 FCC 1 mdlat.in 2 DIA 3-4 Restart 5 Read bas
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.15          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2

natoms   = 360           Number of atoms in simulation
box(1)   = 27.15475      Box size in the X-dir (Angstroms)
box(2)   = 16.29285      Box size in the Y-dir (Angstroms)
box(3)   = 16.29285      Box size in the Z-dir (Angstroms)
ncell(1) = 5             Number of unit cells along X-direction
ncell(2) = 3             Number of unit cells along Y-direction
ncell(3) = 3             Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 0.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------

mtemp    = 7             Temp. control (0 none,1=linear,4=set,5/7=borders)
temp0    = 3000.0        Initial T for mtemp=6
timeini  = 1000.0        Time at temp0 before quench
ntimeini = 99991000      Time steps at temp0 before quench
initemp  = 50.0          Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 77.0          Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 70.0          Berendsen temp. control tau (fs), if 0 not used
trate    = 5.0           Quench rate for mtemp=6/8 (K/fs)

Debye T is 394 Al, 375 Ni, 315 Cu, 230 Pt, 170 Au, 625 Si, 360 Ge

tdebye   = 315.0         Debye temperature for displacements, overrides amp
amp      = 0.000000      Amplitude of initial displacement (Angstroms)

1/B: 7.3d-4 Cu, 5.3d-4 Ni, 3.6d-4 Pt, 5.8d-4 Au, 1.3d-3 Al, 1.0d-3 Si, 
     1.2d-3 Ge, 5.1d-4 Co

bpcbeta  = 1.0d-3        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 0.0           Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)
bpcmode  = 1             Pressure control mode: 0 none, 1 xyz, 2 isotropic

tscaleth = 2.82          Min. thickness of border region at which T is scaled 

damp     = 0.00000       Damping factor

ndump    = 1
nmovie   = 5
moviemode= MOVIEMODE

outtype  = 1
outzmin  = -15
outzmax  = 15


Recoil calculation definitions
------------------------------

irec     = -2             Index of recoiling atom (-1 closest, -2 create)
recatype = 0              Recoil atom type (if < 0 no change)
xrec     = -2.0          Desired initial position
yrec     = -2.0
zrec     = 0.0

recen    = 20.0         Initial recoil energy in eV
rectheta = 7.0           Initial recoil direction in degrees
recphi   = 24.0          Initial recoil direction in degrees

melstop  = 5             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek
                         4 as 3 + sputtered, 5 as 3 - sputtered
elstopmin= 5.0           Lower limit for applying elstop  
sputlim  = 0.5           Sputtering limit, in internal l units. 0.5=box edge
