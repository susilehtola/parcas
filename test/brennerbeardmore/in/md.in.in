Input file for PARCAS MD

Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = STEPS         Max number of time steps
tmax     = 30000.0       Maximum time in fs

seed     = 7457962        Seed for random number generator

Atom type-dependent parameters.
Time unit is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.001         Max time step (in MD units of SQRT(M*L^2/E))
timeEt   = 300.0

ntype    = 6
mass(0)  = 83.80
name(0)  = Kr
mass(1)  = 12.011
name(1)  = C
mass(2)  = 28.086
name(2)  = Si
mass(3)  = 1.001
name(3)  = H
mass(4)  = 39.948
name(4)  = Ar
mass(5)  = 131.293
name(5)  = Xe

iac(0,0) = -1
iac(0,1) = -1
iac(0,2) = -1
iac(0,3) = -1
iac(0,4) = -1
iac(0,5) = -1
iac(1,1) = 1
iac(1,2) = 1
iac(1,3) = 1
iac(1,4) = 1
iac(1,5) = 1
iac(2,2) = 1
iac(2,3) = 1
iac(2,4) = 1
iac(2,5) = 1
iac(3,3) = 1
iac(3,4) = 1
iac(3,5) = 1
iac(4,4) = 1
iac(4,5) = 1
iac(5,5) = 1

potmode  = 101

reppotcut= 0.0          Reppotcut for SW and Tersoff: use 0 for no reppot

Simulation cell
---------------

latflag  = 3             Lattice: 0 FCC 1 mdlat.in 2 DIA 3-4 Restart 5 Read bas
mdlatxyz = 1
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.10          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2

natoms   = 6972
box(1)   = 123.54
box(2)   = 147.571
box(3)   = 50.0          Box size in the Z-dir (Angstroms)
ncell(1) = 18            Number of unit cells along X-direction
ncell(2) = 18            Number of unit cells along Y-direction
ncell(3) = 18            Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 0           Periodicity in Z-direction (1=per, 0=non)


Simulation
----------

mtemp    = 1             Temp. control (0 none,1=linear,4=set,5/7=borders)
temp0    = 300.0        Initial T for mtemp=6
timeini  = 3000.0        Time at temp0 before quench
ntimeini = 99991000      Time steps at temp0 before quench
initemp  = 300.0          Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 300.0          Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 30.0          Berendsen temp. control tau (fs), if 0 not used
trate    = 50           Quench rate for mtemp=6/8 (K/fs)

Debye T is 394 Al, 375 Ni, 315 Cu, 230 Pt, 170 Au, 625 Si, 360 Ge

tdebye   = 0.0           Debye temperature for displacements, overrides amp
amp      = 0.000000      Amplitude of initial displacement (Angstroms)

bpcmode  = 0              Pressure control mode: 0 none, 1 xyz, 2 isotropic

damp     = 0.00000       Damping factor

ndump    = 1              Print data every ndump steps
nmovie   = 100          Number of steps btwn writing to md.movie (0=no movie)
moviemode= 8

nliqanal = 3

Ekdef    = 0.144         Ekin threshold for labeling an atom defect/liquid

Recoil calculation definitions
------------------------------

irec     = -1             Index of recoiling atom (-1 closest, -2 create)
recatype = -1             Recoil atom type
xrec     = 0.0
yrec     = 0.0
zrec     = 0.0

recen    = 0.0000001       Initial recoil energy in eV
rectheta = 180.0           Initial recoil direction in degrees
recphi   = 0.0          Initial recoil direction in degrees
